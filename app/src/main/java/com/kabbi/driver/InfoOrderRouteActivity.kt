package com.kabbi.driver


import android.app.ListActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Route
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class InfoOrderRouteActivity : ListActivity() {

    internal var order: Order? = null
    internal var routes: List<Route> = listOf()
    internal var values: MutableList<String> = mutableListOf()
    private lateinit var simpleDateFormat: SimpleDateFormat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        simpleDateFormat = SimpleDateFormat("dd.MM.yyyy HH.mm.ss")

        runBlocking {
            withContext(Dispatchers.IO){
                val db = AppDatabase.getInstance(this@InfoOrderRouteActivity)
                order = db.orderDao().getById(intent.extras!!.getLong("order_id"))
                routes = db.routeDao().getRoutes(order!!.id)
            }
        }

        for ((_, datetime, lat, lon, speed, _, parking) in routes) {
            values.add("lat " + lat + ", lon " + lon + "\n" + "speed " + speed + ", area " + parking + ", time " + simpleDateFormat.format(Date(datetime)))
        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, values)
        listAdapter = adapter
    }
}
