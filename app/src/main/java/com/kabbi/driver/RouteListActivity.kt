package com.kabbi.driver

import android.app.ProgressDialog
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ListView
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.adapters.RouteAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.NetworkSendRoute
import com.kabbi.driver.events.SendRouteToServer
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.util.*

class RouteListActivity : BaseActivity() {
    private var appPreferences: AppPreferences? = null
    private var listView: ListView? = null
    private var linearLayoutRoutes: LinearLayout? = null
    private var routeAdapter: RouteAdapter? = null
    private var reloadList: MutableList<Order> = mutableListOf()
    private var routes: List<Route>? = null
    private var driver: Driver? = null
    private var service: Service? = null
    private var progressDialog: ProgressDialog? = null
    private var position: Int = 0
    private var sendRoute: AsyncSendRoute? = null
    private var fileName: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        service = (application as App).service
        driver = (application as App).driver

        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_routelist)

        EventBus.getDefault().register(this)

        initToolbar()
        initViews()

        reloadList = ArrayList()

        runBlocking {
            val db = AppDatabase.getInstance(this@RouteListActivity)

            var orderList = listOf<Order>()
            var currentOrder: CurrentOrder? = null

            withContext(Dispatchers.IO) {
                currentOrder = db.currentOrderDao().getCurOrder(driver!!.id)
                orderList = db.orderDao().getAll()
            }
            var currentOrderId = "0"
            currentOrder?.also {
                currentOrderId = it.orderId
            }

            for (order in orderList) {
                withContext(Dispatchers.IO) {
                    val route = db.routeDao().loadRoute(order.id)

                    if (route != null && order.orderId != currentOrderId)
                        reloadList.add(order)
                }
            }

            routeAdapter = RouteAdapter(this@RouteListActivity, reloadList)
            listView!!.adapter = routeAdapter
        }
    }

    private fun initToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = "  " + getString(R.string.btn_route)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViews() {
        listView = findViewById(R.id.lv_route)
        linearLayoutRoutes = findViewById(R.id.ll_routelist)
    }

    @Subscribe
    fun onEvent(sendRouteToServer: SendRouteToServer) {
        position = sendRouteToServer.position
        sendRoute = AsyncSendRoute()
        sendRoute!!.execute(sendRouteToServer.position)
    }

    @Subscribe
    fun onEvent(networkSendRoute: NetworkSendRoute) {
        try {
            if (fileName != null)
                fileName!!.delete()
        } catch (e: Exception) {

        }

        if (networkSendRoute.status == "OK" || networkSendRoute.status == "EMPTY_DATA_IN_DATABASE") {
            for (route in routes!!) {
                runBlocking {
                    withContext(Dispatchers.IO) {
                        AppDatabase.getInstance(this@RouteListActivity).routeDao().delete(route)
                    }
                }
            }

            reloadList.removeAt(position)
            routeAdapter = RouteAdapter(this, reloadList)
            listView!!.adapter = routeAdapter
        } else {
            CustomToast.showMessage(applicationContext, getString(R.string.progress_route_fail))
        }
        if (progressDialog != null)
            progressDialog!!.dismiss()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    private fun getFile(route: String): File? {
        val path: String?
        val sdState = Environment.getExternalStorageState()
        val sdcard = Environment.getExternalStorageDirectory()
        if (sdState == Environment.MEDIA_MOUNTED) {
            path = sdcard.absolutePath + File.separator + "Gootax"

            fileName = File(path, "route.json")

            fileName?.also { file ->
                try {
                    val fileOutputStream = FileOutputStream(file)
                    fileOutputStream.write(route.toByteArray())
                    fileOutputStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        return fileName
    }

    private inner class AsyncSendRoute : AsyncTask<Int, Void, Unit>() {

        override fun doInBackground(vararg params: Int?) {
            try {
                val requestParams = LinkedHashMap<String, String>()
                val jsonOrderRoute = JSONObject()
                runBlocking {
                    withContext(Dispatchers.IO) {
                        routes = AppDatabase.getInstance(this@RouteListActivity).routeDao().getRoutes(reloadList[params[0]!!].id)
                    }
                }
                val jsonArrayRoutes = JSONArray()
                for (route in routes!!) {
                    val jsonRoute = JSONObject()
                    jsonRoute.put("lat", route.lat.toString())
                    jsonRoute.put("lon", route.lon.toString())
                    jsonRoute.put("parking", route.parking)
                    jsonRoute.put("speed", route.speed.toString())
                    jsonRoute.put("status_id", route.corn)
                    jsonRoute.put("time", route.datetime.toString())
                    jsonArrayRoutes.put(jsonRoute)

                }

                jsonOrderRoute.put("order_route", jsonArrayRoutes)

                requestParams["order_id"] = reloadList[params[0]!!].orderId
                requestParams["tenant_login"] = service!!.uri
                requestParams["worker_login"] = driver!!.callSign

                val typedFile = getFile(jsonOrderRoute.toString())

                WebService.setOrderRouteFile(requestParams, driver!!.secretCode, typedFile, reloadList[params[0]!!])
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        override fun onPreExecute() {
            super.onPreExecute()
            progressDialog = ProgressDialog.show(this@RouteListActivity, null, getString(R.string.progress_route), false)
        }
    }
}
