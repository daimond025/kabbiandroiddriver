package com.kabbi.driver.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kabbi.driver.database.dao.*
import com.kabbi.driver.database.entities.*

@Database(entities = [
    Car::class,
    ClientTariff::class,
    CurrentOrder::class,
    DarkWingDuck::class,
    Driver::class,
    DriverTariff::class,
    Order::class,
    Parking::class,
    Route::class,
    Service::class,
    Sound::class,
    WorkDay::class
], version = 8, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun carDao(): CarDao
    abstract fun clientTariffDao(): ClientTariffDao
    abstract fun currentOrderDao(): CurrentOrderDao
    abstract fun darkWingDuckDao(): DarkWingDuckDao
    abstract fun driverDao(): DriverDao
    abstract fun driverTariffDao(): DriverTariffDao
    abstract fun orderDao(): OrderDao
    abstract fun parkingDao(): ParkingDao
    abstract fun routeDao(): RouteDao
    abstract fun serviceDao(): ServiceDao
    abstract fun soundDao(): SoundDao
    abstract fun workDayDao(): WorkDayDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDb(context)
            }
        }

        private fun buildDb(context: Context): AppDatabase {
            INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "application.db")
                    .fallbackToDestructiveMigration()
                    .build()
            return INSTANCE!!
        }
    }
}