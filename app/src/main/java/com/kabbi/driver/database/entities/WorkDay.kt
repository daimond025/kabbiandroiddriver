package com.kabbi.driver.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WorkDay(
        var endWorkDay: Long = 0,
        var startWorkDay: Long = 0,
        var tariff: String = "",
        var shiftId: String = "",
        var car: Long = 0,
        var driver: Long = 0,
        var service: Long = 0
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}