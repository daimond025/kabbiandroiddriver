package com.kabbi.driver.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dark_wing_duck")
data class DarkWingDuck(
        @ColumnInfo(name = "driver_call_sign") var driverCallSign: String? = null,
        @ColumnInfo(name = "address") var address: String? = null,
        @ColumnInfo(name = "car_data") var carData: String? = null,
        @ColumnInfo(name = "lat") var lat: Double = 0.toDouble(),
        @ColumnInfo(name = "lon") var lon: Double = 0.toDouble(),
        @ColumnInfo(name = "timer_help") var timerHelp: Long = 0,
        @ColumnInfo(name = "driver") var driver: Long? = null) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}