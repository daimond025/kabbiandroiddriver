package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.WorkDay

@Dao
interface WorkDayDao {

    @Query("SELECT * FROM workday WHERE id = :id LIMIT 1")
    fun getById(id: Long): WorkDay

    @Query("SELECT * FROM workday WHERE service = :id ORDER BY startWorkDay DESC LIMIT 1")
    fun getByServiceId(id: String): WorkDay

    @Query("SELECT * FROM workday WHERE driver = :driver")
    fun getAll(driver: Long): List<WorkDay>

    @Query("SELECT * FROM workday WHERE driver = :driver AND endWorkDay >= :start AND endWorkDay <= :end")
    fun getWorkDayRange(driver: Long, start: Long, end: Long): List<WorkDay>

    @Query("SELECT * FROM workday WHERE driver = :driver AND endWorkDay >= :start")
    fun getWorkDayRange(driver: Long, start: Long): List<WorkDay>

    @Query("SELECT * FROM workday WHERE driver = :driver AND endWorkDay = 0")
    fun getLastWorkDay(driver: Long): WorkDay

    @Query("SELECT * FROM workday WHERE driver = :driver ORDER BY endWorkDay DESC LIMIT 1")
    fun getLastCloseWorkDay(driver: Long): WorkDay

    @Query("SELECT * FROM workday WHERE endWorkDay = 0 LIMIT 1")
    fun getLastWorkDayEmpty(): WorkDay

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(workDay: WorkDay): Long
}