package com.kabbi.driver.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Driver(

        var callSign: String = "",
        @SerializedName("worker_city_id")
        var cityID: String = "",
        @SerializedName("worker_email")
        var email: String? = null,
        @SerializedName("worker_experience")
        var exp: String? = null,
        @SerializedName("worker_fio")
        var name: String? = null,
        var password: String? = null,
        @SerializedName("worker_phone")
        var phone: String? = null,
        @SerializedName("worker_photo")
        var photo: String? = null,
        var secretCode: String? = null,
        var token: String? = null,
        var help: Boolean = false,
        @SerializedName("promo_code")
        var promoCode: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}