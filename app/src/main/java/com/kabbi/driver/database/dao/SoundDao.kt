package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.Sound

@Dao
interface SoundDao {

    @Query("SELECT * FROM sound WHERE `desc` = :desc LIMIT 1")
    fun get(desc: String): Sound

    @Query("SELECT * FROM sound")
    fun getAll(): List<Sound>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(sound: Sound)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(vararg sounds: Sound)
}