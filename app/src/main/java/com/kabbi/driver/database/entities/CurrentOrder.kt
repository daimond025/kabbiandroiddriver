package com.kabbi.driver.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "current_order")
data class CurrentOrder(
        @ColumnInfo(name = "order_id") var orderId: String,
        @ColumnInfo(name = "time_to_client") var timeToClient: Long,
        @ColumnInfo(name = "begin_order_time") var beginOrderTime: Long = 0,
        @ColumnInfo(name = "wait_time_begin") var waitTimeBegin: Long = 0,
        @ColumnInfo(name = "wait_time_end") var waitTimeEnd: Long = 0,
        @ColumnInfo(name = "wait_time") var waitTime: Long = 0,
        var stage: Int = 0,
        @ColumnInfo(name = "tariff_data") var tariffData: String,
        var address: String,
        var options: String,
        var comment: String,
        var phone: String,
        @ColumnInfo(name = "driver_id") var driver: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}