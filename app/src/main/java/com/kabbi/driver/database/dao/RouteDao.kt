package com.kabbi.driver.database.dao

import androidx.room.*
import com.kabbi.driver.database.entities.Route

@Dao
interface RouteDao {

    @Query("SELECT * FROM route WHERE `order` = :order")
    fun getRoutes(order: Long): List<Route>

    @Query("SELECT * FROM route WHERE `order` = :order LIMIT 1")
    fun loadRoute(order: Long): Route

    @Query("SELECT * FROM route ORDER BY id DESC LIMIT 1")
    fun getLastRoute(): Route

    @Delete
    fun delete(route: Route)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(route: Route): Long
}