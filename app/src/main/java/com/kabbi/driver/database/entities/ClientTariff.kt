package com.kabbi.driver.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "client_tariff")
data class ClientTariff(

        var accrual: String,
        var area: String,
        @ColumnInfo(name = "planting_price")
        var plantingPrice: Double = 0.0,
        @ColumnInfo(name = "planting_include")
        var plantingInclude: Double = 0.0,
        var plantingIncludeTime: Double = 0.0,
        var nextKmPrice: Double = 0.0,
        var nextKmPriceArray: String = "",
        var minPrice: Double = 0.0,
        var supplyPrice: Double = 0.0,
        var waitTime: Long = 0,
        var waitDrivingTime: Long = 0,
        var waitPrice: Double = 0.0,
        var speedDowntime: Double = 0.0,
        var rounding: Double = 1.0,
        var orderType: String,
        var nextCostUnit: String,
        var nextTimePrice: Double = 0.0,
        var order: Long?,
        var tariffId: String,
        var bonusPayment: Boolean = false,
        var maxPayment: Double = 0.toDouble(),
        var clientBonusBalance: Double = 0.toDouble(),
        var bonusId: String,
        var paymentMethod: String,
        var maxPaymentType: String,
        var roundingType: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}