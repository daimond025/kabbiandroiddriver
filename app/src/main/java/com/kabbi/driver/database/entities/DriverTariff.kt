package com.kabbi.driver.database.entities


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "driver_tariff")
@Parcelize
data class DriverTariff(

        var tariff: String,

        @ColumnInfo(name = "active_shift_count")
        @SerializedName("worker_count_active_shift")
        var activeShiftCount: String = "0",

        @ColumnInfo(name = "tariff_type")
        @SerializedName("worker_tariff_type")
        var tariffType: String,

        @ColumnInfo(name = "car_class")
        @SerializedName("worker_car_class")
        var carClass: String,

        @PrimaryKey
        @ColumnInfo(name = "tariff_id")
        @SerializedName("worker_tariff_id")
        var tariffId: String,

        @ColumnInfo(name = "tariff_period_type")
        @SerializedName("worker_tariff_period_type")
        var tariffPeriodType: String,

        @ColumnInfo(name = "tariff_period_info")
        @SerializedName("worker_tariff_period_info")
        var tariffPeriodInfo: String,

        @ColumnInfo(name = "tariff_name")
        @SerializedName("worker_tariff_name")
        var tariffName: String,

        @ColumnInfo(name = "tariff_cost")
        @SerializedName("worker_tariff_cost")
        var tariffCost: String,

        @ColumnInfo(name = "order_fee")
        @SerializedName("worker_order_comission")
        var orderFee: String,

        @ColumnInfo(name = "order_fee_type")
        @SerializedName("worker_order_comission_type")
        var orderFeeType: String,

        @ColumnInfo(name = "shift_count")
        @SerializedName("worker_count_shift")
        var shiftCount: String,

        @ColumnInfo(name = "driver_id")
        var driver: Long?) : Parcelable

