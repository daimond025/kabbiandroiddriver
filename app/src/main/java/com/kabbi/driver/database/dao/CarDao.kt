package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.Car

@Dao
interface CarDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(carItem: Car?)

    @Query("SELECT * FROM car WHERE id = :id")
    fun getById(id: Long): Car

    @Query("SELECT * FROM car WHERE car_id = :id ")
    fun getByCarId(id: String): Car

    @Query("SELECT * FROM car WHERE driver_id = :id ")
    fun getCarsByDriverId (id: Long): List<Car>

    @Query("SELECT * FROM car")
    fun getAll(): List<Car>

    @Query("DELETE FROM car WHERE driver_id = :driver")
    fun deleteAll(driver: Long)
}