package com.kabbi.driver.database.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Car(

        var color: String? = "",

        @SerializedName("worker_car_descr")
        var descr: String,

        @ColumnInfo(name = "license_plate")
        var licensePlate: String?,

        var model: String?,

        @SerializedName("worker_car_class")
        var type: String?,

        @ColumnInfo(name = "car_id")
        @SerializedName("worker_car_id")
        var carId: String,

        @SerializedName("is_company_car")
        @ColumnInfo(name = "is_company_car")
        var isCompanyCar: Boolean,

        @ColumnInfo(name = "driver_id")
        var driver: Long? = null

) : Parcelable {
    @IgnoredOnParcel
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}
