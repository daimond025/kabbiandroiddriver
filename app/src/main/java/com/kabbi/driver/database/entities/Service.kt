package com.kabbi.driver.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Service(
        var balance: String? = null,
        var comment: String? = null,
        var currency: String? = null,
        var logo: String? = null,
        var serviceName: String? = null,
        var tariffs: String? = null,
        var uri: String = "",
        var driver: Long? = null
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}