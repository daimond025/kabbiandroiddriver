package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.CurrentOrder

@Dao
interface CurrentOrderDao {

    @Query("SELECT * FROM current_order WHERE driver_id = :driverId LIMIT 1")
    fun getCurOrder(driverId: Long): CurrentOrder

    @Query("DELETE FROM current_order WHERE driver_id = :driverId")
    fun deleteAll(driverId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(currentOrder: CurrentOrder): Long
}