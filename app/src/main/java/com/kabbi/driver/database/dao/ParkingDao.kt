package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.Parking

@Dao
interface ParkingDao {

    @Query("SELECT * FROM parking WHERE service = :service AND parkingType = 'city' OR parkingType = 'airport'")
    fun getAll(service: Long): List<Parking>

    @Query("SELECT * FROM parking WHERE service = :service")
    fun getAllPing(service: Long): List<Parking>

    @Query("SELECT * FROM parking WHERE service = :service AND parkingType = 'basePolygon' LIMIT 1")
    fun getBase(service: Long): Parking

    @Query("SELECT * FROM parking WHERE parkingID = :parkingId LIMIT 1")
    fun get(parkingId: String): Parking?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(parking: Parking): Long

    @Query("DELETE FROM parking WHERE id = :service")
    fun deleteAll(service: Long)
}