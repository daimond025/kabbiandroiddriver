package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.ClientTariff

@Dao
interface ClientTariffDao {

    @Query("SELECT * FROM client_tariff WHERE `order` = :orderId AND area = :area")
    fun getTariff(orderId: Long, area: String): ClientTariff

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(clientTariff: ClientTariff): Long
}