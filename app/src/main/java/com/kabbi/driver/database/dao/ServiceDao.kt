package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.Service

@Dao
interface ServiceDao {

    @Query("SELECT * FROM service WHERE driver = :driver LIMIT 1")
    fun getByDriver(driver: Long): Service

    @Query("SELECT * FROM service")
    fun getAll(): List<Service>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(service: Service): Long

    @Query("SELECT * FROM service WHERE id = :id LIMIT 1")
    fun getById(id: Long): Service

    @Query("DELETE FROM service WHERE id = :id")
    fun delete(id: Long)
}