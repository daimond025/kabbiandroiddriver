package com.kabbi.driver.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Parking(
        var parkingID: Long = 0,
        var parkingName: String? = "",
        var parkingOrders: String = "",
        var parkingPolygon: String = "",
        var parkingQueue: String = "",
        var parkingType: String = "",
        var service: Long = 0,
        var inDistrict: Double = 0.toDouble(),
        var outDistrict: Double = 0.toDouble(),
        var inDistrCoefficientType: String = "",
        var outDistrCoefficientType: String = "") {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}