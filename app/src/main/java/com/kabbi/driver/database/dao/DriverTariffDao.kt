package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.DriverTariff

@Dao
interface DriverTariffDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(tariff: DriverTariff)

    @Query("SELECT * FROM driver_tariff WHERE tariff_id = :tariff_id")
    fun getDriverTariffByField( tariff_id: String): DriverTariff

    @Query("SELECT * FROM driver_tariff WHERE driver_id = :id")
    fun getDriverTariffs(id: Long): List<DriverTariff>

    @Query("DELETE FROM driver_tariff WHERE driver_id = :id") //fixme they gonna be kidding
    fun deleteAll(id: Long)
}
