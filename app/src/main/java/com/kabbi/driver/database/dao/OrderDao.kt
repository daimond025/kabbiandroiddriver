package com.kabbi.driver.database.dao

import androidx.room.*
import com.kabbi.driver.database.entities.Order

@Dao
interface OrderDao {

    @Query("SELECT * FROM `order` WHERE id = :id LIMIT 1")
    fun getById(id: Long): Order

    @Query("SELECT * FROM `order`")
    fun getAll(): List<Order>

    @Query("SELECT * FROM `order` WHERE workDay = :workDay")
    fun getByWorkDay(workDay: Long): List<Order>

    @Query("SELECT * FROM `order` WHERE driver = :driver AND statusOrder = :status")
    fun getReserveOrder(driver: Long, status: String): List<Order>

    @Query("SELECT * FROM `order` WHERE orderId = :id")
    fun getByOrderId(id: String): Order

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(order: Order): Long

    @Delete
    fun delete(order: Order)
}