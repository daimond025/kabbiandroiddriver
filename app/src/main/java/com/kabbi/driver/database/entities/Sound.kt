package com.kabbi.driver.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Sound(
        @PrimaryKey var desc: String,
        var name: String,
        var path: String,
        @ColumnInfo(name = "is_default") var isDefault: Boolean,
        @ColumnInfo(name = "is_short_sound") var isShortSound: Boolean
)
