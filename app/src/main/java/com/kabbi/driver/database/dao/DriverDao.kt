package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.Driver

@Dao
interface DriverDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(driver: Driver): Long

    @Query("SELECT * FROM driver WHERE id = :id LIMIT 1")
    fun getById(id: Long): Driver

    @Query("SELECT * FROM driver WHERE callSign = :callSign LIMIT 1")
    fun get(callSign: String): Driver

    @Query("DELETE FROM driver WHERE id = :id")
    fun delete(id: Long)
}