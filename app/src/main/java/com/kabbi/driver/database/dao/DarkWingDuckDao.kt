package com.kabbi.driver.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kabbi.driver.database.entities.DarkWingDuck

@Dao
interface DarkWingDuckDao {

    @Query("SELECT * FROM dark_wing_duck WHERE driver_call_sign = :callSign LIMIT 1")
    fun get(callSign: String): DarkWingDuck?

    @Query("SELECT * FROM dark_wing_duck WHERE driver = :driver")
    fun getAll(driver: Long): List<DarkWingDuck>

    @Query("DELETE FROM dark_wing_duck WHERE driver_call_sign = :callSign")
    fun delete(callSign: String)

    @Query("DELETE FROM dark_wing_duck WHERE driver = :driver")
    fun deleteAll(driver: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(duck: DarkWingDuck)
}