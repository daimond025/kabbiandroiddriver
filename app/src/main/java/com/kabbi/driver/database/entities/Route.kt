package com.kabbi.driver.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Route(
        var corn: String = "",
        var datetime: Long = 0,
        var lat: Double = 0.0,
        var lon: Double = 0.0,
        var speed: Double = 0.0,
        var bearing: Float = 0.0F,
        var parking: String = "",
        var order: Long? = 0,
        var hdop: Double = 100.0
) {

    constructor(lat: Double, lon: Double) : this(){
        this.lat = lat
        this.lon = lon
    }

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
