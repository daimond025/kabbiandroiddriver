package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.kabbi.driver.R;
import com.kabbi.driver.events.CloseDialogEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ChangeDialogCompletedFragment extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        EventBus.getDefault().register(this);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        EventBus.getDefault().unregister(this);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_change, null);

        this.setCancelable(false);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.text_order_complete))
                .setPositiveButton("Ok",
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }

    @Subscribe
    public void onEvent(CloseDialogEvent dialogEvent) {
        this.dismiss();
    }

}
