package com.kabbi.driver.fragments

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kabbi.driver.R
import com.kabbi.driver.border.OrderPagerBorderFragment
import com.kabbi.driver.helper.AppPreferences
import java.util.*

class OrderListFragment : Fragment() {
    //    var view: View? = null
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var tabLayout: TabLayout
    private var currentTab = 0
    var titles: MutableList<String> = arrayListOf()
    var tabCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appPreferences = AppPreferences(context)
        tabCount = if (appPreferences.getText("allow_worker_to_create_order") == "1") 3 else 2
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_orderlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titles = ArrayList()
        titles.add(getString(R.string.text_title_orderfragment_free))
        titles.add(getString(R.string.text_title_orderfragment_reserve))
        titles.add(getString(R.string.text_title_orderfragment_border))
        viewPager = view.findViewById(R.id.orderlist_pager)
        pagerAdapter = OrderAdapter(childFragmentManager)
        viewPager.adapter = pagerAdapter
        tabLayout = view.findViewById(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)

        val bundle = this.arguments

        bundle?.apply {
            val page = getInt("page", 0)
            viewPager.currentItem = page
        }

        savedInstanceState?.apply {
            currentTab = getInt("current_tab")
            viewPager.post { viewPager.currentItem = currentTab }
        }

        val myColorStateList = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_pressed)), intArrayOf(Color.WHITE, Color.WHITE, Color.WHITE))
        tabLayout.tabTextColors = myColorStateList
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        currentTab = viewPager.currentItem
        outState.putInt("current_tab", currentTab)
        retainInstance = true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d("Orientation stage. ", "onActivityCreated: $currentTab")
        if (savedInstanceState != null) {
            currentTab = savedInstanceState.getInt("current_tab", 0)
            viewPager.currentItem = currentTab
            tabLayout.setScrollPosition(currentTab, 0f, true)
        }
    }

    private inner class OrderAdapter internal constructor(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {
        override fun getItem(position: Int): Fragment {
            var fragment: Fragment? = null
            val args = Bundle()
            when (position) {
                0 -> {
                    fragment = OrderPagerItemFragment()
                    args.putString("type", "free")
                }
                1 -> {
                    fragment = OrderPagerItemPretimeFragment()
                    args.putString("type", "reserve")
                }
                2 -> {
                    fragment = OrderPagerBorderFragment()
                    args.putString("type", "border")
                }
            }
            fragment!!.arguments = args
            return fragment
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titles[position]
        }
    }
}