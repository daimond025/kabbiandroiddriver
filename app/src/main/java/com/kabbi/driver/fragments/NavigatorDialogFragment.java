package com.kabbi.driver.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;

public class NavigatorDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain, typedValueSubscribe;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();


        RadioButton radioButton1 = new RadioButton(getActivity().getApplicationContext());
        radioButton1.setText(getString(R.string.dialog_nav_google));
        radioButton1.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButton1.setTextColor(getResources().getColor(R.color.text_main_light));
        radioButton1.setTag(getString(R.string.dialog_nav_google));
        radioGroup.addView(radioButton1);

        RadioButton radioButton2 = new RadioButton(getActivity().getApplicationContext());
        radioButton2.setText(getString(R.string.dialog_nav_osm));
        radioButton2.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButton2.setTextColor(getResources().getColor(R.color.text_main_light));
        radioButton2.setTag(getString(R.string.dialog_nav_osm));
        radioGroup.addView(radioButton2);


        ((RadioButton) radioGroup.getChildAt((appPreferences.getText("navigation").equals(getString(R.string.dialog_nav_google))) ? 0 : 1)).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_nav))
                .setPositiveButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {


                                appPreferences.saveText("navigation", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());

                                ((TextView) getActivity().findViewById(R.id.textview_profile_prefmap_nav)).setText(Html.fromHtml("<big><font color='"+String.format("#%06X", 0xFFFFFF & typedValueMain.data)+"'>"
                                        + getString(R.string.text_profile_prefmap_nav) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                                        + radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString()
                                        + "</font></small>"));
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }


}
