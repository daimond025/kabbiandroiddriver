package com.kabbi.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.kabbi.driver.R;
import com.kabbi.driver.adapters.IncomeWeekFragmentAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class IncomeWeekFragment extends Fragment {

    private List<String> dateTitles;
    private List<String> requestDates;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incomeweek, container, false);

        ViewPager incomePager = view.findViewById(R.id.incomepager);
        TabLayout incomeTabLayout = view.findViewById(R.id.income_tabs);

        calculate();

        IncomeWeekFragmentAdapter adapter = new IncomeWeekFragmentAdapter(getChildFragmentManager(), getTitles(), getReqDates());
        incomePager.setAdapter(adapter);
        incomeTabLayout.setupWithViewPager(incomePager);

        return view;
    }

    public void calculate() {
        // List with page header title
        dateTitles = new ArrayList<>();
        // List with dates for network request
        requestDates = new ArrayList<>();

        // Integer of month for comparing
        int month1;
        int month2;

        // Title of page header
        String title;

        // Format pattern for page header title
        DateFormat titleFormatLeft = new SimpleDateFormat("d", Locale.getDefault());
        DateFormat titleFormatRight = new SimpleDateFormat("d MMM", Locale.getDefault());

        // Format pattern for order item
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);

        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        month1 = calendar.get(Calendar.MONTH);
        calendar.add(Calendar.DAY_OF_MONTH, +6);
        month2 = calendar.get(Calendar.MONTH);

        requestDates.add(format.format(calendar.getTime()));
        title = titleFormatRight.format(calendar.getTime());

        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

        // Comparing for page header title. ex: "23 dec-02 jan", "10-17 dec"
        if (month1 != month2) {
            title = titleFormatRight.format(calendar.getTime()).concat("-" + title);
        } else {
            title = titleFormatLeft.format(calendar.getTime()).concat("-" + title);
        }
        dateTitles.add(title);
        requestDates.add(format.format(calendar.getTime()));

        for (int i = 0; i < 3; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
            title = titleFormatRight.format(calendar.getTime());
            requestDates.add(format.format(calendar.getTime()));
            month1 = calendar.get(Calendar.MONTH);
            calendar.add(Calendar.DAY_OF_MONTH, -6);
            month2 = calendar.get(Calendar.MONTH);
            if (month1 != month2) {
                title = titleFormatRight.format(calendar.getTime()).concat("-" + title);
            } else {
                title = titleFormatLeft.format(calendar.getTime()).concat("-" + title);
            }
            requestDates.add(format.format(calendar.getTime()));
            dateTitles.add(title);
        }
    }

    public List<String> getTitles() {
        return dateTitles;
    }

    public List<String> getReqDates() {
        return requestDates;
    }
}