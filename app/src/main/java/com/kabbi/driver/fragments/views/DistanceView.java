package com.kabbi.driver.fragments.views;

import android.app.Activity;
import android.view.View;
import com.kabbi.driver.R;
import com.kabbi.driver.database.entities.ClientTariff;
import com.kabbi.driver.database.entities.Order;

public class DistanceView {
    public View getView(Activity activity, Order order, ClientTariff tariffCity, ClientTariff tariffTrack) {
        View view = activity.getLayoutInflater().inflate(R.layout.item_container, null);


        /*
        try {
            ((TextView) view.findViewById(R.id.textview_reportorder_citylabel)).setText(getString(R.string.text_reportorder_base) + " 1 " + getString(R.string.text_dis) + "/"
                    + (order.isDay() ? jsonTariffCity.getString("next_km_price_day") : jsonTariffCity.getString("next_km_price_night")) + " " + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()));
            if (order.getDisCityInOrder() > 0)
                ((TextView) view.findViewById(R.id.textview_reportorder_citycost)).setText(Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                        + String.valueOf(Round.roundFormatDis(order.getDisCityInOrder())) + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF & typedValueMain.data) + "'>"
                        + formatCost(order.getDisCityInOrder() * Double.valueOf((order.isDay() ? jsonTariffCity.getString("next_km_price_day") : jsonTariffCity.getString("next_km_price_night")))) + " "
                        + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()) + "</font>"));
            else
                ((TextView) view.findViewById(R.id.textview_reportorder_citycost)).setText(Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                        + "0.0 " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF & typedValueMain.data) + "'>"
                        + "0 " + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()) + "</font>"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */

        /*
        try {
            ((TextView) view.findViewById(R.id.textview_reportorder_tracklabel)).setText(getString(R.string.text_reportorder_track) + " 1 " + getString(R.string.text_dis) + "/"
                    + (order.isDay() ? jsonTariffTrack.getString("next_km_price_day") : jsonTariffTrack.getString("next_km_price_night")) + " " + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()));
            if (order.getDisTrackInOrder() > 0)
                ((TextView) view.findViewById(R.id.textview_reportorder_trackcost)).setText(Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                        + String.valueOf(Round.roundFormatDis(order.getDisTrackInOrder())) + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF & typedValueMain.data) + "'>"
                        + formatCost(order.getDisTrackInOrder() * Double.valueOf((order.isDay() ? jsonTariffTrack.getString("next_km_price_day") : jsonTariffTrack.getString("next_km_price_night")))) + " "
                        + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()) + "</font>"));
            else
                ((TextView) view.findViewById(R.id.textview_reportorder_trackcost)).setText(Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                        + "0.0 " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF & typedValueMain.data) + "'>"
                        + "0 " + TypefaceSpanEx.getCurrencyStr(getActivity().getApplicationContext()) + "</font>"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */



        return view;
    }
}
