package com.kabbi.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kabbi.driver.App;
import com.kabbi.driver.R;
import com.kabbi.driver.adapters.IncomeWeekTabAdapter;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.database.entities.Service;
import com.kabbi.driver.events.NetworkReportsDataEvent;
import com.kabbi.driver.events.NetworkStatisticEvent;
import com.kabbi.driver.models.WorkerReport;
import com.kabbi.driver.network.WebService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class IncomeWeekTabFragment extends Fragment {

    private Driver driver;
    private Service service;
    private List<WorkerReport> workerReports;
    private IncomeWeekTabAdapter mAdapter;
    private int countPage;
    private String endDate, startDate;

    private TextView tvMoneySum, tvTimeSum, tvOrdersSum;
    private ProgressBar pBar1, pBar2;
    private LinearLayout llStats;
    private RecyclerView rvIncome;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_incomeweektab, container, false);

        service = ((App) getActivity().getApplication()).service;
        driver = ((App) getActivity().getApplication()).driver;

        tvMoneySum = view.findViewById(R.id.income_money_summ);
        tvTimeSum = view.findViewById(R.id.income_timesum_value);
        tvOrdersSum = view.findViewById(R.id.income_orderssum_value);

        pBar1 = view.findViewById(R.id.p_bar_income1);
        pBar2 = view.findViewById(R.id.p_bar_income2);
        llStats = view.findViewById(R.id.ll_stats);
        rvIncome = view.findViewById(R.id.rv_income);

        workerReports = new ArrayList<>();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        pBar2.setVisibility(View.VISIBLE);
        rvIncome.setVisibility(View.GONE);
        pBar1.setVisibility(View.VISIBLE);
        llStats.setVisibility(View.GONE);

        makeRequest();
    }

    private void makeRequest() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            endDate = bundle.getString("date_end");
            startDate = bundle.getString("date_start");
            getWorkerStats(endDate, startDate);
            int currentPage = 1;
            getWorkerReports(endDate, currentPage, startDate);
        }
    }

    private void getWorkerStats(String endDate, String startDate) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("end_date", endDate);
        params.put("start_date", startDate);
        params.put("tenant_login", service.getUri());
        params.put("worker_login", driver.getCallSign());

        WebService.INSTANCE.getWorkerStatistic(params, driver.getSecretCode());
    }

    private void getWorkerReports(String endDate, int page, String startDate) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("end_date", endDate);
        params.put("page", String.valueOf(page));
        params.put("start_date", startDate);
        params.put("tenant_login", service.getUri());
        params.put("worker_login", driver.getCallSign());

        WebService.INSTANCE.getWorkerReport(params, driver.getSecretCode());
    }

    @Subscribe
    public void onEvent(NetworkStatisticEvent statisticEvent) {
        try {
            tvMoneySum.setText(String.format("%s", String.format(new Locale("ru"), "%.2f", statisticEvent.getEarnings())/*, getString(R.string.main_price)*/));

            int hours = statisticEvent.getWorkingHours() / 3600;
            int minutes = (statisticEvent.getWorkingHours() % 3600) / 60;
            String time = String.format("%1$s %2$s %3$s %4$s", hours, getString(R.string.text_time_hour), minutes, getString(R.string.text_time_min_small));
            tvTimeSum.setText(time);

            tvOrdersSum.setText(String.format(getString(R.string.income_completed), String.valueOf(statisticEvent.getCountOrders())));

            pBar1.setVisibility(View.GONE);
            llStats.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(NetworkReportsDataEvent networkReportsDataEvent) {
        try {
            workerReports.clear();
            workerReports.addAll(networkReportsDataEvent.getData());
            mAdapter = new IncomeWeekTabAdapter(getContext(), workerReports, networkReportsDataEvent.getCurrentPage(), networkReportsDataEvent.getLastPage());
            mAdapter.setListener(page -> {
                if (page <= countPage) {
                    rvIncome.post(() -> {
                        mAdapter.notifyItemInserted(workerReports.size() - 1);
                        IncomeWeekTabFragment.this.getWorkerReports(endDate, page, startDate);
                    });
                    mAdapter.notifyDataSetChanged();
                }
            });

            countPage = networkReportsDataEvent.getLastPage();
            //currentPage = networkReportsDataEvent.getCurrentPage();

            if (networkReportsDataEvent.getCurrentPage() < networkReportsDataEvent.getLastPage() || networkReportsDataEvent.getLastPage() > 1) {
                rvIncome.post(() -> {
                    //add controls item
                    workerReports.add(null);
                    mAdapter.notifyItemInserted(workerReports.size());
                });
            }

            rvIncome.setLayoutManager((new IncomeWeekTabAdapter.WrapContentLinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false)));
            rvIncome.setAdapter(mAdapter);
            rvIncome.addItemDecoration(new IncomeWeekTabAdapter.DividerCustomItemDecoration(getContext()));
            mAdapter.notifyDataSetChanged();

            pBar2.setVisibility(View.GONE);
            rvIncome.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}