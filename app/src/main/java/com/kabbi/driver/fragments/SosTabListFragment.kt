package com.kabbi.driver.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.ListFragment
import android.view.View
import android.widget.ListView
import com.kabbi.driver.HelpMapActivity
import com.kabbi.driver.R
import com.kabbi.driver.adapters.HelpAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.DarkWingDuck
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.helper.AppParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class SosTabListFragment : ListFragment() {

    internal lateinit var driver: Driver
    private lateinit var receiverHelp: BroadcastReceiver
    internal lateinit var adapter: HelpAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        runBlocking {
            withContext(Dispatchers.IO) {
                driver = AppDatabase.getInstance(context!!).driverDao().getById(arguments!!.getLong("driver_id"))
            }
            updateListHelp()
        }
    }

    private fun updateListHelp() {
        runBlocking {
            val dao = AppDatabase.getInstance(context!!).darkWingDuckDao()
            var helpers: List<DarkWingDuck> = arrayListOf()
            withContext(Dispatchers.IO) {
                helpers = dao.getAll(driver.id)
            }

            val updateHelpers = ArrayList<DarkWingDuck>()

            val time = Calendar.getInstance().time.time
            val hr3 = (3600 * 3 * 1000).toLong()

            for (item in helpers) {

                if (item.timerHelp + hr3 > time) {
                    updateHelpers.add(item)
                } else {
                    dao.delete(item.driverCallSign!!)
                }
            }

            setEmptyText(getString(R.string.text_listsos_empty))

            adapter = HelpAdapter(activity!!.applicationContext, updateHelpers)
            listAdapter = adapter
        }
    }

    override fun onStart() {
        super.onStart()

        receiverHelp = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    updateListHelp()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        val intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH_SOS)
        activity!!.registerReceiver(receiverHelp, intentFilterPush)
    }

    override fun onStop() {
        super.onStop()
        activity!!.unregisterReceiver(receiverHelp)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)

        val intent = Intent(activity!!.applicationContext, HelpMapActivity::class.java)
        intent.putExtra("lat", v.getTag(R.id.tag_car_lat).toString())
        intent.putExtra("lon", v.getTag(R.id.tag_car_lon).toString())

        if (v.getTag(R.id.tag_car_desc) != null) {
            intent.putExtra("car_data", v.getTag(R.id.tag_car_desc).toString())
        } else {
            intent.putExtra("car_data", "unknow car")
        }

        startActivity(intent)
    }
}
