package com.kabbi.driver.fragments;


import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.database.entities.Driver;

import java.util.ArrayList;
import java.util.List;

public class SosFragment extends Fragment {

    View view;
    Driver driver;
    private List<String> titles;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sos, container, false);

        driver = ((WorkShiftActivity) getActivity()).getDriver();

        titles = new ArrayList<>();
        titles.add(getString(R.string.text_title_tabsos));
        titles.add(getString(R.string.text_title_tabsoslist));

        ViewPager viewPager = view.findViewById(R.id.sos_pager);

        PagerAdapter pagerAdapter = new HelpAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        ColorStateList myColorStateList = new ColorStateList(
                new int[][]{
                        new int[] { android.R.attr.state_enabled}, // enabled
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_pressed}  // pressed
                },
                new int[] {
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE
                }
        );
        tabLayout.setTabTextColors(myColorStateList);

        return view;
    }

    private class HelpAdapter extends FragmentPagerAdapter {

        HelpAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle args = new Bundle();
            args.putLong("driver_id", driver.getId());

            switch (position) {
                case 0:
                    fragment = new SosTabButtonFragment();
                    break;
                case 1:
                    fragment = new SosTabListFragment();
                    break;
            }

            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            int TAB_COUNT = 2;
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }
    }

}
