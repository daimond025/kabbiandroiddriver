package com.kabbi.driver.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;

import java.util.Locale;


public class LangDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_lang, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();

        int count = 0;
        for (String lang : AppParams.LANG) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
            radioButton.setText(AppParams.getLangLabel(getActivity().getApplicationContext(), count));
            radioButton.setTextColor(getResources().getColor(R.color.text_main_light));
            radioButton.setTag(count);
            radioGroup.addView(radioButton);
            count++;
        }

        ((RadioButton) radioGroup.getChildAt(Integer.valueOf(appPreferences.getText("favorite_lang")))).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_lang))
                .setPositiveButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                Resources res = getActivity().getResources();
                                DisplayMetrics dm = res.getDisplayMetrics();
                                android.content.res.Configuration conf = res.getConfiguration();
                                //sr_ME
                                if (AppParams.LANG[(int)radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag()].length() > 2) {
                                    try {
                                        String[] arrLang = AppParams.LANG[(int)radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag()].split("_");
                                        conf.locale = new Locale(arrLang[0], arrLang[1]);
                                    } catch (Exception  e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    conf.locale = new Locale(AppParams.LANG[(int) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag()]);
                                }
                                res.updateConfiguration(conf, dm);

                                Locale.setDefault(new Locale(AppParams.LANG[(int)radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag()]));

                                appPreferences.saveText("favorite_lang", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());

                                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(AppParams.BROADCAST_LANG));

                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }
}
