package com.kabbi.driver.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.fragment.app.ListFragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.adapters.OrderAdapter
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.NetworkOrdersListPretimeEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.models.OpenOrder
import com.kabbi.driver.network.WebService.getOrdersList
import com.kabbi.driver.offer.OrderOfferActivity
import com.kabbi.driver.util.CustomToast
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import kotlin.collections.LinkedHashMap

class OrderPagerItemPretimeFragment : ListFragment() {

    private val data = arrayOf("one", "two", "three")
    private var adapter: OrderAdapter? = null
    private var orders: MutableList<OpenOrder> = ArrayList()
    private var driver: Driver? = null
    private var service: Service? = null
    private var workDay: WorkDay? = null
    private var receiverOrder: BroadcastReceiver? = null
    private var update = false
    private var handlerTaskInfo: Handler? = null
    private var handlerTaskInfoUpdate: Handler? = null
    private lateinit var runnableInfo: Runnable
    private lateinit var runnableUpdate: Runnable
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var typedValueSubscribe: TypedValue? = null
    private var typedValueBg: TypedValue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driver = (activity as WorkShiftActivity?)!!.driver
        service = (activity as WorkShiftActivity?)!!.service
        workDay = (activity as WorkShiftActivity?)!!.workDay
        typedValueSubscribe = TypedValue()
        typedValueBg = TypedValue()
        update = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val listFragmentView = super.onCreateView(inflater, container, savedInstanceState)
        swipeRefreshLayout = ListFragmentSwipeRefreshLayout(activity!!.applicationContext).apply {
            addView(listFragmentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            setColorSchemeColors(Color.parseColor(AppParams.colorOrange(activity!!.applicationContext)))
            setOnRefreshListener {
                if (InternetConnection.isOnline(activity)) {
                    isRefreshing = true
                    updateOrders()
                } else {
                    isRefreshing = false
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                }
            }
        }
        return swipeRefreshLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listView.setBackgroundColor(typedValueBg!!.data)
    }

    override fun onResume() {
        super.onResume()
        updateOrders()
    }

    private fun updateOrders() {
        update = true
        val requestParams: LinkedHashMap<String?, String?> = LinkedHashMap()
        requestParams["tenant_login"] = service!!.uri
        requestParams["worker_login"] = driver!!.callSign

        arguments?.apply {
            if (getString("type") != "free") {
                if (getString("type") == "reserve") {
                    getOrdersList(activity!!, requestParams, driver!!.secretCode, "reserve")
                } else {
                    val adapter = ArrayAdapter(activity!!, android.R.layout.simple_list_item_1, data)
                    listAdapter = adapter
                    update = false
                    if (swipeRefreshLayout.isRefreshing) swipeRefreshLayout.isRefreshing = false
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)

        receiverOrder = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (!update) updateOrders()
            }
        }

        var intentFilterPush: IntentFilter? = null

        if (arguments!!.getString("type") == "free") {
            intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH_FREE)
        } else if (arguments!!.getString("type") == "reserve") {
            intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH_PRE)
        }

        activity!!.registerReceiver(receiverOrder, intentFilterPush)
        handlerTaskInfo = Handler()

        runnableInfo = Runnable {
            try {
                if (adapter != null) adapter!!.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            handlerTaskInfo!!.postDelayed(runnableInfo, 1000)
        }

        handlerTaskInfo!!.post(runnableInfo)
        handlerTaskInfoUpdate = Handler()

        runnableUpdate = Runnable {
            if (!update) updateOrders()
            handlerTaskInfoUpdate!!.postDelayed(runnableUpdate, 30000)
        }

        handlerTaskInfoUpdate!!.post(runnableUpdate)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        activity!!.unregisterReceiver(receiverOrder)
        try {
            handlerTaskInfo!!.removeCallbacks(runnableInfo)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            handlerTaskInfoUpdate!!.removeCallbacks(runnableUpdate)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        val orderIntent = Intent(activity!!.applicationContext, OrderOfferActivity::class.java)
        orderIntent.putExtra("order_id", v.getTag(R.id.tag_order_id).toString())
        orderIntent.putExtra("service_id", service!!.id)
        orderIntent.putExtra("workday_id", workDay!!.id)
        orderIntent.putExtra("free_order", 3)
        orderIntent.putExtra("type_order", v.getTag(R.id.tag_order_type).toString())
        orderIntent.putExtra("source", "activity")
        orderIntent.putExtra("time", Date().time)
        orderIntent.putExtra("server_time", (activity as WorkShiftActivity?)!!.serverTime)
        orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        if ((activity as WorkShiftActivity?)!!.stageOrder == 0 || v.getTag(R.id.tag_order_type).toString() == "reserve") {
            orderIntent.putExtra("type_source", 0)
        } else {
            orderIntent.putExtra("type_source", 3)
        }
        orderIntent.putExtra("delete", false)
        orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_RESERVE_CANCEL)
        orderIntent.putExtra("code_next", AppParams.ORDER_STATUS_RESERVE)
        orderIntent.putExtra("music", false)
        activity!!.startActivityForResult(orderIntent, WorkShiftActivity.INTENT_ORDER_OFFER)
    }

    @Subscribe
    fun onEvent(ordersListEvent: NetworkOrdersListPretimeEvent) {
        if (ordersListEvent.type == "reserve") {
            try {
                orders.clear()
                orders = ordersListEvent.orders
                adapter = OrderAdapter(activity!!, orders, typedValueSubscribe!!.data)
                listAdapter = adapter
            } catch (e: Exception) {
                e.printStackTrace()
            }
            update = false
        }
        if (swipeRefreshLayout.isRefreshing) swipeRefreshLayout.isRefreshing = false
    }

    private inner class ListFragmentSwipeRefreshLayout(context: Context?) : SwipeRefreshLayout(context!!) {
        override fun canChildScrollUp(): Boolean {
            val listView = listView
            return if (listView.visibility == View.VISIBLE) {
                canListViewScrollUp(listView)
            } else {
                false
            }
        }
    }

    companion object {
        private fun canListViewScrollUp(listView: ListView): Boolean {
            return if (Build.VERSION.SDK_INT >= 16) {
                listView.canScrollVertically(-1)
            } else {
                listView.childCount > 0 && (listView.firstVisiblePosition > 0 || listView.getChildAt(0).top < listView.paddingTop)
            }
        }
    }
}