package com.kabbi.driver.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.loopj.android.http.RequestParams
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject

class SosTabButtonFragment : androidx.fragment.app.Fragment(), View.OnClickListener, AsyncHttpTask.AsyncTaskInterface {

    internal lateinit var driver: Driver
    internal lateinit var service: Service
    internal lateinit var button: Button
    internal lateinit var requestParams: RequestParams
    internal lateinit var appPreferences: AppPreferences
    private var send: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sosbutton, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        send = false

        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(context!!)
                driver = db.driverDao().getById(arguments!!.getLong("driver_id"))
                service = db.serviceDao().getByDriver(driver.id)
            }
        }

        button = view.findViewById(R.id.imagebtn_sos)
        button.setOnClickListener(this)

        if (driver.help) {
            button.text = getString(R.string.text_btnsos_cancel)
        } else {
            button.text = getString(R.string.text_btnsos)
        }
    }

    override fun onClick(v: View) {
        if (InternetConnection.isOnline(activity!!)) {
            if (!send) {
                send = true

                appPreferences = AppPreferences(activity!!.applicationContext)

                requestParams = RequestParams()
                requestParams.add("tenant_login", service.uri)
                requestParams.add("worker_lat", appPreferences.getText("lat"))
                requestParams.add("worker_login", driver.callSign)
                requestParams.add("worker_lon", appPreferences.getText("lon"))

                if (driver.help) {
                    AsyncHttpTask(this, activity).post("unset_signal_sos", requestParams, driver.secretCode)
                } else {
                    AsyncHttpTask(this, activity).post("set_signal_sos", requestParams, driver.secretCode)
                }

            }
        } else {
            CustomToast.showMessage(activity, getString(R.string.text_internet_access))
        }
    }

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        try {
            if (jsonObject != null && jsonObject.getString("info") == "OK") {
                if (typeJson == "unset_signal_sos") {
                    try {
                        driver.help = false
                        CoroutineScope(Dispatchers.IO).launch {
                            AppDatabase.getInstance(context!!).driverDao().save(driver)
                        }
                        button.text = getString(R.string.text_btnsos)
                        send = false
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else if (typeJson == "set_signal_sos") {
                    try {
                        driver.help = true
                        CoroutineScope(Dispatchers.IO).launch {
                            AppDatabase.getInstance(context!!).driverDao().save(driver)
                        }
                        button.text = getString(R.string.text_btnsos_cancel)
                        send = false
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        send = false
    }
}
