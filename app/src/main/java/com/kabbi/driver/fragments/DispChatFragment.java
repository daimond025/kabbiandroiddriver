package com.kabbi.driver.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import com.kabbi.driver.R;
import com.kabbi.driver.WorkActivity;
import com.kabbi.driver.adapters.MessageAdapter;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.database.entities.Service;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.helper.InternetConnection;
import com.kabbi.driver.models.Message;
import com.kabbi.driver.util.CustomToast;
import com.kabbi.driver.util.WebSocket;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

public class DispChatFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, WebSocket.DriverChatInterface {

    private Driver driver;
    private Service service;

    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private LinkedList<Message> mMessages = new LinkedList<Message>();
    private RecyclerView.Adapter mAdapter;
    private String mUsername;
    private SwipeRefreshLayout swipeRefreshLayout;
    private WebSocket webSocket;
    private Activity activity;
    private long timestamp;
    private boolean scrollBottom = false;
    private String your;

    public DispChatFragment() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        mAdapter = new MessageAdapter(activity, mMessages);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        driver = ((WorkActivity) getActivity()).getDriver();
        service = ((WorkActivity) getActivity()).getService();
        mUsername = driver.getName();
        timestamp = (new Date()).getTime();

        webSocket = new WebSocket(getActivity(), this, driver, service);
        webSocket.connect();
        webSocket.emitGetLastMessages(WebSocket.TYPECHAT_DRIVER);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        your = getString(R.string.you);

        AppPreferences appPreferences = new AppPreferences(getActivity());
        if (appPreferences.getText("show_chat_with_dispatcher_before_start_shift").equals("0"))
            view.findViewById(R.id.ll_driverchat).setVisibility(View.GONE);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_chat);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(
                Color.parseColor(AppParams.colorOrange(getActivity().getApplicationContext())));

        final LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        mMessagesView = view.findViewById(R.id.messages);
        mMessagesView.setLayoutManager(layoutParams);
        mMessagesView.setAdapter(mAdapter);

        mInputMessageView = view.findViewById(R.id.message_input);
        mInputMessageView.setOnEditorActionListener((v, id, event) -> {
            if (id == R.id.send || id == EditorInfo.IME_NULL) {
                attemptSend();
                return true;
            }
            return false;
        });

        ImageButton sendButton = view.findViewById(R.id.send_button);
        sendButton.setOnClickListener(v -> attemptSend());


        mMessagesView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                swipeRefreshLayout.setEnabled(layoutParams.findFirstCompletelyVisibleItemPosition() == 0);
            }

        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        webSocket.disconnect();

    }

    private void addMessage(final String username, final String message, int type) {
        mMessages.add(new Message.Builder(type)
                .username(username).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void attemptSend() {
        if (null == mUsername) return;

        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");
        addMessage((new SimpleDateFormat("dd.MM HH:mm")).format(new Date()) + " " + getString(R.string.you), message, Message.TYPE_MESSAGE_YOU);

        webSocket.emitNewMessage(WebSocket.TYPECHAT_DRIVER, message);
    }

    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    @Override
    public void onRefresh() {
        if (InternetConnection.isOnline(getActivity())) {
            swipeRefreshLayout.setRefreshing(true);

            if (mMessages.size() == 0) {
                webSocket.emitGetLastMessages(WebSocket.TYPECHAT_DRIVER);
            } else {
                webSocket.emitGetHistoryMessages(WebSocket.TYPECHAT_DRIVER, timestamp);
            }

        } else {
            swipeRefreshLayout.setRefreshing(false);
            CustomToast.showMessage(getActivity(), getString(R.string.text_internet_access));
        }
    }


    @Override
    public void onNewMessage(final String userName, final String time, final String role, final String message, final String typeChat, final boolean read) {
        activity.runOnUiThread(() -> {
            if (typeChat.equals(WebSocket.TYPECHAT_DRIVER)) {
                String username = time + " " + userName + " (" + role + ")";
                addMessage(username, message, Message.TYPE_MESSAGE);

                CustomToast.showMessage(getContext(), message);

                webSocket.emitMessageIsRead();
            }
        });
    }

    @Override
    public void onUnreadMessage(final List<? extends HashMap<String, String>> messages) {
        activity.runOnUiThread(() -> {
            if (messages.size() > 0) {
                timestamp = Long.valueOf(messages.get(0).get("timestamp"));
                for (HashMap<String, String> message : messages) {
                    //if (message.get("sender_id").equals(driver.getCallSign())) break;
                    addMessage(message.get("user"), message.get("message"), Message.TYPE_MESSAGE);
                }
                webSocket.emitMessageIsRead();
            } else {
                webSocket.emitGetLastMessages(WebSocket.TYPECHAT_DRIVER);
            }
        });
    }

    @Override
    public void onLastMessages(final String typeChat, final List<? extends HashMap<String, String>> messages) {

        activity.runOnUiThread(() -> {
            if (messages.size() > 0 && typeChat.equals(WebSocket.TYPECHAT_DRIVER)) {


                timestamp = Long.valueOf(messages.get(0).get("timestamp"));

                Collections.reverse(messages);
                for (HashMap<String, String> message : messages) {

                    if (message.get("receiver_type").equals(WebSocket.TYPECHAT_DRIVER)) {
                        try {
                            if (message.get("sender_id").equals(driver.getCallSign())) {
                                mMessages.addFirst(new Message.Builder(Message.TYPE_MESSAGE_YOU)
                                        .username(message.get("sender_time") + " " + your)
                                        .message(message.get("message"))
                                        .build());
                            } else {
                                mMessages.addFirst(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username(message.get("user"))
                                        .message(message.get("message"))
                                        .build());
                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                    }
                }

                mAdapter.notifyDataSetChanged();

                if (!scrollBottom) {
                    scrollToBottom();
                    scrollBottom = true;
                }
            }
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onNewOrder(String orderId, String callsign, String tenant_login, Integer secToClose, String uuid, int[] arrival) {

    }

    @Override
    public void onPreOrder(String orderId, String callsign, String tenant_login, Integer secToClose, String uuid) {

    }

    @Override
    public void onUpdateOrder(String orderId, String callsign, String tenant_login, String uuid) {

    }

    @Override
    public void onRejectOrder(String orderId, String callsign, String tenant_login, String uuid) {

    }

    @Override
    public void onRejectDriverFromOrder(String orderId, String callsign, String tenant_login, String uuid) {

    }

    @Override
    public void onCompleteOrder(String orderId, String callsign, String tenant_login, String uuid) {

    }

    @Override
    public void onResponse(String uuid, String infoCode, int result, JSONObject jsonObject) {

    }

    @Override
    public void onPhotocontrolEvent(String uuid) {

    }

    @Override
    public void onOrderListAddItem(String type, String order_id, String uuid) {

    }

    @Override
    public void onOrderListDelItem(String type, String order_id, String uuid) {

    }

    @Override
    public void onConfirmPreOrder(String orderId, String callsign, String tenant_login, Integer secConfirm, String uuid) {

    }
}
