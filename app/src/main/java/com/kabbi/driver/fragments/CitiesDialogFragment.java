package com.kabbi.driver.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.models.DriverCity;

import java.util.List;

public class CitiesDialogFragment extends DialogFragment {

    View view;
    RadioGroup radioGroup;
    AppPreferences appPreferences;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = (RadioGroup) view.findViewById(R.id.radiogroup_theme);

        List<DriverCity> cities = getArguments().getParcelableArrayList("cities");

        int count = 0;
        for (DriverCity driverCity : cities) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());

            radioButton.setTag(R.id.tag_car_id, driverCity.getCityId());
            radioButton.setTag(R.id.tag_car_index, count);
            radioButton.setText(driverCity.getCityName());
            radioButton.setTextColor(getResources().getColor(R.color.black));


            radioGroup.addView(radioButton);
            count++;
        }

        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            if (radioGroup.getChildAt(i).getTag(R.id.tag_car_id).toString().equals(appPreferences.getText("city_id"))) {
                ((RadioButton) radioGroup.getChildAt(i)).setChecked(true);
                break;
            }
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.text_workactivity_dialogtitle_cities))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            Intent result = new Intent();
                            result.putExtra("city_id", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_car_id).toString());

                            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);

                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }

}
