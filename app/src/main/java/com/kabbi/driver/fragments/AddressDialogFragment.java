package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.kabbi.driver.App;
import com.kabbi.driver.R;
import com.kabbi.driver.adapters.AddressAdapter;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.database.entities.Service;
import com.kabbi.driver.events.NetworkAutocompleteEvent;
import com.kabbi.driver.helper.AppParams;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;


public class AddressDialogFragment extends DialogFragment {

    View view;
    Driver driver;
    Service service;
    private AutoCompleteTextView autoCompleteStreet;
    private AddressAdapter addressAdapter;
    private Map<String, String> address;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.activity_border_address, null);
        autoCompleteStreet = view.findViewById(R.id.autocomplete_street);

        driver = ((App) getActivity().getApplication()).driver;
        service = ((App) getActivity().getApplication()).service;

        address = new HashMap<>();

        autoCompleteStreet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 2) {
                    if (addressAdapter == null) {
                        addressAdapter = new AddressAdapter(getActivity(), driver, service, s.toString(), driver.getCityID());
                        autoCompleteStreet.setAdapter(addressAdapter);
                    } else {
                        addressAdapter.updateAddresses(s.toString());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        autoCompleteStreet.setOnItemClickListener((parent, view, position, id) -> {
            autoCompleteStreet.setText(((TextView) view.findViewById(R.id.text1)).getText());

            address.put("label", ((TextView) view.findViewById(R.id.text1)).getText().toString());
            address.put("lat", view.getTag(R.id.tag_lat).toString());
            address.put("lon", view.getTag(R.id.tag_lon).toString());
            address.put("cityid", view.getTag(R.id.tag_cityid).toString());
        });

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_title_address))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            if (address.size() > 0) {
                                Intent intent = new Intent(AppParams.BROADCAST_DIALOG);
                                intent.putExtra("label", address.get("label"));
                                intent.putExtra("lat", address.get("lat"));
                                intent.putExtra("lon", address.get("lon"));
                                intent.putExtra("cityid", address.get("cityid"));
                                intent.putExtra("type", "address");

                                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                                EventBus.getDefault().post(new NetworkAutocompleteEvent(address));
                            }
                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
