package com.kabbi.driver.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.ReportWorkDayListActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.helper.TypefaceSpanEx
import kotlinx.android.synthetic.main.fragment_report_tab_month.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class ReportTabFragment : androidx.fragment.app.Fragment() {

    internal lateinit var view: View
    private lateinit var simpleDateFormatTime: SimpleDateFormat
    internal lateinit var simpleDateFormat: SimpleDateFormat
    internal lateinit var workDays: List<WorkDay>
    internal lateinit var orders: MutableList<Order>
    internal lateinit var workDayIds: ArrayList<String>
    internal var timeStart: Long = 0
    internal var timeEnd: Long = 0
    internal var cost: Double = 0.toDouble()
    internal lateinit var driver: Driver
    internal lateinit var args: Bundle
    private lateinit var broadcastReceiver: BroadcastReceiver
    private lateinit var db: AppDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_report_tab_month, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = AppDatabase.getInstance(context!!)
        runBlocking {
            withContext(Dispatchers.IO){
                driver = db.driverDao().getById(arguments!!.getLong("driver_id"))
            }
        }
        simpleDateFormatTime = SimpleDateFormat("dd.MM.yyyy HH:mm")
        simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
        workDayIds = ArrayList()
        orders = ArrayList()
        cost = 0.0
        args = Bundle()

        val calendar = Calendar.getInstance()

        if (arguments!!.getInt("number_page") == 3) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH))
        } else if (arguments!!.getInt("number_page") == 2) {
            var weekDay = calendar.get(Calendar.DAY_OF_WEEK)
            when (weekDay) {
                1 -> weekDay = -6
                2 -> weekDay = 0
                3 -> weekDay = -1
                4 -> weekDay = -2
                5 -> weekDay = -3
                6 -> weekDay = -4
                7 -> weekDay = -5
            }
            calendar.add(Calendar.DAY_OF_MONTH, weekDay)
        } else if (arguments!!.getInt("number_page") == 1) {
            var lastWorkDay: WorkDay? = null

            runBlocking {
                withContext(Dispatchers.IO){
                    lastWorkDay =  db.workDayDao().getLastWorkDay(driver.id)
                }
            }
            if (lastWorkDay != null)
                calendar.time = Date(lastWorkDay!!.startWorkDay)
        } else if (arguments!!.getInt("number_page") == 4) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH))
        }

        val hoursSec = (calendar.time.hours * 60 * 60 * 1000).toLong()
        val minuteSec = (calendar.time.minutes * 60 * 1000).toLong()
        val secSec = (calendar.time.seconds * 1000).toLong()

        timeEnd = Calendar.getInstance().time.time

        if (arguments!!.getInt("number_page") == 1) {
            timeStart = calendar.timeInMillis
            runBlocking {
                withContext(Dispatchers.IO){
                    workDays =  db.workDayDao().getWorkDayRange(driver.id, timeStart)
                }
            }
            val firstDay = Date(timeStart)
            textview_reporttab_timestart.text = simpleDateFormatTime.format(firstDay)
            textview_reporttab_timeend.text = simpleDateFormatTime.format(Date())

        } else {
            timeStart = calendar.timeInMillis - hoursSec - minuteSec - secSec
            runBlocking {
                withContext(Dispatchers.IO){
                    workDays =  db.workDayDao().getWorkDayRange(driver.id, timeStart, timeEnd)
                }
            }
            val firstDay = Date(timeStart)
            textview_reporttab_timestart.text = simpleDateFormat.format(firstDay)
            textview_reporttab_timeend.text = simpleDateFormat.format(Date())
        }

        for (workDay in workDays) {
            workDayIds.add(workDay.id.toString())

            var resOrders  = listOf<Order>()
            runBlocking {
                withContext(Dispatchers.IO){
                    resOrders =  db.orderDao().getByWorkDay(workDay.id)
                }
            }
            for (order in resOrders) {
                try {
                    cost += order.finalOrderCost.toDouble()

                    orders.add(order)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

        textview_reporttab_countorder.text = orders.size.toString()
        textview_reporttab_allcost.text = TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), cost.roundToInt().toString())

        view.findViewById<View>(R.id.relativelayout_btn).setOnClickListener { v ->
            if (workDayIds.size > 0) {

                val intent = Intent(activity!!.applicationContext, ReportWorkDayListActivity::class.java)
                intent.putStringArrayListExtra("workday_ids", workDayIds)
                startActivity(intent)
            }
        }

        if (arguments!!.getInt("number_page") == 4) {
            view.findViewById<View>(R.id.relativelayout_btn_start).setOnClickListener { v ->
                args.putInt("num", 1)
                val dialogFragment = DatePickerDialogFragment()
                dialogFragment.arguments = args
                dialogFragment.show(fragmentManager!!, "picker")

            }

            view.findViewById<View>(R.id.relativelayout_btn_end).setOnClickListener { v ->
                args.putInt("num", 2)
                val dialogFragment = DatePickerDialogFragment()
                dialogFragment.arguments = args
                dialogFragment.show(fragmentManager!!, "picker")

            }
        }
    }

    override fun onStart() {
        super.onStart()
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.extras!!.getInt("num") == 1) {
                    timeStart = intent.extras!!.getLong("date")
                    (view.findViewById<View>(R.id.textview_reporttab_timestart) as TextView).text = simpleDateFormat.format(Date(timeStart))

                } else if (intent.extras!!.getInt("num") == 2) {
                    timeEnd = intent.extras!!.getLong("date")
                    (view.findViewById<View>(R.id.textview_reporttab_timeend) as TextView).text = simpleDateFormat.format(Date(timeEnd))
                }

                if (timeEnd < timeStart) {
                    timeEnd = timeStart
                    (view.findViewById<View>(R.id.textview_reporttab_timeend) as TextView).text = simpleDateFormat.format(Date(timeEnd))
                }

                orders.clear()
                cost = 0.0
                workDayIds.clear()
                runBlocking {
                    withContext(Dispatchers.IO){
                        workDays =  db.workDayDao().getWorkDayRange(driver.id, timeStart, timeEnd)
                    }
                }
                for (workDay in workDays) {
                    workDayIds.add(workDay.id.toString())

                    var resOrders = listOf<Order>()

                    runBlocking {
                        withContext(Dispatchers.IO){
                           resOrders = db.orderDao().getByWorkDay(workDay.id)
                        }
                    }
                    for (order in resOrders) {
                        orders.add(order)
                        cost += order.costOrder
                    }
                }

                (view.findViewById<View>(R.id.textview_reporttab_countorder) as TextView).text = orders.size.toString()
                (view.findViewById<View>(R.id.textview_reporttab_allcost) as TextView).text = TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), Math.round(cost).toString())
            }
        }
        LocalBroadcastManager.getInstance(activity!!).registerReceiver(broadcastReceiver, IntentFilter("driver.datepicker"))
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(broadcastReceiver)
    }
}
