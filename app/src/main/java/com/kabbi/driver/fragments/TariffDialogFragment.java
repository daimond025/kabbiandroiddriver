package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.events.ChangeBorderTariffEvent;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;

import org.greenrobot.eventbus.EventBus;

public class TariffDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        view = getActivity().getLayoutInflater().inflate(R.layout.dialog_border_tariff, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_border_tariff);
        typedValueMain = new TypedValue();
        String[] tariffs = getArguments().getStringArray("tariffs");
        int count = 0;
        int checked = 0;
        for (String label : tariffs) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setText(label);
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
            radioButton.setTag(count);
            radioGroup.addView(radioButton);

            if (label.equals(appPreferences.getText("border_tariff")))
                checked = count;
            count++;
        }
        ((RadioButton) radioGroup.getChildAt(checked)).setChecked(true);
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.title_toolbar_abonement))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            appPreferences.saveText("border_tariff", ((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
                            ((TextView) getActivity().findViewById(R.id.textview_orderborder_tariff)).setText(((RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId())).getText());
                            EventBus.getDefault().post(new ChangeBorderTariffEvent());
                            Intent intent = new Intent(AppParams.BROADCAST_DIALOG);
                            intent.putExtra("type", "tariff");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
