package com.kabbi.driver.fragments

import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.facebook.drawee.view.SimpleDraweeView
import com.kabbi.driver.*
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.CarDao
import com.kabbi.driver.database.dao.DriverDao
import com.kabbi.driver.database.dao.DriverTariffDao
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.helper.TypefaceSpanEx
import com.kabbi.driver.models.DriverCity
import com.kabbi.driver.models.Position
import com.kabbi.driver.network.WebService
import com.kabbi.driver.network.responses.*
import com.kabbi.driver.service.MainService
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.getDeviceToken
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*
import kotlin.collections.LinkedHashMap

class WorkFragment : Fragment(), androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {
    lateinit var driver: Driver
    internal lateinit var service: Service
    internal lateinit var appPreferences: AppPreferences
    internal lateinit var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    private var params: LinkedHashMap<String?, String?> = linkedMapOf()
    private var receiver: BroadcastReceiver? = null
    private var btnStartWork: Button? = null
    private var progressBar: ProgressBar? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var positions: List<Position> = arrayListOf()
    private var cars: List<Car>? = ArrayList()
    private var driverTariffs: List<DriverTariff> = ArrayList()
    private var driverCities: List<DriverCity>? = ArrayList()
    private var position: Position? = null
    private var car: Car? = null
    private var driverTariff: DriverTariff? = null
    private var driverCity: DriverCity? = null
    private val workFragment = this
    private val scope = CoroutineScope(Dispatchers.Main)
    private lateinit var carDao: CarDao
    private lateinit var driverDao: DriverDao
    private lateinit var driverTariffDao: DriverTariffDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_work, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val database = AppDatabase.getInstance(context!!)
        driverTariffDao = database.driverTariffDao()
        carDao = database.carDao()
        driverDao = database.driverDao()

        appPreferences = AppPreferences(activity!!.applicationContext)

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_work)
        swipeRefreshLayout.setOnRefreshListener(this)
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor(AppParams.colorOrange(activity!!.applicationContext)))

        btnStartWork = view.findViewById(R.id.button_work_beginwork)
        progressBar = view.findViewById(R.id.pbHeaderProgress)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        service = (activity as WorkActivity).service!!
        driver = (activity as WorkActivity).driver!!

        positions = ArrayList()

        view.findViewById<SimpleDraweeView>(R.id.imageview_workfragment).setImageURI(Uri.parse(service.logo))

        (view.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.download_data)
        (view.findViewById<View>(R.id.textview_work_tariff) as TextView).text = ""

        (view.findViewById<View>(R.id.textview_work_nameservice) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + service.serviceName
                + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + service.comment + "</font></small>")

        view.findViewById<View>(R.id.textview_work_balance).visibility = View.INVISIBLE
        (view.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.download_data)
        if (!InternetConnection.isOnline(activity!!))
            CustomToast.showMessage(activity, getString(R.string.text_internet_access))
        hideButton()

        view.findViewById<View>(R.id.textview_work_auto).setOnClickListener {

            scope.launch {

                if (cars != null && cars!!.size > 1) {
                    val dialogFragmentCar = CarDialogFragment()

                    var tariffs: List<DriverTariff> = arrayListOf()
                    var favoriteCar: Car? = null
                    withContext(Dispatchers.IO) {
                        tariffs = driverTariffDao.getDriverTariffs(driver.id!!)
                        favoriteCar = carDao.getByCarId(appPreferences.getText("car_id"))
                    }

                    val bundle = Bundle()
                    bundle.putParcelable("favouriteCar", favoriteCar)
                    bundle.putParcelableArray("cars", cars!!.toTypedArray())
                    bundle.putParcelableArray("tariffs", tariffs.toTypedArray())

                    dialogFragmentCar.arguments = bundle
                    dialogFragmentCar.setTargetFragment(workFragment, DIALOGFRAGMENT_CAR)
                    dialogFragmentCar.show(fragmentManager!!, "dialogFragment_car")
                }
            }
        }

        view.findViewById<View>(R.id.textview_work_tariff).setOnClickListener {
            if (appPreferences.getText("car_id").isNotEmpty()) {
                val intent = Intent(activity!!.applicationContext, TariffActivity::class.java)
                intent.putExtra("driver_id", driver.id)
                intent.putExtra("service_id", service.id)
                startActivity(intent)
            }
        }

        view.findViewById<View>(R.id.textview_work_city).setOnClickListener {
            if (driverCities != null && driverCities!!.size > 1) {
                val dialogFragmentCity = CitiesDialogFragment()

                val bundle = Bundle()
                bundle.putParcelableArrayList("cities", ArrayList(driverCities!!))

                dialogFragmentCity.setTargetFragment(workFragment, DIALOGFRAGMENT_CITY)
                dialogFragmentCity.arguments = bundle
                dialogFragmentCity.show(fragmentManager!!, "dialogFragment_city")
            }
        }

        view.findViewById<View>(R.id.textview_work_balance).setOnClickListener {
            val intent = Intent(activity!!.applicationContext, IncomeActivity::class.java)
            startActivity(intent)
        }

        view.findViewById<View>(R.id.textview_work_position).setOnClickListener {
            if (positions.size > 1) {

                val dialogFragmentPositions = PositionsDialogFragment()

                val bundle = Bundle()
                bundle.putParcelableArrayList("positions", ArrayList(positions))

                dialogFragmentPositions.setTargetFragment(workFragment, DIALOGFRAGMENT_POSITION)
                dialogFragmentPositions.arguments = bundle
                dialogFragmentPositions.show(fragmentManager!!, "dialogFragment_positions")
            }
        }

        btnStartWork!!.setOnClickListener {
            if (InternetConnection.isOnline(activity!!)) {
                val locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || AppParams.LOCAION == "NETWORK") {
                    try {
                        appPreferences = AppPreferences(activity!!)
                        var auto = false
                        if (appPreferences.getText("car_id").isNotEmpty())
                            auto = true
                        if (!position!!.isHasCar)
                            auto = true

                        if (auto && appPreferences.getText("drivertariff_id").isNotEmpty()) {
                            requestLocationWrapper()
                        } else {
                            CustomToast.showMessage(activity!!.applicationContext, getString(R.string.startwork_fail))
                        }
                    } catch (e: Exception) {
                        CustomToast.showMessage(activity, getString(R.string.text_workdayfragment_status_badtariff))
                        e.printStackTrace()
                    }

                } else {
                    CustomToast.showMessage(activity, getString(R.string.gps_disabled))
                }
            } else {
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }

        params = LinkedHashMap()
        params["tenant_login"] = service.uri
        params["worker_login"] = driver.callSign

        getWorkerCities()
    }

    private fun getWorkerCities() {
        runBlocking {
            withContext(Dispatchers.IO) {
                WebService.getCities(params, driver.secretCode)
            }?.also { response ->
                onGetWorkerCitiesResponse(response)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                DIALOGFRAGMENT_POSITION -> for (positionItem in positions) {
                    if (data!!.getStringExtra("position_id") == positionItem.positionId) {
                        position = positionItem
                        appPreferences.saveText("position_id", data.getStringExtra("position_id"))
                        setWorkerPosition(position!!.name)
                        loadDriverProfile()

                        break
                    }
                }
                DIALOGFRAGMENT_CITY -> for (cityItem in driverCities!!) {
                    if (data!!.getStringExtra("city_id") == cityItem.cityId) {
                        driverCity = cityItem
                        appPreferences.saveText("city_id", data.getStringExtra("city_id"))
                        setWorkerCity(driverCity!!.cityName!!)
                        loadWorkerPositions(driverCity!!.cityId!!)

                        break
                    }
                }
                DIALOGFRAGMENT_TARIFF -> {
                }
                DIALOGFRAGMENT_CAR -> for (carItem in cars!!) {
                    if (data!!.getStringExtra("car_id") == carItem.carId) {
                        car = carItem
                        appPreferences.saveText("car_id", data.getStringExtra("car_id"))
                        appPreferences.saveText("car_company", data.getStringExtra("car_company"))
                        setWorkerAuto(car!!.descr)

                        break
                    }
                }
            }
        }
    }

    private fun onGetWorkerCitiesResponse(response: WorkerCitiesResponse) {
        when (response.status) {
            "OK" -> {
                appPreferences = AppPreferences(activity!!)
                if (appPreferences.getText("city_id").isNotEmpty()) {
                    for (item in response.driverCities) {
                        if (appPreferences.getText("city_id") == item.cityId) {
                            driverCity = item
                            break
                        }
                        if (response.driverCity != null) {
                            driverCity = response.driverCity
                        } else {
                            driverCity = item
                        }
                    }
                } else {
                    driverCity = response.driverCity
                    (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.empty_city)
                }

                driverCities = response.driverCities
                appPreferences.saveText("city_id", driverCity!!.cityId)
                setWorkerCity(driverCity!!.cityName!!)
                loadWorkerPositions(driverCity!!.cityId!!)
            }

            else -> {
                (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.empty_city)
                swipeRefreshLayout.isRefreshing = false
                showButton()
            }
        }
    }

    @Subscribe
    fun onEvent(response: GetProfileResponse) {
        runBlocking {

            appPreferences = AppPreferences(activity!!)
            params = LinkedHashMap()
            params["tenant_login"] = service.uri
            params["worker_city_id"] = driverCity!!.cityId
            params["worker_login"] = driver.callSign

            withContext(Dispatchers.IO) {
                WebService.getBalance(params, driver.secretCode)
            }?.also { balance ->
                onBalanceResponse(balance)
            }

            view?.findViewById<View>(R.id.textview_work_balance)?.visibility = View.VISIBLE
            driver.cityID = response.driver.cityID
            driver.exp = response.driver.exp
            driver.name = response.driver.name
            driver.phone = response.driver.phone
            driver.email = response.driver.email
            driver.photo = response.driver.photo
            driver.promoCode = response.driver.promoCode
            (activity!!.application as App).driver = driver

            withContext(Dispatchers.IO) {
                carDao.deleteAll(driver.id)
                driver.id = driverDao.save(driver)
                driverTariffDao.deleteAll(driver.id)
            }

            if (response.driverTariffList.isEmpty()) {
                (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.empty_tariff)
            } else {
                driverTariff = response.driverTariffList[0]
                driverTariffs = response.driverTariffList

                for (itemTariff in response.driverTariffList) {
                    itemTariff.driver = driver.id
                    withContext(Dispatchers.IO) {
                        driverTariffDao.save(itemTariff)
                    }

                    if (appPreferences.getText("drivertariff_id") == itemTariff.tariffId) {
                        driverTariff = itemTariff
                    }
                }

                appPreferences.saveText("drivertariff_id", driverTariff!!.tariffId)
                setWorkerTariff(driverTariff!!.tariffName)

                if (position!!.isHasCar && response.carList.isNotEmpty()) {
                    car = response.carList[0]
                    cars = response.carList

                    for (carItem in response.carList) {
                        carItem.driver = driver.id
                        withContext(Dispatchers.IO) {
                            carDao.save(carItem)
                        }
                        if (appPreferences.getText("car_id") == carItem.carId) {
                            car = carItem
                        }
                    }

                    appPreferences.saveText("car_id", car!!.carId)
                    appPreferences.saveText("car_company", car!!.isCompanyCar.toString() + "")
                    setWorkerAuto(car!!.descr)
                } else {
                    cars = ArrayList()
                    (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.download_empty_car)
                }
            }

            showButton()

            if (swipeRefreshLayout.isRefreshing)
                swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setWorkerCity(cityName: String) {
        (view?.findViewById<View>(R.id.textview_work_city) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                + getString(R.string.dialog_address_city)
                + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + cityName + "</font></small>")
    }

    private fun setWorkerPosition(positionName: String) {
        (view?.findViewById<View>(R.id.textview_work_position) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                + getString(R.string.text_prof)
                + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + positionName + "</font></small>")
    }

    private fun setWorkerTariff(tariffName: String) {
        view?.findViewById<TextView>(R.id.textview_work_tariff)?.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + getString(R.string.text_workactivity_tariff)
                + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + tariffName + "</font></small>")
    }

    private fun setWorkerAuto(autoName: String) {
        view?.findViewById<TextView>(R.id.textview_work_auto)?.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + getString(R.string.text_workactivity_auto)
                + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + autoName + "</font></small>")
    }

    private fun loadWorkerPositions(city_id: String) {
        val params = LinkedHashMap<String, String>()
        params["tenant_login"] = service.uri
        params["worker_city_id"] = city_id
        params["worker_login"] = driver.callSign

        runBlocking {
            withContext(Dispatchers.IO) {
                WebService.getPositions(params, driver.secretCode)
            }?.also { positions ->
                onGetPositionsResponse(positions)
            }
        }
    }

    private fun onGetPositionsResponse(getPositionResponse: GetPositionResponse) {
        when (getPositionResponse.status) {
            "OK" -> {
                appPreferences = AppPreferences(activity!!)
                positions = getPositionResponse.positions
                position = null
                positions = positions.reversed()
                for (item in positions) {
                    position = item
                    if (appPreferences.getText("position_id") == position!!.positionId) {
                        break
                    }
                }
                positions = positions.reversed()
                appPreferences.saveText("position_id", position!!.positionId)

                if (positions.isNotEmpty()) {
                    loadDriverProfile()

                    setWorkerPosition(position!!.name)
                } else {
                    (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.empty_worker)
                }
            }
            else -> {
                (view?.findViewById<View>(R.id.textview_work_auto) as TextView).text = getString(R.string.empty_worker)
                swipeRefreshLayout.isRefreshing = false
                showButton()
            }
        }
    }

//    @Subscribe
//    fun onEvent(profileWorkerEvent: UpdateProfileWorkerEvent) {
//        loadDriverProfile()
//    }

    private fun loadDriverProfile() {

        appPreferences.saveText("position_id", position!!.positionId)
        appPreferences.saveText("drivercity_id", driverCity!!.cityId)

        val params = LinkedHashMap<String, String>()
        params["position_id"] = position!!.positionId
        params["tenant_login"] = service.uri
        params["worker_city_id"] = driverCity!!.cityId!!
        params["worker_login"] = driver.callSign
        WebService.getProfile(activity!!, params, driver.secretCode)
    }

    private fun onBalanceResponse(balanceResponse: BalanceResponse) {
        service.balance = balanceResponse.balance
        service.currency = balanceResponse.currency
        runBlocking {
            withContext(Dispatchers.IO) {
                AppDatabase.getInstance(context!!).serviceDao().save(service)
            }
        }
        (activity!!.application as App).service = service
        appPreferences.saveText("currency", service.currency)
        try {
            view?.findViewById<TextView>(R.id.textview_work_balance)?.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + getString(R.string.text_workactivity_balance)
                    + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                    + TypefaceSpanEx.getCurrencyStr(context, (activity!!.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0), balanceResponse.balance) + "</font></small>")

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun alreadyInUse() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setPositiveButton(R.string.already_in_use_approve) { _, _ ->
                    val requestParams = LinkedHashMap<String, String>()
                    requestParams["car_mileage"] = "0"
                    requestParams["tenant_login"] = service.uri
                    requestParams["worker_login"] = driver.callSign
                    WebService.endWork(requestParams, driver.secretCode)
                }
                setNegativeButton(R.string.already_in_use_cancel) { dialog, _ ->
                    dialog.dismiss()
                }
            }
            builder.setTitle(R.string.already_in_use)
            builder.setMessage(R.string.already_in_use_message)
            builder.create()
        }
        alertDialog?.show()
    }

    private fun requestStartWorkDay(mileage: String) {

        scope.launch {
            try {
                var tariffId = ""
                var carId = ""
                withContext(Dispatchers.IO) {
                    carId = if (position!!.isHasCar) carDao.getByCarId(appPreferences.getText("car_id")).carId else ""
                    tariffId = driverTariffDao.getDriverTariffByField(appPreferences.getText("drivertariff_id")).tariffId
                }
                hideButton()

                getDeviceToken().addOnSuccessListener { v ->
                    val paramsWork = LinkedHashMap<String, String>()
                    paramsWork["app_version"] = AppParams.VERSION_APP.toString()
                    paramsWork["car_mileage"] = mileage
                    paramsWork["device_token"] = v.token
                    paramsWork["tenant_login"] = service.uri
                    paramsWork["worker_car_id"] = carId
                    paramsWork["worker_city_id"] = driver.cityID
                    paramsWork["worker_login"] = driver.callSign
                    paramsWork["worker_tariff_id"] = tariffId

                    runBlocking {
                        withContext(Dispatchers.IO) {
                            WebService.startWorkDay(paramsWork, driver.secretCode)
                        }.also { response ->
                            onStartWorkDayResponse(response)
                        }
                    }
                }

            } catch (e: Exception) {
                CustomToast.showMessage(context, getString(R.string.text_workdayfragment_status_badtariff))
                e.printStackTrace()
            }
        }
    }

    private fun onStartWorkDayResponse(startWorkResponse: StartWorkResponse) {
        when (startWorkResponse.status) {
            "OK" -> startWordDay(startWorkResponse.workerShiftId!!)
            "WORKER_ALLREADY_ON_SHIFT" -> {
                showButton()
                alreadyInUse()
            }
            "CAR_IS_BUSY" -> {
                CustomToast.showMessage(activity, getString(R.string.text_workdayfragment_status))
                showButton()
            }
            "BAD_TARIFF" -> {
                CustomToast.showMessage(activity, getString(R.string.text_workdayfragment_status_badtariff))
                showButton()
            }
            "BAD_BALANCE" -> {
                CustomToast.showMessage(activity, getString(R.string.text_workdayfragment_status_badbalance))
                showButton()
            }
            "OLD_APP_VERSION" -> {
                OldVersionDialogFragment().show(fragmentManager!!, "dialogFragment_oldVersion")
                showButton()
            }
            "WORKER_BLOCKED" -> {
                CustomToast.showMessage(activity, getString(R.string.error_driver_blocked))
                showButton()
            }
            "WORKER_LIMIT" -> {
                CustomToast.showMessage(activity, getString(R.string.startwork_driverlimit))
                showButton()
            }
            "CITY_BLOCKED" -> {
                CustomToast.showMessage(activity, getString(R.string.startwork_cityblocked))
                showButton()
            }
            "INVALID_CAR_MILEAGE" -> {
                CustomToast.showMessage(activity, getString(R.string.error_mileage))
                showButton()
                CustomToast.showMessage(activity, startWorkResponse.status)
                showButton()
            }
            else -> {
                CustomToast.showMessage(activity, startWorkResponse.status)
                showButton()
            }
        }
    }

    private fun showButton() {
        progressBar!!.visibility = View.GONE
        btnStartWork!!.visibility = View.VISIBLE
    }

    private fun hideButton() {
        btnStartWork!!.visibility = View.GONE
        progressBar!!.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    (view?.findViewById<View>(R.id.textview_work_balance) as TextView).text = Html.fromHtml("<big><font color='"
                            + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + getString(R.string.text_workactivity_balance)
                            + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                            + " " + TypefaceSpanEx.getCurrencyStr(activity, getView(), intent.getStringExtra("PUSH")) + "</font></small>")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        val intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH)
        activity!!.registerReceiver(receiver, intentFilterPush)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        activity!!.unregisterReceiver(receiver)
        super.onStop()
    }

    override fun onRefresh() {
        if (InternetConnection.isOnline(activity!!)) {
            swipeRefreshLayout.isRefreshing = true

            params = LinkedHashMap()
            params["tenant_login"] = service.uri
            params["worker_login"] = driver.callSign
            getWorkerCities()

        } else {
            swipeRefreshLayout.isRefreshing = false
            CustomToast.showMessage(activity, getString(R.string.text_internet_access))
        }
    }

    private fun startWordDay(shiftId: String) {
        val intent = Intent(activity!!.applicationContext, WorkShiftActivity::class.java)
        intent.putExtra("service_id", service.id)
        intent.putExtra("showDialogGps", true)
        intent.putExtra("confirm", false)
        intent.putExtra("stageOrder", 0)
        intent.putExtra("showDialog", "empty")

        val newWorkDay = WorkDay()
        newWorkDay.driver = driver.id
        newWorkDay.service = service.id
        newWorkDay.startWorkDay = Calendar.getInstance().time.time
        newWorkDay.shiftId = shiftId

        runBlocking {
            withContext(Dispatchers.IO) {
                newWorkDay.id = AppDatabase.getInstance(context!!).workDayDao().save(newWorkDay)
            }
        }

        (activity!!.application as App).workDay = newWorkDay
        intent.putExtra("workday_id", newWorkDay.id)

        activity!!.stopService(Intent(activity!!.applicationContext, MainService::class.java))
        activity!!.startService(Intent(activity!!.applicationContext, MainService::class.java).putExtra("driver_id", driver.id))

        appPreferences.saveText("driver_id", driver.id.toString())
        appPreferences.saveText("service_id", service.id.toString())
        appPreferences.saveText("workday_id", newWorkDay.id.toString())
        appPreferences.saveText("in_shift", "1")

        startActivity(intent)
        activity!!.finish()
    }

    private fun requestLocationWrapper() {
        val hasWriteLocPermission = ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (hasWriteLocPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request), DialogInterface.OnClickListener { _, _ ->
                    ActivityCompat.requestPermissions(activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE)
                })
                return
            }
            ActivityCompat.requestPermissions(activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE)
            return
        }
        if (appPreferences.getText("control_own_car_mileage") == "1" && appPreferences.getText("car_company") == "true") {
            setMileage()
        } else {
            requestStartWorkDay("0")
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(activity!!)
                .setMessage(message)
                .setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel), null)
                .setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok), okListener)
                .create()
                .show()
    }

    private fun setMileage() {
        val viewDialog = activity!!.layoutInflater.inflate(R.layout.fragment_dialog_edittext_mileage, null)
        AlertDialog.Builder(activity!!)
                .setView(viewDialog)
                .setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                .setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { dialog, _ ->
                    val mileage = (viewDialog.findViewById<View>(R.id.et_mileage) as EditText).text.toString()
                    if (mileage.trim { it <= ' ' }.isNotEmpty()) {
                        requestStartWorkDay(mileage)
                        dialog.dismiss()
                    } else {
                        CustomToast.showMessage(activity, getString(R.string.edittext_valid_required))
                    }
                }
                .create()
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == WorkShiftActivity.PERMISSION_PINGSERVICE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (appPreferences.getText("control_own_car_mileage") == "1" && appPreferences.getText("car_company") == "true") {
                    setMileage()
                } else {
                    requestStartWorkDay("0")
                }
            } else {
                CustomToast.showMessage(activity, getString(R.string.activities_MainActivity_permission_rejected))
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    companion object {

        const val DIALOGFRAGMENT_POSITION = 21
        const val DIALOGFRAGMENT_CITY = 20
        const val DIALOGFRAGMENT_TARIFF = 22
        const val DIALOGFRAGMENT_CAR = 23
    }
}