package com.kabbi.driver.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.kabbi.driver.R;

import java.util.Calendar;


public class DatePickerDialogFragment extends DialogFragment {

    View view;
    DatePicker datePicker;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light);
            inflater = inflater.cloneInContext(contextThemeWrapper);
        }

        view = inflater.inflate(R.layout.fragment_datepicker, container, false);

        datePicker = (DatePicker) view.findViewById(R.id.datepicker_dialog);

        ((Button) view.findViewById(R.id.button_picker_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                calendar.set(Calendar.MONTH, datePicker.getMonth());
                calendar.set(Calendar.YEAR, datePicker.getYear());
                //setDate.changeDate(getArguments().getInt("num"), calendar.getTime().getTime());
                Intent intent = new Intent("driver.datepicker");
                intent.putExtra("num", getArguments().getInt("num"));
                intent.putExtra("date", calendar.getTime().getTime());
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                getDialog().dismiss();

            }
        });

        ((Button) view.findViewById(R.id.button_picker_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });


        return view;
    }

}
