package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;

import com.kabbi.driver.R;
import com.kabbi.driver.events.ChangeFragmentInOrderEvent;

import org.greenrobot.eventbus.EventBus;

public class ChangeDialogFragment extends DialogFragment {

    private View view;
    private TypedValue typedValueMain, typedValueSubscribe;
    private MediaPlayer mediaPlayer;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_change, null);
        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();

        this.setCancelable(false);

        String typeTitle = getString(R.string.text_order_rejected);
        try {
            switch (getArguments().getString("type")) {
                case "order_is_rejected":
                    typeTitle = getString(R.string.text_order_rejected);
                    break;
                case "reject_worker_from_order":
                    typeTitle = getString(R.string.text_reject_worker_from_order);
                    break;
                case "complete_order":
                    typeTitle = getString(R.string.text_complete_worker_from_order);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return new AlertDialog.Builder(getActivity())
                .setTitle(typeTitle)
                .setPositiveButton("Ok",
                        (dialog, whichButton) -> {

                            EventBus.getDefault().post(new ChangeFragmentInOrderEvent(1));

                            dialog.dismiss();
                        }
                )
                .setView(view).create();
    }

}
