package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.models.Address;
import com.mapswithme.maps.api.MWMPoint;
import com.mapswithme.maps.api.MapsWithMeApi;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NavigatorOtherDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain, typedValueSubscribe;
    private String[] arrAddresses = {"B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();

        List<Address> addressList = new ArrayList<>();

        try {
            JSONObject jsonObjectAddress = new JSONObject(getArguments().getString("address"));
            try {

                Address addressA = new Address();
                addressA.setCity(jsonObjectAddress.getJSONObject("A").getString("city"));
                addressA.setStreet(jsonObjectAddress.getJSONObject("A").getString("street"));
                addressA.setLat(Double.valueOf(jsonObjectAddress.getJSONObject("A").getString("lat")));
                addressA.setLon(Double.valueOf(jsonObjectAddress.getJSONObject("A").getString("lon")));
                try {
                    addressA.setHouse(jsonObjectAddress.getJSONObject("A").getString("house"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                addressList.add(addressA);

                for (String itemAdress : arrAddresses) {
                    try {
                        Address addressB = new Address();
                        addressB.setCity(jsonObjectAddress.getJSONObject(itemAdress).getString("city"));
                        addressB.setStreet(jsonObjectAddress.getJSONObject(itemAdress).getString("street"));
                        addressB.setLat(Double.valueOf(jsonObjectAddress.getJSONObject(itemAdress).getString("lat")));
                        addressB.setLon(Double.valueOf(jsonObjectAddress.getJSONObject(itemAdress).getString("lon")));
                        try {
                            addressB.setHouse(jsonObjectAddress.getJSONObject(itemAdress).getString("house"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        addressList.add(addressB);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        int count = 0;
        for (Address address : addressList) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());


            StringBuilder stringBuilder = new StringBuilder("");
            if (!address.getCity().equals("null") && address.getCity().length() > 0) {
                stringBuilder.append(address.getCity() + ", ");
            }

            if (!address.getStreet().equals("null") && address.getStreet().length() > 0) {
                stringBuilder.append(address.getStreet() + ", ");
            }

            if (!address.getHouse().equals("null") && address.getHouse().length() > 0) {
                stringBuilder.append(address.getHouse());
            }

            radioButton.setText(stringBuilder.toString());

            radioButton.setTag(R.id.tag_label, count);
            radioButton.setTag(R.id.tag_lat, address.getLat());
            radioButton.setTag(R.id.tag_lon, address.getLon());
            radioGroup.addView(radioButton);
            count++;
        }

        if (addressList.size() > 0)
            ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_navi))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            switch (appPreferences.getText("far_navi")) {
                                case "0":
                                    clickWazeNavi(Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lat).toString()),
                                            Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lon).toString()));
                                    break;
                                case "1":
                                    clickYandexNavi(Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lat).toString()),
                                            Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lon).toString()));
                                    break;
                                case "2":
                                    clickYandexMapsNavi(Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lat).toString()),
                                            Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lon).toString()));
                                    break;
                                case "3":
                                    clickMapsMeNavi(Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lat).toString()),
                                            Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lon).toString()));
                                    break;
                                default:
                                    clickWazeNavi(Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lat).toString()),
                                            Double.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_lon).toString()));
                            }


                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }

    private void clickMapsMeNavi(double lat, double lon) {
        if (MapsWithMeApi.isMapsWithMeInstalled(getContext())) {
            Intent i = new Intent(getActivity(), WorkShiftActivity.class);
            i.putExtra("from-maps-with-me", true);

            appPreferences = new AppPreferences(getActivity());


            MapsWithMeApi.showPointsOnMap(getActivity(), "",
                    new MWMPoint(lat, lon, ""));
        } else {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.mapswithme.maps.pro" ) );
            startActivity(intent);
        }
    }

    private void clickYandexNavi(double lat, double lon) {

        Intent intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
        intent.setPackage("ru.yandex.yandexnavi");

        appPreferences = new AppPreferences(getActivity());

        PackageManager pm = getActivity().getPackageManager();
        List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);

        // Проверяем, установлен ли Яндекс.Навигатор
        if (infos == null || infos.size() == 0) {
            // Если нет - будем открывать страничку Навигатора в Google Play
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=ru.yandex.yandexnavi"));
        } else {
            try {

                intent.putExtra("lat_from", Double.valueOf(appPreferences.getText("lat")));
                intent.putExtra("lon_from", Double.valueOf(appPreferences.getText("lon")));
                intent.putExtra("lat_to", lat);
                intent.putExtra("lon_to", lon);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Запускаем нужную Activity
        startActivity(intent);
    }

    private void clickYandexMapsNavi(double lat, double lon) {
        Double lat_from = Double.valueOf(appPreferences.getText("lat"));
        Double lon_from = Double.valueOf(appPreferences.getText("lon"));
        String driverURL = String.valueOf(lat_from + "," + lon_from);
        String pointURL = lat + "," + lon;

        Uri uri = Uri.parse("yandexmaps://maps.yandex.ru/?rtext=" + driverURL + "~" + pointURL + "&rtt=auto");

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        // Проверяем, установлено ли хотя бы одно приложение, способное выполнить это действие.
        PackageManager packageManager = getActivity().getPackageManager();
        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
        boolean isIntentSafe = activities.size() > 0;
        if (isIntentSafe) {
            startActivity(intent);
        } else {
            // Открываем страницу приложения Яндекс.Карты в Google Play.
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=ru.yandex.yandexmaps"));
            startActivity(intent);
        }
    }

    private void clickWazeNavi(double lat, double lon) {
        try
        {
            String url = null;

            appPreferences = new AppPreferences(getActivity());

            try {
                url = "waze://?ll=" + lat + "," + lon + "&navigate=yes";
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (url == null)
                url = "waze://?ll=" + appPreferences.getText("lat") + "," + appPreferences.getText("lon") + "&navigate=yes";

            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse(url) );
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }
    }
}
