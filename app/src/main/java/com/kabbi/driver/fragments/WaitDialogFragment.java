package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.events.TimerEvent;
import com.kabbi.driver.events.taximeter.TimeWaitStartEvent;
import com.kabbi.driver.events.taximeter.TimeWaitStopEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class WaitDialogFragment extends DialogFragment {
    View view;
    TextView textView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_wait, null);
        textView = (TextView) view.findViewById(R.id.tv_dialog_wait);

        EventBus.getDefault().post(new TimeWaitStartEvent());

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.text_taximeter_status_wait))
                .setNegativeButton(getString(R.string.text_button_pauseworknext),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                EventBus.getDefault().post(new TimeWaitStopEvent());
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();

    }

    @Subscribe
    public void onEvent(TimerEvent timerEvent) {
        try {
            if (textView != null) {
                textView.setText(((TextView) getActivity().findViewById(R.id.textview_taximeter_timewait)).getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
