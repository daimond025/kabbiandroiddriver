package com.kabbi.driver.fragments;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.database.entities.Service;

import java.util.ArrayList;
import java.util.List;


public class ChatFragment extends Fragment {

    View view;
    TabLayout tabLayout;
    private int TAB_COUNT = 2;
    private Driver driver;
    private Service service;
    private List<String> titles;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        driver = ((WorkShiftActivity) getActivity()).getDriver();
        service = ((WorkShiftActivity) getActivity()).getService();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_taximeter, container, false);

        titles = new ArrayList<>();
        titles.add(getString(R.string.text_title_chat_driver));
        titles.add(getString(R.string.text_title_chat_city));

        viewPager = view.findViewById(R.id.taximeter_pager);

        pagerAdapter = new ChatTabAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        tabLayout = view.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        ColorStateList myColorStateList = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_enabled}, // enabled
                        new int[]{-android.R.attr.state_checked}, // unchecked
                        new int[]{android.R.attr.state_pressed}  // pressed
                },
                new int[]{
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE
                }
        );
        tabLayout.setTabTextColors(myColorStateList);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    private class ChatTabAdapter extends FragmentPagerAdapter {

        public ChatTabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle args = new Bundle();
            args.putLong("driver_id", driver.getId());
            args.putLong("service_id", service.getId());

            switch (position) {
                case 0:
                    fragment = new ChatDriverFragment();
                    break;
                case 1:
                    fragment = new ChatCityFragment();
                    break;

            }
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }
    }

}
