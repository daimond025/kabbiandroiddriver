package com.kabbi.driver.fragments


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import android.text.Html
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Car
import com.kabbi.driver.database.entities.DriverTariff
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences

class CarDialogFragment : DialogFragment() {

    internal lateinit var view: View
    internal lateinit var radioGroup: RadioGroup
    internal lateinit var appPreferences: AppPreferences

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        view = activity!!.layoutInflater.inflate(R.layout.fragment_theme, null)
        appPreferences = AppPreferences(activity!!.applicationContext)
        radioGroup = view.findViewById(R.id.radiogroup_theme)

        var favoriteCar: Car? = null
        var cars: List<Car> = arrayListOf()
        var tariffs: List<DriverTariff> = arrayListOf()
        arguments?.also {
            favoriteCar = it.getParcelable("favouriteCar")
            cars = (it.getParcelableArray("cars") as Array<Car>).toList()
            tariffs = (it.getParcelableArray("tariffs") as Array<DriverTariff>).toList()
        }

        for ((count, car) in cars.withIndex()) {
            val radioButton = RadioButton(activity!!.applicationContext)

            radioButton.setTag(R.id.tag_car_id, car.carId)
            radioButton.setTag(R.id.tag_car_desc, car.descr)
            radioButton.setTag(R.id.tag_car_class, car.type)
            radioButton.setTag(R.id.tag_car_company, car.isCompanyCar.toString() + "")
            radioButton.setTag(R.id.tag_car_index, count)
            radioButton.text = car.descr
            radioButton.setTextColor(resources.getColor(R.color.black))

            radioGroup.addView(radioButton)
        }

        for (i in 0 until radioGroup.childCount) {
            if (radioGroup.getChildAt(i).getTag(R.id.tag_car_id).toString() == favoriteCar?.carId.toString()) {
                (radioGroup.getChildAt(i) as RadioButton).isChecked = true
                break
            }
        }

        return AlertDialog.Builder(activity!!)
                .setTitle(getString(R.string.text_workactivity_dialogtitle))
                .setPositiveButton(getString(R.string.button_ok)) { dialog, _ ->

                    val car = cars[Integer.valueOf(radioGroup.findViewById<View>(radioGroup.checkedRadioButtonId).getTag(R.id.tag_car_index).toString())]

                    var driverTariff: DriverTariff? = null
                    for (dt in tariffs) {
                        if (car.type == dt.carClass) {
                            driverTariff = dt
                            break
                        }
                    }

                    if (driverTariff != null) {
                        appPreferences.saveText("drivertariff_id", driverTariff.tariffId)
                        activity!!.findViewById<TextView>(R.id.textview_work_tariff).text = Html.fromHtml("<big><font color='black'>" + getString(R.string.text_workactivity_tariff)
                                + "</font></big><br><small><font color='" + AppParams.colorGrey(activity!!.applicationContext) + "'>" + driverTariff.tariffName + "</font></small>")
                    } else {
                        appPreferences.saveText("drivertariff_id", "")
                        activity!!.findViewById<TextView>(R.id.textview_work_tariff).text = Html.fromHtml("<font color='black'>" + getString(R.string.text_workactivity_tariff) + "</font><br>")
                    }

                    activity!!.findViewById<TextView>(R.id.textview_work_auto).text = Html.fromHtml("<big><font color='black'>" + getString(R.string.text_workactivity_auto)
                            + "</font></big><br><small><font color='" + AppParams.colorGrey(activity!!.applicationContext) + "'>"
                            + radioGroup.findViewById<View>(radioGroup.checkedRadioButtonId).getTag(R.id.tag_car_desc).toString() + "</font></small>")

                    val result = Intent()
                    result.putExtra("car_id", radioGroup.findViewById<View>(radioGroup.checkedRadioButtonId).getTag(R.id.tag_car_id).toString())
                    result.putExtra("car_company", radioGroup.findViewById<View>(radioGroup.checkedRadioButtonId).getTag(R.id.tag_car_company).toString())

                    targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, result)

                    dialog.dismiss()
                }
                .setNegativeButton(getString(R.string.button_cancel)
                ) { dialog, _ -> dialog.dismiss() }
                .setView(view).create()
    }
}
