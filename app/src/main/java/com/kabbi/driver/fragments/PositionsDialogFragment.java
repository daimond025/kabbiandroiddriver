package com.kabbi.driver.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.models.Position;

import java.util.List;

public class PositionsDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain, typedValueSubscribe;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = (RadioGroup) view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();

        getArguments().getParcelableArrayList("positions");

        List<Position> positions = getArguments().getParcelableArrayList("positions");

        int count = 0;
        for (Position position : positions) {

            Log.d("POSITIONS_DIALOG", position.getName());

            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setText(position.getName());
            radioButton.setTextColor(typedValueMain.data);
            radioButton.setTag(count);
            radioButton.setTag(R.id.tag_label, position.getName());
            radioButton.setTag(R.id.tag_car_class, position.getPositionId());

            radioGroup.addView(radioButton);

            count++;
        }


        int countRadioButton = radioGroup.getChildCount();
        for (int i = 0; i < countRadioButton; i++) {
            if (((RadioButton) radioGroup.getChildAt(i)).getTag(R.id.tag_car_class).toString()
                    .equals(appPreferences.getText("position_id"))) {
                ((RadioButton) radioGroup.getChildAt(i)).setChecked(true);
            }
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.diolog_title_mstr))
                .setPositiveButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((TextView) getActivity().findViewById(R.id.textview_work_position)).setText(Html.fromHtml("<big><font color='"+String.format("#%06X", 0xFFFFFF & typedValueMain.data)+"'>"
                                        + getString(R.string.text_prof)
                                        + "</font></big><br><small><font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                                        + radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_label).toString() + "</font></small>"));



                                Intent result = new Intent();
                                result.putExtra("position_id", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag(R.id.tag_car_class).toString());

                                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, result);


                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }

}
