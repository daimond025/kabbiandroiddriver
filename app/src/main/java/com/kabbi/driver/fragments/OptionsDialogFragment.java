package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.adapters.OptionsAdapter;
import com.kabbi.driver.models.Options;

import java.util.List;

public class OptionsDialogFragment extends DialogFragment {

    public interface SelectOptListener {
        void onSelectOptions(List<Options> optionsList);
    }

    protected List<Options> options;
    private SelectOptListener listener;
    private String title;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_border_options, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView tvTitle = view.findViewById(R.id.dialog_radio_title);
        tvTitle.setText(title);
        RecyclerView mRecyclerView = view.findViewById(R.id.opt_list);
        OptionsAdapter mAdapter = new OptionsAdapter(getContext(), options);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(mAdapter);
        builder.setView(view)
                .setPositiveButton(R.string.button_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onSelectOptions(options);
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        dialog.dismiss();
    }

    public List<Options> getOptions() {
        return this.options;
    }

    public void setOptions(List<Options> options) {
        this.options = options;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SelectOptListener getListener() {
        return listener;
    }

    public void setListener(SelectOptListener listener) {
        this.listener = listener;
    }
}
