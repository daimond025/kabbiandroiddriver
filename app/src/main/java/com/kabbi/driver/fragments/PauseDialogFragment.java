package com.kabbi.driver.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.database.entities.Driver;

import java.util.ArrayList;
import java.util.List;

public class PauseDialogFragment extends DialogFragment {

    View view;
    RadioGroup radioGroup;
    Driver driver;
    List<String> titles;
    private TypedValue typedValueMain;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        radioGroup = view.findViewById(R.id.radiogroup_theme);

        typedValueMain = new TypedValue();

        driver = ((WorkShiftActivity) getActivity()).getDriver();

        titles = new ArrayList<String>();
        titles.add(getString(R.string.status_dialog_pause));
        titles.add(getString(R.string.status_dialog_break));
        titles.add(getString(R.string.status_dialog_dtp));
        titles.add(getString(R.string.status_dialog_gb2d));
        titles.add(getString(R.string.status_dialog_other));


        for (String title : titles) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setText(title);
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
            radioButton.setTag(title);
            radioGroup.addView(radioButton);
        }

        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.status_dialog))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {

                            Intent intent = new Intent();
                            intent.putExtra("title_pause", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());
                            getTargetFragment().onActivityResult(getTargetRequestCode(), WorkDayFragment.REQUEST_CODE, intent);

                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
