package com.kabbi.driver.fragments;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.kabbi.driver.R;
import com.kabbi.driver.events.ThemeEvent;
import com.kabbi.driver.helper.AppPreferences;

import org.greenrobot.eventbus.EventBus;

public class ThemeDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        typedValueMain = new TypedValue();

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);


        RadioButton radioButtonDay = new RadioButton(getActivity().getApplicationContext());
        radioButtonDay.setText(getString(R.string.text_day));
        radioButtonDay.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButtonDay.setTag(R.style.AppThemeLight);


        RadioButton radioButtonNight = new RadioButton(getActivity().getApplicationContext());
        radioButtonNight.setText(getString(R.string.text_night));
        radioButtonNight.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButtonNight.setTag(R.style.AppThemeDark);

        radioGroup.addView(radioButtonDay);
        radioGroup.addView(radioButtonNight);

        if (Integer.valueOf(appPreferences.getText("theme")) == R.style.AppThemeDark)
            radioButtonNight.setChecked(true);
        else
            radioButtonDay.setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_theme))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            appPreferences.saveText("theme", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());
                            EventBus.getDefault().post(new ThemeEvent());
                            Toast.makeText(getContext(), R.string.update_theme, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
