package com.kabbi.driver.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.kabbi.driver.CardActivity
import com.kabbi.driver.IncomeActivity
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.NetworkStatisticEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.network.responses.BalanceResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.LinkedHashMap

class IncomeFragment : Fragment() {
    private lateinit var appPreferences: AppPreferences
    private lateinit var service: Service
    private lateinit var driver: Driver
    private lateinit var incomeWeek: LinearLayout
    private lateinit var currentBalance: LinearLayout
    private lateinit var tvBalance: TextView
    private lateinit var tvIncomeValue: TextView
    private lateinit var tvOrdersCount: TextView
    private lateinit var pBar1: ProgressBar
    private lateinit var pBar2: ProgressBar
    private lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_income, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvBalance = view.findViewById(R.id.tv_balance)
        tvIncomeValue = view.findViewById(R.id.tv_income_value)
        tvOrdersCount = view.findViewById(R.id.tv_orders_count)
        pBar1 = view.findViewById(R.id.p_bar_income1)
        pBar2 = view.findViewById(R.id.p_bar_income2)
        tvBalance.visibility = View.GONE
        tvIncomeValue.visibility = View.GONE
        tvOrdersCount.visibility = View.GONE
        pBar1.visibility = View.VISIBLE
        pBar2.visibility = View.VISIBLE

        appPreferences = AppPreferences(context)
        driver = (activity as IncomeActivity?)!!.driver!!
        service = (activity as IncomeActivity?)!!.service!!

        getStatistics()
        getBalance()

        incomeWeek = view.findViewById(R.id.income_week)
        incomeWeek.setOnClickListener {
            val incomePagerFragment: Fragment = IncomeWeekFragment()
            fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.replace(R.id.income_container, incomePagerFragment)
            fragmentTransaction.commit()
        }
        currentBalance = view.findViewById(R.id.current_balance)
        currentBalance.setOnClickListener {
            if (appPreferences.getText("allow_refill_balance_by_bank_card") == "1") {
                val intent = Intent(context, CardActivity::class.java)
                intent.putExtra("service_id", service.id)
                startActivity(intent)
            }
        }
    }

    private fun getStatistics() {

        val format: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.firstDayOfWeek = Calendar.MONDAY
        calendar[Calendar.DAY_OF_WEEK] = calendar.firstDayOfWeek
        val startDate = format.format(calendar.time)
        calendar.add(Calendar.DAY_OF_MONTH, +6)
        val endDate = format.format(calendar.time)
        val params: MutableMap<String, String> = LinkedHashMap()
        params["end_date"] = endDate
        params["start_date"] = startDate
        params["tenant_login"] = service.uri
        params["worker_login"] = driver.callSign
        WebService.getWorkerStatistic(params, driver.secretCode)
    }

    private fun getBalance() {

        val params: LinkedHashMap<String?, String?> = linkedMapOf()
        params["tenant_login"] = service.uri
        params["worker_city_id"] = appPreferences.getText("city_id")
        params["worker_login"] = driver.callSign

        runBlocking {
            withContext(Dispatchers.IO) {
                WebService.getBalance(params, driver.secretCode)
            }?.also { balance ->
                onBalanceResponse(balance)
            }
        }
    }

    @Subscribe
    fun onEvent(statisticEvent: NetworkStatisticEvent) {
        try {
            tvIncomeValue.text = String.format("%s", String.format(Locale("ru"), "%.2f", statisticEvent.earnings))
            tvOrdersCount.text = String.format(getString(R.string.income_completed), statisticEvent.countOrders.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        pBar1.visibility = View.GONE
        tvOrdersCount.visibility = View.VISIBLE
        tvIncomeValue.visibility = View.VISIBLE
    }

    private fun onBalanceResponse(balanceEvent: BalanceResponse) {
        try {
            tvBalance.text = String.format("%s %s", balanceEvent.balance, balanceEvent.currency)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        pBar2.visibility = View.GONE
        tvBalance.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
}