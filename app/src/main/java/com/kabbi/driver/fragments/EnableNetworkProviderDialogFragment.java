package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;

import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.service.MainService;

public class EnableNetworkProviderDialogFragment extends DialogFragment {
    View view;
    AppPreferences appPreferences;
    private TypedValue typedValueMain;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_button, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        typedValueMain = new TypedValue();

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.text_profile_gps))
                .setPositiveButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                getActivity().stopService(new Intent(getActivity(), MainService.class));
                                ((WorkShiftActivity) getActivity()).startPingService();

                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }
}
