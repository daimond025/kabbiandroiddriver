package com.kabbi.driver.fragments;


import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;

public class MapDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain, typedValueSubscribe;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();

        int count = 0;
        for (String map : AppParams.MAPS) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setText(AppParams.getMapLabel(getActivity().getApplicationContext(), count));
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
            radioButton.setTextColor(getResources().getColor(R.color.text_main_light));
            radioButton.setTag(count);
            count++;

            radioGroup.addView(radioButton);
        }

        ((RadioButton) radioGroup.getChildAt(Integer.valueOf(appPreferences.getText("favorite_map")))).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_map))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            appPreferences.saveText("favorite_map", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());

                            ((TextView) getActivity().findViewById(R.id.textview_profile_prefmap_map)).setText(Html.fromHtml("<big><font color='"+String.format("#%06X", 0xFFFFFF & typedValueMain.data)+"'>"
                                    + getString(R.string.text_profile_map) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                                    + AppParams.getMapLabel(getActivity().getApplicationContext(), Integer.valueOf(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString()))
                                    + "</font></small>"));
                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
