package com.kabbi.driver.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import android.text.Html
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kabbi.driver.*
import com.kabbi.driver.activity.ConfidentialPageActivity
import com.kabbi.driver.activity.DeviceListActivity
import com.kabbi.driver.activity.PrefMap
import com.kabbi.driver.activity.PrefPush
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.CarDao
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.TypefaceSpanEx
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class ProfileFragment : androidx.fragment.app.Fragment() {

    internal var driver: Driver? = null
    internal var service: Service? = null
    internal lateinit var appPreferences: AppPreferences
    private lateinit var tvReport: TextView
    private lateinit var broadcastReceiverLang: BroadcastReceiver
    private lateinit var typedValueMain: TypedValue
    private lateinit var typedValueSubscribe: TypedValue
    private lateinit var carDao: CarDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val database = AppDatabase.getInstance(context!!)
        carDao = database.carDao()

        appPreferences = AppPreferences(activity!!.applicationContext)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        tvReport = view.findViewById(R.id.textview_profile_res)

        if (arguments!!.getBoolean("autorize")) {
            driver = (activity as WorkShiftActivity).driver
            service = (activity as WorkShiftActivity).service
            view.findViewById<View>(R.id.ff_profile_gps).visibility = View.GONE
            view.findViewById<View>(R.id.divider_gps).visibility = View.GONE
        } else {
            driver = (activity as WorkActivity).driver
            service = (activity as WorkActivity).service
        }

        loadView(view)

        view.findViewById<View>(R.id.textview_profile_map).setOnClickListener { startActivity(Intent(activity!!.applicationContext, PrefMap::class.java)) }

        view.findViewById<View>(R.id.textview_profile_nav).setOnClickListener { startActivity(Intent(activity!!.applicationContext, PrefPush::class.java)) }

        view.findViewById<View>(R.id.textview_profile_lang).setOnClickListener {
            val dialogFragment = LangDialogFragment()
            val bundle = Bundle()
            bundle.putBoolean("autorize", arguments!!.getBoolean("autorize"))
            dialogFragment.arguments = bundle
            dialogFragment.show(activity!!.supportFragmentManager, "dialogFragmentLang")
        }


        view.findViewById<View>(R.id.textview_profile_theme).setOnClickListener {
            val dialogFragment = ThemeDialogFragment()
            dialogFragment.show(activity!!.supportFragmentManager, "dialogFragmentTheme")
        }

        view.findViewById<View>(R.id.textview_profile_gps).setOnClickListener {
            val dialogFragment = GeodataDialogFragment()
            dialogFragment.show(activity!!.supportFragmentManager, "dialogFragmentGeodata")
        }

        tvReport.setOnClickListener {
            val intent = Intent(activity!!.applicationContext, ReportWorkDayActivity::class.java)
            intent.putExtra("driver_id", driver!!.id)
            startActivity(intent)
        }

        view.findViewById<View>(R.id.textview_profile_balance).setOnClickListener {
            val intent = Intent(activity!!.applicationContext, IncomeActivity::class.java)
            startActivity(intent)
        }

        view.findViewById<View>(R.id.textview_profile_route).setOnClickListener {
            val intent = Intent(activity!!.applicationContext, RouteListActivity::class.java)
            startActivity(intent)
        }

        view.findViewById<View>(R.id.textview_profile_printer).setOnClickListener {
            activity!!.startActivityForResult(Intent(context, DeviceListActivity::class.java), RESULT_PRINTER)
        }

        view.findViewById<View>(R.id.textview_profile_conf).setOnClickListener {
            val intent = Intent(activity!!.applicationContext, ConfidentialPageActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        broadcastReceiverLang = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                loadView(view!!)
            }
        }
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(activity!!).registerReceiver(broadcastReceiverLang, IntentFilter(AppParams.BROADCAST_LANG))
    }

    override fun onStop() {
        super.onStop()
        androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(broadcastReceiverLang)
    }

    private fun loadView(view: View) = runBlocking {

        val stringBuilder = StringBuilder()
        stringBuilder.append(getString(R.string.text_hint_nickname) + ": " + driver!!.callSign)
        if (driver!!.promoCode != null && driver!!.promoCode!!.isNotEmpty())
            stringBuilder.append("<br>" + getString(R.string.your_promocode) + ": " + driver!!.promoCode)

        (view.findViewById<View>(R.id.textview_profile_name) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + driver!!.name + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>" + stringBuilder.toString() + "</font></small>")

        (view.findViewById<View>(R.id.textview_profile_service) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_service) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>" + service!!.serviceName + "</font></small>")

        try {
            var descr = ""
            withContext(Dispatchers.IO) {
                descr = carDao.getByCarId(appPreferences.getText("car_id")).descr
            }
            view.findViewById<TextView>(R.id.textview_profile_auto).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                    + getString(R.string.text_profile_auto) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                    + descr + "</font></small>")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        view.findViewById<TextView>(R.id.textview_profile_nav).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_push) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_lang).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.language) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_balance).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_balance) + "</font></big><br /><small><font color='" + AppParams.colorGreen(activity!!.applicationContext) + "'>"
                + " " + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), service!!.balance) + "</font></small>")

        tvReport.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_res) + "</font></big><br />")

        view.findViewById<TextView>(R.id.textview_profile_phone).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_phone) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>" + driver!!.phone + "</font></small>")

        view.findViewById<TextView>(R.id.textview_profile_email).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_email) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>" + driver!!.email + "</font></small>")

        view.findViewById<TextView>(R.id.textview_profile_map).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_map) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_gps).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_gps) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_theme).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.text_profile_theme) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_route).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.btn_route) + "</font></big>")

        view.findViewById<TextView>(R.id.textview_profile_printer).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                + getString(R.string.profile_printer)
                + (if (appPreferences.getText("device_printer").isNotEmpty()) "<br>" + appPreferences.getText("device_printer") else "") + "</font></big>")

        if (appPreferences.getText("show_driver_privacy_policy") == "1") {
            view.findViewById<TextView>(R.id.textview_profile_conf)?.visibility = View.VISIBLE
            view.findViewById<View>(R.id.divider_conf).visibility = View.VISIBLE
            view.findViewById<TextView>(R.id.textview_profile_conf).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                    + getString(R.string.confidential_title) + "</font></big>")
        } else {
            view.findViewById<TextView>(R.id.textview_profile_conf)?.visibility = View.GONE
            view.findViewById<View>(R.id.divider_conf)?.visibility = View.GONE
        }
    }

    companion object {
        const val RESULT_PRINTER = 1234
    }
}
