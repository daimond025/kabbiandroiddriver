package com.kabbi.driver.fragments


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.kabbi.driver.*
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.NetworkEndWorkEvent
import com.kabbi.driver.events.NetworkParkingsListEvent
import com.kabbi.driver.events.NetworkWorkerPositionEvent
import com.kabbi.driver.events.NetworkWorkerStateEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.helper.TypefaceSpanEx
import com.kabbi.driver.network.WebService
import com.kabbi.driver.service.MainService
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.loopj.android.http.RequestParams
import kotlinx.android.synthetic.main.fragment_dialog_edittext_mileage.*
import kotlinx.android.synthetic.main.fragment_workday.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import kotlin.math.roundToInt

class WorkDayFragment : Fragment(), AsyncHttpTask.AsyncTaskInterface {

    internal lateinit var view: View
    internal var driver: Driver? = null
    internal var service: Service? = null
    internal var workDay: WorkDay? = null

    private var handlerTaskInfo: Handler? = null
    private var runnable: Runnable? = null
    private lateinit var receiver: BroadcastReceiver
    private var work: String = ""
    internal lateinit var appPreferences: AppPreferences
    private var sendPause: Boolean = false
    private var sendEnd: Boolean = false
    private var showDialog: Boolean = false
    private lateinit var dialogFragment: androidx.fragment.app.DialogFragment
    private var ordersCost = 0.0
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var asyncHttpTask = AsyncHttpTask(this, activity)
    private var asyncHttpTaskPost = AsyncHttpTask(this, activity)
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity as WorkShiftActivity).checkIfHasActiveOrder()

        db = AppDatabase.getInstance(context!!)
        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_workday, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        work = "work"
        sendPause = false
        sendEnd = false
        showDialog = false

        driver = (activity as WorkShiftActivity).driver
        service = (activity as WorkShiftActivity).service
        workDay = (activity as WorkShiftActivity).workDay

        appPreferences = AppPreferences(activity!!.applicationContext)

        val args = Bundle()
        args.putLong("driver_id", driver!!.id)
        dialogFragment = PauseDialogFragment()
        dialogFragment.arguments = args
        dialogFragment.setTargetFragment(this, REQUEST_CODE)

        var orders = listOf<Order>()
        runBlocking {
            withContext(Dispatchers.IO) {
                orders = db.orderDao().getByWorkDay(workDay!!.id)
            }
        }

        textview_workday_countorder.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                + getString(R.string.text_workdayfragment_countorder) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + orders.size.toString() + "</font></small>")

        for (order in orders) {
            if (order.finalOrderCost.isNotBlank()) {
                ordersCost += order.finalOrderCost.toDouble()
            }
        }

        textview_workday_cost.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                + getString(R.string.text_workdayfragment_cost) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, (activity!!.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0), ((ordersCost * 100).roundToInt().toDouble() / 100).toString()) + "</font></small>")


        val currentParking: Parking? = getParking()

        try {
            (view.findViewById<View>(R.id.textview_workday_parking) as TextView).text = Html.fromHtml("<big><font color='"
                    + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                    + getString(R.string.text_workdayfragment_parking) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                    + (currentParking?.parkingName ?: getString(R.string.text_workdayfragment_parking_unknow)) + "</font></small>")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        textview_workday_parking.setOnClickListener {
            val intent = Intent(activity!!.applicationContext, ParkingActivity::class.java)
            intent.putExtra("service_id", service!!.id)
            startActivity(intent)
        }


        button_workday_pause.setOnClickListener {
            if (InternetConnection.isOnline(activity!!)) {
                if (work == "work") {
                    if (!dialogFragment.isAdded)
                        dialogFragment.show(activity!!.supportFragmentManager, "dialogFragment")

                } else if (work == "pause") {
                    if (!sendPause) {
                        setPause("")
                        sendPause = true
                    }
                }
            } else {
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }

        button_workday_end.setOnClickListener { _ ->
            if (InternetConnection.isOnline(activity!!)) {
                if (appPreferences.getText("control_own_car_mileage") == "1" && appPreferences.getText("car_company") == "true") {
                    val viewDialog = activity!!.layoutInflater.inflate(R.layout.fragment_dialog_edittext_mileage, null)
                    val alertDialog = AlertDialog.Builder(activity!!)
                    alertDialog.setTitle(getString(R.string.dialog_endworkday))
                    alertDialog.setView(viewDialog)
                    alertDialog.setNegativeButton(
                            getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                    alertDialog.setPositiveButton(
                            getString(R.string.activities_MainActivity_permission_dialog_ok)) { _, _ ->
                    }
                    val dialog = alertDialog.create()
                    dialog.show()
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { v1 ->
                        val mileage = (viewDialog.findViewById<View>(R.id.et_mileage) as EditText).text.toString()
                        if (mileage.trim { it <= ' ' }.isNotEmpty()) {
                            val requestParams = LinkedHashMap<String, String>()
                            requestParams["car_mileage"] = mileage
                            requestParams["tenant_login"] = service!!.uri
                            requestParams["worker_login"] = driver!!.callSign
                            WebService.endWork(requestParams, driver!!.secretCode)
                            dialog.dismiss()
                        } else {
                            et_mileage.error = getString(R.string.edittext_valid_required)
                        }
                    }
                } else {
                    val alertDialog = AlertDialog.Builder(activity!!)
                    alertDialog.setTitle(getString(R.string.dialog_endworkday))

                    alertDialog.setPositiveButton(getString(R.string.button_confirm)) { _, _ ->
                        val requestParams = LinkedHashMap<String, String>()
                        requestParams["car_mileage"] = "0"
                        requestParams["tenant_login"] = service!!.uri
                        requestParams["worker_login"] = driver!!.callSign

                        WebService.endWork(requestParams, driver!!.secretCode)
                    }
                    alertDialog.setNegativeButton(getString(R.string.button_cancel)) { dialog, _ -> dialog.cancel() }

                    alertDialog.create().show()
                }

            } else {
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }

        view.findViewById<View>(R.id.textview_workday_countorder).setOnClickListener { showReport() }
        view.findViewById<View>(R.id.textview_workday_cost).setOnClickListener { showReport() }

        view.findViewById<View>(R.id.textview_workday_balance).setOnClickListener {
            if (appPreferences.getText("allow_refill_balance_by_bank_card") == "1") {
                val intent = Intent(activity!!.applicationContext, IncomeActivity::class.java)
                startActivity(intent)
            }
        }

        if (appPreferences.getText("is_view_queue") == "0") {
            view.findViewById<View>(R.id.textview_workday_queue).visibility = View.INVISIBLE
        } else {
            view.findViewById<View>(R.id.textview_workday_queue).visibility = View.VISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("WORK_SHIFT_ACTIVITY", "WorkDayFragment onDestroyView")
    }

    override fun onResume() {
        super.onResume()

        (activity as WorkShiftActivity).updatePark = false

        if (appPreferences.getText("parking").isNotEmpty()) {
            val parking: Parking? = getParking()

            try {
                (view.findViewById<View>(R.id.textview_workday_parking) as TextView).text = Html.fromHtml("<big><font color='"
                        + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                        + getString(R.string.text_workdayfragment_parking) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                        + parking?.parkingName + "</font></small>")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            setParkingQueue(parking)
        }
    }

    private fun getParking(): Parking? =
            runBlocking {
                withContext(Dispatchers.IO) {
                    AppDatabase.getInstance(context!!).parkingDao().get(appPreferences.getText("parking"))
                }
            }


    private fun setParkingQueue(parking: Parking?) {
        try {
            var requestParams = RequestParams()
            requestParams.add("worker_login", driver!!.callSign)
            requestParams.add("parking_id", parking?.parkingID?.toString() ?: "")
            requestParams.add("tenant_login", service!!.uri)

            asyncHttpTaskPost.post("set_parking_queue", requestParams, driver!!.secretCode)

            requestParams = RequestParams()
            requestParams.add("worker_city_id", driver!!.cityID)
            requestParams.add("worker_login", driver!!.callSign)
            requestParams.add("parking_id", parking?.parkingID?.toString() ?: "")
            requestParams.add("tenant_login", service!!.uri)

            asyncHttpTask.get("get_parkings_orders_queues", requestParams, driver!!.secretCode)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showReport() {
        val intentReport = Intent(activity, ReportOrderListActivity::class.java)
        intentReport.putExtra("workday_id", workDay!!.id)
        startActivity(intentReport)
    }

    private fun setPause(comment: String) {
        if (InternetConnection.isOnline(activity!!)) {

            val requestParams = LinkedHashMap<String, String>()

            if (work == "work") {
                requestParams["action"] = "pause"
            } else if (work == "pause") {
                requestParams["action"] = "work"
            }

            try {
                requestParams["comment"] = URLEncoder.encode(comment, "UTF-8").replace("+", "%20")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

            requestParams["tenant_login"] = service!!.uri
            requestParams["worker_login"] = driver!!.callSign

            WebService.setWorkerState(requestParams, driver!!.secretCode)

        } else {
            CustomToast.showMessage(activity, getString(R.string.text_internet_access))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            setPause(data!!.getStringExtra("title_pause"))
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)

        (view.findViewById<View>(R.id.textview_workday_balance) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                + getString(R.string.text_profile_balance) + "</font></big><br /><small><font color='" + AppParams.colorGreen(activity!!.applicationContext) + "'>"
                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), service!!.balance) + "</font></small>")

        handlerTaskInfo = Handler()
        runnable = Runnable {

            val time = Calendar.getInstance().time.time - workDay!!.startWorkDay

            val hoursLong = time / 3600000

            var hoursStr = if (hoursLong <= 24) {
                hoursLong.toString()
            } else {
                (hoursLong % 24).toString()
            }

            hoursStr = if (hoursStr.length > 1) hoursStr else "0$hoursStr"

            var minutesStr = (time % 3600000 / 60000).toString()
            minutesStr = if (minutesStr.length > 1) minutesStr else "0$minutesStr"

            var secondsStr = (time % 3600000 % 60000 / 1000).toString()
            secondsStr = if (secondsStr.length > 1) secondsStr else "0$secondsStr"

            (view.findViewById<View>(R.id.textview_workday_time) as TextView).text = "$hoursStr:$minutesStr:$secondsStr"

            handlerTaskInfo!!.postDelayed(runnable, 1000)
        }
        handlerTaskInfo!!.post(runnable)

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    (view.findViewById<View>(R.id.textview_workday_balance) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                            + getString(R.string.text_profile_balance) + "</font></big><br /><small><font color='" + AppParams.colorGreen(activity!!.applicationContext) + "'>"
                            + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, (activity!!.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0), intent.getStringExtra("PUSH")) + "</font></small>")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        val intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH)
        activity!!.registerReceiver(receiver, intentFilterPush)

    }

    override fun onStop() {
        super.onStop()

        EventBus.getDefault().unregister(this)
        activity!!.unregisterReceiver(receiver)

        handlerTaskInfo!!.removeCallbacks(runnable)

    }

    @Subscribe
    fun onEvent(workerPositionEvent: NetworkWorkerPositionEvent) {
        try {
            when (workerPositionEvent.workerStatus) {
                "FREE" -> (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_free)
                "ON_ORDER" -> (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_on_order)
                "ON_BREAK" -> {
                    (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_on_break)
                    if (work != "pause") {
                        work = "pause"
                        try {
                            (view.findViewById<View>(R.id.button_workday_pause) as Button).text = getString(R.string.text_button_pauseworknext)
                        } catch (e: IllegalStateException) {
                            e.printStackTrace()
                        }

                    }
                }
                "OFFER_ORDER" -> (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_offer_order)
                "BLOCKED" -> (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_blocked)
                "SHIFT_IS_CLOSED" -> (view.findViewById<View>(R.id.textview_workday_status) as TextView).text = getString(R.string.status_shift_closed)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Subscribe
    fun onEvent(endWorkEvent: NetworkEndWorkEvent) {
        when (endWorkEvent.status) {
            "OK" -> {
                workDay!!.endWorkDay = Calendar.getInstance().time.time
                runBlocking {
                    withContext(Dispatchers.IO) {
                        workDay!!.id = db.workDayDao().save(workDay!!)
                    }
                }

                try {
                    activity!!.stopService(Intent(activity!!.applicationContext, MainService::class.java))
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

                try {
                    appPreferences.saveText("in_shift", "0")
                    val intent = Intent(activity!!.applicationContext, WorkActivity::class.java)
                    intent.putExtra("service_id", service!!.id)
                    startActivity(intent)
                    activity!!.finish()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

            }
            "INVALID_CAR_MILEAGE" -> CustomToast.showMessage(context, getString(R.string.error_mileage))
            else -> CustomToast.showMessage(context, endWorkEvent.status)
        }
    }

    @Subscribe
    fun onEvent(parkingsListEvent: NetworkParkingsListEvent) {
        try {
            if (parkingsListEvent.parking != null) {
                (view.findViewById<View>(R.id.textview_workday_parking) as TextView).text = Html.fromHtml("<big><font color='"
                        + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                        + getString(R.string.text_workdayfragment_parking) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                        + parkingsListEvent.parking.parkingName + "</font></small>")

                Log.d("SHOW_QUEUE", "NetworkParkingsListEvent")

                setParkingQueue(parkingsListEvent.parking)
            } else {
                (view.findViewById<View>(R.id.textview_workday_parking) as TextView).text = Html.fromHtml("<big><font color='"
                        + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                        + getString(R.string.text_workdayfragment_parking) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                        + getString(R.string.text_workdayfragment_parking_unknow) + "</font></small>")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Subscribe
    fun onEvent(workerStateEvent: NetworkWorkerStateEvent) {
        try {
            if (workerStateEvent.status == "OK" && workerStateEvent.result == 1) {
                if (work == "work") {
                    work = "pause"
                    try {
                        (view.findViewById<View>(R.id.button_workday_pause) as Button).text = getString(R.string.text_button_pauseworknext)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }

                } else if (work == "pause") {
                    work = "work"
                    try {
                        (view.findViewById<View>(R.id.button_workday_pause) as Button).text = getString(R.string.text_button_pausework)
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }

                }
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } finally {
            sendPause = false
        }
    }

    override fun doPostExecute(jsonObjectPolygons: JSONObject?, typeJson: String) {
        if (jsonObjectPolygons != null && typeJson == "get_parkings_orders_queues") {
            try {
                if (jsonObjectPolygons.getString("info") == "OK") {
                    val jsonArray = jsonObjectPolygons.getJSONArray("result").getJSONObject(0).getJSONArray("parkings_queue")
                    for (j in 0 until jsonArray.length()) {
                        if (jsonArray.getString(j) == driver!!.callSign && appPreferences.getText("is_view_queue") == "1") {
                            view.findViewById<View>(R.id.textview_workday_queue).visibility = View.VISIBLE
                            (view.findViewById<View>(R.id.textview_workday_queue) as TextView).text = Html.fromHtml("<big><font color='"
                                    + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                    + getString(R.string.text_workdayfragment_queue) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + (j + 1).toString() + "</font></small>")
                            break
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    companion object {
        const val REQUEST_CODE = 1
    }
}
