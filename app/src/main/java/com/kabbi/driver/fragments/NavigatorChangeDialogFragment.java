package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;

public class NavigatorChangeDialogFragment extends DialogFragment {

    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;
    private TypedValue typedValueMain;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);
        typedValueMain = new TypedValue();

        int count = 0;
        for (String navi : AppParams.NAVI) {
            RadioButton radioButton = new RadioButton(getActivity().getApplicationContext());
            radioButton.setText(AppParams.getNavi(getActivity().getApplicationContext(), count));
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
            radioButton.setTextColor(getResources().getColor(R.color.text_main_light));
            radioButton.setTag(count);
            radioGroup.addView(radioButton);
            count++;
        }

        switch (appPreferences.getText("far_navi")) {
            case "0":
                ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
                break;
            case "1":
                ((RadioButton) radioGroup.getChildAt(1)).setChecked(true);
                break;
            case "2":
                ((RadioButton) radioGroup.getChildAt(2)).setChecked(true);
                break;
            case "3":
                ((RadioButton) radioGroup.getChildAt(3)).setChecked(true);
                break;
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.dialog_navi))
                .setPositiveButton(getString(R.string.button_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                switch (radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString()) {
                                    case "1":
                                        appPreferences.saveText("far_navi", "1");
                                        break;
                                    case "2":
                                        appPreferences.saveText("far_navi", "2");
                                        break;
                                    case "3":
                                        appPreferences.saveText("far_navi", "3");
                                        break;
                                    default:
                                        appPreferences.saveText("far_navi", "0");
                                }
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view).create();
    }
}