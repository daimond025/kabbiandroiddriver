package com.kabbi.driver.fragments;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kabbi.driver.R;
import com.kabbi.driver.WorkShiftActivity;
import com.kabbi.driver.adapters.MessageAdapter;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.events.ChatEmitGetHistoryMessages;
import com.kabbi.driver.events.ChatEmitGetLastMessages;
import com.kabbi.driver.events.ChatEmitMessageIsRead;
import com.kabbi.driver.events.ChatEmitNewMessage;
import com.kabbi.driver.events.ChatOnLastMessagesEvent;
import com.kabbi.driver.events.ChatOnNewMessageEvent;
import com.kabbi.driver.events.ChatOnUnreadedMessageEvent;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.helper.InternetConnection;
import com.kabbi.driver.models.Message;
import com.kabbi.driver.util.CustomToast;
import com.kabbi.driver.util.WebSocket;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

public class ChatDriverFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private Driver driver;

    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private LinkedList<Message> mMessages = new LinkedList<Message>();
    private RecyclerView.Adapter mAdapter;
    private String mUsername;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Activity activity;
    private long timestamp;

    private int count = 0;
    private String your;
    private boolean scrollBottom = false;

    public ChatDriverFragment() {
        super();
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
        mAdapter = new MessageAdapter(activity, mMessages);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        driver = ((WorkShiftActivity) getActivity()).getDriver();

        mUsername = driver.getName();

        timestamp = (new Date()).getTime();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        your = getString(R.string.you);
        AppPreferences appPreferences = new AppPreferences(getActivity());
        if (appPreferences.getText("show_chat_with_dispatcher_on_shift").equals("0"))
            view.findViewById(R.id.ll_driverchat).setVisibility(View.GONE);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_chat);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(
                Color.parseColor(AppParams.colorOrange(getActivity().getApplicationContext())));

        final LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        mMessagesView = (RecyclerView) view.findViewById(R.id.messages);
        mMessagesView.setLayoutManager(layoutParams);
        mMessagesView.setAdapter(mAdapter);

        mInputMessageView = (EditText) view.findViewById(R.id.message_input);
        mInputMessageView.setOnEditorActionListener((v, id, event) -> {
            if (id == R.id.send || id == EditorInfo.IME_NULL) {
                attemptSend();
                return true;
            }
            return false;
        });

        ImageButton sendButton = (ImageButton) view.findViewById(R.id.send_button);
        sendButton.setOnClickListener(v -> attemptSend());


        mMessagesView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                swipeRefreshLayout.setEnabled(layoutParams.findFirstCompletelyVisibleItemPosition() == 0);
            }

        });

        EventBus.getDefault().register(this);
        EventBus.getDefault().post(new ChatEmitGetLastMessages(WebSocket.TYPECHAT_DRIVER, driver));
        EventBus.getDefault().post(new ChatEmitMessageIsRead(driver));
    }

    private void addMessage(final String username, final String message, int type) {
        mMessages.add(new Message.Builder(type)
                .username(username).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void attemptSend() {
        if (null == mUsername) return;

        String message = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            mInputMessageView.requestFocus();
            return;
        }

        mInputMessageView.setText("");
        addMessage((new SimpleDateFormat("dd.MM HH:mm")).format(new Date()) + " " + getString(R.string.you), message, Message.TYPE_MESSAGE_YOU);

        EventBus.getDefault().post(new ChatEmitNewMessage(WebSocket.TYPECHAT_DRIVER, message, driver));
        EventBus.getDefault().post(new ChatEmitMessageIsRead(driver));
    }

    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    @Override
    public void onRefresh() {
        if (InternetConnection.isOnline(getActivity())) {
            swipeRefreshLayout.setRefreshing(true);
            if (mMessages.size() == 0)
                EventBus.getDefault().post(new ChatEmitGetLastMessages(WebSocket.TYPECHAT_DRIVER, driver));
            else
                EventBus.getDefault().post(new ChatEmitGetHistoryMessages(WebSocket.TYPECHAT_DRIVER, timestamp, driver));
        } else {
            swipeRefreshLayout.setRefreshing(false);
            CustomToast.showMessage(getActivity(), getString(R.string.text_internet_access));
        }
    }

    @Subscribe
    public void onEvent(final ChatOnNewMessageEvent chatOnNewMessageEvent) {
        activity.runOnUiThread(() -> {
            if (chatOnNewMessageEvent.getTypeChat().equals(WebSocket.TYPECHAT_DRIVER)) {
                String username = chatOnNewMessageEvent.getTime() + " " + chatOnNewMessageEvent.getUserName() + " (" + chatOnNewMessageEvent.getRole() + ")";
                addMessage(username, chatOnNewMessageEvent.getMessage(), Message.TYPE_MESSAGE);
                EventBus.getDefault().post(new ChatEmitMessageIsRead(driver));
            }
        });
    }

    @Subscribe
    public void onEvent(final ChatOnUnreadedMessageEvent chatOnUnreadedMessageEvent) {

        activity.runOnUiThread(() -> {
            if (chatOnUnreadedMessageEvent.getMessages().size() > 0) {
                timestamp = Long.valueOf(chatOnUnreadedMessageEvent.getMessages().get(0).get("timestamp"));
                for (HashMap<String, String> message : chatOnUnreadedMessageEvent.getMessages()) {
                    addMessage(message.get("user"), message.get("message"), Message.TYPE_MESSAGE);
                }
                EventBus.getDefault().post(new ChatEmitMessageIsRead(driver));
            }
        });
    }

    @Subscribe
    public void onEvent(final ChatOnLastMessagesEvent chatOnLastMessagesEvent) {
        count++;
        activity.runOnUiThread(() -> {
            if (chatOnLastMessagesEvent.getMessages().size() > 0 && chatOnLastMessagesEvent.getTypeChat().equals(WebSocket.TYPECHAT_DRIVER)) {

                timestamp = Long.valueOf(chatOnLastMessagesEvent.getMessages().get(0).get("timestamp"));

                Collections.reverse(chatOnLastMessagesEvent.getMessages());
                for (HashMap<String, String> message : chatOnLastMessagesEvent.getMessages()) {
                    if (message.get("receiver_type").equals(WebSocket.TYPECHAT_DRIVER)) {
                        try {
                            if (message.get("sender_id").equals(driver.getCallSign())) {
                                mMessages.addFirst(new Message.Builder(Message.TYPE_MESSAGE_YOU)
                                        .username(message.get("sender_time") + " " + your)
                                        .message(message.get("message"))
                                        .build());
                            } else {
                                mMessages.addFirst(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username(message.get("user"))
                                        .message(message.get("message"))
                                        .build());
                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }

                    }
                }

                mAdapter.notifyDataSetChanged();
                if (!scrollBottom) {
                    scrollToBottom();
                    scrollBottom = true;
                }
            }
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        });
    }
}
