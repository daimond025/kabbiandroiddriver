package com.kabbi.driver.fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;

public class GeodataDialogFragment extends DialogFragment {
    View view;
    AppPreferences appPreferences;
    RadioGroup radioGroup;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        view = getActivity().getLayoutInflater().inflate(R.layout.fragment_theme, null);
        appPreferences = new AppPreferences(getActivity().getApplicationContext());
        radioGroup = view.findViewById(R.id.radiogroup_theme);

        RadioButton radioButtonGps = new RadioButton(getActivity());
        radioButtonGps.setText("GPS");
        radioButtonGps.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButtonGps.setTag("0");
        radioGroup.addView(radioButtonGps);

        RadioButton radioButtonNw = new RadioButton(getActivity());
        radioButtonNw.setText("NETWORK");
        radioButtonNw.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textsize_domain_link));
        radioButtonNw.setTag("1");
//        radioGroup.addView(radioButtonNw);

        ((RadioButton) radioGroup.getChildAt(Integer.valueOf(appPreferences.getText("gps")))).setChecked(true);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.work_geodata))
                .setPositiveButton(getString(R.string.button_ok),
                        (dialog, whichButton) -> {
                            appPreferences.saveText("gps", radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()).getTag().toString());
                            dialog.dismiss();
                        }
                )
                .setNegativeButton(getString(R.string.button_cancel),
                        (dialog, whichButton) -> dialog.dismiss()
                )
                .setView(view).create();
    }
}
