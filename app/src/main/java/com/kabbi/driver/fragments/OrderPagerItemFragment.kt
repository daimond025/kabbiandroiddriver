package com.kabbi.driver.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.core.view.ViewCompat
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.adapters.OrderAdapter
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.NetworkOrdersListEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.models.OpenOrder
import com.kabbi.driver.network.WebService
import com.kabbi.driver.offer.OrderOfferActivity
import com.kabbi.driver.util.CustomToast
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class OrderPagerItemFragment : androidx.fragment.app.ListFragment() {

    internal var data = arrayOf("one", "two", "three")
    internal var adapter: OrderAdapter? = null
    internal var orders: MutableList<OpenOrder> = mutableListOf()
    internal var driver: Driver? = null
    internal var service: Service? = null
    internal var workDay: WorkDay? = null
    private lateinit var receiverOrder: BroadcastReceiver
    internal var update: Boolean = false
    internal lateinit var appPreferences: AppPreferences
    private lateinit var handlerTaskInfo: Handler
    private lateinit var handlerTaskInfoUpdate: Handler
    private lateinit var runnableInfo: Runnable
    private lateinit var runnableUpdate: Runnable
    internal lateinit var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var typedValueBg: TypedValue? = null

    private fun canListViewScrollUp(listView: ListView): Boolean {
        return if (Build.VERSION.SDK_INT >= 16) {
            ViewCompat.canScrollVertically(listView, -1)
        } else {
            listView.childCount > 0 && (listView.firstVisiblePosition > 0 || listView.getChildAt(0).top < listView.paddingTop)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        driver = (activity as WorkShiftActivity).driver
        service = (activity as WorkShiftActivity).service
        workDay = (activity as WorkShiftActivity).workDay

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
        typedValueBg = TypedValue()
        appPreferences = AppPreferences(activity!!.applicationContext)
        update = false
        orders = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val listFragmentView = super.onCreateView(inflater, container, savedInstanceState)
        swipeRefreshLayout = ListFragmentSwipeRefreshLayout(activity!!.applicationContext)
        swipeRefreshLayout.addView(listFragmentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        swipeRefreshLayout.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT)
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor(AppParams.colorOrange(activity!!.applicationContext)))
        swipeRefreshLayout.setOnRefreshListener {
            if (InternetConnection.isOnline(activity!!)) {
                swipeRefreshLayout.isRefreshing = true
                updateOrders()
            } else {
                swipeRefreshLayout.isRefreshing = false
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }
        return swipeRefreshLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.listView.setBackgroundColor(typedValueBg!!.data)
    }

    override fun onResume() {
        super.onResume()
        updateOrders()
    }

    private fun updateOrders() {
        update = true
        val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
        requestParams["tenant_login"] = service!!.uri
        requestParams["worker_lat"] = appPreferences.getText("lat")
        requestParams["worker_login"] = driver!!.callSign
        requestParams["worker_lon"] = appPreferences.getText("lon")
        if (arguments!!.getString("type") == "free") {
            try {
                WebService.getOrdersList(context!!.applicationContext, requestParams, driver!!.secretCode, arguments!!.getString("type") ?: "")
//                (activity as WorkShiftActivity).spiceManager.execute(GetOrdersListRequest(activity, requestParams, driver!!.secretCode, arguments!!.getString("type")), OrdersOpenListListener(arguments!!.getString("type")!!, activity!!))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else if (arguments!!.getString("type") != "reserve") {
            val adapter = ArrayAdapter(activity!!, android.R.layout.simple_list_item_1, data)
            listAdapter = adapter
            update = false
            if (swipeRefreshLayout.isRefreshing)
                swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        receiverOrder = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (!update)
                    updateOrders()
            }
        }
        var intentFilterPush: IntentFilter? = null
        if (arguments!!.getString("type") == "free") {
            intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH_FREE)
        } else if (arguments!!.getString("type") == "reserve") {
            intentFilterPush = IntentFilter(AppParams.BROADCAST_PUSH_PRE)
        }
        activity!!.registerReceiver(receiverOrder, intentFilterPush)
        handlerTaskInfo = Handler()
        runnableInfo = Runnable {
            try {
                if (adapter != null)
                    adapter!!.notifyDataSetChanged()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            handlerTaskInfo.postDelayed(runnableInfo, 1000)
        }
        handlerTaskInfo.post(runnableInfo)
        handlerTaskInfoUpdate = Handler()
        runnableUpdate = Runnable {
            if (!update)
                updateOrders()
            handlerTaskInfoUpdate.postDelayed(runnableUpdate, 10000)
        }
        handlerTaskInfoUpdate.post(runnableUpdate)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        activity!!.unregisterReceiver(receiverOrder)
        try {
            handlerTaskInfo.removeCallbacks(runnableInfo)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            handlerTaskInfoUpdate.removeCallbacks(runnableUpdate)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        if ((activity as WorkShiftActivity).stageOrder > 0
                && appPreferences.getText("allow_reserve_urgent_orders") == "0"
                && v!!.getTag(R.id.tag_order_type).toString() == "free") {
            CustomToast.showMessage(activity, getString(R.string.status_error_limit))
        } else {
            val orderIntent = Intent(activity!!.applicationContext, OrderOfferActivity::class.java)
            orderIntent.putExtra("order_id", v!!.getTag(R.id.tag_order_id).toString())
            orderIntent.putExtra("service_id", service!!.id)
            orderIntent.putExtra("workday_id", workDay!!.id)
            orderIntent.putExtra("free_order", 2)
            if ((activity as WorkShiftActivity).stageOrder > 0
                    && appPreferences.getText("allow_reserve_urgent_orders") == "1"
                    && v.getTag(R.id.tag_order_type).toString() == "free") {
                orderIntent.putExtra("type_order", "free_br")
            } else {
                orderIntent.putExtra("type_order", v.getTag(R.id.tag_order_type).toString())
            }
            orderIntent.putExtra("source", "activity")
            orderIntent.putExtra("time", Date().time)
            orderIntent.putExtra("server_time", (activity as WorkShiftActivity).serverTime)
            orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

            orderIntent.putExtra("delete", false)
            orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_CANCEL)
            orderIntent.putExtra("music", false)
            if ((activity as WorkShiftActivity).stageOrder > 0
                    && appPreferences.getText("allow_reserve_urgent_orders") == "1"
                    && v.getTag(R.id.tag_order_type).toString() == "free") {
                orderIntent.putExtra("code_next", AppParams.ORDER_STATUS_RESERVE_XZ)
            } else {
                orderIntent.putExtra("code_next", AppParams.ORDER_STATUS_RESERVE_CONFIRM)
            }
            activity!!.startActivityForResult(orderIntent, WorkShiftActivity.INTENT_ORDER_OFFER)
        }
    }

    @Subscribe
    fun onEvent(ordersListEvent: NetworkOrdersListEvent) {

        //TODO fix this piece of shit
        if (ordersListEvent.status == "OK") {
            if (ordersListEvent.type == "free") {
                try {
                    orders.clear()
                    orders = ordersListEvent.orders
                    orders.sortByDescending { it.distance }

                    adapter = OrderAdapter(activity!!, orders, typedValueSubscribe!!.data)
                    listAdapter = adapter
                    if (swipeRefreshLayout.isRefreshing)
                        swipeRefreshLayout.isRefreshing = false
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                update = false
            }
        }
        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false
    }

    private inner class ListFragmentSwipeRefreshLayout(context: Context) : androidx.swiperefreshlayout.widget.SwipeRefreshLayout(context) {

        override fun canChildScrollUp(): Boolean {
            val listView = listView
            return if (listView.visibility == View.VISIBLE) {
                canListViewScrollUp(listView)
            } else {
                false
            }
        }
    }
}
