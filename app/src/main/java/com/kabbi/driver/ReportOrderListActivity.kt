package com.kabbi.driver

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import com.kabbi.driver.adapters.ReportOrderAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.CloseActivityEvent
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


class ReportOrderListActivity : BaseActivity(), AdapterView.OnItemClickListener {

    private lateinit var lvOrders: ListView
    internal lateinit var adapter: ReportOrderAdapter
    internal var workDay: WorkDay? = null
    internal var orders: List<Order> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_report_orderlist)


        EventBus.getDefault().register(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_orderlist)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        lvOrders = findViewById(R.id.listview_report_orderlist)
        runBlocking {
            withContext(Dispatchers.IO){
                val db=  AppDatabase.getInstance(this@ReportOrderListActivity)
                workDay = db.workDayDao().getById(intent.extras!!.getLong("workday_id"))
                orders = db.orderDao().getByWorkDay(workDay!!.id)
            }
        }
        if (orders.isNotEmpty()) {
            adapter = ReportOrderAdapter(applicationContext, orders)
            lvOrders.onItemClickListener = this
            lvOrders.adapter = adapter
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe
    fun onEvent(closeActivityEvent: CloseActivityEvent) {
        finish()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val intent = Intent(applicationContext, ReportOrderDetailActivity::class.java)
        intent.putExtra("order_id", java.lang.Long.valueOf(view.tag.toString()))
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
