package com.kabbi.driver.finish

import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.events.orderStatus.OrderStatusPresenter
import com.kabbi.driver.events.taximeter.CostChange
import org.greenrobot.eventbus.EventBus

class FinishPresenter(orderId: String, driver: Driver) : OrderStatusPresenter() {

    init {
        this.orderId = orderId
        this.driver = driver
    }

    fun updateCost(cost: String, id: String, worker: String) {
        EventBus.getDefault().post(CostChange(getNewUuid(), cost, id, worker))
    }
}