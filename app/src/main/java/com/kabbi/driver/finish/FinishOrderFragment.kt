package com.kabbi.driver.finish


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.kabbi.driver.ConfirmationCodeActivity
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.events.taximeter.StopTaximeterEvent
import com.kabbi.driver.events.taximeter.TaximeterStageEvent
import com.kabbi.driver.fragments.ChangeDialogCompletedFragment
import com.kabbi.driver.helper.*
import com.kabbi.driver.helper.AppParams.*
import com.kabbi.driver.network.WebService
import com.kabbi.driver.service.SendRouteService
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.Utils
import com.kabbi.driver.util.Utils.showSnackBar
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.sufficientlysecure.htmltextview.HtmlTextView
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.floor
import kotlin.math.roundToInt

class FinishOrderFragment : Fragment(), AsyncHttpTask.AsyncTaskInterface {

    internal lateinit var view: View
    private lateinit var jsonTariffCity: JSONObject
    private lateinit var jsonTariffTrack: JSONObject
    private lateinit var tariffCity: ClientTariff
    private lateinit var tariffTrack: ClientTariff
    internal lateinit var btnPaid: Button
    internal lateinit var btnNPaid: Button
    internal lateinit var btnNext: Button
    internal lateinit var btnPaidNoCash: Button
    internal lateinit var btnChangeFare: Button
    internal lateinit var requestParams: MutableMap<String, String>
    internal lateinit var driver: Driver
    internal var service: Service? = null
    internal var workDay: WorkDay? = null
    internal lateinit var order: Order
    private lateinit var currentOrder: CurrentOrder
    private lateinit var jsonOrderRoute: JSONObject
    private var statusOrder = ""
    private var bonus = ""
    private var promo = ""
    private var timeWaitClient: Long = 0
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var appPreferences: AppPreferences? = null
    private var countDownTimer: CountDownTimer? = null
    private var requestType: RequestType = RequestType.RESERVE
    private var showDialogPaid: Int = 0
    private var lifecycleState: String? = null
    private var timeoutTimer: Long = 0
    private var llContainerFinish: LinearLayout? = null
    private var reportList: MutableList<ItemReport>? = null
    private var finalCost = 0.0
    private val arrAddresses = arrayOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private var uuid: String? = null
    private var parkingStart: Parking? = null
    private var parkingEnd: Parking? = null
    private lateinit var presenter: FinishPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_finish_order, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
        val workShiftActivity = (activity as WorkShiftActivity)

        EventBus.getDefault().register(this)
        lifecycleState = "onCreateView"

        reportList = ArrayList()
        llContainerFinish = view.findViewById(R.id.container_finish)
        btnPaid = view.findViewById(R.id.button_finishorder_1)
        btnNPaid = view.findViewById(R.id.button_finishorder_2)
        btnNext = view.findViewById(R.id.button_finishorder_next)
        btnPaidNoCash = view.findViewById(R.id.button_finishorder_nocash)
        btnChangeFare = view.findViewById(R.id.btn_updatecost)

        if (!workShiftActivity.getIsPayBtnActive()) {
            btnNext.isEnabled = false
            btnPaid.isEnabled = false
            btnNPaid.isEnabled = false
            btnPaidNoCash.isEnabled = false
            btnChangeFare.isEnabled = false
        }

        driver = workShiftActivity.driver!!
        service = workShiftActivity.service
        order = workShiftActivity.order!!
        currentOrder = workShiftActivity.currentOrder!!
        workDay = workShiftActivity.workDay
        jsonOrderRoute = JSONObject()

        presenter = FinishPresenter(order.orderId, driver)

        appPreferences = AppPreferences(activity!!)
        doStuff()
    }

    private fun doStuff() {
        runBlocking {
            val clientTariffDao = AppDatabase.getInstance(context!!).clientTariffDao()
            withContext(Dispatchers.IO) {
                tariffCity = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_CITY)
                tariffTrack = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_TRACK)
            }

            try {
                val lastPoint = lastPosition()
                if (lastPoint != null) {
                    when (lastPoint) {
                        "base" -> order.minPrice = tariffCity.minPrice
                        "track" -> order.minPrice = tariffTrack.minPrice
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (appPreferences!!.getText("allow_edit_cost_order") == "0") {
                btnChangeFare.visibility = View.GONE
            }

            if (order.typeCost == "CASH") {
                timeoutTimer = 15000
                view.findViewById<View>(R.id.linearlayout_cash).visibility = View.VISIBLE
            } else {
                timeoutTimer = 50000
                btnPaidNoCash.visibility = View.VISIBLE
            }

            try {
                jsonTariffCity = JSONObject(order.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataCity")
                jsonTariffTrack = JSONObject(order.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataTrack")
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            view.findViewById<TextView>(R.id.textview_reportorder_cost_currency).text = " " + TypefaceSpanEx.getOnlyCurrencyStr(activity!!.applicationContext)

            finalCost = Round.roundFormatDouble(order.costOrder, order.rounding, order.roundingType)

            if (order.enableParkingRatio == 1) {
                val lastRoute = withContext(Dispatchers.IO) {
                    AppDatabase.getInstance(context!!).routeDao().getLastRoute()
                }

                try {
                    val jsonObjectAddress = JSONObject(order.address)
                    parkingStart = Utils.getParkingDopCost(java.lang.Double.valueOf(jsonObjectAddress.getJSONObject("A").getString("lat")),
                            jsonObjectAddress.getJSONObject("A").getString("lon").toDouble(), service!!, context!!)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                parkingEnd = Utils.getParkingDopCost(lastRoute.lat, lastRoute.lon, service!!, context!!)

                parkingEnd?.also { parkEnd ->
                    finalCost = Utils.addParkingCost(parkEnd, finalCost, 1)!!
                }
            }

            if (order.fix == 1) {
                fixPrice(order)
            } else {
                val htmlView = view.findViewById<HtmlTextView>(R.id.textview_reportorder_cost)
                val costView = view.findViewById<TextView>(R.id.textview_reportorder_cost_written_off)

                if (finalCost > order.minPrice) {
                    when {
                        tariffCity.bonusPayment -> {
                            bonus = Round.roundFormat10(finalCost - BonusSystem.getBonusCost(finalCost, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))

                            htmlView.setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>" + Round.roundFormatFinal(finalCost, order.rounding, order.roundingType) + "</strike></font></small></small>"
                                    + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getBonusCost(finalCost, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod)) + "</font>", HtmlTextView.LocalImageGetter())
                            costView.text = String.format(getString(R.string.written_off), bonus)

                            order.finalOrderCost = finalCost.toString()
                        }

                        order.promoCodeDiscount > 0 -> {
                            promo = (finalCost - BonusSystem.getPromoCost(finalCost, order.promoCodeDiscount.toDouble())).toString()
                            htmlView.setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>" + Round.roundFormatFinal(checkParking(order.costOrder), order.rounding, order.roundingType) + "</strike></font></small></small>"
                                    + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getPromoCost(finalCost, order.promoCodeDiscount.toDouble())) + "</font>", HtmlTextView.LocalImageGetter())
                            costView.text = String.format(getString(R.string.promo_mask), order.promoCodeDiscount.toString()) + "%"
                            order.finalOrderCost = BonusSystem.getPromoCost(finalCost, order.promoCodeDiscount.toDouble()).toString()
                        }

                        else -> {
                            htmlView.text = Round.roundFormatFinal(checkParking(order.costOrder), order.rounding, order.roundingType)
                            order.finalOrderCost = Round.roundFormatFinal(checkParking(order.costOrder), order.rounding, order.roundingType)
                        }
                    }
                } else {
                    when {
                        tariffCity.bonusPayment -> {
                            bonus = Round.roundFormat10(order.minPrice - BonusSystem.getBonusCost(order.minPrice, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))
                            htmlView.setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>" + order.minPrice + "</strike></font></small></small>"
                                    + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getBonusCost(order.minPrice, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod)) + "</font>", HtmlTextView.LocalImageGetter())
                            costView.text = String.format(getString(R.string.written_off), bonus)
                            order.finalOrderCost = finalCost.toString()
                        }

                        order.promoCodeDiscount > 0 -> {
                            promo = (order.minPrice - BonusSystem.getPromoCost(order.minPrice, order.promoCodeDiscount.toDouble())).toString()

                            htmlView.setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>" + order.minPrice + "</strike></font></small></small>"
                                    + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getPromoCost(order.minPrice, order.promoCodeDiscount.toDouble())) + "</font>", HtmlTextView.LocalImageGetter())
                            costView.text = String.format(getString(R.string.promo_mask), order.promoCodeDiscount.toString()) + "%"

                            order.finalOrderCost = Round.roundFormat10(BonusSystem.getPromoCost(order.minPrice, order.promoCodeDiscount.toDouble()))
                        }

                        else -> {
                            htmlView.text = order.minPrice.toString() + " "

                            order.finalOrderCost = order.minPrice.toString()
                        }
                    }
                }

                saveOrder(order)

                reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), order.plantingCost.toString()))))

                if (order.dopCost > 0)
                    reportList!!.add(ItemReport(getString(R.string.text_reportorder_dop), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), order.dopCost.toString()))))

                if (order.priceTrackOutOrder > 0) {
                    reportList!!.add(ItemReport(getString(R.string.price_finish_outorder_dis),
                            Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + Round.roundFormatDis(order.disTrackOutOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.priceTrackOutOrder)) + "</font>")))
                }

                if (order.startPoint == "in") {
                    when {
                        order.typeCostBase == ClientTariffParser.ACCRUAL_DISTANCE -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                + tariffCity.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        order.typeCostBase == ClientTariffParser.ACCRUAL_TIME -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                + tariffCity.plantingInclude + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        order.typeCostBase == ClientTariffParser.ACCRUAL_MIXED -> {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + tariffCity.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + colorBlack(activity!!.applicationContext) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + tariffCity.plantingIncludeTime + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        }
                    }
                } else {
                    when {
                        order.typeCostTrack == ClientTariffParser.ACCRUAL_DISTANCE -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                + tariffTrack.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + colorBlack(activity!!.applicationContext) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        order.typeCostTrack == ClientTariffParser.ACCRUAL_TIME -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                + tariffTrack.plantingInclude + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        order.typeCostBase == ClientTariffParser.ACCRUAL_MIXED -> {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + tariffTrack.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                    + tariffTrack.plantingIncludeTime + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), "0") + "</font>")))
                        }
                    }
                }
            }

            view.findViewById<TextView>(R.id.textview_reportorder_sumdistance).text = order.sumDistance.toString() + " " + getString(R.string.text_dis)
            view.findViewById<TextView>(R.id.textview_reportorder_sumtime).text = formatTime(order.sumTime)

            try {

                if (order.fix == 0) {

                    if (order.typeCostBase == "DISTANCE") {
                        try {
                            if (order.disCityInOrder > 0) {
                                val price = if (order.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")
                                val priceDoubleValue = price.toDoubleOrNull() ?: 1.0
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " 1 " + getString(R.string.text_dis) + "/" + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), price),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + Round.roundFormatDis(order.disCityInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='"
                                                + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.disCityInOrder * priceDoubleValue)) + "</font>")))
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostBase == "TIME") {
                        try {
                            if (order.timeCityInOrder > 0) {
                                val timeCity: String
                                val costCity: String
                                when (tariffCity.nextCostUnit) {
                                    ClientTariffParser.COST_UNIT_30 -> {
                                        timeCity = "30"
                                        costCity = if (order.timeCityInOrder > 0) {
                                            if (order.timeCityInOrder / 1800000 == 0L)
                                                tariffCity.nextKmPrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeCityInOrder / 1800000 + 1) * tariffCity.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    ClientTariffParser.COST_UNIT_60 -> {
                                        timeCity = "60"

                                        costCity = if (order.timeCityInOrder > 0) {
                                            if (order.timeCityInOrder / 3600000 == 0L) {
                                                tariffCity.nextKmPrice.toString() + ""
                                            } else {
                                                Log.d("FINISH", "2")
                                                Round.roundFormat10((order.timeCityInOrder / 3600000 + 1) * tariffCity.nextTimePrice)
                                            }
                                        } else {
                                            "0"
                                        }
                                    }
                                    else -> {
                                        timeCity = "1"
                                        costCity = Round.roundFormat10(order.timeCityInOrder.toDouble() / 60000.0 * tariffCity.nextTimePrice)
                                    }
                                }
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " " + timeCity + " " + getString(R.string.text_time_min_small) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + formatTime(order.timeCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), costCity) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostBase == "FIX") {
                        if (order.fix == 0) {
                            if (order.fixCityCost > 0) {
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix_city), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), order.fixCityCost.toString()))))
                            }
                        }
                    } else if (order.typeCostBase == ClientTariffParser.ACCRUAL_MIXED) {
                        try {
                            if (order.timeCityInOrder > 0) {
                                val timeCity: String
                                val costCity: String
                                when (tariffCity.nextCostUnit) {
                                    ClientTariffParser.COST_UNIT_30 -> {
                                        timeCity = "30"
                                        costCity = if (order.timeCityInOrder > 0) {
                                            if (order.timeCityInOrder / 1800000 == 0L)
                                                tariffCity.nextTimePrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeCityInOrder / 1800000 + 1) * tariffCity.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    ClientTariffParser.COST_UNIT_60 -> {
                                        timeCity = "60"
                                        costCity = if (order.timeCityInOrder > 0) {
                                            if (order.timeCityInOrder / 3600000 == 0L)
                                                tariffCity.nextTimePrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeCityInOrder / 3600000 + 1) * tariffCity.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    else -> {
                                        timeCity = "1"
                                        costCity = Round.roundFormat10(order.timeCityInOrder.toDouble() / 60000.0 * tariffCity.nextTimePrice)
                                    }
                                }

                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " " + timeCity + " " + getString(R.string.text_time_min_small) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffCity.getString("next_km_price_day_time") else jsonTariffCity.getString("next_km_price_night_time")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + formatTime(order.timeCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), costCity) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        try {
                            if (order.disCityInOrder > 0) {
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " 1 " + getString(R.string.text_dis) + "/" + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffCity.getJSONArray("next_km_price_night").getJSONObject(0).getString("price") else jsonTariffCity.getJSONArray("next_km_price_night").getJSONObject(0).getString("price")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + Round.roundFormatDis(order.disCityInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.disCityInOrder * (if (order.day) jsonTariffCity.getJSONArray("next_km_price_day").getJSONObject(0).getString("price") else jsonTariffCity.getJSONArray("next_km_price_night").getJSONObject(0).getString("price")).toDouble())) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostBase == ClientTariffParser.ACCRUAL_INTERVAL || order.typeCostTrack == ClientTariffParser.ACCRUAL_INTERVAL) {
                        if (order.priceIntervalInOrder > 0)
                            reportList!!.add(ItemReport(getString(R.string.text_price_interval), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), Round.roundFormatFinal(order.priceIntervalInOrder, order.rounding, order.roundingType)))))
                    }

                    if (order.typeCostTrack == "DISTANCE") {
                        try {
                            if (order.disTrackInOrder > 0) {
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " 1 " + getString(R.string.text_dis) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + Round.roundFormatDis(order.disTrackInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.disTrackInOrder * java.lang.Double.valueOf(if (order.day) jsonTariffTrack.getJSONArray("next_km_price_day").getJSONObject(0).getString("price") else jsonTariffTrack.getJSONArray("next_km_price_night").getJSONObject(0).getString("price")))) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostTrack == "TIME") {
                        try {
                            if (order.timeTrackInOrder > 0) {
                                val timeTrack: String
                                val costTrack: String
                                when (tariffTrack.nextCostUnit) {
                                    ClientTariffParser.COST_UNIT_30 -> {
                                        timeTrack = "30"
                                        costTrack = if (order.timeTrackInOrder > 0) {
                                            if (order.timeTrackInOrder / 1800000 == 0L)
                                                tariffTrack.nextKmPrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeTrackInOrder / 1800000 + 1) * tariffTrack.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    ClientTariffParser.COST_UNIT_60 -> {
                                        timeTrack = "60"
                                        costTrack = if (order.timeTrackInOrder > 0) {
                                            if (order.timeTrackInOrder / 3600000 == 0L)
                                                tariffTrack.nextKmPrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeTrackInOrder / 3600000 + 1) * tariffTrack.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    else -> {
                                        timeTrack = "1"
                                        costTrack = Round.roundFormat10(order.timeTrackInOrder.toDouble() / 60000.0 * tariffTrack.nextTimePrice)
                                    }
                                }

                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " " + timeTrack + " " + getString(R.string.text_time_min_small) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + formatTime(order.timeTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), costTrack) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostTrack == "FIX") {
                        if (order.fix == 0) {
                            if (order.fixOutCityCost > 0) {
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix_outcity), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), order.fixOutCityCost.toString()))))
                            }
                        }
                    } else if (order.typeCostTrack == ClientTariffParser.ACCRUAL_MIXED) {
                        try {

                            if (order.timeTrackInOrder > 0) {

                                val timeTrack: String
                                val costTrack: String
                                when (tariffTrack.nextCostUnit) {
                                    ClientTariffParser.COST_UNIT_30 -> {
                                        timeTrack = "30"
                                        costTrack = if (order.timeTrackInOrder > 0) {
                                            if (order.timeTrackInOrder / 1800000 == 0L)
                                                tariffTrack.nextTimePrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeTrackInOrder / 1800000 + 1) * tariffTrack.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    ClientTariffParser.COST_UNIT_60 -> {
                                        timeTrack = "60"
                                        costTrack = if (order.timeTrackInOrder > 0) {
                                            if (order.timeTrackInOrder / 3600000 == 0L)
                                                tariffTrack.nextTimePrice.toString() + ""
                                            else
                                                Round.roundFormat10((order.timeTrackInOrder / 3600000 + 1) * tariffTrack.nextTimePrice)
                                        } else {
                                            "0"
                                        }
                                    }
                                    else -> {
                                        timeTrack = "1"
                                        costTrack = Round.roundFormat10(order.timeTrackInOrder.toDouble() / 60000.0 * tariffTrack.nextTimePrice)
                                    }
                                }
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " " + timeTrack + " " + getString(R.string.text_time_min_small) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffTrack.getString("next_km_price_day_time") else jsonTariffTrack.getString("next_km_price_night_time")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + formatTime(order.timeTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), costTrack) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        try {
                            if (order.disTrackInOrder > 0) {
                                reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " 1 " + getString(R.string.text_dis) + "/"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                                + Round.roundFormatDis(order.disTrackInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                                + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.disTrackInOrder * java.lang.Double.valueOf(if (order.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")))) + "</font>")))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    } else if (order.typeCostTrack == ClientTariffParser.ACCRUAL_INTERVAL) {

                    }


                    if (order.timeWaitCityInOrder > 0)
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_waitbase) + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")),
                                Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                        + formatTime(order.timeWaitCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.timeWaitCityInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")))) + "</font>")))

                    if (order.timeWaitTrackInOrder > 0)
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_waittrack) + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), if (order.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")),
                                Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                        + formatTime(order.timeWaitTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.timeWaitTrackInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")))) + "</font>")))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }


            if (order.priceClientWait > 0) {

                if (order.timeWaitCityClient > 0)
                    timeWaitClient += order.timeWaitCityClient

                if (order.timeWaitTrackClient > 0)
                    timeWaitClient += order.timeWaitTrackClient

                reportList!!.add(ItemReport(getString(R.string.text_reportorder_beforewait), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                        + formatTime(timeWaitClient) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                        + TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), formatCost(order.priceClientWait))
                        + "</font>")))
            }

            reportList!!.add(ItemReport(getString(R.string.price_finish_round), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), order.rounding.toString()))))

            llContainerFinish!!.removeAllViews()
            val layoutInflater = activity!!.layoutInflater
            for (item in reportList!!) {
                val layoutView = layoutInflater.inflate(R.layout.item_view_finish, null, false)
                (layoutView.findViewById<View>(R.id.textview_reportorder_label) as TextView).text = item.label
                (layoutView.findViewById<View>(R.id.textview_reportorder_info) as TextView).text = item.desc
                llContainerFinish!!.addView(layoutView)
            }

            btnPaid.setOnClickListener {
                if (InternetConnection.isOnline(activity!!)) {
                    Log.d("OPS_PAID_CONFIRM", "require_point_confirmation_code: " + appPreferences!!.getText("require_point_confirmation_code"))
                    if (appPreferences!!.getText("require_point_confirmation_code") == "1") {
                        if (checkConfirmationCode()) {
                            clickPaid()
                        } else {
                            startActivity(Intent(activity, ConfirmationCodeActivity::class.java))
                        }
                    } else {
                        clickPaid()
                    }
                } else {
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                }
            }

            btnPaidNoCash.setOnClickListener {
                if (InternetConnection.isOnline(activity!!)) {
                    Log.d("OPS_PAID", "click btnPaidNoCash")
                    if (appPreferences!!.getText("require_point_confirmation_code") == "1") {
                        if (checkConfirmationCode()) {
                            clickPaidNoCash()
                        } else {
                            startActivity(Intent(activity, ConfirmationCodeActivity::class.java))
                        }
                    } else {
                        clickPaidNoCash()
                    }
                } else {
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                }
            }

            btnNPaid.setOnClickListener {
                if (InternetConnection.isOnline(activity!!)) {
                    Log.d("OPS_PAID", "click btnNPaid")
                    if (appPreferences!!.getText("require_point_confirmation_code") == "1") {
                        if (checkConfirmationCode()) {
                            clickNoPaid()
                        } else {
                            startActivity(Intent(activity, ConfirmationCodeActivity::class.java))
                        }
                    } else {
                        clickNoPaid()
                    }
                } else {
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                }
            }

            btnNext.setOnClickListener {
                if (InternetConnection.isOnline(activity!!)) {
                    btnNext.isEnabled = false
                    btnPaid.isEnabled = false
                    btnNPaid.isEnabled = false
                    btnPaidNoCash.isEnabled = false
                    btnChangeFare.isEnabled = false

                    requestParams = LinkedHashMap()
                    requestParams["order_id"] = currentOrder.orderId
                    requestParams["status_new_id"] = ORDER_STATUS_CAR_ASSIGNED
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign

                    countDownTimer!!.start()

                    if (uuid == null) {
                        uuid = presenter.getNewUuid()
                    }
                    requestType = RequestType.BACK

                    if (SET_ORDER_PARAMS_VIA_SOCKET) {
                        presenter.setOrderStatus(ORDER_STATUS_CAR_ASSIGNED, requestType)
                    } else {
                        WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
                    }

                } else {
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                }
            }

            btnChangeFare.setOnClickListener {
                if (!InternetConnection.isOnline(activity!!)) {
                    CustomToast.showMessage(activity, getString(R.string.text_internet_access))
                    return@setOnClickListener
                }

                val viewDialog = activity!!.layoutInflater.inflate(R.layout.fragment_dialog_edittext_ordercost, null)
                val alertDialog = AlertDialog.Builder(activity!!)
                alertDialog.setTitle(getString(R.string.new_ordercost))
                alertDialog.setView(viewDialog)
                alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { dialog, _ ->
                    val orderCost = viewDialog.findViewById<EditText>(R.id.et_ordercost).text.toString()
                    if (orderCost.isNotEmpty()) {
                        val requestParams = LinkedHashMap<String, String>()
                        requestParams["order_id"] = currentOrder.orderId
                        requestParams["predv_price"] = orderCost
                        requestParams["tenant_login"] = service!!.uri
                        requestParams["worker_login"] = driver.callSign

                        requestType = RequestType.UPDATE

                        if (uuid == null) {
                            uuid = presenter.getNewUuid()
                        }

                        if (SET_ORDER_PARAMS_VIA_SOCKET) {
                            presenter.updateCost(cost = orderCost, id = currentOrder.orderId, worker = driver.callSign)
                        } else {
                            WebService.updateOrderCost(requestParams, driver.secretCode, uuid)
                        }

                        dialog.dismiss()
                    } else {
                        viewDialog.findViewById<EditText>(R.id.et_ordercost).error = getString(R.string.edittext_valid_required)
                    }
                }
                val dialog = alertDialog.create()
                dialog.show()
            }

            countDownTimer = object : CountDownTimer(timeoutTimer, 1000) {
                override fun onTick(millisUntilFinished: Long) {

                    activity?.also {
                        (it as WorkShiftActivity).millisUntilFinished = millisUntilFinished
                    }
                }

                override fun onFinish() {

                    Log.d("OPS_PAID", "timer: onFinish $uuid")
                    requestParams = LinkedHashMap()
                    requestParams["request_id"] = uuid ?: presenter.getUuid()
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign
                    WebService.getResult(requestParams, driver.secretCode)
                }
            }
        }
    }

    private fun lastPosition(): String? = runBlocking {
        withContext(Dispatchers.IO) {
            AppDatabase.getInstance(context!!).routeDao().getLastRoute().parking
        }
    }

    private fun checkParking(cost: Double): Double {
        var chCost = cost

        if (order.enableParkingRatio == 1) {
            parkingEnd?.also {
                chCost = Utils.addParkingCost(it, chCost, 1)!!
            }
        }
        return chCost
    }

    @Subscribe
    fun onEvent(updateOrderCostEvent: NetworkUpdateOrderCostEvent) {
        if (updateOrderCostEvent.status != "OK") {
            showSnackBar(updateOrderCostEvent.status, btnChangeFare)
        }
    }

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        if (jsonObject != null && typeJson == "call_cost") {
            try {
                if (jsonObject.getString("info") == "OK") {

                    val jsonCost = jsonObject.getJSONObject("result").getJSONObject("cost_data")

                    order.costOrder = java.lang.Double.valueOf(jsonCost.getString("summary_cost")) + order.dopCost
                    saveOrder(order)

                    fixPrice(order)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getJsonOrderData(order: Order): JSONObject {
        val jsonDetailOrderData = JSONObject()
        try {

            val intervalCityArray = JSONArray(order.intervalCityJson)
            val intervalTrackArray = JSONArray(order.intervalTrackJson)
            var intervalCity = 0.0
            var intervalTrack = 0.0

            try {
                for (j in 0 until intervalCityArray.length()) {
                    intervalCity += intervalCityArray.getJSONObject(j).getDouble("price")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                for (j in 0 until intervalTrackArray.length()) {
                    intervalTrack += intervalTrackArray.getJSONObject(j).getDouble("price")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            jsonDetailOrderData.put("summary_time", ((order.sumTime / 60000.0 * 10).roundToInt().toDouble() / 10).toString())

            jsonDetailOrderData.put("summary_distance", order.sumDistance.toString())

            jsonDetailOrderData.put("summary_cost", order.finalOrderCost)

            this.saveOrder(order)

            if (tariffCity.accrual == ClientTariffParser.ACCRUAL_MIXED) {
                jsonDetailOrderData.put("city_cost", Round.roundFormat(order.disCityInOrder * tariffCity.nextKmPrice, order.rounding))
                when (tariffCity.nextCostUnit) {
                    ClientTariffParser.COST_UNIT_30 -> if (order.timeCityInOrder > 0) {
                        if (order.timeCityInOrder / 1800000 == 0L)
                            jsonDetailOrderData.put("city_cost_time", tariffCity.nextTimePrice.toString() + "")
                        else
                            jsonDetailOrderData.put("city_cost_time", Round.roundFormat((order.timeCityInOrder / 1800000 + 1) * tariffCity.nextTimePrice, order.rounding))

                    }
                    ClientTariffParser.COST_UNIT_60 -> if (order.timeCityInOrder > 0) {
                        if (order.timeCityInOrder / 3600000 == 0L)
                            jsonDetailOrderData.put("city_cost_time", tariffCity.nextTimePrice.toString() + "")
                        else
                            jsonDetailOrderData.put("city_cost_time", Round.roundFormat((order.timeCityInOrder / 3600000 + 1) * tariffCity.nextTimePrice, order.rounding))
                    }
                    else -> jsonDetailOrderData.put("city_cost_time", Round.roundFormat(order.timeCityInOrder / 60000.0 * tariffCity.nextTimePrice, order.rounding))
                }
            } else {
                jsonDetailOrderData.put("city_cost", Round.roundFormat(order.priceCityInOrder + intervalCity, order.rounding))
            }

            if (tariffTrack.accrual == ClientTariffParser.ACCRUAL_MIXED) {
                jsonDetailOrderData.put("out_city_cost", Round.roundFormat(order.disTrackInOrder * tariffTrack.nextKmPrice, order.rounding))
                when (tariffTrack.nextCostUnit) {
                    ClientTariffParser.COST_UNIT_30 -> if (order.timeTrackInOrder > 0) {
                        if (order.timeTrackInOrder / 1800000 == 0L)
                            jsonDetailOrderData.put("out_city_cost_time", tariffTrack.nextTimePrice.toString() + "")
                        else
                            jsonDetailOrderData.put("out_city_cost_time", Round.roundFormat((order.timeTrackInOrder / 1800000 + 1) * tariffTrack.nextTimePrice, order.rounding))
                    }
                    ClientTariffParser.COST_UNIT_60 -> if (order.timeTrackInOrder > 0) {
                        if (order.timeTrackInOrder / 3600000 == 0L)
                            jsonDetailOrderData.put("out_city_cost_time", tariffTrack.nextTimePrice.toString() + "")
                        else
                            jsonDetailOrderData.put("out_city_cost_time", Round.roundFormat((order.timeTrackInOrder / 3600000 + 1) * tariffTrack.nextTimePrice, order.rounding))
                    }
                    else -> jsonDetailOrderData.put("out_city_cost_time", Round.roundFormat(order.timeTrackInOrder / 60000.0 * tariffTrack.nextTimePrice, order.rounding))
                }
            } else {
                jsonDetailOrderData.put("out_city_cost", Round.roundFormat(order.priceTrackInOrder + intervalTrack, order.rounding))
            }


            if (order.timeCityInOrder > 0)
                jsonDetailOrderData.put("city_time", (Math.round(order.timeCityInOrder / 60000.0 * 100).toDouble() / 100).toString())
            else
                jsonDetailOrderData.put("city_time", "0")
            jsonDetailOrderData.put("city_distance", (if (order.disCityInOrder > 0) Round.roundFormatDis(order.disCityInOrder) else 0).toString())

            if (order.timeTrackInOrder > 0)
                jsonDetailOrderData.put("out_city_time", (Math.round(order.timeTrackInOrder / 60000.0 * 100).toDouble() / 100).toString())
            else
                jsonDetailOrderData.put("out_city_time", "0")
            jsonDetailOrderData.put("out_city_distance", (if (order.disTrackInOrder > 0) Round.roundFormatDis(order.disTrackInOrder) else 0).toString())

            jsonDetailOrderData.put("city_time_wait", Math.round((order.timeWaitCityInOrder / 1000).toFloat()).toString())
            jsonDetailOrderData.put("out_time_wait", Math.round((order.timeWaitTrackInOrder / 1000).toFloat()).toString())

            jsonDetailOrderData.put("before_time_wait", (Math.round(timeWaitClient / 60000.0 * 100).toDouble() / 100).toString())

            if (order.disTrackOutOrder > 0)
                jsonDetailOrderData.put("distance_for_plant", Round.roundFormatDis(order.disTrackOutOrder).toString())
            else
                jsonDetailOrderData.put("distance_for_plant", "0")

            jsonDetailOrderData.put("city_wait_cost", Round.roundFormat(order.priceCityWaitInOrder, order.rounding))
            jsonDetailOrderData.put("out_wait_cost", Round.roundFormat(order.priceTrackWaitInOrder, order.rounding))
            if (order.priceTrackOutOrder > 0)
                jsonDetailOrderData.put("distance_for_plant_cost", Round.roundFormat(order.priceTrackOutOrder, order.rounding))
            else
                jsonDetailOrderData.put("distance_for_plant_cost", "0")
            if (order.priceClientWait > 0)
                jsonDetailOrderData.put("before_time_wait_cost", Round.roundFormat(order.priceClientWait, order.rounding))
            else
                jsonDetailOrderData.put("before_time_wait_cost", "0")

            if (order.promoCodeDiscount > 0) {
                jsonDetailOrderData.put("promo_discount_value", promo)
                jsonDetailOrderData.put("promo_discount_percent", order.promoCodeDiscount)
            }

            if (tariffCity.bonusPayment) {
                jsonDetailOrderData.put("writeoff_bonus_id", tariffCity.bonusId)
                jsonDetailOrderData.put("bonus", bonus)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Log.d("JSON_DET", jsonDetailOrderData.toString())

        return jsonDetailOrderData
    }

    private fun fixPrice(order: Order) {
        reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(activity!!.applicationContext, getView(), Round.roundFormat(order.costOrder, order.rounding)))))

        (view.findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = Round.roundFormatDouble(checkParking(order.costOrder + order.priceClientWait + order.priceCityWaitInOrder + order.priceTrackWaitInOrder), order.rounding, order.roundingType).toString()

        val finalFixCost = Round.roundFormatDouble(order.costOrder, order.rounding, order.roundingType)

        when {
            tariffCity.bonusPayment -> {
                bonus = Round.roundFormat10((finalCost
                        + order.priceClientWait
                        + order.priceCityWaitInOrder
                        + order.priceTrackWaitInOrder) - BonusSystem.getBonusCost(finalCost
                        + order.priceClientWait
                        + order.priceCityWaitInOrder
                        + order.priceTrackWaitInOrder, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))

                (view.findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView)
                        .setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>"
                                + Round.roundFormat10(finalCost
                                + order.priceClientWait
                                + order.priceCityWaitInOrder
                                + order.priceTrackWaitInOrder) + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>"
                                + Round.roundFormat10(BonusSystem.getBonusCost(finalCost
                                + order.priceClientWait
                                + order.priceCityWaitInOrder
                                + order.priceTrackWaitInOrder, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))
                                + "</font>", HtmlTextView.LocalImageGetter())
                (view.findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.written_off), bonus)

                this.order.finalOrderCost = Round.roundFormatFinal(finalCost
                        + order.priceClientWait
                        + order.priceCityWaitInOrder
                        + order.priceTrackWaitInOrder, order.rounding, order.roundingType)


            }
            order.promoCodeDiscount > 0 -> {
                promo = (finalFixCost - BonusSystem.getPromoCost(finalFixCost, order.promoCodeDiscount.toDouble())).toString()

                view.findViewById<HtmlTextView>(R.id.textview_reportorder_cost).setHtmlFromString("<small><small><font color='"
                        + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'><strike>"
                        + Round.roundFormatFinal(checkParking(order.costOrder
                        + order.priceClientWait
                        + order.priceCityWaitInOrder
                        + order.priceTrackWaitInOrder), order.rounding, order.roundingType) + "</strike></font></small></small>"
                        + "  <font color='#2E7D32'>"
                        + BonusSystem.getPromoCost(finalFixCost, order.promoCodeDiscount.toDouble())
                        + "</font>", HtmlTextView.LocalImageGetter())
                view.findViewById<TextView>(R.id.textview_reportorder_cost_written_off).text = String.format(getString(R.string.promo_mask), order.promoCodeDiscount.toString()) + "%"

                this.order.finalOrderCost = BonusSystem.getPromoCost(Round.roundFormatDouble(checkParking(order.costOrder
                        + order.priceClientWait
                        + order.priceCityWaitInOrder
                        + order.priceTrackWaitInOrder), order.rounding, order.roundingType), order.promoCodeDiscount.toDouble()).toString()
            }
            else -> {
                (view.findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = Round.roundFormatDouble(checkParking(order.costOrder /*+ order.priceClientWait + order.priceCityWaitInOrder + order.priceTrackWaitInOrder*/), order.rounding, order.roundingType).toString()

                this.order.finalOrderCost = Round.roundFormatDouble(checkParking(order.costOrder + order.priceClientWait + order.priceCityWaitInOrder + order.priceTrackWaitInOrder), order.rounding, order.roundingType).toString()
            }
        }

        saveOrder(this.order)
    }

    @Subscribe
    fun onEvent(pushOrderEvent: PushOrderEvent) {
        if (pushOrderEvent.orderId == order.orderId) {
            when (pushOrderEvent.command) {
                "order_is_rejected" -> {

                    Log.d("OPS_PAID", "PushOrderEvent " + "order_is_rejected")

                    EventBus.getDefault().post(StopTimer())
                    EventBus.getDefault().post(StopTaximeterEvent("close"))

                    runBlocking {
                        withContext(Dispatchers.IO) {
                            val db = AppDatabase.getInstance(context!!)
                            db.currentOrderDao().deleteAll(driver.id)
                            db.orderDao().delete(order)
                        }
                    }
                }

                "order_is_updated" -> {

                    val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
                    requestParams["order_id"] = pushOrderEvent.orderId
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign
                    WebService.getOrderForId(requestParams, driver.secretCode)
                }
            }
        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            val orderData = orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data")

            if (order.orderId == orderData.getString("order_id")) {

                currentOrder.address = orderData.getString("address")

                val activity = activity as WorkShiftActivity

                activity.order!!.address = orderData.getString("address")
                activity.order!!.typeCost = orderData.getString("payment")

                var bonusPayment = 0

                try {
                    bonusPayment = Integer.valueOf(orderData.getString("bonus_payment"))

                    try {
                        if (orderData.isNull("bonusData") || orderData.getJSONArray("bonusData").length() == 0)
                            bonusPayment = 0
                    } catch (e: Exception) {
                    }

                } catch (e: Exception) {
                }

                if (bonusPayment == 1) {
                    tariffCity.bonusPayment = true
                    try {
                        if (orderData.getJSONObject("bonusData").getString("payment_method") == ClientTariffParser.METHOD_PAYMENT_FULL) {
                            if (orderData.getJSONObject("bonusData").getString("max_payment_type") == ClientTariffParser.TYPE_MAX_PAYMENT_BONUS) {
                                tariffCity.maxPayment = java.lang.Double.valueOf(orderData.getJSONObject("bonusData").getString("client_bonus_balance "))
                            } else {
                                tariffCity.maxPayment = 100.0
                            }
                        } else {
                            tariffCity.maxPayment = java.lang.Double.valueOf(orderData.getJSONObject("bonusData").getString("max_payment"))
                        }

                        tariffCity.clientBonusBalance = floor((orderData.getJSONObject("bonusData").getString("client_bonus_balance ")).toDouble())
                        tariffCity.bonusId = orderData.getJSONObject("bonusData").getString("bonus_id")
                        tariffCity.paymentMethod = orderData.getJSONObject("bonusData").getString("payment_method")
                        tariffCity.maxPaymentType = orderData.getJSONObject("bonusData").getString("max_payment_type")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    tariffCity.bonusPayment = false
                }

                runBlocking {
                    withContext(Dispatchers.IO) {
                        AppDatabase.getInstance(context!!).clientTariffDao().save(tariffCity)
                    }
                }

                val costData = orderData.getJSONObject("costData")
                val fix = costData.getInt("is_fix")

                if (fix == 1) {
                    /*  activity.order!!*/
                    order.costOrder = costData.getString("summary_cost").toDouble()
                    order.fix = fix
                }

                EventBus.getDefault().post(TaximeterStageEvent(3, ORDER_STATUS_COMPLETED))

                saveCurrentOrder()

                order.address = currentOrder.address
                order.tariffData = currentOrder.tariffData

                var disCity = activity.order!!.disCityInOrder
                var disTrack = activity.order!!.disTrackInOrder

                if (order.startPoint == "in") {
                    if (tariffCity.accrual == ClientTariffParser.ACCRUAL_DISTANCE
                            || tariffCity.accrual == ClientTariffParser.ACCRUAL_MIXED
                            || tariffCity.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                        if (disCity < 0)
                            disCity = 0.0
                        disCity += tariffCity.plantingInclude
                    }
                } else {
                    if (tariffTrack.accrual == ClientTariffParser.ACCRUAL_DISTANCE
                            || tariffTrack.accrual == ClientTariffParser.ACCRUAL_MIXED
                            || tariffTrack.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                        if (disTrack < 0)
                            disTrack = 0.0
                        disTrack += tariffTrack.plantingInclude
                    }
                }

                EventBus.getDefault().post(StopTimer())
                EventBus.getDefault().post(StopTaximeterEvent("close"))

                activity.order = order
                activity.currentOrder = currentOrder
                activity.stageOrder = 2
                activity.onNavigationDrawerItemSelected(1)
                EventBus.getDefault().post(SetStageOrderEvent(2))
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(requestResult: NetworkRequestResult) {
        countDownTimer!!.cancel()

        if (requestResult.status == "OK" || requestResult.status == "EMPTY_DATA_IN_DATABASE") {

            if (requestResult.result == 1 || requestResult.status == "EMPTY_DATA_IN_DATABASE") {

            } else if (requestResult.status == "FAIL") {
                activity!!.runOnUiThread {
                    btnNext.isEnabled = true
                    btnPaid.isEnabled = true
                    btnNPaid.isEnabled = true
                    btnPaidNoCash.isEnabled = true
                    btnChangeFare.isEnabled = true
                }

            } else {
                activity!!.runOnUiThread {
                    countDownTimer = object : CountDownTimer(15000, 1000) {
                        override fun onTick(millisUntilFinished: Long) {}

                        override fun onFinish() {
                            try {
                                presenter.clearUuid()
                                uuid = null
                                btnNext.isEnabled = true
                                btnPaid.isEnabled = true
                                btnNPaid.isEnabled = true
                                btnPaidNoCash.isEnabled = true
                                btnChangeFare.isEnabled = true
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                    CustomToast.showMessage(activity, getString(R.string.toast_paid_false))
                    order.typeCost = "CASH"
                    saveOrder(order)
                    btnNext.isEnabled = true
                    btnPaid.isEnabled = true
                    btnNPaid.isEnabled = true
                    btnPaidNoCash.isEnabled = true
                    btnChangeFare.isEnabled = true
                    view.findViewById<View>(R.id.linearlayout_cash).visibility = View.VISIBLE
                    view.findViewById<View>(R.id.textview_finishorder_cash).visibility = View.VISIBLE
                    btnPaidNoCash.visibility = View.GONE
                }
            }
        } else if (requestResult.status == "OK") {

            currentOrder.stage = 2
            saveCurrentOrder()
            EventBus.getDefault().post(StopTimer())
            EventBus.getDefault().post(StopTaximeterEvent("close"))

            (activity as WorkShiftActivity).also { activity ->
                activity.order = order
                activity.currentOrder = currentOrder
                activity.stageOrder = 1
                activity.onNavigationDrawerItemSelected(1)
            }

        } else {

            val message = when (requestResult.status) {
                EMPTY_RESPONSE -> getString(R.string.contact_support)
                RESPONSE_ERROR -> getString(R.string.response_error)
                else -> getString(R.string.request_error)
            }
            val snackbar = Snackbar.make(btnChangeFare, message, Snackbar.LENGTH_INDEFINITE)
            snackbar.setAction("OK") {
                snackbar.dismiss()
            }
            snackbar.show()

            btnNext.isEnabled = true
            btnPaid.isEnabled = true
            btnNPaid.isEnabled = true
            btnPaidNoCash.isEnabled = true
            btnChangeFare.isEnabled = true
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        try {
            countDownTimer!!.cancel()

            if ((pushResponseEvent.infoCode == "OK" || pushResponseEvent.infoCode == "EMPTY_DATA_IN_DATABASE") && pushResponseEvent.uuid == uuid && requestType == RequestType.RESERVE) {

                if (pushResponseEvent.result == 1 || pushResponseEvent.infoCode == "EMPTY_DATA_IN_DATABASE") {
                    sendRoute()

                    CoroutineScope(Dispatchers.IO).launch {
                        AppDatabase.getInstance(this@FinishOrderFragment.context!!).currentOrderDao().deleteAll(driver.id)
                    }

                    AppPreferences(activity!!).saveText("parking", "")

                    EventBus.getDefault().post(StopTimer())
                    EventBus.getDefault().post(StopTaximeterEvent("close"))

                    val activity = activity as WorkShiftActivity
                    activity.order = null
                    activity.currentOrder = null
                    activity.stageOrder = 0
                    activity.stage = 0

                    EventBus.getDefault().post(SetStageOrderEvent(0))

                    if (lifecycleState == "onStop")
                        showDialogPaid = 1

                    activity.onNavigationDrawerItemSelected(1)

                    ChangeDialogCompletedFragment().show(activity.supportFragmentManager, "dialogFragment_completed")

                } else if (pushResponseEvent.infoCode == "FAIL") {

                    activity!!.runOnUiThread {
                        btnNext.isEnabled = true
                        btnPaid.isEnabled = true
                        btnNPaid.isEnabled = true
                        btnPaidNoCash.isEnabled = true
                        btnChangeFare.isEnabled = true
                    }

                } else {

                    activity!!.runOnUiThread {
                        countDownTimer = object : CountDownTimer(15000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                            }

                            override fun onFinish() {
                                try {
                                    presenter.clearUuid()
                                    uuid = null
                                    btnNext.isEnabled = true
                                    btnPaid.isEnabled = true
                                    btnNPaid.isEnabled = true
                                    btnPaidNoCash.isEnabled = true
                                    btnChangeFare.isEnabled = true
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }

                        CustomToast.showMessage(activity, getString(R.string.toast_paid_false))
                        order.typeCost = "CASH"
                        saveOrder(order)
                        btnNext.isEnabled = true
                        btnPaid.isEnabled = true
                        btnNPaid.isEnabled = true
                        btnPaidNoCash.isEnabled = true
                        btnChangeFare.isEnabled = true
                        view.findViewById<View>(R.id.linearlayout_cash).visibility = View.VISIBLE
                        view.findViewById<View>(R.id.textview_finishorder_cash).visibility = View.VISIBLE
                        btnPaidNoCash.visibility = View.GONE
                    }
                }
            } else if (pushResponseEvent.infoCode == "OK" && pushResponseEvent.uuid == uuid && requestType == RequestType.BACK) {

                currentOrder.stage = 2
                saveCurrentOrder()

                EventBus.getDefault().post(StopTimer())
                EventBus.getDefault().post(StopTaximeterEvent("close"))

                val activity = activity as WorkShiftActivity
                activity.order = order
                activity.currentOrder = currentOrder
                activity.stageOrder = 1
                activity.onNavigationDrawerItemSelected(1)

            } else {
                activity?.runOnUiThread {
                    btnNext.isEnabled = true
                    btnPaid.isEnabled = true
                    btnNPaid.isEnabled = true
                    btnPaidNoCash.isEnabled = true
                    btnChangeFare.isEnabled = true
                }
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun saveOrder(order: Order) = runBlocking {
        withContext(Dispatchers.IO) {
            AppDatabase.getInstance(context!!).orderDao().save(order)
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {

        if ((orderStatusEvent.result == 0 || orderStatusEvent.result == 2) && orderStatusEvent.status == "OK") {
            sendRoute()
            CoroutineScope(Dispatchers.IO).launch {
                AppDatabase.getInstance(this@FinishOrderFragment.context!!).currentOrderDao().deleteAll(driver.id)
            }

            AppPreferences(activity!!).saveText("parking", "")

            EventBus.getDefault().post(StopTimer())
            EventBus.getDefault().post(StopTaximeterEvent("close"))

            val activity = activity as WorkShiftActivity

            activity.order = null
            activity.currentOrder = null
            activity.stageOrder = 0
            activity.stage = 0
            EventBus.getDefault().post(SetStageOrderEvent(0))

            if (lifecycleState == "onStop")
                showDialogPaid = 1

            activity.onNavigationDrawerItemSelected(1)

            ChangeDialogCompletedFragment().show(activity.supportFragmentManager, "dialogFragment_completed")
        } else if (requestType != RequestType.BACK && requestType != RequestType.RESERVE) {
            showSnackBar(orderStatusEvent.status, btnChangeFare)
        }
    }

    private fun sendRoute() {
        val requestParamsRaw = LinkedHashMap<String, String>()

        try {
            Log.d("SEND_ROUTE_STEP", "2")

            val jsonRawCalc = JSONObject()

            val intervalCityArray = JSONArray(order.intervalCityJson)
            val intervalTrackArray = JSONArray(order.intervalTrackJson)
            var intervalCity = 0.0
            var intervalTrack = 0.0

            try {
                for (j in 0 until intervalCityArray.length()) {
                    intervalCity += intervalCityArray.getJSONObject(j).getDouble("price")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                for (j in 0 until intervalTrackArray.length()) {
                    intervalTrack += intervalTrackArray.getJSONObject(j).getDouble("price")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            jsonRawCalc.put("timeWaitCityInOrder", order.timeWaitCityInOrder)
            jsonRawCalc.put("timeWaitTrackInOrder", order.timeWaitTrackInOrder)
            jsonRawCalc.put("timeWaitCityClient", order.timeWaitCityClient)
            jsonRawCalc.put("timeWaitTrackClient", order.timeWaitTrackClient)
            jsonRawCalc.put("timeCityInOrder", order.timeCityInOrder)
            jsonRawCalc.put("timeTrackInOrder", order.timeTrackInOrder)
            jsonRawCalc.put("timeCityOutOrder", order.timeCityOutOrder)
            jsonRawCalc.put("timeTrackOutOrder", order.timeTrackOutOrder)
            jsonRawCalc.put("timeFreeInOrder", order.timeFreeInOrder)

            jsonRawCalc.put("disCityInOrder", order.disCityInOrder)
            jsonRawCalc.put("disTrackInOrder", order.disTrackInOrder)
            jsonRawCalc.put("disCityOutOrder", order.disCityOutOrder)
            jsonRawCalc.put("disTrackOutOrder", order.disTrackOutOrder)

            jsonRawCalc.put("priceCityInOrder", order.priceCityInOrder + intervalCity)
            jsonRawCalc.put("priceCityOutOrder", order.priceCityOutOrder)
            jsonRawCalc.put("priceTrackInOrder", order.priceTrackInOrder + intervalTrack)
            jsonRawCalc.put("priceTrackOutOrder", order.priceTrackOutOrder)
            jsonRawCalc.put("priceCityWaitInOrder", order.priceCityWaitInOrder)
            jsonRawCalc.put("priceTrackWaitInOrder", order.priceTrackWaitInOrder)
            jsonRawCalc.put("priceClientWait", order.priceClientWait)
            jsonRawCalc.put("arrayIntervalCity", JSONArray(order.intervalCityJson))
            jsonRawCalc.put("arrayIntervalTrack", JSONArray(order.intervalTrackJson))
            jsonRawCalc.put("tariff", JSONObject(order.tariffData))
            jsonRawCalc.put("provider", (activity as WorkShiftActivity).provider)

            if (order.promoCodeDiscount > 0) {
                jsonRawCalc.put("promo_discount_value", promo)
                jsonRawCalc.put("promo_discount_percent", order.promoCodeDiscount)
            }

            try {
                jsonRawCalc.put("device_info", "driver/" + activity!!.packageManager.getPackageInfo(activity!!.packageName, 0).versionCode
                        + " (" + "android " + Build.VERSION.RELEASE + "; " + Build.MANUFACTURER + " " + Build.MODEL + ")")
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            try {
                requestParamsRaw["order_calc_data"] = URLEncoder.encode(jsonRawCalc.toString(), "UTF-8").replace("+", "%20")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

            requestParamsRaw["order_id"] = order.orderId
            requestParamsRaw["tenant_login"] = service!!.uri
            requestParamsRaw["worker_login"] = driver.callSign

            WebService.setOrderCalc(requestParamsRaw, driver.secretCode)

            if (appPreferences!!.getText("print_check") == "1") {
                (activity as WorkShiftActivity).printCheck((view.findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text.toString(), service!!.currency!!, order.orderIdService)
            }
            activity!!.startService(Intent(context, SendRouteService::class.java)
                    .putExtra("uri", service!!.uri)
                    .putExtra("order_id", order.orderId))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    override fun onStart() {
        super.onStart()
        Log.d("FOF", "onStart")

        lifecycleState = "onStart"

        if (showDialogPaid == 1 || requestType == RequestType.BACK) {
            showDialogPaid = 0
            (activity as WorkShiftActivity).onNavigationDrawerItemSelected(1)
            ChangeDialogCompletedFragment().show(activity!!.supportFragmentManager, "dialogFragment_completed")
        }
    }

    override fun onStop() {
        super.onStop()
        lifecycleState = "onStop"
    }

    private fun formatCost(cost: Double): String {
        val decimalFormat = NumberFormat.getNumberInstance(Locale.ENGLISH) as DecimalFormat
        decimalFormat.applyPattern("##0.00")
        return decimalFormat.format(cost).replace(",", ".")
    }

    private fun formatTime(mSec: Long): String {
        return if (mSec > 0)
            String.format(Locale.ENGLISH, "%d " + getString(R.string.text_time_min_small) + ", %d " + getString(R.string.text_time_sec_small),
                    TimeUnit.MILLISECONDS.toMinutes(mSec),
                    TimeUnit.MILLISECONDS.toSeconds(mSec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mSec)))
        else
            String.format(Locale.ENGLISH, "%d " + getString(R.string.text_time_min_small) + ", %d " + getString(R.string.text_time_sec_small),
                    TimeUnit.MILLISECONDS.toMinutes(0),
                    TimeUnit.MILLISECONDS.toSeconds(0) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(0)))
    }

    private fun checkConfirmationCode(): Boolean {
        var isConf = false
        try {
            appPreferences = AppPreferences(activity!!)
            val jsonAddress = JSONObject(currentOrder.address)
            var jsonArray = JSONArray()

            Log.d("OPS_PAID_CONFIRM", jsonAddress.toString())

            try {
                Log.d("OPS_PAID_CONFIRM", appPreferences!!.getText("confirm_codes"))
                jsonArray = JSONArray(appPreferences!!.getText("confirm_codes"))
            } catch (e: Exception) {
                e.printStackTrace()
            }

            var countConfAddresses = 0

            for (point in arrAddresses) {
                if (point == "A") continue
                try {
                    jsonAddress.getJSONObject(point).getString("confirmation_code")
                    countConfAddresses++
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            Log.d("OPS_PAID_CONFIRM", "countAddresses: $countConfAddresses")

            if (countConfAddresses == jsonArray.length() || countConfAddresses == 0) {
                isConf = true
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return isConf
    }

    private fun clickPaid() {
        btnNext.isEnabled = false
        btnPaid.isEnabled = false
        btnNPaid.isEnabled = false
        btnPaidNoCash.isEnabled = false
        btnChangeFare.isEnabled = false

        statusOrder = ORDER_STATUS_COMPLETED
        requestParams = LinkedHashMap()
        try {
            requestParams["detail_order_data"] = URLEncoder.encode(getJsonOrderData(order).toString(), "UTF-8").replace("+", "%20")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        requestParams["order_id"] = order.orderId
        requestParams["status_new_id"] = ORDER_STATUS_COMPLETED
        requestParams["tenant_login"] = service!!.uri
        requestParams["worker_login"] = driver.callSign

        countDownTimer!!.start()

        if (uuid == null) {
            uuid = presenter.getNewUuid()
        }

        requestType = RequestType.RESERVE

        if (SET_ORDER_PARAMS_VIA_SOCKET) {
            presenter.setOrderStatus(ORDER_STATUS_COMPLETED, type = requestType, params = getJsonOrderData(order).toString())
        } else {
            WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
        }
    }

    private fun clickPaidNoCash() {
        btnNext.isEnabled = false
        btnPaid.isEnabled = false
        btnNPaid.isEnabled = false
        btnPaidNoCash.isEnabled = false
        btnChangeFare.isEnabled = false

        statusOrder = ORDER_STATUS_COMPLETED_NOCASH
        requestParams = LinkedHashMap()
        try {
            requestParams["detail_order_data"] = URLEncoder.encode(getJsonOrderData(order).toString(), "UTF-8").replace("+", "%20")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        requestParams["order_id"] = order.orderId
        requestParams["status_new_id"] = ORDER_STATUS_COMPLETED_NOCASH
        requestParams["tenant_login"] = service!!.uri
        requestParams["worker_login"] = driver.callSign

        countDownTimer!!.start()

        if (uuid == null) {
            uuid = presenter.getNewUuid()
        }

        requestType = RequestType.RESERVE

        if (SET_ORDER_PARAMS_VIA_SOCKET) {
            presenter.setOrderStatus(ORDER_STATUS_COMPLETED_NOCASH, type = requestType, params = getJsonOrderData(order).toString())
        } else {
            WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
        }

        (activity as WorkShiftActivity).uuidPay = uuid
        (activity as WorkShiftActivity).setIsPayBtnActive(false)
    }

    private fun clickNoPaid() {
        btnNext.isEnabled = false
        btnPaid.isEnabled = false
        btnNPaid.isEnabled = false
        btnPaidNoCash.isEnabled = false
        btnChangeFare.isEnabled = false

        statusOrder = ORDER_STATUS_COMPLETED_FALSE
        requestParams = LinkedHashMap()
        try {
            requestParams["detail_order_data"] = URLEncoder.encode(getJsonOrderData(order).toString(), "UTF-8").replace("+", "%20")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

        requestParams["order_id"] = order.orderId
        requestParams["status_new_id"] = ORDER_STATUS_COMPLETED_FALSE
        requestParams["tenant_login"] = service!!.uri
        requestParams["worker_login"] = driver.callSign

        countDownTimer!!.start()

        if (uuid == null) {
            uuid = presenter.getNewUuid()
        }

        requestType = RequestType.RESERVE

        if (SET_ORDER_PARAMS_VIA_SOCKET) {
            presenter.setOrderStatus(ORDER_STATUS_COMPLETED_FALSE, type = requestType, params = getJsonOrderData(order).toString())
        } else {
            WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
        }
    }

    private fun saveCurrentOrder() {
        CoroutineScope(Dispatchers.IO).launch {
            AppDatabase.getInstance(context!!).currentOrderDao().save(currentOrder)
        }
    }

    private inner class ItemReport(val label: String, val desc: Spanned)
}
