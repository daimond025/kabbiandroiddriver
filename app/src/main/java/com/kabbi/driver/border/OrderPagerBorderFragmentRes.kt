package com.kabbi.driver.border

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.helper.ClientTariffParser
import com.kabbi.driver.helper.TypefaceSpanEx
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class OrderPagerBorderFragmentRes {

    internal lateinit var context: Context
    internal lateinit var activity: Activity

    fun getViewTariffs(act: Activity, jsonObject: JSONObject): View {
        val viewTariff = act.layoutInflater.inflate(R.layout.fragment_dialog_tariff, null)
        val linearLayoutCity = viewTariff.findViewById<LinearLayout>(R.id.linearlayout_dialog_city)
        val linearLayoutTrack = viewTariff.findViewById<LinearLayout>(R.id.linearlayout_dialog_track)
        activity = act
        context = act

        try {
            try {
                (viewTariff.findViewById<View>(R.id.textview_titledialog_city) as TextView).text = ((if (jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getInt("isDay") == 1) activity.getString(R.string.day) else activity.getString(R.string.night)) + " "
                        + activity.getString(R.string.city))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            try {
                (viewTariff.findViewById<View>(R.id.textview_titledialog_track) as TextView).text = ((if (jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getInt("isDay") == 1) activity.getString(R.string.day) else activity.getString(R.string.night)) + " "
                        + activity.getString(R.string.track))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val tariffsItemCity = jsonToMap(jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getJSONObject("tariffDataCity"),
                    jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getInt("isDay"), activity.applicationContext)
            val tariffsItemTrack = jsonToMap(jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getJSONObject("tariffDataTrack"),
                    jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getInt("isDay"), activity.applicationContext)

            for (itemTariff in tariffsItemCity) {
                val viewItem = activity.layoutInflater.inflate(R.layout.detail_dialog_tariffs, null)
                (viewItem.findViewById<View>(R.id.textview_clienttariff_label) as TextView).text = itemTariff.label
                (viewItem.findViewById<View>(R.id.textview_clienttariff_cost) as TextView).text = itemTariff.value
                linearLayoutCity.addView(viewItem)
            }

            for (itemTariff in tariffsItemTrack) {
                val viewItem = activity.layoutInflater.inflate(R.layout.detail_dialog_tariffs, null)
                (viewItem.findViewById<View>(R.id.textview_clienttariff_label) as TextView).text = itemTariff.label
                (viewItem.findViewById<View>(R.id.textview_clienttariff_cost) as TextView).text = itemTariff.value
                linearLayoutTrack.addView(viewItem)
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return viewTariff
    }

    private fun jsonToMap(jsonObject: JSONObject, day: Int, context: Context): List<ItemTariff> {
        val itemTariff = ArrayList<ItemTariff>()
        if (day == 1) {
            try {
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff1), jsonObject.getString("planting_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_TIME) {
                    itemTariff.add(ItemTariff(activity.applicationContext.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_day") + " " + activity.getString(R.string.text_time_min)))

                    if (jsonObject.getString("next_cost_unit_day") == ClientTariffParser.COST_UNIT_30)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/30" + activity.getString(R.string.text_time_min)))
                    else if (jsonObject.getString("next_cost_unit_day") == ClientTariffParser.COST_UNIT_60)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_hour)))
                    else
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_min)))

                } else if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_DISTANCE) {
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_day") + " " + activity.getString(R.string.text_dis)))
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_dis)))

                } else if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_MIXED) {
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_day_time") + " " + activity.getString(R.string.text_time_min)))
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_day") + " " + activity.getString(R.string.text_dis)))

                    if (jsonObject.getString("next_cost_unit_day") == ClientTariffParser.COST_UNIT_30)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/30" + activity.getString(R.string.text_time_min)))
                    else if (jsonObject.getString("next_cost_unit_day") == ClientTariffParser.COST_UNIT_60)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_hour)))
                    else
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_min)))

                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_dis)))
                }
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff4), jsonObject.getString("min_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff9), jsonObject.getString("wait_time_day") + " " + activity.getString(R.string.text_time_min)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff5), jsonObject.getString("supply_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff6), jsonObject.getString("wait_driving_time_day") + " " + activity.getString(R.string.text_time_sec)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff7), jsonObject.getString("wait_price_day") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff8), jsonObject.getString("speed_downtime_day") + " " + activity.getString(R.string.text_dis) + "/" + activity.getString(R.string.text_time_hour)))

            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else {
            try {
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff1), jsonObject.getString("planting_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_TIME) {
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_night") + " " + activity.getString(R.string.text_time_min)))

                    if (jsonObject.getString("next_cost_unit_night") == ClientTariffParser.COST_UNIT_30)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/30" + activity.getString(R.string.text_time_min)))
                    else if (jsonObject.getString("next_cost_unit_night") == ClientTariffParser.COST_UNIT_60)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_hour)))
                    else
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_min)))

                } else if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_DISTANCE) {
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_night") + " " + activity.getString(R.string.text_dis)))
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_dis)))

                } else if (jsonObject.getString("accrual") == ClientTariffParser.ACCRUAL_MIXED) {
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_night_time") + " " + activity.getString(R.string.text_time_min)))
                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff2), jsonObject.getString("planting_include_night") + " " + activity.getString(R.string.text_dis)))

                    if (jsonObject.getString("next_cost_unit_night") == ClientTariffParser.COST_UNIT_30)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/30" + activity.getString(R.string.text_time_min)))
                    else if (jsonObject.getString("next_cost_unit_night") == ClientTariffParser.COST_UNIT_60)
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_hour)))
                    else
                        itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night_time") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_time_min)))

                    itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff3), jsonObject.getString("next_km_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context) + "/" + activity.getString(R.string.text_dis)))
                }
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff4), jsonObject.getString("min_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff9), jsonObject.getString("wait_time_night") + " " + activity.getString(R.string.text_time_min)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff5), jsonObject.getString("supply_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff6), jsonObject.getString("wait_driving_time_night") + " " + activity.getString(R.string.text_time_sec)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff7), jsonObject.getString("wait_price_night") + " " + TypefaceSpanEx.getOnlyCurrencyStr(context)))
                itemTariff.add(ItemTariff(activity.getString(R.string.text_client_tariff8), jsonObject.getString("speed_downtime_night") + " " + activity.getString(R.string.text_dis) + "/" + activity.getString(R.string.text_time_hour)))
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        return itemTariff
    }

    private inner class ItemTariff internal constructor(val label: String, val value: String)
}
