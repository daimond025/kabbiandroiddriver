package com.kabbi.driver.border

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.adapters.AddressAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.ChangeBorderTariffEvent
import com.kabbi.driver.fragments.OptionsDialogFragment
import com.kabbi.driver.fragments.TariffDialogFragment
import com.kabbi.driver.helper.*
import com.kabbi.driver.models.Options
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.Utils
import com.loopj.android.http.RequestParams
import kotlinx.android.synthetic.main.fragment_border.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import kotlin.math.roundToLong

class OrderPagerBorderFragment : Fragment(), AdapterView.OnItemClickListener, AsyncHttpTask.AsyncTaskInterface {

    internal lateinit var driver: Driver
    internal lateinit var service: Service
    internal var workDay: WorkDay? = null
    internal lateinit var view: View
    internal lateinit var requestParams: RequestParams
    internal lateinit var appPreferences: AppPreferences
    private lateinit var geoCode: GeoCode
    private lateinit var items: MutableList<HashMap<String, String>>
    private lateinit var asyncHttpTask: AsyncHttpTask
    private lateinit var asyncHttpTaskPost: AsyncHttpTask
    private lateinit var asyncHttpTaskCreateOrder: AsyncHttpTask
    private lateinit var jsonTariff: JSONObject
    private lateinit var createOrderButton: Button
    private lateinit var btnGetOpt: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var phoneLayout: TextInputLayout
    private lateinit var addressLayout: TextInputLayout
    private lateinit var autocomplete: AutoCompleteTextView
    private var active: Boolean = false
    private var createOrder: Boolean = false
    private var jsonTariffs: MutableList<JSONObject>? = null
    private var parkingStart: Parking? = null
    private val optList = ArrayList<Options>()
    private val selectedOptions = ArrayList<String>()
    private lateinit var db: AppDatabase
    private var address: HashMap<String, String> = HashMap()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_border, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.view = view

        db = AppDatabase.getInstance(context!!)
        jsonTariffs = ArrayList()

        asyncHttpTask = AsyncHttpTask(this, activity!!.applicationContext)
        asyncHttpTaskPost = AsyncHttpTask(this, activity!!.applicationContext)
        asyncHttpTaskCreateOrder = AsyncHttpTask(this, activity!!.applicationContext)

        val activity = activity as WorkShiftActivity
        driver = activity.driver!!
        service = activity.service!!
        workDay = activity.workDay
        appPreferences = AppPreferences(activity.applicationContext)
        createOrder = false

        initViews()

        items = ArrayList()

        geoCode = GeoCode(activity, Integer.valueOf(appPreferences.getText("favorite_map")), appPreferences.getText("lat"), appPreferences.getText("lon"))

        requestParams = RequestParams()
        requestParams.add("tenant_login", service.uri)
        requestParams.add("worker_login", driver.callSign)
        asyncHttpTask.get("get_client_tariff", requestParams, driver.secretCode)

        setupListeners()
    }

    private fun initViews() {
        createOrderButton = button_beginorder
        btnGetOpt = add_opt_btn
        progressBar = pbHeaderProgress
        phoneLayout = phone_input_layout
        addressLayout = address_input_layout
        autocomplete = autocomplete_address

        progressBar.visibility = View.VISIBLE
        createOrderButton.visibility = View.GONE
        btnGetOpt.visibility = View.GONE
    }

    private fun setupListeners() {
        val activity = activity as WorkShiftActivity
        val adapter = AddressAdapter(activity, driver, service, "", driver.cityID)
        autocomplete.setAdapter(adapter)

        autocomplete.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.also {
                    if (it.count() < 2) getCallCost() }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                addressLayout.error = null
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.also { if (s.count() > 2) adapter.updateAddresses(it.toString()) }
            }
        })

        autocomplete.setOnItemClickListener { _: AdapterView<*>?, view: View, _: Int, _: Long ->
            val text = ((view as ViewGroup).getChildAt(0) as AppCompatTextView).text
            autocomplete.setText(text)

            address["street"] = text.toString()
            address["lat"] = view.getTag(R.id.tag_lat).toString()
            address["lon"] = view.getTag(R.id.tag_lon).toString()
            address["city_id"] = view.getTag(R.id.tag_cityid).toString()
            address["city"] = ""
            address["house"] = ""
            address["housing"] = ""
            address["porch"] = ""

            items.add(address)
            getCallCost()
        }

        phoneLayout.editText!!.addTextChangedListener(object : PhoneNumberFormattingTextWatcher() {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                super.beforeTextChanged(s, start, count, after)
                phoneLayout.error = null
            }
        })

        phoneLayout.editText!!.setOnFocusChangeListener { _, hasFocus ->
            phoneLayout.hint =
                    if (hasFocus) getString(R.string.text_orderborder_phonehint)
                    else getString(R.string.text_orderborder_phone)
        }

        createOrderButton.setOnClickListener {
            appPreferences = AppPreferences(activity)

            if (geoCode.lat != null
                    && activity.stageOrder == 0
                    && InternetConnection.isOnline(activity)
                    && appPreferences.getText("border_tariff").isNotEmpty()
                    && jsonTariffs!!.size > 0) {

                if (!createOrder) {

                    if (phoneLayout.editText!!.text.isNullOrEmpty()) {
                        phoneLayout.error = "This field is required"
                        return@setOnClickListener
                    }

                    if (autocomplete_address.text.isNullOrEmpty()) {
                        addressLayout.error = "This field is required"
                        return@setOnClickListener
                    }

                    if (address["lat"].isNullOrEmpty() || address["lon"].isNullOrEmpty()) {
                        addressLayout.error = "Wrong address"
                        return@setOnClickListener
                    }

                    btnGetOpt.visibility = View.GONE
                    createOrderButton.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE

                    createOrder = true

                    requestParams = RequestParams()
                    requestParams.add("client_phone", phoneLayout.editText!!.text.replace("\\D+".toRegex(), ""))

                    val jsonArray = JSONArray()

                    for (item in items) {
                        jsonArray.put(JSONObject(item as Map<String, String>))
                    }

                    var tariffId = jsonTariffs!![0].getJSONObject("tariffInfo").getString("tariffId")
                    for (jsonTariff in jsonTariffs!!) {
                        try {
                            if (jsonTariff.getJSONObject("tariffInfo").getString("tariffLabel") == appPreferences.getText("border_tariff"))
                                tariffId = jsonTariff.getJSONObject("tariffInfo").getString("tariffId")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                    val jsonRequestParams = JSONObject()
                    jsonRequestParams.put("address", jsonArray)

                    try {
                        Log.d("CREATE_ORDER: Address: ", jsonRequestParams.toString())
                        requestParams.add("address", URLEncoder.encode(jsonRequestParams.toString(), "UTF-8").replace("+", "%20"))
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    }

                    requestParams.add("tariff_id", tariffId)
                    requestParams.add("tenant_login", service.uri)

                    if (selectedOptions.size > 0)
                        try {
                            val options = selectedOptions.toString().replace("\\[|\\]".toRegex(), "").replace(", ".toRegex(), ",")
                            requestParams.add("option_ids", URLEncoder.encode(options, "UTF-8").replace("+", "%20"))
                        } catch (e: UnsupportedEncodingException) {
                            e.printStackTrace()
                        }

                    requestParams.add("worker_login", driver.callSign)

                    val handler = Handler()
                    handler.postDelayed({ asyncHttpTaskCreateOrder.post("create_order", requestParams, driver.secretCode) }, 1000)
                }
            } else {
                if (geoCode.lat == null) {
                    CustomToast.showMessage(activity, activity.getString(R.string.text_orderborder_error_address))
                } else if (activity.stageOrder != 0) {
                    CustomToast.showMessage(activity, activity.getString(R.string.toast_not_finished_order))
                } else if (!InternetConnection.isOnline(activity)) {
                    CustomToast.showMessage(activity, activity.getString(R.string.text_internet_access))
                } else if (appPreferences.getText("border_tariff").isEmpty()) {
                    CustomToast.showMessage(activity, activity.getString(R.string.error_tariff_empty))
                }
            }
        }

        btnGetOpt.setOnClickListener {
            val dialogFragment = OptionsDialogFragment()
            dialogFragment.options = optList
            dialogFragment.setTitle(activity.getString(R.string.choose_options))
            dialogFragment.listener = OptionsDialogFragment.SelectOptListener { optionsList ->
                selectedOptions.clear()
                for (options in optionsList) {
                    if (options.isChecked)
                        selectedOptions.add(options.optionId)
                }
            }
            dialogFragment.show(fragmentManager!!, "fragmentOptions")
        }

        textview_orderborder_tariff.setOnClickListener {
            val dialogFragment = TariffDialogFragment()
            val args = Bundle()
            val labelTariffs = ArrayList<String>()
            for (jsonTariff in jsonTariffs!!) {
                try {
                    labelTariffs.add(jsonTariff.getJSONObject("tariffInfo").getString("tariffLabel"))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }
            args.putStringArray("tariffs", labelTariffs.toTypedArray())
            dialogFragment.arguments = args
            dialogFragment.show(fragmentManager!!, "dialogFragmentTariff")
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        active = true
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        active = false
    }

    @Subscribe
    fun onEvent(itemEvent: ChangeBorderTariffEvent) {
        try {
            optList.clear()
            getAdditionalOptions()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getCallCost() {
        if (items.size >= 2 && geoCode.street != null && (jsonTariffs != null) and (jsonTariffs!!.size > 0)) {
            try {
                appPreferences = AppPreferences(activity!!)

                requestParams = RequestParams()
                val jsonArray = JSONArray()

                for (item in items) {
                    jsonArray.put(JSONObject(item as Map<String, String>))
                }

                val jsonRequestParams = JSONObject()
                try {
                    jsonRequestParams.put("address", jsonArray)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                var tariffId = jsonTariffs!![0].getJSONObject("tariffInfo").getString("tariffId")
                for (jsonTariff in jsonTariffs!!) {
                    try {
                        if (jsonTariff.getJSONObject("tariffInfo").getString("tariffLabel") == appPreferences.getText("border_tariff"))
                            tariffId = jsonTariff.getJSONObject("tariffInfo").getString("tariffId")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                try {
                    requestParams.add("address", URLEncoder.encode(jsonRequestParams.toString(), "UTF-8").replace("+", "%20"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }

                requestParams.add("order_date", (Date().time / 1000).toString())
                requestParams.add("tariff_id", tariffId)
                requestParams.add("tenant_login", service.uri)
                requestParams.add("worker_login", driver.callSign)
                asyncHttpTaskPost.post("call_cost", requestParams, driver.secretCode)
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: IndexOutOfBoundsException) {
                e.printStackTrace()
            }
        }
    }

    private fun getAdditionalOptions() {
        try {
            if (InternetConnection.isOnline(activity!!)
                    && appPreferences.getText("border_tariff").isNotEmpty()
                    && jsonTariffs!!.size > 0) {

                if (!createOrder) {
                    requestParams = RequestParams()
                    var tariffId = jsonTariffs!![0].getJSONObject("tariffInfo").getString("tariffId")
                    for (jsonTariff in jsonTariffs!!) {
                        try {
                            if (jsonTariff.getJSONObject("tariffInfo").getString("tariffLabel") == appPreferences.getText("border_tariff"))
                                tariffId = jsonTariff.getJSONObject("tariffInfo").getString("tariffId")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                    }
                    requestParams.add("tariff_id", tariffId)
                    requestParams.add("tenant_login", service.uri)
                    requestParams.add("worker_login", driver.callSign)

                    val handler = Handler()
                    handler.postDelayed({ asyncHttpTaskCreateOrder.get("get_additional_options", requestParams, driver.secretCode) }, 1000)
                }
            } else {
                if (!InternetConnection.isOnline(activity!!)) {
                    CustomToast.showMessage(activity, activity!!.getString(R.string.text_internet_access))
                } else if (appPreferences.getText("border_tariff").isEmpty()) {
                    CustomToast.showMessage(activity, activity!!.getString(R.string.error_tariff_empty))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {}

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        if (jsonObject != null && typeJson == "get_client_tariff") {
            try {
                if (active && jsonObject.getString("info") == "OK") {
                    textview_orderborder_tariff.text = jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getString("tariffLabel")
                    add_opt_btn.visibility = View.VISIBLE
                    jsonTariff = jsonObject

                    appPreferences.saveText("border_tariff", jsonObject.getJSONArray("result").getJSONObject(0).getJSONObject("tariffInfo").getString("tariffLabel"))
                }
                val jsonArrayTariffs = jsonObject.getJSONArray("result")
                for (j in 0 until jsonArrayTariffs.length()) {
                    try {
                        jsonTariffs!!.add(jsonArrayTariffs.getJSONObject(j))
                    } catch (e: JSONException) {
                        view.findViewById<View>(R.id.add_opt_btn).visibility = View.GONE
                        e.printStackTrace()
                    }

                }
                progressBar.visibility = View.GONE
                createOrderButton.visibility = View.VISIBLE
                btnGetOpt.visibility = View.GONE
                getAdditionalOptions()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else if (jsonObject != null && typeJson == "call_cost") {
            try {
                if (jsonObject.getString("info") == "OK") {

                    val jsonCost = jsonObject.getJSONObject("result").getJSONObject("cost_data")
                    view.findViewById<View>(R.id.linearlayout_orderborder_cost).visibility = View.VISIBLE
                    ((view.findViewById<View>(R.id.linearlayout_orderborder_cost) as LinearLayout).getChildAt(1) as TextView).text = (TypefaceSpanEx.getCurrencyStr(activity, getView(), jsonCost.getString("summary_cost"))
                            + " " + jsonCost.getString("summary_distance") + " " + activity!!.getString(R.string.text_dis) + " " + jsonCost.getString("summary_time") + " " + activity!!.getString(R.string.text_time_min))

                }
            } catch (e: JSONException) {
                e.printStackTrace()
                view.findViewById<View>(R.id.linearlayout_orderborder_cost).visibility = View.GONE
            }

        } else if (jsonObject != null && typeJson == "get_additional_options") {
            try {
                if (jsonObject.getString("info") == "OK") {
                    val optArray = jsonObject.getJSONArray("result")
                    optList.clear()
                    var i = 0
                    while (optArray.length() > i) {
                        val optObject = optArray.getJSONObject(i)
                        val options = Options(optObject.getString("option"), optObject.getString("id"))
                        optList.add(options)
                        i++
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            progressBar.visibility = View.GONE
            createOrderButton.visibility = View.VISIBLE
            if (optList.size > 0) {
                btnGetOpt.visibility = View.VISIBLE
            }
        } else if (jsonObject != null && typeJson == "create_order") {
            try {
                if (jsonObject.getString("info") == "OK") {
                    requestParams = RequestParams()
                    requestParams.add("order_id", jsonObject.getJSONObject("result").getString("order_id"))
                    requestParams.add("tenant_login", service.uri)
                    requestParams.add("worker_login", driver.callSign)
                    asyncHttpTask.get("get_order_for_id", requestParams, driver.secretCode)

                } else {
                    CustomToast.showMessage(context, jsonObject.getString("info"))
                    createOrder = false
                    progressBar.visibility = View.GONE
                    createOrderButton.visibility = View.VISIBLE
                    btnGetOpt.visibility = View.VISIBLE
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else if (jsonObject != null && typeJson == "get_order_for_id") {
            try {
                if (jsonObject.getString("info") == "OK" && jsonObject.getJSONObject("result").getJSONObject("order_data").getString("costData").length > 3) {
                    Log.d("ORDER_FOR_ID", jsonObject.toString())
                    val jsonObjectOrder = jsonObject.getJSONObject("result").getJSONObject("order_data")

                    runBlocking {
                        withContext(Dispatchers.IO) {
                            AppDatabase.getInstance(context!!).currentOrderDao().deleteAll(driver.id)
                        }
                    }
                    val options = if (jsonObjectOrder.getJSONArray("options").length() > 0) {
                        jsonObjectOrder.getJSONArray("options").toString()
                    } else ""

                    val comment = if (jsonObjectOrder.getString("comment").isNotEmpty() && jsonObjectOrder.getString("comment") != "null") {
                        jsonObjectOrder.getString("comment")
                    } else ""

                    val phone = phoneLayout.editText!!.text.replace("\\D+".toRegex(), "")

                    val currentOrder = CurrentOrder(
                            orderId = jsonObjectOrder.getString("order_id"),
                            timeToClient = 0,
                            beginOrderTime = Date().time,
                            waitTimeBegin = Date().time,
                            waitTimeEnd = Date().time,
                            waitTime = 0,
                            stage = 2,
                            tariffData = jsonObjectOrder.getString("costData"),
                            address = jsonObjectOrder.getString("address"),
                            options = options,
                            comment = comment,
                            phone = phone,
                            driver = driver.id
                    )

                    try {
                        if (jsonObjectOrder.getJSONObject("costData").getInt("enable_parking_ratio") == 1) {
                            val jsonObjectAddress = jsonObjectOrder.getJSONObject("address")
                            parkingStart = Utils.getParkingDopCost((jsonObjectAddress.getJSONObject("A").getString("lat")).toDouble(),
                                    (jsonObjectAddress.getJSONObject("A").getString("lon")).toDouble(), service, context!!)
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    val order = Order()
                    order.pasName = AppParams.ORDER_LABEL_NEW
                    order.address = jsonObjectOrder.getString("address")
                    order.workDay = workDay!!.id
                    order.statusOrder = AppParams.ORDER_LABEL_NEW
                    order.createDatetime = Date().time
                    order.orderId = jsonObjectOrder.getString("order_id")
                    order.orderIdService = jsonObjectOrder.getString("order_number")
                    order.orderTime = jsonObjectOrder.getString("order_time")
                    order.driver = driver.id
                    order.handBrake = jsonObjectOrder.getJSONObject("tariff").getString("auto_downtime")
                    order.day = jsonObjectOrder.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay") == 1
                    order.tariffLabel = jsonObjectOrder.getJSONObject("tariff").getString("name")

                    try {
                        order.dopCost = (jsonObjectOrder.getJSONObject("costData").getString("additionals_cost")).toDouble()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (!jsonObjectOrder.isNull("promo_code_discount")) {
                        order.promoCodeDiscount = jsonObjectOrder.getInt("promo_code_discount")
                    }
                    val parser = ClientTariffParser()
                    val tariffCity = parser.createTariff(jsonObjectOrder.getJSONObject("costData").getJSONObject("tariffInfo").getJSONObject("tariffDataCity"),
                            jsonObjectOrder.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay"), ClientTariffParser.AREA_CITY, jsonObjectOrder)

                    val tariffTrack = parser.createTariff(jsonObjectOrder.getJSONObject("costData").getJSONObject("tariffInfo").getJSONObject("tariffDataTrack"),
                            jsonObjectOrder.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay"), ClientTariffParser.AREA_TRACK, jsonObjectOrder)

                    if (jsonObjectOrder.getJSONObject("costData").getInt("is_fix") == 1 && order.promoCodeDiscount > 0) {
                        if (parkingStart != null) {
                            order.costOrder = Utils.addParkingCost(parkingStart!!, (jsonObjectOrder.getJSONObject("costData").getString("summary_cost_no_discount")).toDouble(), 0)!!
                        } else {
                            order.costOrder = (jsonObjectOrder.getJSONObject("costData").getString("summary_cost_no_discount")).toDouble()
                        }
                    } else {
                        if (parkingStart != null) {
                            order.costOrder = Utils.addParkingCost(parkingStart!!, java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("costData").getString("summary_cost")), 0)!!
                        } else {
                            order.costOrder = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("costData").getString("summary_cost"))
                        }
                    }

                    if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1") {
                        order.fix = 1
                        tariffCity.accrual = (ClientTariffParser.ACCRUAL_WIFI)
                        tariffTrack.accrual = (ClientTariffParser.ACCRUAL_WIFI)
                    } else {
                        order.fix = jsonObjectOrder.getJSONObject("costData").getInt("is_fix")
                    }

                    order.timeWaitCityClient = tariffCity.waitTime * -60000
                    order.timeWaitTrackClient = tariffTrack.waitTime * -60000

                    if (order.onlyCity) {
                        order.startPoint = "in"
                    } else {
                        order.startPoint = jsonObjectOrder.getJSONObject("costData").getString("start_point_location")
                    }

                    try {
                        order.fixCityCost = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("costData").getString("city_cost"))
                        order.fixOutCityCost = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("costData").getString("out_city_cost"))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    order.clientPhone = phone

                    if (jsonObjectOrder.getJSONObject("costData").getString("start_point_location") == "in") {
                        if (parkingStart != null) {
                            order.plantingCost = Utils.addParkingCost(parkingStart!!, tariffCity.plantingPrice, 0)!!
                        } else {
                            order.plantingCost = tariffCity.plantingPrice
                        }
                        order.minPrice = tariffCity.minPrice
                        order.rounding = (if (tariffCity.rounding == 0.0) 1.0 else tariffCity.rounding)
                        order.roundingType = tariffCity.roundingType

                        when (tariffCity.accrual) {
                            ClientTariffParser.ACCRUAL_TIME -> order.timeCityInOrder = tariffCity.plantingInclude.roundToLong() * -60000
                            ClientTariffParser.ACCRUAL_DISTANCE -> order.disCityInOrder = -tariffCity.plantingInclude
                            ClientTariffParser.ACCRUAL_MIXED -> {
                                order.timeCityInOrder = tariffCity.plantingIncludeTime.roundToLong() * -60000
                                order.disCityInOrder = -tariffCity.plantingInclude
                            }
                            ClientTariffParser.ACCRUAL_INTERVAL -> order.disCityInOrder = -tariffCity.plantingInclude
                        }
                    } else {
                        if (parkingStart != null) {
                            order.plantingCost = Utils.addParkingCost(parkingStart!!, tariffTrack.plantingPrice, 0)!!
                        } else {
                            order.plantingCost = tariffTrack.plantingPrice
                        }
                        order.minPrice = tariffTrack.minPrice
                        order.rounding = (if (tariffTrack.rounding == 0.0) 1.0 else tariffTrack.rounding)
                        order.roundingType = tariffTrack.roundingType

                        when (tariffTrack.accrual) {
                            ClientTariffParser.ACCRUAL_TIME -> order.timeTrackInOrder = tariffTrack.plantingInclude.roundToLong() * -60000
                            ClientTariffParser.ACCRUAL_DISTANCE -> order.disTrackInOrder = -tariffTrack.plantingInclude
                            ClientTariffParser.ACCRUAL_MIXED -> {
                                order.timeTrackInOrder = tariffTrack.plantingIncludeTime.roundToLong() * -60000
                                order.disTrackInOrder = -tariffTrack.plantingInclude
                            }
                            ClientTariffParser.ACCRUAL_INTERVAL -> order.disTrackInOrder = -tariffTrack.plantingInclude
                        }
                    }

                    order.typeCost = "CASH"

                    try {
                        order.enableParkingRatio = jsonObjectOrder.getJSONObject("costData").getInt("enable_parking_ratio")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val intent = Intent(activity, WorkShiftActivity::class.java)
                    intent.putExtra("showDialogGps", false)
                    intent.putExtra("service_id", service.id)
                    intent.putExtra("workday_id", (activity as WorkShiftActivity).workDay!!.id)
                    intent.putExtra("time", 0)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

                    try {
                        appPreferences.saveText("hand_brake", jsonObjectOrder.getJSONObject("tariff").getString("auto_downtime"))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    try {
                        appPreferences.saveText("require_point_confirmation_code", jsonObjectOrder.getJSONObject("settings").getString("require_point_confirmation_code"))
                        appPreferences.saveText("confirm_codes", "")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    intent.putExtra("showDialog", "empty")

                    try {

                        runBlocking {
                            withContext(Dispatchers.IO) {
                                currentOrder.id = db.currentOrderDao().save(currentOrder)

                                order.curOrderId = currentOrder.id
                                order.id = db.orderDao().save(order)

                                tariffCity.order = order.id
                                tariffTrack.order = order.id

                                db.clientTariffDao().save(tariffCity)
                                db.clientTariffDao().save(tariffTrack)
                            }
                        }

                        val route = Route()
                        route.order = order.id
                        route.parking = "base"
                        route.lat = appPreferences.getText("lat").toDouble()
                        route.lon = appPreferences.getText("lon").toDouble()
                        route.speed = 0.0
                        route.bearing = 0f
                        route.datetime = Date().time
                        runBlocking {
                            withContext(Dispatchers.IO) {
                                db.routeDao().save(route)
                            }
                        }

                        intent.putExtra("stageOrder", 1)
                    } catch (e: Exception) {
                        intent.putExtra("stageOrder", 0)
                        CustomToast.showMessage(activity, "Save Order - Error")
                    }

                    val view = activity!!.currentFocus
                    if (view != null) {
                        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(view.windowToken, 0)
                    }

                    val activity = activity as WorkShiftActivity

                    activity.order = order
                    activity.currentOrder = currentOrder
                    activity.stageOrder = 1
                    activity.onNavigationDrawerItemSelected(1)

                } else {
                    CustomToast.showMessage(activity, "Empty tariff")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                createOrder = false
            }

        } else if (jsonObject == null) {
            createOrder = false
            progressBar.visibility = View.GONE
            createOrderButton.visibility = View.VISIBLE
        }
    }
}