package com.kabbi.driver

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.facebook.common.executors.CallerThreadExecutor
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.CarDao
import com.kabbi.driver.database.dao.SoundDao
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.events.taximeter.DestroyMainActivityEvent
import com.kabbi.driver.finish.FinishOrderFragment
import com.kabbi.driver.fragments.*
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.myOrders.OrderRaw
import com.kabbi.driver.myOrders.OwnOrdersListFragment
import com.kabbi.driver.network.WebService
import com.kabbi.driver.network.responses.GetProfileResponse
import com.kabbi.driver.offer.OrderOfferActivity
import com.kabbi.driver.service.MainService
import com.kabbi.driver.taximeter.TaximeterFragment
import com.kabbi.driver.util.*
import com.loopj.android.http.RequestParams
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class WorkShiftActivity : BaseActivity(), NavigationDrawerWorkFragment.NavigationDrawerCallbacks, AsyncHttpTask.AsyncTaskInterface, Runnable {

    private val applicationUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private val job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)
    private var checkingForActiveOrder = false

    private lateinit var receiverTracking: BroadcastReceiver
    private lateinit var broadcastReceiverLang: BroadcastReceiver
    private lateinit var mBroadcastReceiverBluetooth: BroadcastReceiver
    private lateinit var preorderReceiver: BroadcastReceiver

    private lateinit var soundDao: SoundDao
    private lateinit var carDao: CarDao

    private lateinit var tvCountSat: TextView
    private lateinit var viewIndicator: View

    private lateinit var point: LatLng
    private lateinit var mBluetoothDevice: BluetoothDevice

    internal lateinit var appPreferences: AppPreferences
    internal lateinit var toolbar: Toolbar
    internal lateinit var ivAccuracyGps: ImageView
    internal lateinit var tvAccuracyGps: TextView
    internal lateinit var requestParams: RequestParams
    internal lateinit var mBluetoothAdapter: BluetoothAdapter

    internal var dialog: AlertDialog? = null
    internal var progressDialog: ProgressDialog? = null

    private var asyncHttpTask = AsyncHttpTask(this, this)
    private var createWorkDay: Boolean = false
    private var isPayBtnActive: Boolean = false
    private var asyncHttpTaskPost = AsyncHttpTask(this, this)
    private var mNavigationDrawerFragment: NavigationDrawerWorkFragment? = null
    private var mTitle: CharSequence? = null
    private var freeDriver = true
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var lifetimeLocation: Long = 0
    private var currentLocationTime: Long = 0
    private var showDialogRejected: Int = 0
    private var lifecycleState: String? = null
    private var mediaPlayer: MediaPlayer? = null
    private var mBluetoothSocket: BluetoothSocket? = null
    private var dateFormat: SimpleDateFormat? = null
    private var bitmapLogo: Bitmap? = null

    private var orderId: String = ""
    private var uuid: String = ""
    private lateinit var orderRaw: OrderRaw

    var provider: String? = null
        private set
    var serverTime: Long = 0
        private set
    var currentLat: Double = 0.toDouble()
        private set
    var currentLon: Double = 0.toDouble()
        private set
    var service: Service? = null
        internal set
    var driver: Driver? = null
        internal set
    var workDay: WorkDay? = null
        internal set

    var order: Order? = null
    var currentOrder: CurrentOrder? = null
    var updatePark: Boolean = false
    var millisUntilFinished: Long = 0
    var uuidPay: String? = null
    var stageOrder: Int = 0
    var stage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val database = AppDatabase.getInstance(this)
        carDao = database.carDao()
        soundDao = database.soundDao()
        appPreferences = AppPreferences(this)

        if (savedInstanceState == null) {
            isPayBtnActive = true
            millisUntilFinished = 0
            uuidPay = ""
        } else {
            isPayBtnActive = savedInstanceState.getBoolean("isBtnActive")
            millisUntilFinished = savedInstanceState.getLong("millis")
            uuidPay = savedInstanceState.getString("uuidPay")
        }

        EventBus.getDefault().register(this)
        EventBus.getDefault().post(DestroyMainActivityEvent(false))

        workDay = (application as App).workDay
        driver = (application as App).driver
        service = (application as App).service

        uiScope.launch {
            initMediaPlayer()
            mediaPlayer?.isLooping = true
        }

        currentLat = 0.0
        currentLon = 0.0

        showDialogRejected = 0
        lifecycleState = "onCreate"

        if (appPreferences.getText("print_check") == "1") {
            uriToBitmap(service)
            dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
            bluetoothConnect(mBluetoothAdapter)
            mBroadcastReceiverBluetooth = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)) {
                        BluetoothAdapter.STATE_ON -> {
                            Toast.makeText(applicationContext, getString(R.string.device_on), Toast.LENGTH_LONG).show()
                            bluetoothConnect(mBluetoothAdapter)
                        }
                        BluetoothAdapter.STATE_OFF -> {
                            Toast.makeText(applicationContext, getString(R.string.device_off), Toast.LENGTH_LONG).show()

                            try {
                                if (mBluetoothSocket != null)
                                    mBluetoothSocket!!.close()
                            } catch (e: Exception) {
                                Log.e("Tag", "Exe ", e)
                            }
                        }
                    }
                }
            }
            registerReceiver(mBroadcastReceiverBluetooth, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        }

        runBlocking {
            val db = AppDatabase.getInstance(this@WorkShiftActivity)
            if (service == null || driver == null || workDay == null) {
                try {
                    withContext(Dispatchers.IO) {
                        service = db.serviceDao().getById((appPreferences.getText("service_id")).toLong())
                        driver = db.driverDao().getById((appPreferences.getText("driver_id")).toLong())
                        workDay = db.workDayDao().getById((appPreferences.getText("workday_id")).toLong())
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    finish()
                }
            }

            try {
                withContext(Dispatchers.IO) {
                    currentOrder = db.currentOrderDao().getCurOrder(driver!!.id)
                    if (currentOrder != null) {
                        order = db.orderDao().getByOrderId(currentOrder!!.orderId)
                        stageOrder = if (currentOrder!!.stage == 3) 2 else 1
                    }
                }
            } catch (e: Exception) {
                stageOrder = 0
                e.printStackTrace()
            }

            setTheme(Integer.valueOf(appPreferences.getText("theme")))
            setContentView(R.layout.activity_workshift)

            typedValueMain = TypedValue()
            typedValueSubscribe = TypedValue()

            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD)

            updatePark = false
            createWorkDay = false
            currentLocationTime = Date().time
            lifetimeLocation = 0
            serverTime = 0

            provider = "network"

            mNavigationDrawerFragment = supportFragmentManager.findFragmentById(R.id.navigation_drawer) as NavigationDrawerWorkFragment?

            mTitle = getString(R.string.text_menu_field_authrize1)

            toolbar = findViewById(R.id.toolbar_gps)
            setSupportActionBar(toolbar)
            supportActionBar!!.title = ""

            ivAccuracyGps = findViewById(R.id.imageview_toolbar_gps)
            tvAccuracyGps = findViewById(R.id.textview_toolbar_provider)
            tvCountSat = findViewById(R.id.textview_count_sat)
            viewIndicator = findViewById(R.id.view_indicator)

            mNavigationDrawerFragment!!.setUp(R.id.navigation_drawer, findViewById<View>(R.id.drawer_layout) as androidx.drawerlayout.widget.DrawerLayout)

            try {
                if (stageOrder < 2 && intent.getBooleanExtra("showDialogGps", false)) {
                    showProgressDialog()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            appPreferences.saveText("service_id", service!!.id.toString())
            appPreferences.saveText("driver_id", driver!!.id.toString())

            try {
                if (intent.extras!!.getString("showDialog") == "rejected") {
                    CustomToast.showMessage(this@WorkShiftActivity, getString(R.string.text_rejected_dialog))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            loadDriverProfile()
        }
    }

    fun checkIfHasActiveOrder() {
        uuid = UUID.randomUUID().toString()

        val requestParams = LinkedHashMap<String, String>()
        requestParams["tenant_login"] = service?.uri ?: ""
        requestParams["worker_login"] = driver?.callSign ?: ""
        checkingForActiveOrder = true
        WebService.getOwnOrders(this, requestParams, driver?.secretCode)
    }

    @Subscribe
    fun onEvent(ownOrdersEvent: NetworkOwnOrdersEvent) {
        if (!checkingForActiveOrder) return
        checkingForActiveOrder = false

        if (ownOrdersEvent.status == "OK") {

            if (ownOrdersEvent.activeOrders.isNotEmpty()) {

                orderId = ownOrdersEvent.activeOrders[0].orderId

                val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
                requestParams["order_id"] = orderId
                requestParams["tenant_login"] = service!!.uri
                requestParams["worker_login"] = driver!!.callSign
                WebService.getOrderForId(requestParams, driver!!.secretCode)
            }
        }
    }

    @Subscribe
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {
        if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 2) {
            EventBus.getDefault().post(PushResponseEvent(uuid, orderStatusEvent.status, 1, JSONObject()))
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {

        runOnUiThread {
            if (progressDialog != null && progressDialog!!.isShowing)
                progressDialog!!.dismiss()
        }

        if (pushResponseEvent.uuid == uuid) {
            when (pushResponseEvent.infoCode) {

                "OK" -> buildOrder(pushResponseEvent.result, orderId, orderRaw, this)

                "ORDER_IS_BUSY" -> runOnUiThread { CustomToast.showMessage(this, getString(R.string.error_order_is_busy)) }

                "EMPTY_DATA_IN_DATABASE" -> runOnUiThread { CustomToast.showMessage(this, getString(R.string.error_empty)) }

                "DRIVER_BLOCKED" -> runOnUiThread { CustomToast.showMessage(this, getString(R.string.error_driver_blocked)) }

                "SHIFT_IS_CLOSED" -> runOnUiThread { CustomToast.showMessage(this, getString(R.string.status_shift_closed)) }

                "DRIVER_PRE_ORDER_BLOCKED" -> {
                    runOnUiThread { CustomToast.showMessage(this, getString(R.string.error_driver_blocked)) }
                }
            }
        }
    }

    private suspend fun initMediaPlayer() = withContext(Dispatchers.IO) {
        var sound: Sound? = null
        withContext(Dispatchers.IO) {
            sound = soundDao.get("ringtone_rejected")
        }
        sound?.also {
            mediaPlayer = if (it.isDefault) {
                MediaPlayer.create(this@WorkShiftActivity, Uri.parse("android.resource://" + packageName + "/"
                        + Utils.getResourceId(applicationContext, "rejected", Locale.getDefault().language)))
            } else {
                MediaPlayer.create(this@WorkShiftActivity, Uri.parse(it.path))
            }
        }
    }

    fun setIsPayBtnActive(isActive: Boolean) {
        this.isPayBtnActive = isActive
    }

    fun getIsPayBtnActive(): Boolean {
        return this.isPayBtnActive
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong("millis", millisUntilFinished)
        outState.putBoolean("isBtnActive", isPayBtnActive)
        outState.putString("uuidPay", uuidPay)
        super.onSaveInstanceState(outState)
    }

    private fun showProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(getString(R.string.search_sattelite_start))
        progressDialog!!.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.text_reload_pingservice)) { dialog, _ ->
            try {
                stopService(Intent(applicationContext, MainService::class.java))
                startPingService()
                dialog.dismiss()
                showProgressDialog()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        if (stageOrder == 0) {
            progressDialog!!.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.close_workshift)) { dialog, _ ->
                try {
                    val requestParams = LinkedHashMap<String, String>()
                    requestParams["car_mileage"] = "0"
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver!!.callSign
                    WebService.endWork(requestParams, driver!!.secretCode)
                    dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    override fun onResume() {
        super.onResume()

        try {
            if (intent.extras!!.getString("showDialog") == "push") {
                when (intent.extras!!.getString("push")) {
                    "update_free_orders" -> mNavigationDrawerFragment!!.reSelectItem(2)
                    "update_pre_orders" -> mNavigationDrawerFragment!!.reSelectItem(2)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        appPreferences = AppPreferences(this)
        if (appPreferences.getText("parking").isNotEmpty()) {
            var parking: Parking? = null
            runBlocking {
                withContext(Dispatchers.IO) {
                    parking = AppDatabase.getInstance(this@WorkShiftActivity).parkingDao().get(appPreferences.getText("parking"))
                }
            }
            setParkingQueue(parking)
        }

        freeDriver = true
    }

    override fun onNavigationDrawerItemSelected(position: Int) {

        val fragmentManager = supportFragmentManager
        onSectionAttached(position)

        if (mNavigationDrawerFragment != null) {
            mNavigationDrawerFragment!!.setMenuItem(position)
        }

        val fragment = when (position) {
            1 -> stageFragment()
            2 -> OrderListFragment()
            3 -> OwnOrdersListFragment()
            4 -> profileFragment()
            5 -> ChatFragment()
            else -> SosFragment()
        }
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit()
    }

    private fun stageFragment(): Fragment {
        return when (stageOrder) {
            0 -> WorkDayFragment()
            1 -> TaximeterFragment()
            else -> FinishOrderFragment()
        }
    }

    private fun profileFragment(): ProfileFragment {
        val fragmentProfile = ProfileFragment()
        val args = Bundle()
        args.putBoolean("autorize", true)
        fragmentProfile.arguments = args
        return fragmentProfile
    }

    fun startPingService() {
        startService(Intent(applicationContext, MainService::class.java).putExtra("driver_id", driver!!.id))
    }

    private fun onSectionAttached(number: Int) {
        mTitle = when (number) {
            1 -> getString(R.string.text_menu_field_authrize1)
            2 -> getString(R.string.text_menu_field_authrize2)
            3 -> getString(R.string.text_menu_field_authrize5)
            4 -> getString(R.string.text_menu_field_authrize3)
            5 -> getString(R.string.text_menu_field_chat)
            else -> getString(R.string.text_menu_field_authrize4)
        }
    }

    private fun restoreActionBar() {
        val actionBar = supportActionBar
        actionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        actionBar.setDisplayShowTitleEnabled(true)
        toolbar.title = "  " + mTitle!!
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (!mNavigationDrawerFragment!!.isDrawerOpen) {
            restoreActionBar()
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onStart() {
        super.onStart()

        lifecycleState = "onStart"

        if (showDialogRejected == 1) {
            showDialogRejected = 0
            ChangeDialogFragment().show(supportFragmentManager, "dialogFragment_rejected")
        }

        initReceivers()

        EventBus.getDefault().post(ShowWindowViewEvent(false))
    }

    private fun initReceivers() {

        receiverTracking = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                try {
                    if (progressDialog != null && progressDialog!!.isShowing) {
                        progressDialog!!.dismiss()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                try {

                    when {
                        intent.extras!!.getFloat(AppParams.ACCURACY_GPS) <= AppParams.ACCURACY_GPS_HIGHT -> ivAccuracyGps.setImageResource(R.drawable.gps_hight_level)
                        intent.extras!!.getFloat(AppParams.ACCURACY_GPS) <= AppParams.ACCURACY_GPS_MIDLE -> ivAccuracyGps.setImageResource(R.drawable.gps_midle_level)
                        else -> ivAccuracyGps.setImageResource(R.drawable.gps_low_level)
                    }

                    if (intent.extras!!.getString("provider") == LocationManager.NETWORK_PROVIDER) {
                        tvAccuracyGps.text = " nw "
                    } else {
                        tvAccuracyGps.text = " gps "
                    }

                    if (currentLocationTime - lifetimeLocation > 60000) {
                        tvAccuracyGps.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
                    } else {
                        tvAccuracyGps.setTextColor(ContextCompat.getColor(applicationContext, R.color.green))
                    }

                    lifetimeLocation = intent.extras!!.getLong("lifetime")

                    provider = intent.extras!!.getString("provider")
                } catch (e3: Exception) {
                    e3.printStackTrace()
                    ivAccuracyGps.setImageResource(R.drawable.gps_zero_level)
                }
            }
        }
        registerReceiver(receiverTracking, IntentFilter(AppParams.BROADCAST_ORDER))

        broadcastReceiverLang = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    toolbar.title = "  " + getString(R.string.text_menu_field_authrize3)
                    toolbar.setTitleTextColor(resources.getColor(R.color.white))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        val manager = androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this)
        manager.registerReceiver(broadcastReceiverLang, IntentFilter(AppParams.BROADCAST_LANG))

        preorderReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Snackbar.make(findViewById(R.id.drawer_layout), R.string.time_to_order, Snackbar.LENGTH_LONG).show()
            }
        }
        manager.registerReceiver(preorderReceiver, IntentFilter(AppParams.BROADCAST_PREORDER))
    }

    @Subscribe
    fun onEvent(endWorkEvent: NetworkEndWorkEvent) {
        if (endWorkEvent.status == "OK") {
            workDay!!.endWorkDay = Calendar.getInstance().time.time
            saveWorkDay()

            try {
                stopService(Intent(applicationContext, MainService::class.java))
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            try {
                appPreferences.saveText("in_shift", "0")

                val intent = Intent(applicationContext, WorkActivity::class.java)
                intent.putExtra("service_id", service!!.id)
                startActivity(intent)
                finish()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    @Subscribe
    fun onEvent(themeEvent: ThemeEvent) {
        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
    }

    @Subscribe
    fun onEvent(trackingEvent: TrackingEvent) {

        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }

        try {

            currentLon = trackingEvent.lon
            currentLat = trackingEvent.lat

            when {
                trackingEvent.accuracyGps <= AppParams.ACCURACY_GPS_HIGHT -> {
                    ivAccuracyGps.setImageResource(R.drawable.gps_hight_level)
                }
                trackingEvent.accuracyGps <= AppParams.ACCURACY_GPS_MIDLE -> {
                    ivAccuracyGps.setImageResource(R.drawable.gps_midle_level)
                }
                else -> {
                    ivAccuracyGps.setImageResource(R.drawable.gps_low_level)
                }
            }

            if (trackingEvent.provider == LocationManager.NETWORK_PROVIDER) {
                tvAccuracyGps.text = " nw "
            } else {
                tvAccuracyGps.text = " gps "
            }

            if (currentLocationTime - lifetimeLocation > 60000) {
                tvAccuracyGps.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
            } else {
                tvAccuracyGps.setTextColor(ContextCompat.getColor(applicationContext, R.color.green))
            }

            lifetimeLocation = trackingEvent.lifetime

            provider = trackingEvent.provider
        } catch (e3: Exception) {
            e3.printStackTrace()
            ivAccuracyGps.setImageResource(R.drawable.gps_zero_level)
        }
    }

    @Subscribe
    fun onEvent(workerPositionEvent: NetworkWorkerPositionEvent) {
        try {
            serverTime = workerPositionEvent.cityTime

            appPreferences = AppPreferences(this)
            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                CustomToast.showMessage(applicationContext, getString(R.string.gps_disabled))

            if (appPreferences.getText("parking").isEmpty() && !updatePark) {
                point = LatLng(appPreferences.getText("lat").toDouble(), appPreferences.getText("lon").toDouble())
                val requestParams = LinkedHashMap<String?, String?>()
                requestParams["tenant_login"] = service!!.uri
                requestParams["worker_city_id"] = driver!!.cityID
                requestParams["worker_login"] = driver!!.callSign
                WebService.getParkingList(applicationContext, requestParams, secretCode = driver!!.secretCode, serviceId = service!!.id, point = point)
                updatePark = true
            }

            if (workerPositionEvent.status == "SHIFT_IS_CLOSED" && stageOrder == 0) {
                if (appPreferences.getText("control_own_car_mileage") == "1" && appPreferences.getText("car_company") == "true") {
                    if (dialog == null || !dialog!!.isShowing) {
                        val viewDialog = layoutInflater.inflate(R.layout.fragment_dialog_edittext_mileage, null)
                        val alertDialog = AlertDialog.Builder(this)
                        alertDialog.setView(viewDialog)
                        alertDialog.setTitle(getString(R.string.dialog_endworkday1))
                        alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { _, _ -> }
                        dialog = alertDialog.create()
                        dialog!!.show()
                        dialog!!.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                            val mileage = (viewDialog.findViewById<View>(R.id.et_mileage) as EditText).text.toString()
                            if (mileage.trim { it <= ' ' }.isNotEmpty()) {
                                val requestParams = LinkedHashMap<String, String>()
                                requestParams["car_mileage"] = mileage
                                requestParams["shift_id"] = workDay!!.shiftId
                                requestParams["tenant_login"] = service!!.uri
                                requestParams["worker_login"] = driver!!.callSign

                                WebService.setCarMileage(requestParams, driver!!.secretCode)
                                dialog!!.dismiss()
                            } else {
                                (viewDialog.findViewById<View>(R.id.et_mileage) as EditText).error = getString(R.string.edittext_valid_required)
                            }
                        }
                    }
                } else {
                    closeShift()
                }
            }

            if (workerPositionEvent.status == "REQUIRED_INSPECTION_CAR") {
                val controlIntent = Intent(applicationContext, CarInspectionActivity::class.java)
                startActivity(controlIntent)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(carMileage: NetworkCarMileage) {
        when (carMileage.status) {
            "OK" -> closeShift()
            "INVALID_CAR_MILEAGE" -> CustomToast.showMessage(applicationContext, getString(R.string.text_hint_mileage) + " " + carMileage.currentMileage)
            else -> CustomToast.showMessage(applicationContext, carMileage.status)
        }
    }

    @Subscribe
    fun onEvent(connectedEvent: ConnectedEvent) {

        viewIndicator.setBackgroundResource(if (connectedEvent.isConnected) R.drawable.custom_view_circle_green else R.drawable.custom_view_circle_red)

        currentLocationTime = Date().time

        if (currentLocationTime - lifetimeLocation > 60000) {
            tvAccuracyGps.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
        }
    }

    @SuppressLint("SetTextI18n")
    @Subscribe
    fun onEvent(satelliteInfoEvent: SatelliteInfoEvent) {
        tvCountSat.text = "${satelliteInfoEvent.satelliteCoustUsed}/${satelliteInfoEvent.satelliteCount}"
        try {
            if (progressDialog != null && progressDialog!!.isShowing) {
                progressDialog!!.setMessage(String.format(getString(R.string.search_sattelite), " " + satelliteInfoEvent.satelliteCoustUsed + "/" + satelliteInfoEvent.satelliteCount))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                INTENT_ORDER_OFFER -> {
                    runBlocking {
                        stageOrder = data!!.extras!!.getInt("stageOrder")
                        withContext(Dispatchers.IO) {
                            val db = AppDatabase.getInstance(this@WorkShiftActivity)
                            order = db.orderDao().getByOrderId(data.extras!!.getString("order_id", ""))
                            currentOrder = db.currentOrderDao().getCurOrder(driver!!.id)
                        }
                        onNavigationDrawerItemSelected(1)
                    }
                }
                ProfileFragment.RESULT_PRINTER -> try {
                    appPreferences.saveText("device_printer", data!!.getStringExtra("address"))
                    textview_profile_printer.text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                            + getString(R.string.profile_printer) + "<br>" + data.getStringExtra("address") + "</font></big>")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val fragmentManager = supportFragmentManager
        val bundle = Bundle()
        val fragmentTransaction = fragmentManager.beginTransaction()
        try {
            if (intent.extras!!.containsKey("intent_type")) {
                mNavigationDrawerFragment!!.reSelectItem(2)
                when (intent.extras!!.getString("intent_type")) {
                    "order_is_rejected" -> {
                        val ownOrdersFragment = OwnOrdersListFragment()
                        fragmentTransaction.replace(R.id.container, ownOrdersFragment)
                        fragmentTransaction.commitAllowingStateLoss()
                    }
                    "update_free_orders" -> {
                        val freeOrdersFragment = OrderListFragment()
                        fragmentTransaction.replace(R.id.container, freeOrdersFragment)

                        bundle.putInt("page", 0)
                        freeOrdersFragment.arguments = bundle
                        fragmentTransaction.commitAllowingStateLoss()
                    }
                    "update_pre_orders", "remove_from_reserved_pre_order" -> {
                        val freeOrdersFragment = OrderListFragment()
                        fragmentTransaction.replace(R.id.container, freeOrdersFragment)
                        bundle.putInt("page", 1)
                        freeOrdersFragment.arguments = bundle
                        fragmentTransaction.commitAllowingStateLoss()
                    }
                }
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(updateOwnOrdersEvent: UpdateOwnOrdersEvent) {
        when (updateOwnOrdersEvent.type) {
            "order" -> if (stageOrder == 0 && freeDriver && updateOwnOrdersEvent.action == "add") {

                mediaPlayer?.pause()
                freeDriver = false
                val orderIntent = Intent(this, OrderOfferActivity::class.java)
                orderIntent.putExtra("order_id", updateOwnOrdersEvent.orderId)
                orderIntent.putExtra("service_id", service!!.id)
                orderIntent.putExtra("workday_id", workDay!!.id)
                orderIntent.putExtra("free_order", 1)
                orderIntent.putExtra("type_order", "free")
                orderIntent.putExtra("time", Date().time)
                orderIntent.putExtra("source", "activity")
                orderIntent.putExtra("server_time", serverTime)
                orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                orderIntent.putExtra("type_source", 0)
                orderIntent.putExtra("delete", false)
                orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_ASSIGNED_CANCEL)
                orderIntent.putExtra("music", true)
                startActivityForResult(orderIntent, INTENT_ORDER_OFFER)
            }

            "preorder" -> {
            }
        }
    }

    @Subscribe
    fun onEvent(pushOrderEvent: PushOrderEvent) {
        when (pushOrderEvent.command) {
            "order_new" -> if (stageOrder == 0 && freeDriver) {

                mediaPlayer?.pause()
                freeDriver = false
                val orderIntent = Intent(this, OrderOfferActivity::class.java)
                orderIntent.putExtra("order_id", pushOrderEvent.orderId)
                orderIntent.putExtra("service_id", service!!.id)
                orderIntent.putExtra("workday_id", workDay!!.id)
                orderIntent.putExtra("free_order", 0)
                orderIntent.putExtra("type_order", "free")
                orderIntent.putExtra("time", Date().time)
                orderIntent.putExtra("source", "activity")
                orderIntent.putExtra("server_time", serverTime)
                orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                orderIntent.putExtra("type_source", 0)
                orderIntent.putExtra("delete", false)
                orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_CANCEL)
                orderIntent.putExtra("offer_sec", pushOrderEvent.secToClose)
                orderIntent.putExtra("music", true)

                startActivityForResult(orderIntent, INTENT_ORDER_OFFER)
            }

            "order_is_rejected" -> rejectOrder(pushOrderEvent.orderId, "order_is_rejected")

            "reject_worker_from_order" -> rejectOrder(pushOrderEvent.orderId, "reject_worker_from_order")

            "complete_order" -> rejectOrder(pushOrderEvent.orderId, "complete_order")
        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            if (order != null && stageOrder > 0 && order!!.orderId == orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id")) {
                if (orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONObject("status").getString("status_group") == "car_at_place") {
                    stage = 1
                } else if (orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONObject("status").getString("status_group") == "executing") {
                    stage = 2
                }

                if (orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONArray("options") != null) {
                    val jsonArrayOptions = orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONArray("options")
                    findViewById<LinearLayout>(R.id.linearlayout_taximeterorder_options)?.also { linearLayoutOptions ->

                        linearLayoutOptions.removeAllViews()
                        val views = ArrayList<View>()

                        var countOption = 1

                        var layoutView: View? = null
                        for (j in 0 until jsonArrayOptions.length()) {
                            val jsonObjectOption = jsonArrayOptions.getJSONObject(j)

                            when (countOption) {
                                1 -> {
                                    val layoutInflater = layoutInflater
                                    layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_option, null, false)
                                    (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option1) as TextView).text = jsonObjectOption.getString("name")

                                    if (j == jsonArrayOptions.length() - 1) {
                                        views.add(layoutView)
                                    }
                                }
                                2 -> {
                                    (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option2) as TextView).text = jsonObjectOption.getString("name")
                                    views.add(layoutView)
                                    countOption = 0
                                }
                            }
                            countOption++
                        }

                        for (item in views) {
                            linearLayoutOptions.addView(item)
                        }
                    }
                }

            } else {

                orderRaw = Gson().fromJson(orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").toString(), OrderRaw::class.java)

                if (orderId == orderRaw.order_id) {
                    val requestParams = LinkedHashMap<String, String>()
                    requestParams["order_id"] = orderId
                    requestParams["status_new_id"] = AppParams.ORDER_STATUS_CONFIRM
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["time_to_client"] = "0"
                    requestParams["worker_login"] = driver!!.callSign
                    WebService.setOrderStatus(requestParams, driver!!.secretCode, uuid, AppParams.RequestType.NORMAL)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(changeFragmentInOrderEvent: ChangeFragmentInOrderEvent) {
        onNavigationDrawerItemSelected(changeFragmentInOrderEvent.itemSelected)
        mediaPlayer!!.pause()
    }

    override fun onStop() {
        super.onStop()

        lifecycleState = "onStop"
        unregisterReceiver(receiverTracking)
        val manager = androidx.localbroadcastmanager.content.LocalBroadcastManager.getInstance(this)
        manager.unregisterReceiver(broadcastReceiverLang)
        manager.unregisterReceiver(preorderReceiver)

        if (stageOrder == 1)
            EventBus.getDefault().post(ShowWindowViewEvent(true))
        job.cancel()
    }

    override fun onDestroy() {

        if (appPreferences.getText("print_check") == "1") {
            try {
                if (mBluetoothSocket != null)
                    mBluetoothSocket!!.close()
            } catch (e: Exception) {
                Log.e("Tag", "Exe ", e)
            }

            unregisterReceiver(mBroadcastReceiverBluetooth)
        }

        EventBus.getDefault().post(DestroyMainActivityEvent(true))
        EventBus.getDefault().unregister(this@WorkShiftActivity)
        mediaPlayer?.stop()

        super.onDestroy()
    }

    private fun closeShift() {
        appPreferences.saveText("in_shift", "0")

        workDay!!.endWorkDay = Calendar.getInstance().time.time
        saveWorkDay()
        stopService(Intent(applicationContext, MainService::class.java))

        val intentWork = Intent(applicationContext, WorkActivity::class.java)
        intentWork.putExtra("service_id", service!!.id)
        intentWork.putExtra("end_workshift", true)
        startActivity(intentWork)
        finish()
    }

    private fun saveWorkDay() = runBlocking {
        withContext(Dispatchers.IO) {
            AppDatabase.getInstance(this@WorkShiftActivity).workDayDao().save(workDay!!)
        }
    }

    private fun rejectOrder(orderId: String, type: String) {
        try {
            if (order != null && order!!.orderId == orderId) {

                if (type != "complete_order") {
                    uiScope.launch {
                        initMediaPlayer()
                        mediaPlayer!!.start()
                    }
                }

                order = null
                currentOrder = null
                stageOrder = 0

                if (lifecycleState == "onStop")
                    showDialogRejected = 1

                val changeDialogFragment = ChangeDialogFragment()
                val bundle = Bundle()
                bundle.putString("type", type)
                changeDialogFragment.arguments = bundle

                changeDialogFragment.show(supportFragmentManager, "dialogFragment_rejected")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setParkingQueue(parking: Parking?) {
        try {
            requestParams = RequestParams()
            requestParams.add("worker_login", driver!!.callSign)
            requestParams.add("parking_id", parking?.parkingID?.toString() ?: "")
            requestParams.add("tenant_login", service!!.uri)

            asyncHttpTaskPost.post("set_parking_queue", requestParams, driver!!.secretCode)

            requestParams = RequestParams()
            requestParams.add("worker_city_id", driver!!.cityID)
            requestParams.add("worker_login", driver!!.callSign)
            requestParams.add("parking_id", parking?.parkingID?.toString() ?: "")
            requestParams.add("tenant_login", service!!.uri)

            asyncHttpTask.get("get_parkings_orders_queues", requestParams, driver!!.secretCode)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {

    }

    @Subscribe
    fun onEvent(parkingsListEvent: NetworkParkingsListEvent) {
        try {
            if (parkingsListEvent.parking != null) {
                appPreferences.saveText("parking", parkingsListEvent.parking.parkingID.toString() + "")
            } else {
                appPreferences.saveText("parking", "")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun doPostExecute(jsonObjectPolygons: JSONObject?, typeJson: String) {
        if (jsonObjectPolygons != null && typeJson == "get_parkings_orders_queues") {
            try {
                if (jsonObjectPolygons.getString("info") == "OK") {
                    val result = jsonObjectPolygons.getJSONArray("result").getJSONObject(0)
                    if (!result.isNull("parkings_queue")) {
                        val jsonArray = result.getJSONArray("parkings_queue")

                        for (j in 0 until jsonArray.length()) {

                            if (jsonArray.getString(j) == driver!!.callSign) {

                                findViewById<TextView>(R.id.textview_workday_queue)?.text = Html.fromHtml("<big><font color='"
                                        + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                                        + getString(R.string.text_workdayfragment_queue) + "</font></big><br /><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>"
                                        + (j + 1).toString() + "</font></small>")
                                break
                            }
                        }
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @Subscribe
    fun onEvent(workDayDisconnect: PushWorkDayDisconect) {
        workDay!!.endWorkDay = Calendar.getInstance().time.time
        saveWorkDay()
        stopService(Intent(applicationContext, MainService::class.java))

        val intentWork = Intent(applicationContext, WorkActivity::class.java)
        intentWork.putExtra("service_id", service!!.id)
        intentWork.putExtra("end_workshift", false)
        startActivity(intentWork)
        CustomToast.showMessage(applicationContext, getString(R.string.other_device) + "\n" + workDayDisconnect.newDeviceInfo)
        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_PINGSERVICE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startPingService()
            } else {
                CustomToast.showMessage(this, getString(R.string.activities_MainActivity_permission_rejected))
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    fun showToastRejected() {
        this.runOnUiThread { CustomToast.showMessage(this, getString(R.string.text_rejected_dialog)) }
    }

    fun printCheck(roundSum: String, currency: String, orderId: String) = uiScope.launch {
        if (mBluetoothSocket != null && mBluetoothSocket!!.isConnected) {
            withContext(Dispatchers.IO) {
                try {

                    val os = mBluetoothSocket!!
                            .outputStream
                    os.write(PrinterCommands.ESC_ALIGN_LEFT)
                    try {

                        if (bitmapLogo != null) {
                            val command = Utils.decodeBitmap(Bitmap.createScaledBitmap(bitmapLogo!!, 180, 180, false))
                            os.write(command!!)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.e("PrintTools", "the file isn't exists")
                    }

                    os.write(PrinterCommands.FEED_LINE)
                    val stringBuilder = StringBuilder()
                    stringBuilder.append(service!!.serviceName + "\n")
                    stringBuilder.append("ИНН: 010088454\n")
                    stringBuilder.append("Заказ: $orderId\n")
                    stringBuilder.append(dateFormat!!.format(Date()) + "\n")
                    try {
                        val desc = carDao.getByCarId(appPreferences.getText("car_id")).descr
                        stringBuilder.append(desc + "\n")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    stringBuilder.append("Сумма: $roundSum $currency\n")
                    stringBuilder.append("Спасибо за заказ!")
                    os.write(stringBuilder.toString().toByteArray(charset("cp866")))
                    os.write(PrinterCommands.FEED_LINE)
                    os.write(PrinterCommands.FEED_LINE)
                    os.write(PrinterCommands.FEED_LINE)

                } catch (e: Exception) {
                    Log.e("Main", "Exe ", e)
                }
            }
        }
    }

    private fun bluetoothConnect(mBluetoothAdapter: BluetoothAdapter?) {
        if (mBluetoothAdapter == null) {
            CustomToast.showMessage(this, getString(R.string.profile_bluetooth_empty))
        } else if (mBluetoothAdapter.isEnabled && appPreferences.getText("device_printer").isNotEmpty()) {

            val mDeviceAddress = appPreferences.getText("device_printer")
            mBluetoothDevice = mBluetoothAdapter
                    .getRemoteDevice(mDeviceAddress)
            val mBluetoothConnectThread = Thread(this)
            mBluetoothConnectThread.start()
        } else if (appPreferences.getText("device_printer").isEmpty()) {
            CustomToast.showMessage(this, getString(R.string.profile_printer_empty))
        } else {
            CustomToast.showMessage(this, getString(R.string.profile_bluetooth_off))
        }
    }

    override fun run() {
        try {
            mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID)
            mBluetoothAdapter.cancelDiscovery()
            mBluetoothSocket!!.connect()
        } catch (eConnectException: IOException) {
            closeSocket(mBluetoothSocket!!)
        }
    }

    private fun closeSocket(nOpenSocket: BluetoothSocket) {
        try {
            nOpenSocket.close()
        } catch (ex: IOException) {
        }

    }

    private fun loadDriverProfile() {
        try {
            val params = LinkedHashMap<String, String>()
            params["position_id"] = appPreferences.getText("position_id")
            params["tenant_login"] = service!!.uri
            params["worker_city_id"] = appPreferences.getText("drivercity_id")
            params["worker_login"] = driver!!.callSign
            WebService.getProfile(this, params, driver!!.secretCode)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(workerGetProfileResponse: GetProfileResponse) {
        driver!!.promoCode = workerGetProfileResponse.driver.promoCode
        CoroutineScope(Dispatchers.IO).launch {
            AppDatabase.getInstance(this@WorkShiftActivity).driverDao().save(driver!!)
        }
    }

    private fun uriToBitmap(service: Service?) {
        try {
            val imageRequest = ImageRequestBuilder
                    .newBuilderWithSource(Uri.parse(service!!.logo))
                    .setAutoRotateEnabled(true)
                    .build()

            val imagePipeline = Fresco.getImagePipeline()
            val dataSource = imagePipeline.fetchDecodedImage(imageRequest, this)

            dataSource.subscribe(object : BaseBitmapDataSubscriber() {
                override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>?) {
                    dataSource?.close()
                }

                public override fun onNewResultImpl(bitmap: Bitmap?) {
                    if (dataSource.isFinished && bitmap != null) {
                        bitmapLogo = Bitmap.createBitmap(bitmap)
                        dataSource.close()
                    }
                }
            }, CallerThreadExecutor.getInstance())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {

        const val INTENT_ORDER_OFFER = 132
        const val PERMISSION_PINGSERVICE = 430
        const val PERMISSION_LOGGING = 431
        const val PERMISSION_WINDOW = 433
    }
}
