package com.kabbi.driver

import android.app.Activity
import android.content.Intent
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.kabbi.driver.adapters.GalleryAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.ChatEmitNewMessage
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.models.Photo
import com.kabbi.driver.service.MainService
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.WebSocket
import com.loopj.android.http.RequestParams
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

// https://stackoverflow.com/questions/15551148/android-camera-view-not-open
class CameraActivity : Activity(), View.OnClickListener, SurfaceHolder.Callback, GalleryAdapter.reCaptureListener, AsyncHttpTask.AsyncTaskInterface {
    private var directory = File(Environment.getExternalStorageDirectory().toString() + "/Pictures/Photocontrol/")
    private var RECAPTURE: Boolean = false
    private var photoList: ArrayList<Photo>? = null
    private var requestParamsArrayList: ArrayList<RequestParams>? = null
    private var mCamera: Camera? = null
    private var mPreview: SurfaceView? = null
    private var asyncHttpTask: AsyncHttpTask? = null
    private var progressBar: ProgressBar? = null
    private var requestParams: RequestParams? = null
    private var driver: Driver? = null
    private var service: Service? = null
    private var appPreferences: AppPreferences? = null
    private var cameraId = -1
    private var recyclerView: androidx.recyclerview.widget.RecyclerView? = null

    private var sendButton: Button? = null
    private var frameImage: ImageView? = null
    private var tvPhotoDescr: TextView? = null
    private var lightLabel: TextView? = null
    private var rlGallery: RelativeLayout? = null
    private var llControls: LinearLayout? = null
    private var llFrame: LinearLayout? = null
    private var lightBtn: LinearLayout? = null
    private var light = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_camera)
        appPreferences = AppPreferences(this)
        photoList = ArrayList()
        photoList = intent.getSerializableExtra("types_array") as ArrayList<Photo>

        requestParamsArrayList = ArrayList()

        driver = (application as App).driver
        service = (application as App).service
        asyncHttpTask = AsyncHttpTask(this, applicationContext)
        mPreview = findViewById(R.id.surface_view)
        mPreview!!.holder.addCallback(this)
        mPreview!!.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)

        cameraId = findCamera()

        progressBar = findViewById(R.id.pbHeaderProgress)

        llFrame = findViewById(R.id.ll_frame)
        llControls = findViewById(R.id.ll_controls)
        rlGallery = findViewById(R.id.rl_gallery)

        llFrame!!.visibility = View.VISIBLE
        llControls!!.visibility = View.VISIBLE
        rlGallery!!.visibility = View.GONE

        sendButton = findViewById(R.id.btn_send)
        sendButton!!.setOnClickListener { sendPhotosToReview() }
        lightLabel = findViewById(R.id.light_label)
        lightBtn = findViewById(R.id.btn_light)
        lightBtn!!.setOnClickListener {
            light = !light
            try {
                setParams()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        frameImage = findViewById(R.id.frame_image)
        tvPhotoDescr = findViewById(R.id.frame_text)
        val cameraButton = findViewById<FloatingActionButton>(R.id.fab_capture)
        cameraButton.setOnClickListener(this)
        RECAPTURE = false
        request_count = 0
        response_count = 0
        CURRENT_PHOTO = 0
        onRefreshFrame(photoList!!, CURRENT_PHOTO)
    }

    override fun reCapture(photo: Photo) {
        llFrame!!.visibility = View.VISIBLE
        llControls!!.visibility = View.VISIBLE
        rlGallery!!.visibility = View.GONE
        RECAPTURE = true
        CURRENT_PHOTO = photoList!!.indexOf(photo)
        deleteIncorrectPhoto(photoList!![CURRENT_PHOTO].photoDescr)
        prepareFrame(photo)
    }

    private fun onRefreshFrame(photoList: ArrayList<Photo>, current: Int) {
        val targetPhoto: Photo
        if (current < photoList.size) {
            targetPhoto = photoList[current]
            prepareFrame(targetPhoto)
        } else {
            onCaptureCompleted()
        }
    }

    private fun prepareFrame(photo: Photo) {
        when (photo.photoDescr) {
            "front" -> {
                frameImage!!.setImageResource(R.drawable.countour_1)
                tvPhotoDescr!!.text = getString(R.string.pc_car_front)
            }
            "back" -> {
                frameImage!!.setImageResource(R.drawable.countour_1)
                tvPhotoDescr!!.text = getString(R.string.pc_car_back)
            }
            "right_side" -> {
                frameImage!!.setImageResource(R.drawable.contour_right)
                tvPhotoDescr!!.text = getString(R.string.pc_car_right_side)
            }
            "left_side" -> {
                frameImage!!.setImageResource(R.drawable.contour_left)
                tvPhotoDescr!!.text = getString(R.string.pc_car_left_side)
            }
            "front_interior" -> {
                frameImage!!.setImageResource(0)
                tvPhotoDescr!!.text = getString(R.string.pc_car_front_interior)
            }
            "back_interior" -> {
                frameImage!!.setImageResource(0)
                tvPhotoDescr!!.text = getString(R.string.pc_car_back_interior)
            }
            "truck" -> {
                frameImage!!.setImageResource(0)
                tvPhotoDescr!!.text = getString(R.string.pc_car_truck)
            }
            "documents" -> {
                frameImage!!.setImageResource(0)
                tvPhotoDescr!!.text = getString(R.string.pc_car_documents)
            }
        }
    }

    override fun onClick(v: View) {
        try {
            mCamera!!.takePicture(null, null, { data, camera ->
                val outStream: FileOutputStream
                val photo: File
                try {
                    directory = File(Environment.getExternalStorageDirectory().toString() + "/Pictures/Photocontrol/")
                    if (!directory.exists()) {
                        directory.mkdirs()
                        //Log.d(getClass().getSimpleName(), "'Photocontrol' directory was created");
                    }

                    photo = File(directory.toString() + "/" + photoList!![CURRENT_PHOTO].photoDescr + ".jpg")
                    outStream = FileOutputStream(photo)
                    outStream.write(data)
                    outStream.close()
                    photoList!![CURRENT_PHOTO].photoPath = directory.toString() + "/" + photoList!![CURRENT_PHOTO].photoDescr + ".jpg"
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                camera.startPreview()
                if (photoList!!.size > CURRENT_PHOTO)
                    CURRENT_PHOTO = CURRENT_PHOTO + 1

                if (!RECAPTURE) {
                    onRefreshFrame(photoList!!, CURRENT_PHOTO)
                } else {
                    light = false
                    setParams()
                    onCaptureCompleted()
                }
            })
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        //Log.d("TEST", "onClick: CURRENT_PHOTO = " + CURRENT_PHOTO);
    }

    private fun deleteIncorrectPhoto(photoDescr: String) {
        val photo = File("$directory/$photoDescr.jpg")
        if (photo.delete()) {
            Log.i(javaClass.simpleName, "Incorrect photo was deleted")
        } else {
            Log.e(javaClass.simpleName, "Error on deleting photo!")
        }
    }

    private fun onCaptureCompleted() {
        llFrame!!.visibility = View.GONE
        llControls!!.visibility = View.GONE
        rlGallery!!.visibility = View.VISIBLE
        recyclerView = findViewById(R.id.rv_gallery)
        recyclerView!!.setHasFixedSize(true)
        // for 2 stage of photo_event
        if (!RECAPTURE)
            recyclerView!!.addItemDecoration(GalleryAdapter.ItemDecorationGalleryColumns(
                    resources.getDimensionPixelSize(R.dimen.spacing),
                    200))
        val layoutManager = androidx.recyclerview.widget.GridLayoutManager(applicationContext, 3)
        recyclerView!!.layoutManager = layoutManager
        val adapter = GalleryAdapter(applicationContext, this, photoList)
        recyclerView!!.adapter = adapter
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        setParams()
        onRefreshFrame(photoList!!, CURRENT_PHOTO)
        mCamera!!.startPreview()
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        try {
            if (Build.VERSION.SDK_INT > 22) {
                mCamera = Camera.open()
            } else {
                try {
                    releaseCameraAndPreview()
                    mCamera = Camera.open(cameraId)
                } catch (e: Exception) {
                    Log.e(getString(R.string.app_name), "failed to open Camera")
                    e.printStackTrace()
                }

            }
            mCamera!!.startPreview()
            mCamera!!.setPreviewDisplay(mPreview!!.holder)
            setParams()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun releaseCameraAndPreview() {
        if (mCamera != null) {
            mCamera!!.release()
            mCamera = null
        }
    }

    private fun findCamera(): Int {
        // Search for the front facing camera
        val numberOfCameras = Camera.getNumberOfCameras()
        for (i in 0 until numberOfCameras) {
            val info = Camera.CameraInfo()
            Camera.getCameraInfo(i, info)
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d("TEST", "Camera found")
                cameraId = i
                break
            }
        }
        return cameraId
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        mCamera!!.release()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onResume() {
        super.onResume()
        try {
            if (Build.VERSION.SDK_INT > 22) {
                mCamera = Camera.open()
                //mCamera.setPreviewDisplay(mPreview.getHolder());
            } else {
                try {
                    //releaseCameraAndPreview();
                    mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK)
                } catch (e: Exception) {
                    Log.e(getString(R.string.app_name), "failed to open Camera")
                    e.printStackTrace()
                }

                /*mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                setParams();
                mCamera.startPreview();
                mCamera.setPreviewDisplay(mPreview.getHolder());*/
            }
            setParams()
            mCamera!!.startPreview()
            mPreview!!.visibility = View.VISIBLE
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    public override fun onPause() {
        /*super.onPause();

        if (mCamera != null) {
            mCamera.stopPreview();
            //mCamera.release();
            //mCamera = null;
        }*/

        try {
            mPreview!!.visibility = View.GONE
            //holder.removeCallback(this);
            mCamera!!.release()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        super.onPause()
    }

    public override fun onDestroy() {
        super.onDestroy()
        //mCamera.release();
        clearDirectory()
        finish()
    }

    private fun sendPhotosToReview() {
        recyclerView!!.visibility = View.INVISIBLE
        try {
            if (InternetConnection.isOnline(applicationContext)) {
                request_count = 0
                while (photoList!!.size > request_count) {

                    requestParams = RequestParams()
                    requestParams!!.put("photos[0]", File(photoList!![request_count].photoPath))
                    requestParams!!.add("tenant_login", service!!.uri)
                    requestParams!!.add("type_ids[0]", photoList!![request_count].photoId)
                    requestParams!!.add("worker_login", driver!!.callSign)

                    requestParamsArrayList!!.add(requestParams!!)

                    sendButton!!.visibility = View.GONE
                    progressBar!!.visibility = View.VISIBLE
                    request_count++
                }
                sendPhoto(0)
            } else {
                if (!InternetConnection.isOnline(applicationContext)) {
                    CustomToast.showMessage(applicationContext, applicationContext.getString(R.string.text_internet_access))
                }
            }
            Log.d("TEST", "REQ: $request_count")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun sendPhoto(index: Int) {
        val handler = Handler()
        handler.post {

            Log.d("TEST", requestParamsArrayList!![index].toString())
            asyncHttpTask!!.post("send_photos_to_review", requestParamsArrayList!![index], driver!!.secretCode)
        }
    }

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        if (jsonObject != null && typeJson == "send_photos_to_review") {
            try {
                response_count += 1

                if (response_count < request_count)
                    sendPhoto(response_count)

                Log.d("TEST", "RESP: $response_count")

                if (response_count == request_count) {
                    requestParams = RequestParams()
                    requestParams!!.add("tenant_login", service!!.uri)
                    requestParams!!.add("worker_login", driver!!.callSign)

                    val handler = Handler()
                    handler.postDelayed({ asyncHttpTask!!.get("complete_upload_photo_report", requestParams, driver!!.secretCode) }, 1000)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else if (jsonObject != null && typeJson == "complete_upload_photo_report") {
            try {
                sendButton!!.visibility = View.VISIBLE
                progressBar!!.visibility = View.GONE
                CustomToast.showMessage(applicationContext, getString(R.string.pc_photo_send_succesful))
                clearDirectory()

                val intentWork = Intent(this@CameraActivity, WorkShiftActivity::class.java)
                startActivity(intentWork)
                startService(Intent(applicationContext, MainService::class.java))

                val carDao = AppDatabase.getInstance(this).carDao()

                CoroutineScope(Dispatchers.Main).launch {
                    withContext(Dispatchers.IO) {
                        val car = carDao.getByCarId(appPreferences!!.getText("car_id"))
                        val message = String.format(getString(R.string.pc_required_text_message), driver!!.name, car.descr)
                        EventBus.getDefault().post(ChatEmitNewMessage(WebSocket.TYPECHAT_DRIVER, message, driver))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else if (jsonObject == null) {
            CustomToast.showMessage(applicationContext, getString(R.string.pc_error_loading_types))
        }
    }

    private fun clearDirectory() {
        directory = File(Environment.getExternalStorageDirectory().toString() + "/Pictures/Photocontrol/")
        if (directory.isDirectory) {
            val children = directory.list()
            for (child in children) {
                File(directory, child).delete()
            }
        }
    }

    private fun setParams() {
        val params = mCamera!!.parameters
        val size = mCamera!!.parameters.previewSize
        params.setPreviewSize(size.width, size.height)
        if (params.flashMode != null) {
            lightBtn!!.visibility = View.VISIBLE
        } else {
            lightBtn!!.visibility = View.GONE
        }
        if (light) {
            lightLabel!!.setText(R.string.pc_light_turn_off)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                params.flashMode = Camera.Parameters.FLASH_MODE_TORCH

            } else {
                params.flashMode = Camera.Parameters.FLASH_MODE_TORCH
            }
        } else {
            lightLabel!!.setText(R.string.pc_light_turn_on)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                params.flashMode = Camera.Parameters.FLASH_MODE_OFF
            } else {
                params.flashMode = Camera.Parameters.FLASH_MODE_OFF
            }
        }

        if (params.supportedFocusModes.contains(
                        Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
            params.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO
        }
        mCamera!!.parameters = params
    }

    companion object {

        private var CURRENT_PHOTO: Int = 0

        private var request_count: Int = 0
        private var response_count: Int = 0
    }
}