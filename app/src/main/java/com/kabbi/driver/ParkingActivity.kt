package com.kabbi.driver

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import com.kabbi.driver.adapters.ParkingAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Parking
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.loopj.android.http.RequestParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.util.*

class ParkingActivity : BaseActivity(), AsyncHttpTask.AsyncTaskInterface, AdapterView.OnItemClickListener, androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener {

    private var parkings: List<Parking> = arrayListOf()
    private var parkingsInfo: MutableMap<Long, Array<Int>> = mutableMapOf()
    internal lateinit var listView: ListView
    internal lateinit var adapter: ParkingAdapter
    internal lateinit var driver: Driver
    internal lateinit var service: Service
    internal lateinit var requestParams: RequestParams
    internal lateinit var appPreferences: AppPreferences
    private var asyncHttpTask = AsyncHttpTask(this, this)
    private var swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_parking)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_parking)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_parking)
        swipeRefreshLayout!!.setOnRefreshListener(this)

        parkingsInfo = HashMap()
        parkings = ArrayList()
        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@ParkingActivity)
                service = db.serviceDao().getById(intent.extras!!.getLong("service_id"))
                parkings = db.parkingDao().getAll(service.id)
                driver = db.driverDao().getById(service.driver!!)
            }
        }

        requestParams = RequestParams()
        requestParams.add("worker_city_id", driver.cityID)
        requestParams.add("worker_login", driver.callSign)
        requestParams.add("tenant_login", service.uri)

        asyncHttpTask.get("get_parkings_orders_queues", requestParams, driver.secretCode)
    }

    override fun doPostExecute(jsonObject: JSONObject, typeJson: String) {
        if (parkings.isNotEmpty()) {
            listView = findViewById(R.id.listview_parking)
            try {
                if (jsonObject.getString("info") == "OK") {
                    val jsonParkings = jsonObject.getJSONArray("result")
                    Log.i("COUNT_ORDER", jsonObject.toString())
                    for (j in 0 until jsonParkings.length()) {
                        val jsonParking = jsonParkings.getJSONObject(j)

                        var prQueue = 0
                        var prOrders = 0
                        if (jsonParking.getString("parkings_queue") != "null") {
                            val jsonCount = jsonParking.getJSONArray("parkings_queue")
                            prQueue = jsonCount.length()
                        }
                        if (jsonParking.getString("parking_orders") != "null")
                            prOrders = jsonParking.getInt("parking_orders")

                        parkingsInfo[jsonParking.getLong("parking_id")] = arrayOf(prQueue, prOrders)
                    }
                    adapter = ParkingAdapter(this, parkings, parkingsInfo, typedValueMain!!.data)
                    listView.onItemClickListener = this
                    listView.adapter = adapter
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        swipeRefreshLayout!!.isRefreshing = false
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        appPreferences.saveText("parking", view.tag.toString())
        onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRefresh() {
        if (InternetConnection.isOnline(this)) {
            swipeRefreshLayout!!.isRefreshing = true
            asyncHttpTask.get("get_parkings_orders_queues", requestParams, driver.secretCode)
        } else {
            swipeRefreshLayout!!.isRefreshing = false
            CustomToast.showMessage(this, getString(R.string.text_internet_access))
        }
    }
}
