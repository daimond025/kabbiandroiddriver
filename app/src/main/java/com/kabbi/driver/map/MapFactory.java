package com.kabbi.driver.map;


import android.content.Context;
import androidx.fragment.app.Fragment;

import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.map.google.FragmentGMap;
import com.kabbi.driver.map.osm.FragmentOSMMap;
import com.kabbi.driver.map.yandex.FragmentYaMap;

public class MapFactory {

    public static final String MAP_TYPE_GOOGLE = "1";
    public static final String MAP_TYPE_YANDEX = "3";
    public static final String MAP_TYPE_OSM = "2";

    public static Fragment getFragmentMap(Context context) {
        AppPreferences appPrefs = new AppPreferences(context);
        //String mapType = appPrefs.getText("mapType");
        String mapType = appPrefs.getText("favorite_map");
        Fragment fragmentMap;

        switch (mapType) {
            case MAP_TYPE_YANDEX:
                appPrefs.saveText("mapType", MAP_TYPE_YANDEX);
                fragmentMap = FragmentYaMap.newInstance();
                break;
            case MAP_TYPE_GOOGLE:
                default:
                appPrefs.saveText("mapType", MAP_TYPE_GOOGLE);
                fragmentMap = FragmentGMap.Companion.newInstance();
                break;
            case MAP_TYPE_OSM:
                appPrefs.saveText("mapType", MAP_TYPE_OSM);
                fragmentMap = FragmentOSMMap.newInstance();
                break;
        }


        return fragmentMap;
    }

}