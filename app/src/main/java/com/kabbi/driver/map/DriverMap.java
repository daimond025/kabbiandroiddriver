package com.kabbi.driver.map;


import android.content.Context;
import android.view.View;
import com.google.android.gms.maps.GoogleMap;
import com.kabbi.driver.database.entities.Parking;
import com.kabbi.driver.database.entities.Route;

import java.util.List;
import java.util.Map;

public abstract class DriverMap {

    public abstract void initMap(Context context);
    public abstract void initMap(Context context, GoogleMap map);

    public abstract View getMapView();

    public abstract void setCenterMap(double lat, double lon);

    public abstract void updateCar(double lat, double lon, float rotate);

    public abstract void addLine(double lat1, double lon1, double lat2, double lon2);

    public abstract void addRoute(List<Route>routes);

    public abstract void addMarker(double lat, double lon, int drawable);

    public abstract void setPath(List<Map<String, Double>>points, int stage);

    public abstract void setPathRoutes(List<Route>routes, Parking parking);

    public abstract void updatePathRoutes();

    public abstract void clear();
}
