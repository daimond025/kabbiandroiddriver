package com.kabbi.driver.map.google


import android.content.Context
import android.graphics.*
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.events.CarEvent
import com.kabbi.driver.events.UpdateAdressesToMapEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.util.GMapV2Direction
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class FragmentGMap : SupportMapFragment(), OnMapReadyCallback {

    var stage: Int = 0
    private var updateCar = 0
    private var appPrefs: AppPreferences? = null
    private var googleMap: GoogleMap? = null
    private val mOriginalContentView: View? = null
    private var car: Marker? = null
    private var driver: Driver? = null
    private val arrAddresses = arrayOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        EventBus.getDefault().register(this)
        appPrefs = AppPreferences(activity!!.applicationContext)
        getMapAsync(this)
        runBlocking {
            withContext(Dispatchers.IO) {
                driver = AppDatabase.getInstance(context!!).driverDao().getById(arguments!!.getLong("driver_id"))
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    override fun getView(): View? {
        return mOriginalContentView
    }

    override fun onMapReady(map: GoogleMap) {
        this.googleMap = map

        initMap()
        initPoints()

        if (appPrefs!!.getText("lat").isNotEmpty())
            googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(
                    java.lang.Double.valueOf(appPrefs!!.getText("lat")),
                    java.lang.Double.valueOf(appPrefs!!.getText("lon"))), AppParams.ZOOM.toFloat()))

    }

    private fun initMap() {
        googleMap!!.uiSettings.isCompassEnabled = false
    }

    private fun initPoints() {
        googleMap!!.clear()
        val points = ArrayList<Map<String, Double>>()
        try {
            CoroutineScope(Dispatchers.Main).launch {
                var jsonObjectAddress = JSONObject()

                withContext(Dispatchers.IO) {
                    val curOrder = AppDatabase.getInstance(this@FragmentGMap.context!!).currentOrderDao().getCurOrder(driver!!.id)
                    jsonObjectAddress = JSONObject(curOrder.address)

                    for (itemAddress in arrAddresses) {
                        try {
                            points.add(object : HashMap<String, Double>() {
                                init {
                                    put("lat", jsonObjectAddress.getJSONObject(itemAddress).getString("lat").toDouble())
                                    put("lon", jsonObjectAddress.getJSONObject(itemAddress).getString("lon").toDouble())
                                }
                            })
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        }
                    }

                    stage = (activity as WorkShiftActivity).stage

                    googleMap?.also { googleMap ->
                        try {
                            val iconsBitmap = ArrayList<Bitmap>()
                            for (itemAddress in arrAddresses) {
                                iconsBitmap.add(drawTextToBitmap(context!!, R.drawable.pin_empty, itemAddress))
                            }

                            var count = 0
                            val tr = MyTaskRouteInfo()
                            val pointsLatLng = ArrayList<LatLng>()

                            for (item in points) {
                                if (stage == 0) {
                                    pointsLatLng.add(LatLng(java.lang.Double.valueOf(appPrefs!!.getText("lat")), java.lang.Double.valueOf(appPrefs!!.getText("lon"))))
                                    pointsLatLng.add(LatLng(item["lat"] ?: 0.0, item["lon"] ?: 0.0))
                                    googleMap.addMarker(MarkerOptions().position(LatLng(item["lat"] ?: 0.0, item["lon"]
                                            ?: 0.0)).icon(BitmapDescriptorFactory.fromBitmap(iconsBitmap[count])).draggable(false))
                                    break
                                }

                                pointsLatLng.add(LatLng(item["lat"] ?: 0.0, item["lon"] ?: 0.0))
                                googleMap.addMarker(MarkerOptions().position(LatLng(item["lat"] ?: 0.0, item["lon"]
                                        ?: 0.0)).icon(BitmapDescriptorFactory.fromBitmap(iconsBitmap[count])).draggable(false))

                                count++
                            }
                            tr.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pointsLatLng)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(updateAdressesToMapEvent: UpdateAdressesToMapEvent) {
        activity!!.runOnUiThread {
            initPoints()
            car = null
        }
    }

    private fun drawTextToBitmap(gContext: Context,
                                 gResId: Int,
                                 gText: String): Bitmap {
        val resources = gContext.resources
        val scale = resources.displayMetrics.density
        var bitmap = BitmapFactory.decodeResource(resources, gResId)

        var bitmapConfig: Bitmap.Config? = bitmap.config
        if (bitmapConfig == null) {
            bitmapConfig = Bitmap.Config.ARGB_8888
        }
        bitmap = bitmap.copy(bitmapConfig, true)

        val canvas = Canvas(bitmap)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = getResources().getColor(R.color.white)
        paint.textSize = (18 * scale).toInt().toFloat()
        val bounds = Rect()
        paint.getTextBounds(gText, 0, gText.length, bounds)
        val x = (bitmap.width - bounds.width()) / 2.2.toFloat()
        val y = (bitmap.height + bounds.height()) / 2.5.toFloat()

        canvas.drawText(gText, x, y, paint)

        return bitmap
    }

    @Subscribe
    fun onEvent(carEvent: CarEvent) {
        if (googleMap != null) {
            try {
                if (car == null) {
                    car = googleMap!!.addMarker(MarkerOptions()
                            .position(LatLng(
                                    carEvent.lat,
                                    carEvent.lon))
                            .anchor(0.5f, 0.5f)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow_day)))
                } else {
                    car!!.position = LatLng(carEvent.lat, carEvent.lon)
                }

                if (updateCar >= 10) {
                    googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(carEvent.lat, carEvent.lon), AppParams.ZOOM.toFloat()))
                    updateCar = 0
                }

                updateCar++

                car!!.rotation = java.lang.Float.valueOf(carEvent.bearing)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }


    internal inner class MyTaskRouteInfo : AsyncTask<ArrayList<LatLng>, Unit, Unit>() {

        private lateinit var directionPoint: ArrayList<LatLng>
        private lateinit var rectLine: PolylineOptions

        override fun doInBackground(vararg params: ArrayList<LatLng>) {

            rectLine = PolylineOptions().color(Color.parseColor("#0080FC"))

            for (i in 0 until params[0].size - 1) {

                try {
                    val md = GMapV2Direction()

                    val doc = md.getDocument(params[0][i], params[0][i + 1], GMapV2Direction.MODE_DRIVING)

                    directionPoint = md.getDirection(doc) as ArrayList<LatLng>

                    for (n in directionPoint.indices) {
                        rectLine.add(directionPoint[n])
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

//            return null
        }

        override fun onPostExecute(result: Unit) {
            super.onPostExecute(result)
            if (googleMap != null) {
                try {
                    googleMap!!.addPolyline(rectLine)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    companion object {

        fun newInstance(): FragmentGMap {
            return FragmentGMap()
        }
    }
}