package com.kabbi.driver.map;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.kabbi.driver.R;
import com.kabbi.driver.database.entities.Parking;
import com.kabbi.driver.database.entities.Route;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.PreferencesMap;
import com.kabbi.driver.util.GMapV2Direction;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.PathOverlay;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DriverMapOsm extends DriverMap {

    MapView mapView;
    MapController mapController;
    PreferencesMap preferencesMap;
    Context context;
    Marker marker;
    private String[] arrAddresses = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    int updateCar = 0;


    @Override
    public void initMap(Context context) {
        mapView = new MapView(context, null);
        mapController = (MapController)mapView.getController();
        this.context = context;

        mapView.getController().setZoom(AppParams.ZOOM);

        mapView.setBuiltInZoomControls(true);


        preferencesMap = new PreferencesMap(context);
    }

    @Override
    public void initMap(Context context, GoogleMap map) {

    }

    @Override
    public View getMapView() {
        return mapView;
    }

    @Override
    public void setCenterMap(double lat, double lon) {
        //mapController.setCenter(new GeoPoint(lat, lon));
        mapView.getController().setCenter(new GeoPoint(lat, lon));
        mapView.getController().setZoom(AppParams.ZOOM);
    }

    @Override
    public void updateCar(double lat, double lon, float rotate) {
        try {
            mapView.getOverlays().remove(marker);
            Drawable carDrawer = context.getResources().getDrawable(R.drawable.arrow_day);
            marker = new Marker(mapView);
            marker.setPosition(new GeoPoint(lat, lon));
            marker.setIcon(carDrawer);
            marker.setRotation(rotate);
            mapView.getOverlays().add(marker);
            mapView.invalidate();

            if (updateCar >= 10) {

                mapView.getController().setCenter(new GeoPoint(lat, lon));
                mapView.getController().setZoom(AppParams.ZOOM);

                updateCar = 0;
            }

            updateCar++;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addLine(double lat1, double lon1, double lat2, double lon2) {

    }

    @Override
    public void addRoute(List<Route> routes) {

    }

    @Override
    public void addMarker(double lat, double lon, int drawable) {
        Marker markerCar = new Marker(mapView);
        markerCar.setPosition(new GeoPoint(lat, lon));
        markerCar.setIcon(context.getResources().getDrawable(drawable));
        mapView.getOverlays().add(markerCar);
        mapView.invalidate();

    }

    @Override
    public void clear() {
        mapView.getOverlays().clear();
    }

    @Override
    public void setPath(List<Map<String, Double>>points, int stage) {
        try {
            List<Bitmap> iconsBitmap = new ArrayList<>();
            for (String itemAddress : arrAddresses) {
                iconsBitmap.add(drawTextToBitmap(context, R.drawable.pin_empty, itemAddress));
            }

            int count = 0;
            MyTaskRouteInfo tr = new MyTaskRouteInfo();
            ArrayList pointsLatLng = new ArrayList<LatLng>();

            for (Map<String, Double> item : points) {
                if (stage == 0) {
                    switch (count) {
                        case 0:
                            pointsLatLng.add(new LatLng(item.get("lat"), item.get("lon")));
                            break;
                        case 1:
                            pointsLatLng.add(new LatLng(item.get("lat"), item.get("lon")));
                            //Drawable carDrawer = context.getResources().getDrawable(icons.get(0));
                            Drawable carDrawer = new BitmapDrawable(context.getResources(), iconsBitmap.get(0));
                            Marker point = new Marker(mapView);
                            point.setPosition(new GeoPoint(item.get("lat"), item.get("lon")));
                            point.setIcon(carDrawer);
                            mapView.getOverlays().add(point);
                            mapView.invalidate();
                            break;
                    }
                } else {
                    if (count != 0) {
                        pointsLatLng.add(new LatLng(item.get("lat"), item.get("lon")));
                        //Drawable carDrawer = context.getResources().getDrawable(icons.get(count-1));
                        Drawable carDrawer = new BitmapDrawable(context.getResources(), iconsBitmap.get(count-1));
                        Marker point = new Marker(mapView);
                        point.setPosition(new GeoPoint(item.get("lat"), item.get("lon")));
                        point.setIcon(carDrawer);
                        mapView.getOverlays().add(point);
                        mapView.invalidate();
                    }
                }
                count++;
            }

            tr.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pointsLatLng);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPathRoutes(List<Route> routes, Parking parking) {

    }

    @Override
    public void updatePathRoutes() {

    }

    private Bitmap drawTextToBitmap(Context gContext,
                                    int gResId,
                                    String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, gResId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(context.getResources().getColor(R.color.white));
        paint.setTextSize((int) (18 * scale));
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        float x = (bitmap.getWidth() - bounds.width())/(float)2.2;
        float y = (bitmap.getHeight() + bounds.height())/(float)2.5;

        canvas.drawText(gText, x, y, paint);

        return bitmap;
    }

    class MyTaskRouteInfo extends AsyncTask<ArrayList<LatLng>, Void, Void> {

        ArrayList<LatLng> directionPoint;
        List<Route> routes;
        PathOverlay myPath;

        @Override
        protected Void doInBackground(ArrayList<LatLng>... params) {
            routes = new ArrayList<>();

            myPath = new PathOverlay(Color.BLUE, context);

            for(int i=0; i<(params[0].size()-1); i++){
                int j = i;

                try {
                    GMapV2Direction md = new GMapV2Direction();

                    Document doc = md.getDocument(params[0].get(j), params[0].get(j+1), GMapV2Direction.MODE_DRIVING);

                    directionPoint = md.getDirection(doc);

                    for (int n = 0; n < directionPoint.size(); n++) {
                        myPath.addPoint(new GeoPoint(directionPoint.get(n).latitude, directionPoint.get(n).longitude));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                mapView.getOverlays().add(myPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
