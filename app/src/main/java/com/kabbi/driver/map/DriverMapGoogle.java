package com.kabbi.driver.map;


import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.*;
import com.kabbi.driver.R;
import com.kabbi.driver.database.entities.Parking;
import com.kabbi.driver.database.entities.Route;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.PreferencesMap;
import com.kabbi.driver.util.GMapV2Direction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DriverMapGoogle extends DriverMap {


    GoogleMap map;
    Marker marker;
    PreferencesMap preferencesMap;
    PolylineOptions rectLine;
    PolygonOptions rectPolygone;
    LatLng point1, point2;
    int countLine = 0;
    ArrayList<LatLng> directionPoint;
    Context context;



    @Override
    public void initMap(Context context) {

    }

    @Override
    public void initMap(Context context, GoogleMap _map) {
        map = _map;

        this.context = context;

        marker = map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow_day)).draggable(false));

        preferencesMap = new PreferencesMap(context);

        map.getUiSettings().setCompassEnabled(false);

        rectLine = new PolylineOptions().color(Color.parseColor("#0080FC"));
        directionPoint = new ArrayList<LatLng>();

        rectPolygone = new PolygonOptions().strokeColor(Color.parseColor("#ff0000"));

        if (preferencesMap.orientation_north)
            map.getUiSettings().setRotateGesturesEnabled(false);

    }

    @Override
    public View getMapView() {
        return null;
    }

    @Override
    public void setCenterMap(double lat, double lon) {
        LatLng centerLocation = new LatLng(lat, lon);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(centerLocation, AppParams.ZOOM));
    }


    @Override
    public void updateCar(double lat, double lon, float rotate) {
        try {
            marker.setPosition(new LatLng(lat, lon));
            marker.setRotation(rotate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addLine(double lat1, double lon1, double lat2, double lon2) {

    }

    @Override
    public void addRoute(List<Route> routes) {
        PolylineOptions rectLine = new PolylineOptions().color(Color.parseColor("#0080FC"));

        boolean firstPoint = false;
        for (Route route : routes) {
            if (!firstPoint) {
                rectLine.add(new LatLng(route.getLat(), route.getLon()));
                firstPoint = true;
            }


            if (route.getSpeed() > 0)
                rectLine.add(new LatLng(route.getLat(), route.getLon()));
        }

        map.addPolyline(rectLine);
    }

    @Override
    public void addMarker(double lat, double lon, int drawable) {

    }

    @Override
    public void setPath(List<Map<String, Double>>points, int stage) {
        try {
            List<Integer> icons = new ArrayList<>();
            icons.add(R.drawable.zakaz_a);
            icons.add(R.drawable.zakaz_b);
            icons.add(R.drawable.zakaz_c);
            icons.add(R.drawable.zakaz_d);
            icons.add(R.drawable.zakaz_e);
            int count = 0;
            MyTaskRouteInfo tr = new MyTaskRouteInfo();
            ArrayList pointsLatLng = new ArrayList<LatLng>();

            for (Map<String, Double> item : points) {
                pointsLatLng.add(new LatLng(item.get("lat"), item.get("lon")));
                map.addMarker(new MarkerOptions().position(new LatLng(item.get("lat"), item.get("lon"))).icon(BitmapDescriptorFactory.fromResource(icons.get(count))).draggable(false));

                count++;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                tr.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pointsLatLng);
            else
                tr.execute(pointsLatLng);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPathRoutes(List<Route> routes, Parking parking) {
        for (Route route : routes) {
            if (route.getSpeed() > 0) {
                rectLine.add(new LatLng(route.getLat(), route.getLon()));
            }
        }
        map.addPolyline(rectLine);

        try {
            JSONArray jsonArrayCoords = (new JSONObject(parking.getParkingPolygon())).getJSONObject("parking_polygone").getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0);
            for (int k = 0; k < jsonArrayCoords.length(); k++) {
                JSONArray jsonArrayPoint = jsonArrayCoords.getJSONArray(k);
                //polygon.addVertex(new Point((float) jsonArrayPoint.getDouble(1), (float) jsonArrayPoint.getDouble(0)));
                rectPolygone.add(new LatLng(jsonArrayPoint.getDouble(1), jsonArrayPoint.getDouble(0)));
            }

            map.addPolygon(rectPolygone);
        } catch (JSONException e) {
            e.printStackTrace();
        }





    }

    @Override
    public void updatePathRoutes() {

    }

    @Override
    public void clear() {
        map.clear();
    }

    class MyTaskRouteInfo extends AsyncTask<ArrayList<LatLng>, Void, Void> {

        ArrayList points;
        ArrayList<LatLng> directionPoint;
        PolylineOptions rectLine;

        @Override
        protected Void doInBackground(ArrayList<LatLng>... params) {

            rectLine = new PolylineOptions().color(Color.parseColor("#0080FC"));


            for(int i=0; i<(params[0].size()-1); i++){
                int j = i;

                try {
                    GMapV2Direction md = new GMapV2Direction();

                    Document doc = md.getDocument(params[0].get(j), params[0].get(j+1), GMapV2Direction.MODE_DRIVING);

                    directionPoint = md.getDirection(doc);

                    for (int n = 0; n < directionPoint.size(); n++) {
                        rectLine.add(directionPoint.get(n));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                map.addPolyline(rectLine);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


}
