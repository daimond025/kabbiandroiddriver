package com.kabbi.driver.map;

import android.R;
import android.content.Context;
import com.kabbi.driver.database.entities.Route;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;

import java.util.ArrayList;
import java.util.List;

public class OverlayRect extends Overlay {

    OverlayRectItem overlayRectItem;
    Context mContext;
    MapController mMapController;
    RectRender rectRender;

     OverlayRect(MapController mapController, List<Route>routes) {
        super(mapController);
        mMapController = mapController;
        mContext = mapController.getContext();
        rectRender = new RectRender();
        setIRender(rectRender);
        // TODO Auto-generated constructor stub
        overlayRectItem = new OverlayRectItem(new GeoPoint(0,0), mContext.getResources().getDrawable(R.drawable.btn_star));

        for (Route route : routes) {
            overlayRectItem.geoPoint.add(new GeoPoint(route.getLat(), route.getLon()));
        }


        addOverlayItem(overlayRectItem);
    }

    @Override
    public List<OverlayItem> prepareDraw() {
        // TODO Auto-generated method stub
        ArrayList<OverlayItem> draw = new ArrayList<OverlayItem>();
        overlayRectItem.screenPoint.clear();
        for( GeoPoint point : overlayRectItem.geoPoint){
            overlayRectItem.screenPoint.add(mMapController.getScreenPoint(point));
        }
        draw.add(overlayRectItem);

        return draw;
    }

    @Override
    public int compareTo(Object another) {
        return 0;
    }
}
