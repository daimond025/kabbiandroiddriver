package com.kabbi.driver;


import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsMapActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_map);
    }
}
