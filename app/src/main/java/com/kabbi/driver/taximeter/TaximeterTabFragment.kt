package com.kabbi.driver.taximeter

/** Fragment with "Arrived" or "Cancel" button
 */

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Vibrator
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.maps.model.LatLng
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.events.taximeter.StartTaximeterEvent
import com.kabbi.driver.events.taximeter.StopTaximeterEvent
import com.kabbi.driver.events.taximeter.SummaryTaximeterEvent
import com.kabbi.driver.events.taximeter.TaximeterStageEvent
import com.kabbi.driver.fragments.WaitDialogFragment
import com.kabbi.driver.helper.*
import com.kabbi.driver.helper.AppParams.*
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.buildAddress
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class TaximeterTabFragment : Fragment() {

    internal lateinit var view: View
    internal var currentOrder: CurrentOrder? = null
    internal lateinit var order: Order
    internal lateinit var driver: Driver
    internal var service: Service? = null
    internal var workDay: WorkDay? = null
    private var curTime: Long = 0
    private var millisUntilFinished: Long = 0
    private var timeWait: Long = 0
    private var timeWaitPlanting: Long = 0
    private var delta_time: Long = 0
    private var updateDeltaTime: Long = 0
    private var timeWaitCityDay: Long = 0
    private var timeWaitCityNight: Long = 0
    private var timeWaitBaseDay: Long = 0
    private var timeWaitBaseNight: Long = 0
    private var timeDisCityDay: Long = 0
    private var timeDisCityNight: Long = 0
    private var timeDisBaseDay: Long = 0
    private var timeDisBaseNight: Long = 0
    internal lateinit var color: String
    internal lateinit var area: String
    private lateinit var btnTaximeter: Button
    private lateinit var btnTaximeterCancel: Button
    private lateinit var ibUp: ImageButton
    private lateinit var ibDown: ImageButton
    internal lateinit var progressBar: ProgressBar
    private lateinit var progressBarCancel: ProgressBar
    private lateinit var pBarWait: ProgressBar
    internal var stage: Int = 0
    private lateinit var statusOrder: String
    private lateinit var minutesStr: String
    private lateinit var secondsStr: String
    private lateinit var hoursStr: String
    private lateinit var minutesStrWait: String
    private lateinit var secondsStrWait: String
    private lateinit var hoursStrWait: String
    private lateinit var tvTimer: TextView
    private lateinit var tvAllTime: TextView
    private lateinit var tvTitle: TextView
    private lateinit var tvDis: TextView
    private lateinit var tvTimeWait: TextView
    private lateinit var tvPriceCount: TextView
    private lateinit var tvTariff: TextView
    private lateinit var tvAddress: TextView
    internal lateinit var requestParams: MutableMap<String, String>
    private var tariffCity: ClientTariff? = null
    private var tariffTrack: ClientTariff? = null
    private var disCityDay: Double = 0.toDouble()
    private var disCityNight: Double = 0.toDouble()
    private var disBaseDay: Double = 0.toDouble()
    private var disBaseNight: Double = 0.toDouble()
    private var priceCityDday: Double = 0.toDouble()
    private var priceCityDisNight: Double = 0.toDouble()
    private var priceBaseDday: Double = 0.toDouble()
    private var priceBaseDisNight: Double = 0.toDouble()
    private var priceCityWaitDay: Double = 0.toDouble()
    private var priceCityWaitNight: Double = 0.toDouble()
    private var priceBaseWaitDay: Double = 0.toDouble()
    private var priceBaseWaitNight: Double = 0.toDouble()
    private var includeCityTime: Long = 0
    private var includeTrackTime: Long = 0
    private var clickBtn: Boolean = false
    private var clickBtnCancel: Boolean = false
    private var tariffDay = ""
    private var tariffArea = ""
    internal var currency = ""
    private var handlerTaskInfo: Handler? = null
    private var runnable: Runnable? = null
    private var vibe: Vibrator? = null
    private var taximeterTabFragment: TaximeterTabFragment? = null
    private var notificationManager: NotificationManager? = null
    private var dateFormat: SimpleDateFormat? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var appPreferences: AppPreferences? = null
    private var waitDialogFragment: androidx.fragment.app.DialogFragment? = null
    private var requestType: RequestType = RequestType.NONE
    private var countDownTimer: CountDownTimer? = null
    private var timeZone: TimeZone? = null
    private var parseDate: Date? = null
    private var sdf: SimpleDateFormat? = null
    private var addressList: ArrayList<String>? = null
    private var position = 0
    private val arrAddresses = arrayOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private lateinit var db: AppDatabase
    private lateinit var presenter: TaximeterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        db = AppDatabase.getInstance(context!!)
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = (activity as WorkShiftActivity)
        driver = activity.driver!!
        service = activity.service
        workDay = activity.workDay
        order = activity.order!!

        val database = AppDatabase.getInstance(context!!)
        runBlocking {
            withContext(Dispatchers.IO) {
                currentOrder = database.currentOrderDao().getCurOrder(driver.id)
            }
        }

        vibe = activity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (currentOrder == null) {
            EventBus.getDefault().post(ChangeFragmentInOrderEvent(1))
            return
        }

        timeZone = TimeZone.getDefault()

        if (currentOrder == null || currentOrder!!.orderId == null) {

            CustomToast.showMessage(activity, "Order not found")

            activity.order = null
            activity.currentOrder = null
            activity.stageOrder = 0
            activity.onNavigationDrawerItemSelected(1)
        }

        presenter = TaximeterPresenter(currentOrder!!.orderId, driver)

        EventBus.getDefault().post(CloseDialogEvent("completed"))

        timeWait = 0
        timeWaitPlanting = 0
        disCityDay = 0.0
        disCityNight = 0.0
        disBaseDay = 0.0
        disBaseNight = 0.0
        priceCityWaitDay = 0.0
        priceCityWaitNight = 0.0
        priceBaseWaitDay = 0.0
        priceBaseWaitNight = 0.0
        priceCityDday = 0.0
        priceCityDisNight = 0.0
        priceBaseDday = 0.0
        priceBaseDisNight = 0.0
        timeDisCityDay = 0
        timeDisCityNight = 0
        timeDisBaseDay = 0
        timeDisBaseNight = 0

        timeWaitCityDay = 0
        timeWaitCityNight = 0
        timeWaitBaseDay = 0
        timeWaitBaseNight = 0

        includeCityTime = 0
        includeTrackTime = 0

        delta_time = 0
        updateDeltaTime = 0

        val clientTariffDao = database.clientTariffDao()

        runBlocking {
            withContext(Dispatchers.IO) {
                tariffCity = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_CITY)
                tariffTrack = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_TRACK)
            }
        }
        doStuff()
    }

    private fun lastPosition(): String? = runBlocking {
        withContext(Dispatchers.IO) {
            db.routeDao().getLastRoute().parking
        }
    }

    private fun doStuff() {

        val activity = activity as WorkShiftActivity

        sdf = SimpleDateFormat("MM.dd.yyyy", Locale.ENGLISH)
        dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH)
        try {
            parseDate = dateFormat!!.parse(order.orderTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        if (tariffCity == null || tariffTrack == null) {

            appPreferences!!.saveText("step_1", "0")
            appPreferences!!.saveText("step_2", "0")
            appPreferences!!.saveText("step_3", "0")
            appPreferences!!.saveText("step_4", "0")

            deleteCurrentOrder()

            activity.order = null
            activity.currentOrder = null
            activity.stageOrder = 0
            activity.onNavigationDrawerItemSelected(1)
        }

        if (order.startPoint == "in") {
            if (tariffCity?.accrual == ClientTariffParser.ACCRUAL_MIXED)
                includeCityTime = tariffCity!!.plantingIncludeTime.toLong()
            else if (tariffCity?.accrual == ClientTariffParser.ACCRUAL_TIME)
                includeCityTime = tariffCity!!.plantingInclude.toLong()
            else if (tariffCity?.accrual == ClientTariffParser.ACCRUAL_DISTANCE || tariffCity?.accrual == ClientTariffParser.ACCRUAL_FIX)
                includeCityTime = 0
        } else {
            if (tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_MIXED)
                includeTrackTime = tariffTrack!!.plantingIncludeTime.toLong()
            else if (tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_TIME)
                includeTrackTime = tariffTrack!!.plantingInclude.toLong()
            else if (tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_DISTANCE || tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_FIX)
                includeTrackTime = 0
        }

        taximeterTabFragment = this

        waitDialogFragment = WaitDialogFragment()

        clickBtn = false
        clickBtnCancel = false

        tvPriceCount.text = if (order.fix == 1) {
            TypefaceSpanEx.getCurrencyStr(activity, getView(), Round.roundFormatFinal(order.costOrder, order.rounding, order.roundingType))
        } else {
            TypefaceSpanEx.getCurrencyStr(activity, getView(), Round.roundFormatFinal(order.plantingCost + order.dopCost, order.rounding, order.roundingType))
        }

        val clientName = view.findViewById<TextView>(R.id.textview_profile_name)
        clientName.text = String.format("%s\ntel: %s", order.clientName, order.clientPhone)

        tariffDay = if (order.day) getString(R.string.day) else getString(R.string.night)
        tvTariff.text = String.format("%s %s", order.tariffLabel, tariffDay)

        stage = currentOrder!!.stage

        when (stage) {
            0 -> statusOrder = ORDER_STATUS_CONFIRM
            1 -> statusOrder = ORDER_STATUS_CAR_ASSIGNED
            2 -> statusOrder = ORDER_STATUS_COMPLETED
            3 -> {
            }
            else -> statusOrder = ORDER_STATUS_COMPLETED
        }

        EventBus.getDefault().post(StartTaximeterEvent(order, stage, statusOrder))

        setStage(stage)

        btnTaximeterCancel.setOnClickListener {
            if (btnTaximeterCancel.tag.toString() == "cancel") {
                btnTaximeterCancel.visibility = View.GONE
                progressBarCancel.visibility = View.VISIBLE

                if (!clickBtnCancel) {
                    clickBtn = true
                    requestParams = LinkedHashMap()
                    requestParams["order_id"] = currentOrder!!.orderId
                    requestParams["status_new_id"] = ORDER_STATUS_CANCEL_IN_WORK
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign

                    val uuid = presenter.getNewUuid()

                    if (SET_ORDER_PARAMS_VIA_SOCKET) {
                        presenter.setOrderStatus(ORDER_STATUS_CANCEL_IN_WORK, RequestType.CANCEL)
                    } else {
                        WebService.setOrderStatus(requestParams, driver.secretCode, uuid, RequestType.CANCEL)
                    }
                }
            } else if (btnTaximeterCancel.tag.toString() == "pause") {
                waitDialogFragment!!.isCancelable = false
                waitDialogFragment!!.show(activity.supportFragmentManager, "dialogFragmentWait")
            }
        }

        btnTaximeter.setOnClickListener {
            if (stage == 0 && appPreferences!!.getText("deny_setting_arrival_status_early") == "1") {
                val jsonObjectAddress = JSONObject(order.address)
                val meterToPoint = DistanceGPS.getDistanceMeter(
                        jsonObjectAddress.getJSONObject("A").getString("lat").toDouble(),
                        jsonObjectAddress.getJSONObject("A").getString("lon").toDouble(),
                        activity.currentLat,
                        activity.currentLon)

                if (meterToPoint > appPreferences!!.getText("min_distance_to_set_arrival_status").toDouble()) {

                    CustomToast.showMessage(context, String.format(getString(R.string.min_start), meterToPoint.toString()))
                    return@setOnClickListener
                }
            }

            if (InternetConnection.isOnline(activity)) {
                btnTaximeter.visibility = View.GONE
                progressBar.visibility = View.VISIBLE

                if (!clickBtn) {
                    clickBtn = true

                    val uuid = presenter.getNewUuid()

                    val status = when (stage) {

                        0 -> {
                            pBarWait.visibility = View.VISIBLE
                            tvTimer.visibility = View.GONE
                            ORDER_STATUS_ARRIVED
                        }

                        1 -> ORDER_STATUS_CAR_ASSIGNED

                        2 -> ORDER_STATUS_STOP_ORDER

                        else -> return@setOnClickListener
                    }

                    requestParams = LinkedHashMap()
                    requestParams["order_id"] = currentOrder!!.orderId
                    requestParams["status_new_id"] = status
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign

                    countDownTimer!!.start()

                    if (SET_ORDER_PARAMS_VIA_SOCKET) {
                        presenter.setOrderStatus(status, RequestType.RESERVE)
                    } else {
                        WebService.setOrderStatus(requestParams, driver.secretCode, uuid, RequestType.RESERVE)
                    }
                }
            } else {
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }

        ibUp.setOnClickListener {
            try {
                if (position > 0) {
                    tvAddress.text = String.format("%s", addressList!![position - 1])
                    position -= 1
                    vibe!!.vibrate(50)
                    if (position == 0)
                        ibUp.setBackgroundResource(R.drawable.up_block)
                    else
                        ibUp.setBackgroundResource(R.drawable.up_active)

                    if (position == addressList!!.size - 1) {
                        ibDown.setBackgroundResource(R.drawable.down_block)
                    } else {
                        ibDown.setBackgroundResource(R.drawable.down_active)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        ibDown.setOnClickListener {
            try {
                tvAddress.text = String.format("%s", addressList!![position + 1])
                position += 1
                vibe!!.vibrate(100)
                if (position > 0)
                    ibUp.setBackgroundResource(R.drawable.up_active)
                else
                    ibUp.setBackgroundResource(R.drawable.up_block)

                if (position == addressList!!.size - 1) {
                    ibDown.setBackgroundResource(R.drawable.down_block)
                } else {
                    ibDown.setBackgroundResource(R.drawable.down_active)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        countDownTimer = object : CountDownTimer(TIME.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                try {
                    requestParams = LinkedHashMap()
                    requestParams["request_id"] = presenter.getUuid()
                    requestParams["tenant_login"] = service!!.uri
                    requestParams["worker_login"] = driver.callSign
                    WebService.getResult(requestParams, driver.secretCode)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun changeArrowsState(stage: Int) {
        addressList = ArrayList()
        try {
            val jsonObjectAddress = JSONObject(order.address)
            for (itemAddress in arrAddresses) {
                if (!jsonObjectAddress.has(itemAddress)) break
                val json = jsonObjectAddress.getJSONObject(itemAddress)
                val address = buildAddress(LatLng(json.getDouble("lat"), json.getDouble("lon")), json)
                addressList!!.add(address)
            }

            if (stage == 0) {
                ibUp.setBackgroundResource(R.drawable.up_block)
                tvAddress.text = addressList!![0]
                if (addressList!!.size > 1)
                    ibDown.setBackgroundResource(R.drawable.down_active)
                else
                    ibDown.setBackgroundResource(R.drawable.down_block)
            } else {
                if (addressList!!.size > 1) {
                    position = 1
                    tvAddress.text = addressList!![1]
                    ibUp.setBackgroundResource(R.drawable.up_block)
                    ibDown.setBackgroundResource(R.drawable.down_block)
                    if (position > 0)
                        ibUp.setBackgroundResource(R.drawable.up_active)
                    if (position < addressList!!.size - 1) {
                        ibDown.setBackgroundResource(R.drawable.down_active)
                    }
                } else {
                    ibUp.setBackgroundResource(R.drawable.up_block)
                    ibDown.setBackgroundResource(R.drawable.down_block)
                    tvAddress.text = getString(R.string.to_coord)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_taximeter_tab_taxi, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        appPreferences = AppPreferences(activity!!)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        notificationManager = activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager!!.cancelAll()

        tvDis = view.findViewById(R.id.textview_taximeter_dis)
        tvAllTime = view.findViewById(R.id.texview_taximeter_alltime)
        tvTimer = view.findViewById(R.id.textview_taximeter_timer)
        tvTitle = view.findViewById(R.id.textview_taximeter_title)
        tvTimeWait = view.findViewById(R.id.textview_taximeter_timewait)
        tvPriceCount = view.findViewById(R.id.textview_taximeter_pricecount)
        tvTariff = view.findViewById(R.id.textview_taximeter_tariff)
        tvAddress = view.findViewById(R.id.tv_address)

        btnTaximeter = view.findViewById(R.id.button_taximeter_tab1)
        progressBar = view.findViewById(R.id.pbHeaderProgress)
        pBarWait = view.findViewById(R.id.pb_wait)

        btnTaximeterCancel = view.findViewById(R.id.button_taximeter_tab1_cancel)
        progressBarCancel = view.findViewById(R.id.pbHeaderProgress_cancel)
        ibUp = view.findViewById(R.id.ib_up)
        ibDown = view.findViewById(R.id.ib_down)

        btnTaximeterCancel.tag = "cancel"
    }

    override fun onStart() {
        super.onStart()

        changeArrowsState(stage)
        handlerTaskInfo = Handler()

        runnable = Runnable {
            if (parseDate != null && updateDeltaTime != (activity as WorkShiftActivity).serverTime) {
                delta_time = Date().time - ((activity as WorkShiftActivity).serverTime * 1000 - timeZone!!.getOffset(parseDate!!.time))
                updateDeltaTime = (activity as WorkShiftActivity).serverTime
            }

            curTime = Date().time/* - delta_time*/


            when (stage) {
                0 -> try {
                    millisUntilFinished = 0

                    if (curTime - currentOrder!!.beginOrderTime <= currentOrder!!.timeToClient) {
                        color = colorGreen(activity!!.applicationContext)
                        millisUntilFinished = currentOrder!!.beginOrderTime + currentOrder!!.timeToClient - curTime
                    } else {
                        color = colorRed(activity!!.applicationContext)
                        millisUntilFinished = curTime - (currentOrder!!.beginOrderTime + currentOrder!!.timeToClient)
                    }
                    hoursStr = if ((millisUntilFinished / 3600000).toString().length > 1) (millisUntilFinished / 3600000).toString() else "0" + (millisUntilFinished / 3600000).toString()
                    minutesStr = if ((millisUntilFinished % 3600000 / 60000).toString().length > 1) (millisUntilFinished % 3600000 / 60000).toString() else "0" + (millisUntilFinished % 3600000 / 60000).toString()
                    secondsStr = if ((millisUntilFinished % 3600000 % 60000 / 1000).toString().length > 1) (millisUntilFinished % 3600000 % 60000 / 1000).toString() else "0" + (millisUntilFinished % 3600000 % 60000 / 1000).toString()


                    if (millisUntilFinished / 3600000 > 0)
                        tvTimer.text = Html.fromHtml("<big><big><font color='$color'>$hoursStr:$minutesStr:$secondsStr</font></big></big>")
                    else
                        tvTimer.text = Html.fromHtml("<big><big><font color='$color'>$minutesStr:$secondsStr</font></big></big>")
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

                1 -> try {
                    var tariffTimeWait: Long = 0
                    tariffTimeWait = if (area == "base") {
                        if (order.day) {
                            JSONObject(currentOrder!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataCity").getString("wait_time_day").toLong() * 60 * 1000
                        } else {
                            JSONObject(currentOrder!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataCity").getString("wait_time_night").toLong() * 60 * 1000
                        }
                    } else {
                        if (order.day) {
                            JSONObject(currentOrder!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataTrack").getString("wait_time_day").toLong() * 60 * 1000
                        } else {
                            JSONObject(currentOrder!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataTrack").getString("wait_time_night").toLong() * 60 * 1000
                        }
                    }

                    color = colorBlack(activity!!.applicationContext)
                    if (order.pasName == ORDER_LABEL_RESERVE) {
                        if (dateFormat!!.parse(order.orderTime).time + tariffTimeWait > curTime) {
                            color = colorGreen(activity!!.applicationContext)
                            millisUntilFinished = Math.abs(curTime - dateFormat!!.parse(order.orderTime).time - tariffTimeWait)
                        } else {
                            color = colorRed(activity!!.applicationContext)
                            millisUntilFinished = curTime - dateFormat!!.parse(order.orderTime).time - tariffTimeWait

                        }
                    } else {
                        if (currentOrder!!.waitTimeBegin + tariffTimeWait > curTime) {
                            color = colorGreen(activity!!.applicationContext)
                            millisUntilFinished = tariffTimeWait - (curTime - currentOrder!!.waitTimeBegin)
                        } else {
                            color = colorRed(activity!!.applicationContext)
                            millisUntilFinished = curTime - currentOrder!!.waitTimeBegin - tariffTimeWait

                        }
                    }

                    hoursStr = if ((millisUntilFinished / 3600000).toString().length > 1) (millisUntilFinished / 3600000).toString() else "0" + (millisUntilFinished / 3600000).toString()
                    minutesStr = if ((millisUntilFinished % 3600000 / 60000).toString().length > 1) (millisUntilFinished % 3600000 / 60000).toString() else "0" + (millisUntilFinished % 3600000 / 60000).toString()
                    secondsStr = if ((millisUntilFinished % 3600000 % 60000 / 1000).toString().length > 1) (millisUntilFinished % 3600000 % 60000 / 1000).toString() else "0" + (millisUntilFinished % 3600000 % 60000 / 1000).toString()
                    if (millisUntilFinished / 3600000 > 0)
                        tvTimer.text = Html.fromHtml("<big><big><font color='$color'>$hoursStr:$minutesStr:$secondsStr</font></big></big>")
                    else
                        tvTimer.text = Html.fromHtml("<big><big><font color='$color'>$minutesStr:$secondsStr</font></big></big>")


                    val timeToCancel = (Integer.valueOf(appPreferences!!.getText("allow_cancel_order_after_time")) * 60 * 1000).toLong()

                    if (order.pasName == ORDER_LABEL_NEW && appPreferences!!.getText("allow_cancel_order") == "1" && curTime - currentOrder!!.waitTimeBegin - tariffTimeWait > timeToCancel || order.pasName == ORDER_LABEL_RESERVE && appPreferences!!.getText("allow_cancel_order") == "1" && curTime - dateFormat!!.parse(order.orderTime).time - tariffTimeWait > timeToCancel) {
                        view.findViewById<View>(R.id.ll_taximeter_cancel).visibility = View.VISIBLE
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                2 -> tvTimer.visibility = View.INVISIBLE
            }

            handlerTaskInfo!!.postDelayed(runnable, 1000)
        }
        handlerTaskInfo!!.post(runnable)
    }

    private fun setStage(orderStage: Int) {
        activity?.apply {
            when (orderStage) {
                1 -> this.runOnUiThread {
                    changeArrowsState(stage)
                    (this as WorkShiftActivity).stage = stage
                    btnTaximeter.text = getString(R.string.text_taximeter_btn_wait)
                    tvTitle.text = getString(R.string.text_taximeter_status_wait)
                }
                2 -> this.runOnUiThread {
                    changeArrowsState(stage)
                    (this as WorkShiftActivity).stage = stage
                    view.findViewById<View>(R.id.ll_taximeter_cancel).visibility = View.GONE
                    view.findViewById<View>(R.id.ll_taximeter_time).visibility = View.GONE
                    btnTaximeter.text = getString(R.string.text_taximeter_btn_stop)
                    tvTitle.text = getString(R.string.text_taximeter_status_tracking)
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        handlerTaskInfo!!.removeCallbacks(runnable)
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(summaryTaximeterEvent: SummaryTaximeterEvent) {
        try {
            area = summaryTaximeterEvent.area

            tvPriceCount.text = TypefaceSpanEx.getCurrencyStr(activity, getView(), Round.roundFormat(summaryTaximeterEvent.summaryCost, order!!.rounding))

            tariffArea = if (summaryTaximeterEvent.area == "base") getString(R.string.city) else getString(R.string.track)
            tvTariff.text = String.format("%s %s %s", order.tariffLabel, tariffArea, tariffDay)

            var disCity = summaryTaximeterEvent.order.disCityInOrder
            var disTrack = summaryTaximeterEvent.order.disTrackInOrder

            if (order.startPoint == "in") {
                if (tariffCity!!.accrual == ClientTariffParser.ACCRUAL_DISTANCE ||
                        tariffCity!!.accrual == ClientTariffParser.ACCRUAL_MIXED ||
                        tariffCity!!.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                    disCity += tariffCity!!.plantingInclude
                    if (disCity < 0) disCity = 0.0
                }
            } else {
                if (tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_DISTANCE ||
                        tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_MIXED ||
                        tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                    disTrack += tariffTrack!!.plantingInclude
                    if (disTrack < 0) disTrack = 0.0
                }
            }

            tvDis.text = (((disCity + disTrack) * 1000).roundToInt().toDouble() / 1000).toString()

            timeWait = (if (summaryTaximeterEvent.order.timeWaitCityInOrder > 0) summaryTaximeterEvent.order.timeWaitCityInOrder else 0) + if (summaryTaximeterEvent.order.timeWaitTrackInOrder > 0) summaryTaximeterEvent.order.timeWaitTrackInOrder else 0

            if (timeWait > 0) {
                hoursStrWait = if ((timeWait / 3600000).toString().length > 1) (timeWait / 3600000).toString() else "0" + (timeWait / 3600000).toString()
                minutesStrWait = if ((timeWait % 3600000 / 60000).toString().length > 1) (timeWait % 3600000 / 60000).toString() else "0" + (timeWait % 3600000 / 60000).toString()
                secondsStrWait = if ((timeWait % 3600000 % 60000 / 1000).toString().length > 1) (timeWait % 3600000 % 60000 / 1000).toString() else "0" + (timeWait % 3600000 % 60000 / 1000).toString()

                color = String.format("#%06X", 0xFFFFFF and typedValueMain!!.data)

                if (timeWait / 3600000 > 0) {
                    tvTimeWait.text = Html.fromHtml("<font color='$color'>$hoursStrWait:$minutesStrWait:$secondsStrWait</font>")
                } else {
                    tvTimeWait.text = Html.fromHtml("<font color='$color'>$minutesStrWait:$secondsStrWait</font>")
                }
            }

            millisUntilFinished = ((if (summaryTaximeterEvent.order.timeCityInOrder > 0)
                summaryTaximeterEvent.order.timeCityInOrder + includeCityTime * 60000
            else
                summaryTaximeterEvent.order.timeCityInOrder + includeCityTime * 60000)
                    + (if (summaryTaximeterEvent.order.timeTrackInOrder > 0)
                summaryTaximeterEvent.order.timeTrackInOrder + includeTrackTime * 60000
            else
                summaryTaximeterEvent.order.timeTrackInOrder + includeTrackTime * 60000)
                    + (if (summaryTaximeterEvent.order.timeWaitCityInOrder > 0) summaryTaximeterEvent.order.timeWaitCityInOrder else 0)
                    + (if (summaryTaximeterEvent.order.timeWaitTrackInOrder > 0) summaryTaximeterEvent.order.timeWaitTrackInOrder else 0)
                    + summaryTaximeterEvent.order.timeFreeInOrder)

            hoursStr = if ((millisUntilFinished / 3600000).toString().length > 1) (millisUntilFinished / 3600000).toString() else "0" + (millisUntilFinished / 3600000).toString()
            minutesStr = if ((millisUntilFinished % 3600000 / 60000).toString().length > 1) (millisUntilFinished % 3600000 / 60000).toString() else "0" + (millisUntilFinished % 3600000 / 60000).toString()
            secondsStr = if ((millisUntilFinished % 3600000 % 60000 / 1000).toString().length > 1) (millisUntilFinished % 3600000 % 60000 / 1000).toString() else "0" + (millisUntilFinished % 3600000 % 60000 / 1000).toString()

            tvAllTime.text = "$hoursStr:$minutesStr:$secondsStr"

            if (appPreferences!!.getText("gps") == "1" && appPreferences!!.getText("gps_server") == "1" && summaryTaximeterEvent.stage == 2 || order.handBrake == "0" && summaryTaximeterEvent.stage == 2) {
                view.findViewById<View>(R.id.ll_taximeter_cancel).visibility = View.VISIBLE
                btnTaximeterCancel.text = getString(R.string.text_button_pausework)
                btnTaximeterCancel.tag = "pause"
                btnTaximeterCancel.setBackgroundDrawable(resources.getDrawable(R.drawable.custom_button_orange))
                EventBus.getDefault().post(TimerEvent())
            }

        } catch (e: NullPointerException) {

        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        changeArrowsState(stage)

        orderForIdEvent.json?.let { event ->

            val data = event.getJSONObject("result").getJSONObject("order_data")
            order.also { order ->
                if (order.orderId == data.getString("order_id")) {

                    val cost = data.getJSONObject("costData")
                    if (cost.getInt("is_fix") == 1) {
                        order.fix = 1
                        order.costOrder = cost.getDouble("summary_cost")
                        tvPriceCount.text = "${order.costOrder}"
                    }

                    clickBtn = false
                    progressBar.visibility = View.GONE
                    btnTaximeter.visibility = View.VISIBLE
                }
            }
        }
    }

    @Subscribe
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {
        if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 1) {
            requestType = orderStatusEvent.type

            activity?.runOnUiThread {
                pBarWait.visibility = View.GONE
                tvTimer.visibility = View.VISIBLE
            }
            curTime = if (orderStatusEvent.cityTime == 0L) Date().time else orderStatusEvent.cityTime

        } else if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 2) {
            responseUpdate()
            requestType = orderStatusEvent.type

        } else {
            val status = orderStatusEvent.status
            showSnackBar(status)
            if (status != "FAIL" || status != RESPONSE_ERROR) {
                clearOrder()
            }
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        if (pushResponseEvent.infoCode == "OK" && pushResponseEvent.uuid == presenter.getUuid()) {

            countDownTimer!!.cancel()

            if (requestType != RequestType.CANCEL) {
                responseUpdate()
            }
        } else if (pushResponseEvent.uuid == presenter.getUuid()) {
            clearOrder()
        }

        clickBtn = false
        clickBtnCancel = false

        activity!!.runOnUiThread {
            progressBar.visibility = View.GONE
            btnTaximeter.visibility = View.VISIBLE
            progressBarCancel.visibility = View.GONE
        }
    }

    private fun clearOrder() {
        EventBus.getDefault().post(StopTimer())
        EventBus.getDefault().post(StopTaximeterEvent("close"))

        deleteCurrentOrder()
        deleteOrder()

        val activity = (activity as WorkShiftActivity)
        activity.showToastRejected()
        activity.order = null
        activity.currentOrder = null
        activity.stageOrder = 0
        activity.onNavigationDrawerItemSelected(1)
    }

    @Subscribe
    fun onEvent(requestResult: NetworkRequestResult) {

        when (requestResult.status) {

            "OK" -> {
                countDownTimer!!.cancel()

                if (requestType != RequestType.CANCEL) {
                    responseUpdate()
                }
                presenter.clearUuid()
                showButtons()
            }
            else -> showSnackBar(requestResult.status)
        }
    }

    private fun showSnackBar(status: String) {
        val message = when (status) {
            EMPTY_RESPONSE -> getString(R.string.contact_support)
            RESPONSE_ERROR -> getString(R.string.response_error)
            else -> getString(R.string.request_error)
        }
        val snackbar = Snackbar.make(view.findViewById(R.id.tv_address), message, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("OK") {
            snackbar.dismiss()
            pBarWait.visibility = View.GONE
            tvTimer.visibility = View.VISIBLE
            showButtons()
        }
        snackbar.show()
    }

    private fun showButtons() {
        clickBtn = false
        clickBtnCancel = false

        activity!!.runOnUiThread {
            progressBar.visibility = View.GONE
            btnTaximeter.visibility = View.VISIBLE
            progressBarCancel.visibility = View.GONE
            btnTaximeterCancel.visibility = View.VISIBLE
        }
    }

    private fun responseUpdate() {
        when (stage) {
            0 -> {
                stage = 1

                EventBus.getDefault().post(TaximeterStageEvent(1, ORDER_STATUS_ARRIVED))

                currentOrder!!.stage = stage
                currentOrder!!.waitTimeBegin = Date().time
                saveCurrentOrder()

                setStage(stage)

                clickBtn = false

                activity!!.runOnUiThread {
                    progressBar.visibility = View.GONE
                    btnTaximeter.visibility = View.VISIBLE
                }
            }
            1 -> {
                stage = 2

                EventBus.getDefault().post(TaximeterStageEvent(2, ORDER_STATUS_CAR_ASSIGNED))

                currentOrder!!.stage = stage
                currentOrder!!.waitTimeEnd = Date().time
                saveCurrentOrder()

                setStage(stage)

                clickBtn = false

                activity!!.runOnUiThread {
                    progressBar.visibility = View.GONE
                    btnTaximeter.visibility = View.VISIBLE
                }
            }
            2 -> {
                stage = 3
                val activity = activity as WorkShiftActivity

                try {
                    val lastPoint = lastPosition()
                    if (lastPoint != null) {
                        when (lastPoint) {
                            "base" -> {
                                order.rounding = tariffCity!!.rounding
                                order.minPrice = tariffCity!!.minPrice
                                order.roundingType = tariffCity!!.roundingType
                            }
                            "track" -> {
                                order.rounding = tariffTrack!!.rounding
                                order.minPrice = tariffTrack!!.minPrice
                                order.roundingType = tariffTrack!!.roundingType
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                EventBus.getDefault().post(TaximeterStageEvent(3, ORDER_STATUS_COMPLETED))

                currentOrder!!.stage = stage
                saveCurrentOrder()

                order.address = currentOrder!!.address
                order.tariffData = currentOrder!!.tariffData


                var disCity = activity.order!!.disCityInOrder
                var disTrack = activity.order!!.disTrackInOrder


                if (order.startPoint == "in") {
                    if (tariffCity!!.accrual == ClientTariffParser.ACCRUAL_DISTANCE ||
                            tariffCity!!.accrual == ClientTariffParser.ACCRUAL_MIXED ||
                            tariffCity!!.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                        disCity += tariffCity!!.plantingInclude
                        if (disCity < 0) disCity = 0.0
                    }
                } else {
                    if (tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_DISTANCE ||
                            tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_MIXED ||
                            tariffTrack!!.accrual == ClientTariffParser.ACCRUAL_INTERVAL) {
                        disTrack += tariffTrack!!.plantingInclude
                        if (disTrack < 0) disTrack = 0.0
                    }
                }

                order.sumDistance = Round.roundFormatDis(disCity + disTrack)


                millisUntilFinished = ((if (activity.order!!.timeCityInOrder > 0) activity.order!!.timeCityInOrder + includeCityTime * 60000 else activity.order!!.timeCityInOrder + includeCityTime * 60000)
                        + (if (activity.order!!.timeTrackInOrder > 0) activity.order!!.timeTrackInOrder + includeTrackTime * 60000 else activity.order!!.timeTrackInOrder + includeTrackTime * 60000)
                        + (if (activity.order!!.timeWaitCityInOrder > 0) activity.order!!.timeWaitCityInOrder else 0)
                        + if (activity.order!!.timeWaitTrackInOrder > 0) activity.order!!.timeWaitTrackInOrder else 0)


                order.sumTime = millisUntilFinished

                if (order.fix == 0) {
                    order.costOrder = (activity.order!!.priceTrackInOrder
                            + activity.order!!.priceCityInOrder
                            + activity.order!!.priceClientWait
                            + activity.order!!.priceCityWaitInOrder
                            + activity.order!!.priceTrackWaitInOrder
                            + activity.order!!.priceIntervalInOrder
                            + activity.order!!.priceTrackOutOrder
                            + order.plantingCost + order.dopCost)

                    if (order.costOrder < order.minPrice)
                        order.costOrder = order.minPrice
                }

                order.typeCostBase = tariffCity!!.accrual
                order.typeCostTrack = tariffTrack!!.accrual
                order.priceIntervalInOrder = activity.order!!.priceIntervalInOrder

                runBlocking {
                    withContext(Dispatchers.IO) {
                        db.orderDao().save(order)
                    }
                }

                activity.order = order
                activity.currentOrder = currentOrder
                activity.stageOrder = 2
                activity.onNavigationDrawerItemSelected(1)

                activity.setIsPayBtnActive(true)
            }
        }
    }

    private fun saveCurrentOrder() = runBlocking {
        withContext(Dispatchers.IO) {
            currentOrder?.also { current -> db.currentOrderDao().save(current) }
        }
    }

    private fun deleteCurrentOrder() = runBlocking {
        withContext(Dispatchers.IO) {
            db.currentOrderDao().deleteAll(driver.id)
        }
    }

    private fun deleteOrder() = runBlocking {
        withContext(Dispatchers.IO) {
            db.orderDao().delete(order)
        }
    }
}