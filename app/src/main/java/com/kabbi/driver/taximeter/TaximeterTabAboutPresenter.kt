package com.kabbi.driver.taximeter

import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.kabbi.driver.EditAddressesSecondActivity
import com.kabbi.driver.R
import com.kabbi.driver.border.OrderPagerBorderFragmentRes
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.events.taximeter.CostChange
import com.kabbi.driver.fragments.AddressDialogFragment
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.UuidProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.util.*

class TaximeterTabAboutPresenter(val fragment: TaximeterTabAboutFragment,
                                 val activity: androidx.fragment.app.FragmentActivity = fragment.activity!!,
                                 val appPreferences: AppPreferences = AppPreferences(activity)
) : UuidProvider() {

    private var list: MutableList<String> = mutableListOf()
    private var listAddresses: MutableList<EditAddress> = arrayListOf()
    private var currentOrder: CurrentOrder? = null
    private var jsonTariff: JSONObject? = null
    private var db: AppDatabase = AppDatabase.getInstance(activity)
    private lateinit var jsonObjectOrder: JSONObject
    private var stringBuilder: StringBuilder = StringBuilder()
    private val orderPagerBorderFragmentRes = OrderPagerBorderFragmentRes()

    private var service: Service
    private var order: Order
    private var workDay: WorkDay
    private var driver: Driver
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

    init {
        val activity = activity as EditAddressesSecondActivity

        driver = activity.driver
        service = activity.service
        workDay = activity.workDay
        order = activity.order

        runBlocking {
            withContext(Dispatchers.IO) {
                currentOrder = db.currentOrderDao().getCurOrder(driver.id)
            }
        }

        if (currentOrder == null) {
            EventBus.getDefault().post(ChangeFragmentInOrderEvent(1))
        } else {
            list = updateAddress(currentOrder!!.address)
            fragment.setup(order.comment, list)
        }

        if (appPreferences.getText("allow_edit_order") == "0") {
            fragment.enableAddButton()
        }
        EventBus.getDefault().register(this)
    }

    fun onButtonClicked(button: String) {
        when (button) {

            "tariff" -> {
                if (jsonTariff == null) {
                    val orderJsonTariff = JSONObject(currentOrder!!.tariffData)
                    val dataCity = orderJsonTariff.getJSONObject("tariffInfo").getJSONObject("tariffDataCity")
                    val dataTrack = orderJsonTariff.getJSONObject("tariffInfo").getJSONObject("tariffDataTrack")
                    jsonTariff = JSONObject()
                    jsonTariff!!.put("info", "OK")
                    jsonTariff!!.put("code", 0)
                    jsonTariff!!.put("result", JSONArray().put(JSONObject().put("tariffInfo", JSONObject().put("isDay", orderJsonTariff.getJSONObject("tariffInfo").getInt("isDay")).put("tariffLabel", order.tariffLabel).put("tariffType", orderJsonTariff.getJSONObject("tariffInfo").getString("tariffType")).put("tariffDataCity", dataCity).put("tariffDataTrack", dataTrack))))
                }
                val builder = AlertDialog.Builder(activity)
                builder.setView(orderPagerBorderFragmentRes.getViewTariffs(activity, jsonTariff!!)).setPositiveButton(activity.getString(R.string.button_exit)) { dialog, _ -> dialog.dismiss() }.create().show()
            }

            "fab" -> if (list.size >= AppParams.COUNT_ADDRESS) {
                CustomToast.showMessage(activity, activity.getString(R.string.toast_max_source))
            } else {
                val dialogFragmentAddress = AddressDialogFragment()
                dialogFragmentAddress.show(fragment.fragmentManager!!, "fragmentAddress")
            }

            "button_confirm" -> {
                val requestParams = LinkedHashMap<String, String>()
                requestParams["address"] = URLEncoder.encode(getJSONAddress(list).toString(), "UTF-8").replace("+", "%20")
                requestParams["order_id"] = currentOrder!!.orderId
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign
                WebService.updateOrder(requestParams, driver.secretCode, getNewUuid())
                fragment.showProgressBar()
            }

            "button_updatecost" -> {
                val viewDialog = activity.layoutInflater.inflate(R.layout.fragment_dialog_edittext_ordercost, null)
                val alertDialog = AlertDialog.Builder(activity)
                alertDialog.setTitle(activity.getString(R.string.new_ordercost))
                alertDialog.setView(viewDialog)
                alertDialog.setNegativeButton(activity.getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                alertDialog.setPositiveButton(activity.getString(R.string.activities_MainActivity_permission_dialog_ok)) { dialog, _ ->
                    val orderCost = viewDialog.findViewById<EditText>(R.id.et_ordercost).text.toString()
                    if (orderCost.isNotBlank()) {
                        val requestParams = LinkedHashMap<String, String>()
                        requestParams["order_id"] = currentOrder!!.orderId
                        requestParams["predv_price"] = orderCost
                        requestParams["tenant_login"] = service.uri
                        requestParams["worker_login"] = driver.callSign

                        if (AppParams.SET_ORDER_PARAMS_VIA_SOCKET) {
                            EventBus.getDefault().post(CostChange(getNewUuid(), orderCost, currentOrder!!.orderId, driver.callSign))
                        } else {
                            WebService.updateOrderCost(requestParams, driver.secretCode, getNewUuid())
                        }

                        dialog.dismiss()
                    } else {
                        viewDialog.findViewById<EditText>(R.id.et_ordercost).error = activity.getString(R.string.edittext_valid_required)
                    }
                }
                val dialog = alertDialog.create()
                dialog.show()
            }
        }
    }

    private fun getJSONAddress(list: List<String>): JSONObject {
        val jsonObjectAddresses = JSONObject()
        val jsonArrayAddresses = JSONArray()
        try {
            for (item in list) {
                val jsonItem = JSONObject()
                for (itemAddress in listAddresses) {
                    if (itemAddress.fullAddress == item) {
                        try {
                            jsonItem.put("city_id", itemAddress.cityId)
                            jsonItem.put("city", "")
                            jsonItem.put("street", itemAddress.fullAddress)
                            jsonItem.put("house", "")
                            jsonItem.put("housing", "")
                            jsonItem.put("porch", "")
                            jsonItem.put("lat", itemAddress.lat)
                            jsonItem.put("lon", itemAddress.lon)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        break
                    }
                }
                jsonArrayAddresses.put(jsonItem)
            }
            jsonObjectAddresses.put("address", jsonArrayAddresses)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return jsonObjectAddresses
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        if (pushResponseEvent.infoCode == "OK" && pushResponseEvent.uuid == getUuid() && pushResponseEvent.jsonObject != null) {
            try {
                val jsonNewOrder = pushResponseEvent.jsonObject
                val jsonDB = JSONObject(currentOrder!!.address)
                val jsonNewDB = JSONObject()

                jsonNewDB.put("A", jsonDB.getJSONObject("A"))
                for ((count, editAddress) in listAddresses.withIndex()) {
                    jsonNewDB.put(arrAddresses[count], editAddress.editAddressToJson())
                }

                if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1")
                    order.fix = 1
                else
                    order.fix = jsonNewOrder.getInt("is_fix")

                order.costOrder = java.lang.Double.valueOf(jsonNewOrder.getString("summary_cost"))
                order.dopCost = java.lang.Double.valueOf(jsonNewOrder.getString("additionals_cost"))
                currentOrder!!.address = jsonNewDB.toString()
                order.address = jsonNewDB.toString()
                runBlocking {
                    withContext(Dispatchers.IO) {
                        db.currentOrderDao().save(currentOrder!!)
                        db.orderDao().save(order)
                    }
                }
                EventBus.getDefault().post(UpdateAdressesToMapEvent())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else if (pushResponseEvent.uuid == getUuid()) {
            activity.runOnUiThread { updateAddress(currentOrder!!.address) }
        }
        activity.runOnUiThread {
            try {
                if (appPreferences.getText("allow_edit_order") == "1") {
                    fragment.showConfirmButton()
                }
                fragment.hideProgressBar()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun updateAddress(json: String): MutableList<String> {
        listAddresses.clear()
        val addressList = ArrayList<String>()
        jsonObjectOrder = JSONObject(json)
        stringBuilder.setLength(0)
        if (jsonObjectOrder.getJSONObject("A").getString("city").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("city"))
            stringBuilder.append(jsonObjectOrder.getJSONObject("A").getString("city") + ", ")
        stringBuilder.append(jsonObjectOrder.getJSONObject("A").getString("street"))
        if (jsonObjectOrder.getJSONObject("A").getString("house").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("house"))
            stringBuilder.append(", " + activity.getString(R.string.text_house) + " " + jsonObjectOrder.getJSONObject("A").getString("house"))
        if (jsonObjectOrder.getJSONObject("A").getString("housing").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("housing"))
            stringBuilder.append(", " + activity.getString(R.string.text_housing) + " " + jsonObjectOrder.getJSONObject("A").getString("housing"))
        if (jsonObjectOrder.getJSONObject("A").getString("porch").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("porch"))
            stringBuilder.append(", " + activity.getString(R.string.text_porch) + " " + jsonObjectOrder.getJSONObject("A").getString("porch"))
        try {
            if (jsonObjectOrder.getJSONObject("A").getString("apt").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("apt"))
                stringBuilder.append(", " + activity.getString(R.string.text_apt) + " " + jsonObjectOrder.getJSONObject("A").getString("apt"))
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        fragment.setStartPointText(stringBuilder.toString())

        for (itemAddress in arrAddresses) {
            try {
                jsonObjectOrder.getJSONObject(itemAddress)
                stringBuilder.setLength(0)
                val editAddress = EditAddress()
                if (!jsonObjectOrder.getJSONObject(itemAddress).isNull("city") && jsonObjectOrder.getJSONObject(itemAddress).getString("city").isNotEmpty()) {
                    stringBuilder.append(jsonObjectOrder.getJSONObject(itemAddress).getString("city"))
                    stringBuilder.append(", " + jsonObjectOrder.getJSONObject(itemAddress).getString("street"))
                } else {
                    stringBuilder.append(jsonObjectOrder.getJSONObject(itemAddress).getString("street"))
                }
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("house").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("house"))
                    stringBuilder.append(", " + activity.getString(R.string.text_house) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("house"))
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("housing").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("housing"))
                    stringBuilder.append(", " + activity.getString(R.string.text_housing) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("housing"))
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("porch").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("porch"))
                    stringBuilder.append(", " + activity.getString(R.string.text_porch) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("porch"))
                try {
                    if (jsonObjectOrder.getJSONObject(itemAddress).getString("apt").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("apt"))
                        stringBuilder.append(", " + activity.getString(R.string.text_apt) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("apt"))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                fragment.showSmth(stringBuilder.toString())

                addressList.add(stringBuilder.toString())
                editAddress.lat = jsonObjectOrder.getJSONObject(itemAddress).getString("lat")
                editAddress.lon = jsonObjectOrder.getJSONObject(itemAddress).getString("lon")
                editAddress.city = jsonObjectOrder.getJSONObject(itemAddress).getString("city")
                editAddress.cityId = jsonObjectOrder.getJSONObject(itemAddress).getString("city_id")
                editAddress.street = jsonObjectOrder.getJSONObject(itemAddress).getString("street")
                editAddress.house = jsonObjectOrder.getJSONObject(itemAddress).getString("house")
                editAddress.housing = jsonObjectOrder.getJSONObject(itemAddress).getString("housing")
                editAddress.porch = jsonObjectOrder.getJSONObject(itemAddress).getString("porch")
                editAddress.apt = jsonObjectOrder.getJSONObject(itemAddress).getString("apt")
                editAddress.fullAddress = stringBuilder.toString()
                listAddresses.add(editAddress)
            } catch (e: JSONException) {
            }
        }
        return addressList
    }

    @Subscribe
    fun onEvent(autocompleteEvent: NetworkAutocompleteEvent) {
        list.add(autocompleteEvent.address["label"]!!)
        val editAddress = EditAddress()
        editAddress.lat = autocompleteEvent.address["lat"]
        editAddress.lon = autocompleteEvent.address["lon"]
        editAddress.city = ""
        editAddress.cityId = autocompleteEvent.address["cityid"]
        editAddress.street = ""
        editAddress.house = ""
        editAddress.housing = ""
        editAddress.porch = ""
        editAddress.apt = ""
        editAddress.fullAddress = autocompleteEvent.address["label"]
        listAddresses.add(editAddress)

        fragment.autocomplete(list)
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        if (order.orderId == orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id")) {
            list = updateAddress(orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("address")).also {
                fragment.onOrder(it)
            }
        }
    }

    @Subscribe
    fun onEvent(updateOrderEvent: NetworkUpdateOrderEvent) {
        if (updateOrderEvent.status != "OK" || updateOrderEvent.result != 1) {
            updateAddress(currentOrder!!.address)
            if (appPreferences.getText("allow_edit_order") == "1") {
                fragment.showConfirmButton()
            }
            fragment.hideProgressBar()
        }
    }

    private inner class EditAddress {
        var fullAddress: String? = null
        var city: String? = null
        var cityId: String? = null
        var street: String? = null
        var lat: String? = null
        var lon: String? = null
        var house: String? = null
        var housing: String? = null
        var porch: String? = null
        var apt: String? = null

        fun editAddressToJson(): JSONObject {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("city_id", this.cityId)
                jsonObject.put("city", this.city)
                jsonObject.put("parking", "")
                jsonObject.put("apt", this.apt)
                jsonObject.put("parking_id", "")
                jsonObject.put("lat", this.lat)
                jsonObject.put("lon", this.lon)
                jsonObject.put("street", this.street)
                if (this.street!!.isEmpty())
                    jsonObject.put("street", this.fullAddress)
                else
                    jsonObject.put("street", this.street)
                jsonObject.put("house", this.house)
                jsonObject.put("housing", this.housing)
                jsonObject.put("porch", this.porch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject
        }
    }
}