package com.kabbi.driver.taximeter

import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.events.orderStatus.OrderStatusPresenter
import com.kabbi.driver.events.taximeter.CostChange
import org.greenrobot.eventbus.EventBus

class TaximeterPresenter(orderId: String, driver: Driver): OrderStatusPresenter() {

    init {
        super.orderId = orderId
        super.driver = driver
    }

    fun updateCost(cost: String, id: String, worker: String) {
        EventBus.getDefault().post(CostChange(getNewUuid(), cost, id, worker))
    }
}