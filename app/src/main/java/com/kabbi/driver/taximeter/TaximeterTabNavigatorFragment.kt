package com.kabbi.driver.taximeter


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.CurrentOrder
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.CarEvent
import com.kabbi.driver.events.NetworkOrderForIdEvent
import com.kabbi.driver.events.UpdateAdressesToMapEvent
import com.kabbi.driver.events.taximeter.SummaryTaximeterEvent
import com.kabbi.driver.fragments.NavigatorOtherDialogFragment
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.Round
import com.kabbi.driver.helper.TypefaceSpanEx
import com.kabbi.driver.map.DriverMap
import com.kabbi.driver.map.DriverMapOsm
import com.kabbi.driver.map.MapFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class TaximeterTabNavigatorFragment : androidx.fragment.app.Fragment() {

    internal lateinit var view: View
    internal lateinit var driverMap: DriverMap
    internal lateinit var appPreferences: AppPreferences
    internal var driver: Driver? = null
    internal var service: Service? = null
    internal var order: Order? = null
    internal var currentOrder: CurrentOrder? = null
    private lateinit var tvPrice: TextView
    private lateinit var tvCurrency: TextView
    private lateinit var broadcastReceiverTracker: BroadcastReceiver
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_taximeter_tab_navigator, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        runBlocking {
            appPreferences = AppPreferences(activity!!.applicationContext)

            driver = (activity as WorkShiftActivity).driver
            service = (activity as WorkShiftActivity).service
            order = (activity as WorkShiftActivity).order
            withContext(Dispatchers.IO){
                currentOrder = AppDatabase.getInstance(context!!).currentOrderDao().getCurOrder(driver!!.id)
            }

            tvPrice = view.findViewById(R.id.textview_taximeter_navigator)
            tvCurrency = view.findViewById(R.id.textview_taximeter_navigator_currency)

            try {
                val mapFragment = MapFactory.getFragmentMap(activity!!.applicationContext)
                val args = Bundle()
                args.putLong("driver_id", driver!!.id!!)
                mapFragment.arguments = args
                childFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_map, mapFragment)
                        .commit()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                tvPrice.text = Round.roundLocale((activity as WorkShiftActivity).order!!.plantingCost, service!!.currency)
                tvCurrency.text = " " + TypefaceSpanEx.getOnlyCurrencyStr(activity)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            view.findViewById<View>(R.id.ib_navigator).setOnClickListener { v ->
                val navigatorOtherDialog = NavigatorOtherDialogFragment()
                val bundle = Bundle()
                bundle.putString("address", currentOrder!!.address)
                navigatorOtherDialog.arguments = bundle

                navigatorOtherDialog.show(fragmentManager!!, "dialogFragment_car")
            }
        }
    }

    override fun onStart() {
        super.onStart()

        broadcastReceiverTracker = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {

                try {
                    if (appPreferences.getText("favorite_map") != "1") {
                        driverMap.updateCar(intent.extras!!.getDouble(AppParams.COORDS_GPS_LAT), intent.extras!!.getDouble(AppParams.COORDS_GPS_LON), intent.extras!!.getFloat(AppParams.BEARING_GPS))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                EventBus.getDefault().post(CarEvent(intent.extras!!.getDouble(AppParams.COORDS_GPS_LAT), intent.extras!!.getDouble(AppParams.COORDS_GPS_LON), intent.extras!!.getFloat(AppParams.BEARING_GPS)))
            }
        }
        activity!!.registerReceiver(broadcastReceiverTracker, IntentFilter(AppParams.BROADCAST_ORDER))

        unknownMethod()

        Log.d("SET_PATH", "stage: " + (activity as WorkShiftActivity).stage)
    }

    @Subscribe
    fun onEvent(summaryTaximeterEvent: SummaryTaximeterEvent) {
        try {
            tvPrice.text = Round.roundLocale(summaryTaximeterEvent.summaryCost, service!!.currency)
            tvCurrency.text = " " + TypefaceSpanEx.getOnlyCurrencyStr(activity)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(updateAdressesToMapEvent: UpdateAdressesToMapEvent) {
        activity!!.runOnUiThread {
            if (appPreferences.getText("favorite_map") != "1") {

                (view.findViewById<View>(R.id.fragment_map) as FrameLayout).removeAllViews()

                driverMap = DriverMapOsm()
                driverMap.initMap(activity!!.applicationContext)
                (view.findViewById<View>(R.id.fragment_map) as FrameLayout).addView(driverMap.mapView)

                unknownMethod()
            }
        }
    }

    private fun unknownMethod() {
        try {
            if (appPreferences.getText("favorite_map") != "1") {
                driverMap.setCenterMap(java.lang.Double.valueOf(appPreferences.getText("lat")), java.lang.Double.valueOf(appPreferences.getText("lon")))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (appPreferences.getText("favorite_map") != "1") {
            val points = ArrayList<Map<String, Double>>()

            try {
                val jsonObjectAddress = JSONObject((activity as WorkShiftActivity).order!!.address)

                points.add(object : HashMap<String, Double>() {
                    init {
                        put("lat", java.lang.Double.valueOf(appPreferences.getText("lat")))
                        put("lon", java.lang.Double.valueOf(appPreferences.getText("lon")))
                    }
                })

                try {
                    points.add(object : HashMap<String, Double>() {
                        init {
                            put("lat", java.lang.Double.valueOf(jsonObjectAddress.getJSONObject("A").getString("lat")))
                            put("lon", java.lang.Double.valueOf(jsonObjectAddress.getJSONObject("A").getString("lon")))
                        }
                    })
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                for (itemAddress in arrAddresses) {
                    try {
                        points.add(object : HashMap<String, Double>() {
                            init {
                                put("lat", java.lang.Double.valueOf(jsonObjectAddress.getJSONObject(itemAddress).getString("lat")))
                                put("lon", java.lang.Double.valueOf(jsonObjectAddress.getJSONObject(itemAddress).getString("lon")))
                            }
                        })
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }

            try {
                driverMap.clear()
                driverMap.setPath(points, (activity as WorkShiftActivity).stage)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        activity!!.unregisterReceiver(broadcastReceiverTracker)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            if (order!!.orderId == orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id") && orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONObject("costData").getInt("is_fix") == 1) {
                tvPrice.text = orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getJSONObject("costData").getString("summary_cost")
                tvCurrency.text = " " + TypefaceSpanEx.getOnlyCurrencyStr(activity)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
