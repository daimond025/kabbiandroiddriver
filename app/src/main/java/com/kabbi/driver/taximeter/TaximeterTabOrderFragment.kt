package com.kabbi.driver.taximeter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.kabbi.driver.ConfirmationCodeActivity
import com.kabbi.driver.EditAddressesSecondActivity
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.border.OrderPagerBorderFragmentRes
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.CurrentOrder
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.*
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.AddressParsed
import com.kabbi.driver.util.AsyncHttpTask
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.Utils.showSnackBar
import com.kabbi.driver.util.buildAddressFromJson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*

class TaximeterTabOrderFragment : Fragment(), AsyncHttpTask.AsyncTaskInterface, View.OnClickListener {

    internal lateinit var view: View
    internal lateinit var driver: Driver
    internal lateinit var service: Service
    internal lateinit var order: Order
    internal lateinit var stringBuilder: StringBuilder
    internal lateinit var progressBar: ProgressBar

    private lateinit var jsonObjectOrder: JSONObject
    private lateinit var imageButton: ImageButton
    private lateinit var orderPagerBorderFragmentRes: OrderPagerBorderFragmentRes
    private lateinit var llAddressesTo: LinearLayout
    private lateinit var builder: AlertDialog.Builder
    private lateinit var fab: Button
    private lateinit var btnConfirmEdit: Button
    private lateinit var btnUpdateCost: Button
    private lateinit var btnConfirmationCode: Button
    private lateinit var appPreferences: AppPreferences
    private lateinit var lv: ListView
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var presenter: TaximeterPresenter

    private var divider = false
    private var jsonTariff: JSONObject? = null
    private var list: MutableList<String> = arrayListOf()
    private var listAddresses: MutableList<EditAddress> = arrayListOf()
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

    internal var currentOrder: CurrentOrder? = null

    private fun getTotalListViewHeight(listView: ListView) {

        val mAdapter = listView.adapter

        var totalHeight = 0

        for (i in 0 until mAdapter.count) {
            val mView = mAdapter.getView(i, null, listView)

            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

            totalHeight += mView.measuredHeight
        }
        totalHeight += 50

        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (mAdapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        runBlocking {

            val activity = activity as WorkShiftActivity
            driver = activity.driver!!
            service = activity.service!!
            order = activity.order!!
            withContext(Dispatchers.IO) {
                currentOrder = AppDatabase.getInstance(context!!).currentOrderDao().getCurOrder(driver.id)
            }

            if (currentOrder == null) {
                EventBus.getDefault().post(ChangeFragmentInOrderEvent(1))
                return@runBlocking
            }

            presenter = TaximeterPresenter(currentOrder!!.orderId, driver)

            listAddresses = ArrayList()
            list = updateAddress(currentOrder!!.address)

            adapter = if (appPreferences.getText("require_point_confirmation_code") == "1") {
                ArrayAdapter(activity, R.layout.list_item_handle_right_code, R.id.text, list)
            } else {
                ArrayAdapter(activity, R.layout.list_item_handle_right, R.id.text, list)
            }

            lv.adapter = adapter
            getTotalListViewHeight(lv)

            view.findViewById<TextView>(R.id.tv_start_order).text = order.orderTime

            if (order.comment != null && order.comment.isNotEmpty()) {
                view.findViewById<View>(R.id.textview_taximeterorder_comment).visibility = View.VISIBLE
                (view.findViewById<View>(R.id.textview_taximeterorder_comment) as TextView).text = order.comment

            }

            try {
                if (order.clientPhone != null && order.clientPhone.isNotEmpty()) {
                    imageButton.setOnClickListener {
                        if (order.clientPhone != null) {
                            val telecomManager = activity.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                            val simState = telecomManager.simState
                            if (simState == TelephonyManager.SIM_STATE_READY) {
                                val callIntent = Intent(Intent.ACTION_DIAL)
                                callIntent.data = Uri.parse("tel:+" + order.clientPhone)
                                startActivity(callIntent)
                            } else {
                                CustomToast.showMessage(activity, getString(R.string.text_order_sim_not_exist))
                            }
                        }
                    }
                    divider = true
                } else {
                    imageButton.visibility = View.INVISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val phone = view.findViewById<TextView>(R.id.textview_taximeterorder_phone)
            
            if (order.clientName.isNotEmpty()) {
                phone.text = order.clientName
            } else {
                phone.setTextColor(resources.getColor(R.color.red))
                phone.text = getString(R.string.text_taximeter_order_name_unknow)
            }

            try {
                if (order.dopOption.isNotBlank()) {
                    val jsonArrayOptions = JSONArray(order.dopOption)
                    val linearLayoutOptions = view.findViewById<LinearLayout>(R.id.linearlayout_taximeterorder_options)
                    val views = ArrayList<View>()
                    var countOption = 1
                    var layoutView: View? = null
                    for (j in 0 until jsonArrayOptions.length()) {
                        val jsonObjectOption = jsonArrayOptions.getJSONObject(j)
                        when (countOption) {
                            1 -> {
                                val layoutInflater = activity.layoutInflater
                                layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_option, null, false)
                                (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option1) as TextView).text = jsonObjectOption.getString("name")

                                if (j == jsonArrayOptions.length() - 1) {
                                    views.add(layoutView)
                                }
                            }
                            2 -> {
                                (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option2) as TextView).text = jsonObjectOption.getString("name")
                                views.add(layoutView)
                                countOption = 0
                            }
                        }
                        countOption++
                    }
                    for (item in views) {
                        linearLayoutOptions.addView(item)
                    }
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            view.findViewById<TextView>(R.id.textview_orderborder_tariff).text = String.format("%s %s", order.tariffLabel, if (order.day) getString(R.string.text_day) else getString(R.string.text_night))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        view = inflater.inflate(R.layout.fragment_taximeter_tab_order, container, false)

        orderPagerBorderFragmentRes = OrderPagerBorderFragmentRes()

        stringBuilder = StringBuilder()

        fab = view.findViewById(R.id.fab)
        fab.setOnClickListener(this)
        btnConfirmEdit = view.findViewById(R.id.btn_confirm_editaddress)
        btnUpdateCost = view.findViewById(R.id.btn_updatecost)
        btnConfirmationCode = view.findViewById(R.id.btn_check_code)
        btnConfirmEdit.setOnClickListener(this)
        btnUpdateCost.setOnClickListener(this)
        progressBar = view.findViewById(R.id.pbHeaderProgress)

        llAddressesTo = view.findViewById(R.id.linearlayout_taximeterorder_second_source)

        lv = view.findViewById(R.id.list)

        view.findViewById<View>(R.id.textview_orderborder_tariff).setOnClickListener(this)

        builder = AlertDialog.Builder(context!!)
        builder.setPositiveButton(getString(R.string.text_button_order_offer_close)) { dialog, _ -> dialog.cancel() }
        if (appPreferences.getText("require_point_confirmation_code") == "1") {
            btnConfirmationCode.visibility = View.VISIBLE
        } else {
            btnConfirmationCode.visibility = View.GONE
        }

        imageButton = view.findViewById(R.id.imagebutton_taximeterorder_phone)
        if (appPreferences.getText("allow_edit_order") == "0") {
            fab.visibility = View.GONE
            btnConfirmEdit.visibility = View.GONE

            lv.isEnabled = false
        }
        if (appPreferences.getText("allow_edit_cost_order") == "0") {
            btnUpdateCost.visibility = View.GONE
        }
        lv.setOnItemClickListener { parent, view, position, id ->
            builder.setMessage((view.findViewById<View>(R.id.text) as TextView).text)
            val alert = builder.create()
            alert.show()
        }

        btnConfirmationCode.setOnClickListener { v -> startActivity(Intent(activity, ConfirmationCodeActivity::class.java)) }

        view.findViewById<View>(R.id.point_a_phone).setOnClickListener { v ->
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + (view.findViewById<View>(R.id.point_a_phone) as TextView).text)
            startActivity(intent)
        }
        return view
    }

    private fun getJSONAddress(list: List<String>): JSONObject {
        val jsonObjectAddresses = JSONObject()
        val jsonArrayAddresses = JSONArray()
        try {
            for (item in list) {
                val jsonItem = JSONObject()
                for (itemAddress in listAddresses) {
                    if (itemAddress.fullAddress == item) {
                        try {
                            jsonItem.put("city_id", itemAddress.cityId)
                            jsonItem.put("city", "")
                            jsonItem.put("street", itemAddress.fullAddress)
                            jsonItem.put("house", "")
                            jsonItem.put("housing", "")
                            jsonItem.put("porch", "")
                            jsonItem.put("lat", itemAddress.lat)
                            jsonItem.put("lon", itemAddress.lon)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        break
                    }
                }
                jsonArrayAddresses.put(jsonItem)
            }
            jsonObjectAddresses.put("address", jsonArrayAddresses)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return jsonObjectAddresses
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        appPreferences = AppPreferences(activity!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            orderForIdEvent.json?.let { json ->
                val data = json.getJSONObject("result").getJSONObject("order_data")
                if (order.orderId == data.getString("order_id")) {
                    list = updateAddress(data.getString("address"))
                    if (appPreferences.getText("require_point_confirmation_code") == "1") {
                        adapter = ArrayAdapter(activity!!, R.layout.list_item_handle_right_code, R.id.text, list)
                        appPreferences.saveText("confirm_codes", "")
                    } else {
                        adapter = ArrayAdapter(activity!!, R.layout.list_item_handle_right, R.id.text, list)
                    }
                    lv.adapter = adapter
                    getTotalListViewHeight(lv)

                    if (!data.getString("comment").isNullOrBlank()) {
                        view.findViewById<View>(R.id.textview_taximeterorder_comment).visibility = View.VISIBLE
                        view.findViewById<TextView>(R.id.textview_taximeterorder_comment).text = data.getString("comment")
                    } else {
                        view.findViewById<View>(R.id.textview_taximeterorder_comment).visibility = View.GONE
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(autocompleteEvent: NetworkAutocompleteEvent) {

        list.add(autocompleteEvent.address["label"]!!)
        val editAddress = EditAddress()
        editAddress.lat = autocompleteEvent.address["lat"]
        editAddress.lon = autocompleteEvent.address["lon"]
        editAddress.city = ""
        editAddress.cityId = autocompleteEvent.address["cityid"]
        editAddress.street = ""
        editAddress.house = ""
        editAddress.housing = ""
        editAddress.porch = ""
        editAddress.apt = ""
        editAddress.fullAddress = autocompleteEvent.address["label"]
        listAddresses.add(editAddress)

        adapter = if (appPreferences.getText("require_point_confirmation_code") == "1") {
            ArrayAdapter(activity!!, R.layout.list_item_handle_right_code, R.id.text, list)
        } else {
            ArrayAdapter(activity!!, R.layout.list_item_handle_right, R.id.text, list)
        }
        lv.adapter = adapter
        getTotalListViewHeight(lv)

        view.findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE

    }

    @Subscribe
    fun onEvent(updateOrderEvent: NetworkUpdateOrderEvent) {
        if (updateOrderEvent.status == "OK" && updateOrderEvent.result == 1) {

        } else {
            updateAddress(currentOrder!!.address)
            if (appPreferences.getText("allow_edit_order") == "1") {
                btnConfirmEdit.visibility = View.VISIBLE
            }
            progressBar.visibility = View.GONE
        }
    }

    @Subscribe
    fun onEvent(updateOrderCostEvent: NetworkUpdateOrderCostEvent) {
        if (updateOrderCostEvent.status != "OK" || updateOrderCostEvent.result != 1) {
            CustomToast.showMessage(activity, updateOrderCostEvent.status)
            showSnackBar(updateOrderCostEvent.status, btnUpdateCost)
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        if (pushResponseEvent.infoCode == "OK" && pushResponseEvent.uuid == presenter.getUuid() && pushResponseEvent.jsonObject != null) {
            try {
                val jsonNewOrder = pushResponseEvent.jsonObject
                val jsonDB = JSONObject(currentOrder!!.address)
                val jsonNewDB = JSONObject()

                jsonNewDB.put("A", jsonDB.getJSONObject("A"))
                for ((count, editAddress) in listAddresses.withIndex()) {
                    jsonNewDB.put(arrAddresses[count], editAddress.editAddressToJson())
                }
                if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1")
                    order.fix = 1
                else
                    order.fix = jsonNewOrder.getInt("is_fix")

                order.costOrder = jsonNewOrder.getString("summary_cost").toDouble()
                order.dopCost = jsonNewOrder.getString("additionals_cost").toDouble()
                currentOrder!!.address = jsonNewDB.toString()
                order.address = jsonNewDB.toString()

                runBlocking {
                    withContext(Dispatchers.IO) {
                        val db = AppDatabase.getInstance(context!!)
                        db.currentOrderDao().save(currentOrder!!)
                        db.orderDao().save(order)
                    }
                }

                EventBus.getDefault().post(UpdateAdressesToMapEvent())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else if (pushResponseEvent.uuid == presenter.getUuid()) {
            activity!!.runOnUiThread { updateAddress(currentOrder!!.address) }
        }
        activity!!.runOnUiThread {
            try {
                if (appPreferences.getText("allow_edit_order") == "1") {
                    btnConfirmEdit.visibility = View.VISIBLE
                }
                progressBar.visibility = View.GONE
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    override fun doPostExecute(jsonObject: JSONObject, typeJson: String) {
        if (typeJson == "get_client_tariff") {
            try {
                if (jsonObject.getString("info") == "OK") {
                    (view.findViewById<View>(R.id.textview_orderborder_tariff) as TextView).text = (jsonObject.getJSONObject("result").getJSONObject("tariffInfo").getString("tariffLabel") + " "
                            + if (order.day) getString(R.string.text_day) else getString(R.string.text_night))
                    jsonTariff = jsonObject

                    view.findViewById<View>(R.id.textview_orderborder_tariff).setOnClickListener(this)
                }
            } catch (e: Exception) {
            }
        }
    }

    override fun onClick(v: View) {
        when (v.tag.toString()) {

            "tariff" -> try {
                if (jsonTariff == null) {
                    val orderJsonTariff = JSONObject(currentOrder!!.tariffData)
                    val dataCity = orderJsonTariff.getJSONObject("tariffInfo").getJSONObject("tariffDataCity")
                    val dataTrack = orderJsonTariff.getJSONObject("tariffInfo").getJSONObject("tariffDataTrack")

                    jsonTariff = JSONObject()
                    jsonTariff!!.put("info", "OK")
                    jsonTariff!!.put("code", 0)
                    jsonTariff!!.put("result", JSONArray().put(JSONObject().put("tariffInfo", JSONObject().put("isDay", orderJsonTariff.getJSONObject("tariffInfo").getInt("isDay"))
                            .put("tariffLabel", order.tariffLabel)
                            .put("tariffType", orderJsonTariff.getJSONObject("tariffInfo").getString("tariffType"))
                            .put("tariffDataCity", dataCity)
                            .put("tariffDataTrack", dataTrack))))
                }
                val builder = AlertDialog.Builder(activity!!)
                builder.setView(orderPagerBorderFragmentRes.getViewTariffs(activity!!, jsonTariff!!)).setPositiveButton(getString(R.string.button_exit)) { dialog, which -> dialog.dismiss() }.create().show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            "fab" -> startActivity(Intent(activity, EditAddressesSecondActivity::class.java))

            "button_confirm" -> {
                val requestParams = LinkedHashMap<String, String>()
                try {
                    requestParams["address"] = URLEncoder.encode(getJSONAddress(list).toString(), "UTF-8").replace("+", "%20")

                    Log.d("UPDATE_ADDRESS", getJSONAddress(list).toString())

                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }

                requestParams["order_id"] = currentOrder!!.orderId
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign

                WebService.updateOrder(requestParams, driver.secretCode, presenter.getNewUuid())
                btnConfirmEdit.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            }

            "button_updatecost" -> {
                val viewDialog = activity!!.layoutInflater.inflate(R.layout.fragment_dialog_edittext_ordercost, null)
                val alertDialog = AlertDialog.Builder(activity!!)
                alertDialog.setTitle(getString(R.string.new_ordercost))
                alertDialog.setView(viewDialog)
                alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { _, _ -> }
                val dialog = alertDialog.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    val orderCost = (viewDialog.findViewById<View>(R.id.et_ordercost) as EditText).text.toString()
                    if (orderCost.isNotBlank()) {
                        val requestParams = LinkedHashMap<String, String>()
                        requestParams["order_id"] = currentOrder!!.orderId
                        requestParams["predv_price"] = orderCost
                        requestParams["tenant_login"] = service.uri
                        requestParams["worker_login"] = driver.callSign

                        if (AppParams.SET_ORDER_PARAMS_VIA_SOCKET) {
                            presenter.updateCost(cost = orderCost, id = currentOrder!!.orderId, worker = driver.callSign)
                        } else {
                            WebService.updateOrderCost(requestParams, driver.secretCode, presenter.getNewUuid())
                        }

                        dialog.dismiss()
                    } else {
                        viewDialog.findViewById<EditText>(R.id.et_ordercost).error = getString(R.string.edittext_valid_required)
                    }
                }
            }
        }
    }

    private fun updateAddress(json: String): MutableList<String> {
        listAddresses.clear()
        val addressList = ArrayList<String>()
        val layoutInflater = activity!!.layoutInflater

        try {
            jsonObjectOrder = JSONObject(json)

            val mPointA = buildAddressFromJson(jsonObjectOrder.getJSONObject("A"))

            view.findViewById<TextView>(R.id.textview_taximeterorder_point_a).text = mPointA.stringValue()

            if (appPreferences.getText("require_point_confirmation_code") == "1") {
                if (!mPointA.comment.isNullOrBlank()) {
                    view.findViewById<View>(R.id.point_a_comment).visibility = View.VISIBLE
                    view.findViewById<TextView>(R.id.point_a_comment).text = mPointA.comment
                }

                if (!mPointA.phone.isNullOrBlank()) {
                    view.findViewById<View>(R.id.ll_phone_block).visibility = View.VISIBLE
                    view.findViewById<TextView>(R.id.point_a_phone).text = mPointA.phone
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        llAddressesTo.removeAllViews()

        var countAddresses = 0
        for (itemAddress in arrAddresses) {
            try {
                if (!jsonObjectOrder.has(itemAddress))
                    break

                val mPoint = buildAddressFromJson(jsonObjectOrder.getJSONObject(itemAddress))
                val layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_source, null, false)
                layoutView.findViewById<TextView>(R.id.textview_orderoffer_source).text = mPoint.stringValue()
                llAddressesTo.addView(layoutView)
                view.findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE

                if (appPreferences.getText("require_point_confirmation_code") == "1") {
                    if (!mPoint.comment.isNullOrBlank()) {
                        layoutView.findViewById<View>(R.id.point_a_comment).visibility = View.VISIBLE
                        layoutView.findViewById<TextView>(R.id.point_a_comment).text = mPoint.comment
                    }

                    if (!mPoint.phone.isNullOrBlank()) {
                        layoutView.findViewById<View>(R.id.ll_phone_block).visibility = View.VISIBLE
                        layoutView.findViewById<TextView>(R.id.point_a_phone).text = mPoint.phone
                    }

                    layoutView.findViewById<View>(R.id.point_a_phone).setOnClickListener {
                        val intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:" + mPoint.phone)
                        startActivity(intent)
                    }
                }

                addressList.add(mPoint.stringValue())

                val editAddress = EditAddress(mPoint)
                listAddresses.add(editAddress)

                countAddresses++
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        if (countAddresses == 0) view.findViewById<View>(R.id.tv_to).visibility = View.INVISIBLE

        return addressList
    }

    private inner class EditAddress {
        var fullAddress: String? = null
        var city: String? = null
        var cityId: String? = null
        var street: String? = null
        var lat: String? = null
        var lon: String? = null
        var house: String? = null
        var housing: String? = null
        var porch: String? = null
        var apt: String? = null

        constructor()

        constructor(address: AddressParsed) {
            this.fullAddress = address.stringValue()
            this.city = address.city
            this.cityId = address.cityId
            this.street = address.street
            this.lat = address.lat.toString()
            this.lon = address.lon.toString()
            this.house = address.house
            this.housing = address.housing
            this.porch = address.porch
            this.apt = address.apt
        }

        fun editAddressToJson(): JSONObject {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("city_id", this.cityId)
                jsonObject.put("city", this.city)
                jsonObject.put("parking", "")
                jsonObject.put("apt", this.apt)
                jsonObject.put("parking_id", "")
                jsonObject.put("lat", this.lat)
                jsonObject.put("lon", this.lon)
                jsonObject.put("street", this.street)

                if (this.street!!.isEmpty())
                    jsonObject.put("street", this.fullAddress)
                else
                    jsonObject.put("street", this.street)
                jsonObject.put("house", this.house)
                jsonObject.put("housing", this.housing)
                jsonObject.put("porch", this.porch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject
        }
    }
}