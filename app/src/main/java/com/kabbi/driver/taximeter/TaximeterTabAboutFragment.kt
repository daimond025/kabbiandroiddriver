package com.kabbi.driver.taximeter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.events.NetworkUpdateOrderCostEvent
import com.kabbi.driver.util.CustomToast
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class TaximeterTabAboutFragment : Fragment(), View.OnClickListener {

    internal lateinit var view: View
    internal lateinit var order: Order
    internal lateinit var progressBar: ProgressBar
    private var builder: AlertDialog.Builder? = null
    private lateinit var fab: Button
    private lateinit var btnConfirmEdit: Button
    private lateinit var lv: ListView
    private var adapter: ArrayAdapter<String>? = null
    private lateinit var presenter: TaximeterTabAboutPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_taximeter_tab_about, container, false)
        fab = view.findViewById(R.id.fab)
        fab.setOnClickListener(this)
        btnConfirmEdit = view.findViewById(R.id.btn_confirm_editaddress)
        btnConfirmEdit.setOnClickListener(this)
        progressBar = view.findViewById(R.id.pbHeaderProgress)
        lv = view.findViewById(R.id.list)
        builder = AlertDialog.Builder(context!!)
        builder!!.setPositiveButton(getString(R.string.text_button_order_offer_close)) { dialog, _ -> dialog.cancel() }

        lv.setOnItemClickListener { _, view, _, _ ->
            builder!!.setMessage(view.findViewById<TextView>(R.id.text).text)
            val alert = builder!!.create()
            alert.show()
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = TaximeterTabAboutPresenter(this)
    }

    fun enableAddButton() {
        fab.visibility = View.GONE
        btnConfirmEdit.visibility = View.GONE
        lv.isEnabled = false
    }

    fun setup(text: String, list: MutableList<String>) {
        adapter = ArrayAdapter(activity!!, R.layout.list_item_handle_right, R.id.text, list)
        lv.adapter = adapter
        getTotalHeightOfListView(lv)

        if (text.isNotBlank()) {
            view.findViewById<View>(R.id.textview_taximeterorder_comment).visibility = View.VISIBLE
            view.findViewById<TextView>(R.id.textview_taximeterorder_comment).text = text
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    private fun getTotalHeightOfListView(listView: ListView) {
        val mAdapter = listView.adapter
        var totalHeight = 0
        for (i in 0 until mAdapter.count) {
            val mView = mAdapter.getView(i, null, listView)
            mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
            totalHeight += mView.measuredHeight
        }
        totalHeight += 50
        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (mAdapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()
    }

    @Subscribe
    fun onEvent(updateOrderCostEvent: NetworkUpdateOrderCostEvent) {
        if (updateOrderCostEvent.status != "OK" || updateOrderCostEvent.result != 1) {
            CustomToast.showMessage(activity, updateOrderCostEvent.status)
        }
    }

    override fun onClick(v: View) {
        presenter.onButtonClicked(v.tag.toString())
    }

    fun setStartPointText(text: String) {
        view.findViewById<TextView>(R.id.textview_taximeterorder_point_a).text = text
    }

    fun showSmth(text: String) {
        val layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_source, null, false)
        layoutView.findViewById<TextView>(R.id.textview_orderoffer_source).text = text
        view.findViewById<LinearLayout>(R.id.linearlayout_taximeterorder_second_source).addView(layoutView)
        view.findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE
    }

    fun autocomplete(list: MutableList<String>) {
        adapter = ArrayAdapter(activity!!, R.layout.list_item_handle_right, R.id.text, list)
        lv.adapter = adapter
        getTotalHeightOfListView(lv)
        view.findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE
    }

    fun onOrder(list: MutableList<String>) {
        adapter = ArrayAdapter(activity!!, R.layout.list_item_handle_right, R.id.text, list)
        lv.adapter = adapter
        getTotalHeightOfListView(lv)
    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    fun showConfirmButton() {
        btnConfirmEdit.visibility = View.VISIBLE
    }

    fun showProgressBar() {
        btnConfirmEdit.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

}