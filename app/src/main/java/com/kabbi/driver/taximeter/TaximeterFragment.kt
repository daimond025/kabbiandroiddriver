package com.kabbi.driver.taximeter

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.kabbi.driver.R

class TaximeterFragment : Fragment() {

    var tabCount = 3
    var titles: MutableList<String> = arrayListOf()
    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var tabLayout: TabLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_taximeter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titles.add(getString(R.string.text_title_taximeter_taxi))
        titles.add(getString(R.string.text_title_taximeter_order))
        titles.add(getString(R.string.text_title_taximeter_navigator))

        val viewPager: ViewPager = view.findViewById(R.id.taximeter_pager)
        pagerAdapter = TaximeterAdapter(childFragmentManager)
        viewPager.adapter = pagerAdapter
        tabLayout = view.findViewById(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)

        val myColorStateList = ColorStateList(arrayOf(intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_pressed)), intArrayOf(
                Color.WHITE,
                Color.WHITE,
                Color.WHITE
        ))
        tabLayout.tabTextColors = myColorStateList
    }

    private inner class TaximeterAdapter internal constructor(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {
        override fun getItem(position: Int): Fragment {
            var fragment: Fragment? = null
            when (position) {
                0 -> fragment = TaximeterTabFragment()
                1 -> fragment = TaximeterTabOrderFragment()
                2 -> fragment = TaximeterTabNavigatorFragment()
            }
            return fragment!!
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titles[position]
        }
    }
}