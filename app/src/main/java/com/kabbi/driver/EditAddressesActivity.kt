package com.kabbi.driver

import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.fragments.AddressDialogFragment
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*
import kotlin.collections.LinkedHashMap

class EditAddressesActivity : BaseActivity(), View.OnClickListener {
    internal lateinit var service: Service
    private lateinit var currentOrder: CurrentOrder
    internal lateinit var driver: Driver
    internal lateinit var workDay: WorkDay
    internal lateinit var order: Order
    internal lateinit var stringBuilder: StringBuilder
    internal lateinit var progressBar: ProgressBar
    private lateinit var jsonObjectOrder: JSONObject
    private var appPreferences: AppPreferences? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var builder: AlertDialog.Builder? = null
    private var lv: ListView? = null
    private var adapter: ArrayAdapter<String>? = null
    private var list: MutableList<String>? = null
    private var listAddresses: MutableList<EditAddress>? = null
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private var uuid = ""
    private var fab: Button? = null
    private var btnComfirmEdit: Button? = null

    private fun getTotalHeightofListView(listView: ListView) {
        val mAdapter = listView.adapter
        var totalHeight = 0
        for (i in 0 until mAdapter.count) {
            val mView = mAdapter.getView(i, null, listView)
            mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
            totalHeight += mView.measuredHeight
        }
        totalHeight += 50
        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (mAdapter.count - 1)
        listView.layoutParams = params
        listView.requestLayout()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_edit_addresses)
        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.change_addresses)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        workDay = (application as App).workDay
        driver = (application as App).driver
        service = (application as App).service

        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@EditAddressesActivity)
                currentOrder = db.currentOrderDao().getCurOrder(driver.id)
                order = db.orderDao().getByOrderId(currentOrder.orderId)
            }
        }

        stringBuilder = StringBuilder()
        fab = findViewById(R.id.fab)
        fab!!.setOnClickListener(this)
        btnComfirmEdit = findViewById(R.id.btn_confirm_editaddress)
        btnComfirmEdit!!.setOnClickListener(this)
        progressBar = findViewById(R.id.pbHeaderProgress)
        builder = AlertDialog.Builder(applicationContext)
        builder!!.setPositiveButton(getString(R.string.text_button_order_offer_close)) { dialog, _ -> dialog.cancel() }
        lv = findViewById(R.id.list)

        lv!!.setOnItemClickListener { _, view, _, _ ->
            builder!!.setMessage((view.findViewById<View>(R.id.text) as TextView).text)
            val alert = builder!!.create()
            alert.show()
        }
        listAddresses = ArrayList()
        list = updateAddress(currentOrder.address)
        adapter = ArrayAdapter(applicationContext, R.layout.list_item_handle_right, R.id.text, list!!)
        lv!!.adapter = adapter
        getTotalHeightofListView(lv!!)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    private fun updateAddress(json: String): MutableList<String> {
        listAddresses!!.clear()
        val addressList = ArrayList<String>()
        val layoutInflater = layoutInflater
        try {
            jsonObjectOrder = JSONObject(json)
            stringBuilder.setLength(0)
            if (jsonObjectOrder.getJSONObject("A").getString("city").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("city"))
                stringBuilder.append(jsonObjectOrder.getJSONObject("A").getString("city") + ", ")
            stringBuilder.append(jsonObjectOrder.getJSONObject("A").getString("street"))
            if (jsonObjectOrder.getJSONObject("A").getString("house").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("house"))
                stringBuilder.append(", " + getString(R.string.text_house) + " " + jsonObjectOrder.getJSONObject("A").getString("house"))
            if (jsonObjectOrder.getJSONObject("A").getString("housing").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("housing"))
                stringBuilder.append(", " + getString(R.string.text_housing) + " " + jsonObjectOrder.getJSONObject("A").getString("housing"))
            if (jsonObjectOrder.getJSONObject("A").getString("porch").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("porch"))
                stringBuilder.append(", " + getString(R.string.text_porch) + " " + jsonObjectOrder.getJSONObject("A").getString("porch"))
            try {
                if (jsonObjectOrder.getJSONObject("A").getString("apt").isNotEmpty() && !jsonObjectOrder.getJSONObject("A").isNull("apt"))
                    stringBuilder.append(", " + getString(R.string.text_apt) + " " + jsonObjectOrder.getJSONObject("A").getString("apt"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            (findViewById<View>(R.id.textview_taximeterorder_point_a) as TextView).text = stringBuilder.toString()
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        for (itemAddress in arrAddresses) {
            try {
                jsonObjectOrder.getJSONObject(itemAddress)
                stringBuilder.setLength(0)
                val editAddress = EditAddress()
                if (!jsonObjectOrder.getJSONObject(itemAddress).isNull("city") && jsonObjectOrder.getJSONObject(itemAddress).getString("city").isNotEmpty()) {
                    stringBuilder.append(jsonObjectOrder.getJSONObject(itemAddress).getString("city"))
                    stringBuilder.append(", " + jsonObjectOrder.getJSONObject(itemAddress).getString("street"))
                } else {
                    stringBuilder.append(jsonObjectOrder.getJSONObject(itemAddress).getString("street"))
                }
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("house").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("house"))
                    stringBuilder.append(", " + getString(R.string.text_house) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("house"))
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("housing").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("housing"))
                    stringBuilder.append(", " + getString(R.string.text_housing) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("housing"))
                if (jsonObjectOrder.getJSONObject(itemAddress).getString("porch").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("porch"))
                    stringBuilder.append(", " + getString(R.string.text_porch) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("porch"))
                try {
                    if (jsonObjectOrder.getJSONObject(itemAddress).getString("apt").isNotEmpty() && !jsonObjectOrder.getJSONObject(itemAddress).isNull("apt"))
                        stringBuilder.append(", " + getString(R.string.text_apt) + " " + jsonObjectOrder.getJSONObject(itemAddress).getString("apt"))
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                val layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_source, null, false)
                (layoutView.findViewById<View>(R.id.textview_orderoffer_source) as TextView).text = stringBuilder.toString()
                (findViewById<View>(R.id.linearlayout_taximeterorder_second_source) as LinearLayout).addView(layoutView)
                findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE
                addressList.add(stringBuilder.toString())
                editAddress.lat = jsonObjectOrder.getJSONObject(itemAddress).getString("lat")
                editAddress.lon = jsonObjectOrder.getJSONObject(itemAddress).getString("lon")
                editAddress.city = jsonObjectOrder.getJSONObject(itemAddress).getString("city")
                editAddress.cityId = jsonObjectOrder.getJSONObject(itemAddress).getString("city_id")
                editAddress.street = jsonObjectOrder.getJSONObject(itemAddress).getString("street")
                editAddress.house = jsonObjectOrder.getJSONObject(itemAddress).getString("house")
                editAddress.housing = jsonObjectOrder.getJSONObject(itemAddress).getString("housing")
                editAddress.porch = jsonObjectOrder.getJSONObject(itemAddress).getString("porch")
                editAddress.apt = jsonObjectOrder.getJSONObject(itemAddress).getString("apt")
                editAddress.fullAddress = stringBuilder.toString()
                listAddresses!!.add(editAddress)
            } catch (e: JSONException) {
            }

        }
        return addressList
    }

    override fun onClick(v: View) {
        when (v.tag.toString()) {
            "fab" -> if (list!!.size >= AppParams.COUNT_ADDRESS) {
                CustomToast.showMessage(this, getString(R.string.toast_max_source))
            } else {
                val dialogFragmentAddress = AddressDialogFragment()
                dialogFragmentAddress.show(supportFragmentManager, "fragmentAddress")
            }
            "button_confirm" -> {
                val requestParams = LinkedHashMap<String, String>()
                try {
                    requestParams["address"] = URLEncoder.encode(getJSONAddress(list!!).toString(), "UTF-8").replace("+", "%20")
                    Log.d("UPDATE_ADDRESS", getJSONAddress(list!!).toString())
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }

                requestParams["order_id"] = currentOrder.orderId
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign
                val uuidObject = UUID.randomUUID()
                uuid = uuidObject.toString()
                WebService.updateOrder(requestParams, driver.secretCode, uuid)
                btnComfirmEdit!!.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            }
            "button_updatecost" -> {
                val viewDialog = layoutInflater.inflate(R.layout.fragment_dialog_edittext_ordercost, null)
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle(getString(R.string.new_ordercost))
                alertDialog.setView(viewDialog)
                alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { _, _ -> }
                val dialog = alertDialog.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    val orderCost = (viewDialog.findViewById<View>(R.id.et_ordercost) as EditText).text.toString()
                    if (orderCost.trim { it <= ' ' }.isNotEmpty()) {
                        val requestParams1 = LinkedHashMap<String, String>()
                        requestParams1["order_id"] = currentOrder.orderId
                        requestParams1["predv_price"] = orderCost
                        requestParams1["tenant_login"] = service.uri
                        requestParams1["worker_login"] = driver.callSign
                        val uuidObject1 = UUID.randomUUID()
                        uuid = uuidObject1.toString()
                        WebService.updateOrderCost(requestParams1, driver.secretCode, uuid)
                        dialog.dismiss()
                    } else {
                        (viewDialog.findViewById<View>(R.id.et_ordercost) as EditText).error = getString(R.string.edittext_valid_required)
                    }
                }
            }
        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            if (order.orderId == orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id")) {
                list = updateAddress(orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("address"))
                adapter = ArrayAdapter(this, R.layout.list_item_handle_right, R.id.text, list!!)
                lv!!.adapter = adapter
                getTotalHeightofListView(lv!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Subscribe
    fun onEvent(autocompleteEvent: NetworkAutocompleteEvent) {
        list!!.add(autocompleteEvent.address["label"]!!)
        val editAddress = EditAddress()
        editAddress.lat = autocompleteEvent.address["lat"]
        editAddress.lon = autocompleteEvent.address["lon"]
        editAddress.city = ""
        editAddress.cityId = autocompleteEvent.address["cityid"]
        editAddress.street = ""
        editAddress.house = ""
        editAddress.housing = ""
        editAddress.porch = ""
        editAddress.apt = ""
        editAddress.fullAddress = autocompleteEvent.address["label"]
        listAddresses!!.add(editAddress)
        adapter = ArrayAdapter(this, R.layout.list_item_handle_right, R.id.text, list!!)
        lv!!.adapter = adapter
        getTotalHeightofListView(lv!!)
        findViewById<View>(R.id.linearlayout_taximeterorder_to).visibility = View.VISIBLE
    }

    @Subscribe
    fun onEvent(updateOrderEvent: NetworkUpdateOrderEvent) {
        if (updateOrderEvent.status == "OK" && updateOrderEvent.result == 1) {
        } else {
            updateAddress(currentOrder.address)
            if (appPreferences!!.getText("allow_edit_order") == "1") {
                btnComfirmEdit!!.visibility = View.VISIBLE
            }
            progressBar.visibility = View.GONE
        }
    }

    @Subscribe
    fun onEvent(updateOrderCostEvent: NetworkUpdateOrderCostEvent) {
        if (updateOrderCostEvent.status == "OK" && updateOrderCostEvent.result == 1) {
        } else {
            CustomToast.showMessage(this, updateOrderCostEvent.status)
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        if (pushResponseEvent.infoCode == "OK" && pushResponseEvent.uuid == uuid && pushResponseEvent.jsonObject != null) {
            try {
                val jsonNewOrder = pushResponseEvent.jsonObject
                val jsonDB = JSONObject(currentOrder.address)
                val jsonNewDB = JSONObject()
                jsonNewDB.put("A", jsonDB.getJSONObject("A"))
                for ((count, editAddress) in listAddresses!!.withIndex()) {
                    jsonNewDB.put(arrAddresses[count], editAddress.editAddressToJson())
                }
                if (appPreferences!!.getText("gps") == "1" && appPreferences!!.getText("gps_server") == "1")
                    order.fix = 1
                else
                    order.fix = jsonNewOrder.getInt("is_fix")
                order.costOrder = java.lang.Double.valueOf(jsonNewOrder.getString("summary_cost"))
                order.dopCost = java.lang.Double.valueOf(jsonNewOrder.getString("additionals_cost"))
                currentOrder.address = jsonNewDB.toString()
                order.address = jsonNewDB.toString()
                runBlocking {
                    withContext(Dispatchers.IO) {
                        val db = AppDatabase.getInstance(this@EditAddressesActivity)
                        currentOrder.id = db.currentOrderDao().save(currentOrder)
                        order.id = db.orderDao().save(order)
                    }
                    EventBus.getDefault().post(UpdateAdressesToMapEvent())
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else if (pushResponseEvent.uuid == uuid) {
            runOnUiThread { updateAddress(currentOrder.address) }
        }
        runOnUiThread {
            try {
                if (appPreferences!!.getText("allow_edit_order") == "1") {
                    btnComfirmEdit!!.visibility = View.VISIBLE
                }
                progressBar.visibility = View.GONE
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private fun getJSONAddress(list: List<String>): JSONObject {
        val jsonObjectAddresses = JSONObject()
        val jsonArrayAddresses = JSONArray()
        try {
            for (item in list) {
                val jsonItem = JSONObject()
                for (itemAddress in listAddresses!!) {
                    if (itemAddress.fullAddress == item) {
                        try {
                            jsonItem.put("city_id", itemAddress.cityId)
                            jsonItem.put("city", "")
                            jsonItem.put("street", itemAddress.fullAddress)
                            jsonItem.put("house", "")
                            jsonItem.put("housing", "")
                            jsonItem.put("porch", "")
                            jsonItem.put("lat", itemAddress.lat)
                            jsonItem.put("lon", itemAddress.lon)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        break
                    }
                }
                jsonArrayAddresses.put(jsonItem)
            }
            jsonObjectAddresses.put("address", jsonArrayAddresses)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return jsonObjectAddresses
    }

    private inner class EditAddress {
        var fullAddress: String? = null
        var city: String? = null
        var cityId: String? = null
        var street: String? = null
        var lat: String? = null
        var lon: String? = null
        var house: String? = null
        var housing: String? = null
        var porch: String? = null
        var apt: String? = null

        fun editAddressToJson(): JSONObject {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("city_id", this.cityId)
                jsonObject.put("city", this.city)
                jsonObject.put("parking", "")
                jsonObject.put("apt", this.apt)
                jsonObject.put("parking_id", "")
                jsonObject.put("lat", this.lat)
                jsonObject.put("lon", this.lon)
                jsonObject.put("street", this.street)
                if (this.street!!.isEmpty())
                    jsonObject.put("street", this.fullAddress)
                else
                    jsonObject.put("street", this.street)
                jsonObject.put("house", this.house)
                jsonObject.put("housing", this.housing)
                jsonObject.put("porch", this.porch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return jsonObject
        }
    }
}