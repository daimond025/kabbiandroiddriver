package com.kabbi.driver


import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.CurrentOrder
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ConfirmationCodeActivity : BaseActivity() {

    private lateinit var currentOrder: CurrentOrder
    private var llContainer: LinearLayout? = null
    private var appPreferences: AppPreferences? = null
    private val arrAddresses = arrayOf("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_confirmation_code)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.enter_code)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        llContainer = findViewById(R.id.ll_container)
        val btnConfirm = findViewById<Button>(R.id.btn_confirm)

        val driver = (application as App).driver

        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                currentOrder = AppDatabase.getInstance(this@ConfirmationCodeActivity).currentOrderDao().getCurOrder(driver.id)
            }

            btnConfirm.setOnClickListener {
                appPreferences!!.saveText("confirm_codes", "")
                var countIncorrect = 0
                val jsonArrayCorrect = JSONArray()
                for (i in 0 until llContainer!!.childCount) {
                    val editText = llContainer!!.getChildAt(i).findViewById<EditText>(R.id.et_code)
                    if (editText.text.toString().trim { it <= ' ' }.isNotEmpty()) {
                        if (editText.text.toString() != editText.getTag(R.id.tag_type).toString()) {
                            editText.error = getString(R.string.incorrect_code)
                            countIncorrect++
                        } else {
                            val jsonCorrect = JSONObject()
                            try {
                                jsonCorrect.put("point", editText.getTag(R.id.tag_label).toString())
                                jsonCorrect.put("code", editText.getTag(R.id.tag_type).toString())
                                jsonArrayCorrect.put(jsonCorrect)
                                appPreferences!!.saveText("confirmation_order_id", currentOrder.orderId)
                                appPreferences!!.saveText("confirm_codes", jsonArrayCorrect.toString())
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }

                if (countIncorrect == 0) {
                    finish()
                }
            }

            try {
                val jsonAddress = JSONObject(currentOrder.address)

                Log.d("OPS_PAID_CONFIRM", jsonAddress.toString())

                val addresses = HashMap<String, String>()
                if (currentOrder.orderId == appPreferences!!.getText("confirmation_order_id")) {
                    var jsonArray = JSONArray()
                    try {
                        jsonArray = JSONArray(appPreferences!!.getText("confirm_codes"))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    for (i in 0 until jsonArray.length()) {
                        val jsonItem = jsonArray.get(i) as JSONObject
                        addresses[jsonItem.getString("point")] = jsonItem.getString("code")
                    }
                }

                for (point in arrAddresses) {
                    if (point == "A") continue
                    try {
                        val codeView = layoutInflater.inflate(R.layout.item_code, null, false)
                        val etCode = codeView.findViewById<EditText>(R.id.et_code)
                        val tvAddress = codeView.findViewById<TextView>(R.id.tv_address)

                        tvAddress.text = getAddressFromJson(jsonAddress.getJSONObject(point))
                        etCode.setTag(R.id.tag_type, jsonAddress.getJSONObject(point).getString("confirmation_code"))
                        etCode.setTag(R.id.tag_label, point)

                        if (addresses.containsKey(point)) {
                            etCode.setText(addresses[point])
                        }

                        llContainer!!.addView(codeView)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    private fun getAddressFromJson(jsonObject: JSONObject): String {
        val stringBuilder = StringBuilder()

        try {
            if (!jsonObject.isNull("city") && jsonObject.getString("city").isNotEmpty()) {
                stringBuilder.append(jsonObject.getString("city"))
                stringBuilder.append(", " + jsonObject.getString("street"))
            } else {
                stringBuilder.append(jsonObject.getString("street"))
            }

            if (jsonObject.getString("house").isNotEmpty() && !jsonObject.isNull("house"))
                stringBuilder.append(", " + getString(R.string.text_house) + " " + jsonObject.getString("house"))


            if (jsonObject.getString("housing").isNotEmpty() && !jsonObject.isNull("housing"))
                stringBuilder.append(", " + getString(R.string.text_housing) + " " + jsonObject.getString("housing"))

            if (jsonObject.getString("porch").isNotEmpty() && !jsonObject.isNull("porch"))
                stringBuilder.append(", " + getString(R.string.text_porch) + " " + jsonObject.getString("porch"))

            try {
                if (jsonObject.getString("apt").isNotEmpty() && !jsonObject.isNull("apt"))
                    stringBuilder.append(", " + getString(R.string.text_apt) + " " + jsonObject.getString("apt"))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return stringBuilder.toString()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }
}
