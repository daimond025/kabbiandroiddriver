package com.kabbi.driver


import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import android.text.Html
import android.text.Spanned
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.ClientTariff
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.events.CloseActivityEvent
import com.kabbi.driver.border.OrderPagerBorderFragmentRes
import com.kabbi.driver.helper.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.sufficientlysecure.htmltextview.HtmlTextView
import java.util.*
import java.util.concurrent.TimeUnit

class ReportOrderDetailActivity : BaseActivity() {

    internal  var order: Order? = null
    private lateinit var jsonTariffCity: JSONObject
    private lateinit var jsonTariffTrack: JSONObject
    private lateinit var tariffCity: ClientTariff
    private lateinit var tariffTrack: ClientTariff
    private lateinit var orderPagerBorderFragmentRes: OrderPagerBorderFragmentRes
    private var bonus = ""
    private var promo = ""
    internal lateinit var typedValueMain: TypedValue
    internal lateinit var typedValueSubscribe: TypedValue
    private var timeWaitClient: Long = 0
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private var llContainerFinish: LinearLayout? = null
    private var reportList: MutableList<ItemReport>? = null

    private var finalCost = 0.0
    private var viewGroup: ViewGroup? = null
    private lateinit var db: AppDatabase

    @SuppressLint("StringFormatInvalid")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        db = AppDatabase.getInstance(this)
        val appPreferences = AppPreferences(applicationContext)

        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_report_orderdetail)

        viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        reportList = ArrayList()
        llContainerFinish = findViewById(R.id.container_finish)

        EventBus.getDefault().register(this)

        runBlocking {
            withContext(Dispatchers.IO) {
                order = db.orderDao().getById(intent.extras!!.getLong("order_id"))
            }
        }

        try {
            jsonTariffCity = JSONObject(order!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataCity")
            jsonTariffTrack = JSONObject(order!!.tariffData).getJSONObject("tariffInfo").getJSONObject("tariffDataTrack")
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        if (order!!.orderIdService != null)
            toolbar.title = "  " + getString(R.string.title_toolbar_orderdetail) + order!!.orderIdService
        else
            toolbar.title = "  " + getString(R.string.title_toolbar_orderdetail) + order!!.orderId
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        orderPagerBorderFragmentRes = OrderPagerBorderFragmentRes()

        findViewById<TextView>(R.id.textview_reportorder_cost_currency).text = " " + TypefaceSpanEx.getOnlyCurrencyStr(applicationContext)

        finalCost = Round.roundFormatDouble(order!!.costOrder, order!!.rounding, order!!.roundingType)
        initialize()
    }

    private fun initialize() {
        val clientTariffDao = db.clientTariffDao()

        runBlocking {
            withContext(Dispatchers.IO) {
                tariffCity = clientTariffDao.getTariff(order!!.id, ClientTariffParser.AREA_CITY)
                tariffTrack = clientTariffDao.getTariff(order!!.id, ClientTariffParser.AREA_TRACK)
            }
        }
        if (order!!.fix == 1) {
            fixPrice(order!!)
        } else {
            if (finalCost > order!!.minPrice) {
                when {
                    tariffCity.bonusPayment -> {
                        bonus = Round.roundFormat10(finalCost - BonusSystem.getBonusCost(finalCost, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))

                        (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>" + Round.roundFormatFinal(finalCost, order!!.rounding, order!!.roundingType) + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getBonusCost(finalCost, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod)) + "</font>", HtmlTextView.LocalImageGetter())
                        (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.written_off), bonus)
                    }
                    order!!.promoCodeDiscount > 0 -> {
                        promo = Round.roundFormat10(finalCost - BonusSystem.getPromoCost(finalCost, order!!.promoCodeDiscount.toDouble()))

                        (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>" + Round.roundFormatFinal(finalCost, order!!.rounding, order!!.roundingType) + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getPromoCost(finalCost, order!!.promoCodeDiscount.toDouble())) + "</font>", HtmlTextView.LocalImageGetter())
                        (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.promo_mask), order!!.promoCodeDiscount.toString()) + "%"

                    }
                    else -> (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = Round.roundFormatFinal(finalCost, order!!.rounding, order!!.roundingType)
                }
            } else {
                when {
                    tariffCity.bonusPayment -> {
                        bonus = Round.roundFormat10(order!!.minPrice - BonusSystem.getBonusCost(order!!.minPrice, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))

                        (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>" + order!!.minPrice + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getBonusCost(order!!.minPrice, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod)) + "</font>", HtmlTextView.LocalImageGetter())
                        (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.written_off), bonus)
                    }
                    order!!.promoCodeDiscount > 0 -> {
                        promo = Round.roundFormat10(order!!.minPrice - BonusSystem.getPromoCost(order!!.minPrice, order!!.promoCodeDiscount.toDouble()))

                        (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>" + order!!.minPrice + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>" + Round.roundFormat10(BonusSystem.getPromoCost(order!!.minPrice, order!!.promoCodeDiscount.toDouble())) + "</font>", HtmlTextView.LocalImageGetter())
                        (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.promo_mask), order!!.promoCodeDiscount.toString()) + "%"

                    }
                    else -> (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = order!!.minPrice.toString() + " "
                }
            }

            reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.plantingCost.toString()))))

            if (order!!.dopCost > 0)
                reportList!!.add(ItemReport(getString(R.string.text_reportorder_dop), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.dopCost.toString()))))

            if (order!!.priceTrackOutOrder > 0) {
                reportList!!.add(ItemReport(getString(R.string.price_finish_outorder_dis),
                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + Round.roundFormatDis(order!!.disTrackOutOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.priceTrackOutOrder)) + "</font>")))
            }

            if (order!!.startPoint == "in") {
                when {
                    order!!.typeCostBase == ClientTariffParser.ACCRUAL_DISTANCE -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                            + tariffCity.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    order!!.typeCostBase == ClientTariffParser.ACCRUAL_TIME -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                            + tariffCity.plantingInclude + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    order!!.typeCostBase == ClientTariffParser.ACCRUAL_MIXED -> {
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + tariffCity.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + AppParams.colorBlack(applicationContext) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + tariffCity.plantingIncludeTime + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    }
                }
            } else {
                when {
                    order!!.typeCostTrack == ClientTariffParser.ACCRUAL_DISTANCE -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                            + tariffTrack.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + AppParams.colorBlack(applicationContext) + "'>"
                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    order!!.typeCostTrack == ClientTariffParser.ACCRUAL_TIME -> reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                            + tariffTrack.plantingInclude + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    order!!.typeCostBase == ClientTariffParser.ACCRUAL_MIXED -> {
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + tariffTrack.plantingInclude + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                        reportList!!.add(ItemReport(getString(R.string.text_reportorder_free), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + tariffTrack.plantingIncludeTime + " " + getString(R.string.text_time_min_small) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, "0") + "</font>")))
                    }
                }
            }
        }

        (findViewById<View>(R.id.textview_reportorder_sumdistance) as TextView).text = order!!.sumDistance.toString() + " " + getString(R.string.text_dis)
        (findViewById<View>(R.id.textview_reportorder_sumtime) as TextView).text = formatTime(order!!.sumTime)

        try {
            if (order!!.fix == 0) {
                if (order!!.typeCostBase == "DISTANCE") {
                    try {
                        if (order!!.disCityInOrder > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " 1 " + getString(R.string.text_dis) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + Round.roundFormatDis(order!!.disCityInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.disCityInOrder * java.lang.Double.valueOf(if (order!!.day) jsonTariffCity.getString("next_km_price_day")
                                    else jsonTariffCity.getString("next_km_price_night")))) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                } else if (order!!.typeCostBase == "TIME") {
                    try {
                        if (order!!.timeCityInOrder > 0) {
                            val timeCity: String
                            val costCity: String
                            when (tariffCity.nextCostUnit) {
                                ClientTariffParser.COST_UNIT_30 -> {
                                    timeCity = "30"
                                    costCity = if (order!!.timeCityInOrder > 0) {
                                        if (order!!.timeCityInOrder / 1800000 == 0L)
                                            tariffCity.nextKmPrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeCityInOrder / 1800000 + 1) * tariffCity.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                ClientTariffParser.COST_UNIT_60 -> {
                                    timeCity = "60"

                                    costCity = if (order!!.timeCityInOrder > 0) {
                                        if (order!!.timeCityInOrder / 3600000 == 0L) {
                                            tariffCity.nextKmPrice.toString()
                                        } else {
                                            Round.roundFormat10((order!!.timeCityInOrder / 3600000 + 1) * tariffCity.nextTimePrice)
                                        }
                                    } else {
                                        "0"
                                    }
                                }
                                else -> {
                                    timeCity = "1"
                                    costCity = Round.roundFormat10(order!!.timeCityInOrder.toDouble() / 60000.0 * tariffCity.nextTimePrice)
                                }
                            }
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " " + timeCity + " " + getString(R.string.text_time_min_small) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + formatTime(order!!.timeCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, costCity) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (order!!.typeCostBase == "FIX") {
                    if (order!!.fix == 0) {
                        if (order!!.fixCityCost > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix_city), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.fixCityCost.toString()))))

                        }
                    }
                } else if (order!!.typeCostBase == ClientTariffParser.ACCRUAL_MIXED) {
                    try {
                        if (order!!.timeCityInOrder > 0) {
                            val timeCity: String
                            val costCity: String
                            when (tariffCity.nextCostUnit) {
                                ClientTariffParser.COST_UNIT_30 -> {
                                    timeCity = "30"
                                    costCity = if (order!!.timeCityInOrder > 0) {
                                        if (order!!.timeCityInOrder / 1800000 == 0L)
                                            tariffCity.nextTimePrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeCityInOrder / 1800000 + 1) * tariffCity.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                ClientTariffParser.COST_UNIT_60 -> {
                                    timeCity = "60"
                                    costCity = if (order!!.timeCityInOrder > 0) {
                                        if (order!!.timeCityInOrder / 3600000 == 0L)
                                            tariffCity.nextTimePrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeCityInOrder / 3600000 + 1) * tariffCity.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                else -> {
                                    timeCity = "1"
                                    costCity = Round.roundFormat10(order!!.timeCityInOrder.toDouble() / 60000.0 * tariffCity.nextTimePrice)
                                }
                            }

                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " " + timeCity + " " + getString(R.string.text_time_min_small) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("next_km_price_day_time") else jsonTariffCity.getString("next_km_price_night_time")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + formatTime(order!!.timeCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, costCity) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    try {
                        if (order!!.disCityInOrder > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_base) + " 1 " + getString(R.string.text_dis) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + Round.roundFormatDis(order!!.disCityInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.disCityInOrder * java.lang.Double.valueOf(if (order!!.day) jsonTariffCity.getString("next_km_price_day") else jsonTariffCity.getString("next_km_price_night")))) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (order!!.typeCostBase == ClientTariffParser.ACCRUAL_INTERVAL || order!!.typeCostTrack == ClientTariffParser.ACCRUAL_INTERVAL) {
                    if (order!!.priceIntervalInOrder > 0)
                        reportList!!.add(ItemReport(getString(R.string.text_price_interval), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, Round.roundFormatFinal(order!!.priceIntervalInOrder, order!!.rounding, order!!.roundingType)))))
                }

                if (order!!.typeCostTrack == "DISTANCE") {
                    try {
                        if (order!!.disTrackInOrder > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " 1 " + getString(R.string.text_dis) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + Round.roundFormatDis(order!!.disTrackInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.disTrackInOrder * java.lang.Double.valueOf(if (order!!.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")))) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (order!!.typeCostTrack == "TIME") {
                    try {
                        if (order!!.timeTrackInOrder > 0) {
                            val timeTrack: String
                            val costTrack: String
                            when (tariffTrack.nextCostUnit) {
                                ClientTariffParser.COST_UNIT_30 -> {
                                    timeTrack = "30"
                                    costTrack = if (order!!.timeTrackInOrder > 0) {
                                        if (order!!.timeTrackInOrder / 1800000 == 0L)
                                            tariffTrack.nextKmPrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeTrackInOrder / 1800000 + 1) * tariffTrack.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                ClientTariffParser.COST_UNIT_60 -> {
                                    timeTrack = "60"
                                    costTrack = if (order!!.timeTrackInOrder > 0) {
                                        if (order!!.timeTrackInOrder / 3600000 == 0L)
                                            tariffTrack.nextKmPrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeTrackInOrder / 3600000 + 1) * tariffTrack.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                else -> {
                                    timeTrack = "1"
                                    costTrack = Round.roundFormat10(order!!.timeTrackInOrder.toDouble() / 60000.0 * tariffTrack.nextTimePrice)
                                }
                            }

                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " " + timeTrack + " " + getString(R.string.text_time_min_small) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + formatTime(order!!.timeTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, costTrack) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                } else if (order!!.typeCostTrack == "FIX") {
                    if (order!!.fix == 0) {
                        if (order!!.fixOutCityCost > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix_outcity), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.fixOutCityCost.toString()))))
                        }
                    }
                } else if (order!!.typeCostTrack == ClientTariffParser.ACCRUAL_MIXED) {
                    try {

                        if (order!!.timeTrackInOrder > 0) {

                            val timeTrack: String
                            val costTrack: String
                            when (tariffTrack.nextCostUnit) {
                                ClientTariffParser.COST_UNIT_30 -> {
                                    timeTrack = "30"
                                    costTrack = if (order!!.timeTrackInOrder > 0) {
                                        if (order!!.timeTrackInOrder / 1800000 == 0L)
                                            tariffTrack.nextTimePrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeTrackInOrder / 1800000 + 1) * tariffTrack.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                ClientTariffParser.COST_UNIT_60 -> {
                                    timeTrack = "60"
                                    costTrack = if (order!!.timeTrackInOrder > 0) {
                                        if (order!!.timeTrackInOrder / 3600000 == 0L)
                                            tariffTrack.nextTimePrice.toString()
                                        else
                                            Round.roundFormat10((order!!.timeTrackInOrder / 3600000 + 1) * tariffTrack.nextTimePrice)
                                    } else {
                                        "0"
                                    }
                                }
                                else -> {
                                    timeTrack = "1"
                                    costTrack = Round.roundFormat10(order!!.timeTrackInOrder.toDouble() / 60000.0 * tariffTrack.nextTimePrice)
                                }
                            }
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " " + timeTrack + " " + getString(R.string.text_time_min_small) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("next_km_price_day_time") else jsonTariffTrack.getString("next_km_price_night_time")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + formatTime(order!!.timeTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, costTrack) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    try {
                        if (order!!.disTrackInOrder > 0) {
                            reportList!!.add(ItemReport(getString(R.string.text_reportorder_track) + " 1 " + getString(R.string.text_dis) + "/"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")),
                                    Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                            + Round.roundFormatDis(order!!.disTrackInOrder).toString() + " " + getString(R.string.text_dis) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                            + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.disTrackInOrder * java.lang.Double.valueOf(if (order!!.day) jsonTariffTrack.getString("next_km_price_day") else jsonTariffTrack.getString("next_km_price_night")))) + "</font>")))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                if (order!!.timeWaitCityInOrder > 0)
                    reportList!!.add(ItemReport(getString(R.string.text_reportorder_waitbase) + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")),
                            Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                    + formatTime(order!!.timeWaitCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.timeWaitCityInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order!!.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")))) + "</font>")))

                if (order!!.timeWaitTrackInOrder > 0)
                    reportList!!.add(ItemReport(getString(R.string.text_reportorder_waittrack) + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")),
                            Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                    + formatTime(order!!.timeWaitTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.timeWaitTrackInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order!!.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")))) + "</font>")))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (order!!.priceClientWait > 0) {

            if (order!!.timeWaitCityClient > 0)
                timeWaitClient += order!!.timeWaitCityClient

            if (order!!.timeWaitTrackClient > 0)
                timeWaitClient += order!!.timeWaitTrackClient

            reportList!!.add(ItemReport(getString(R.string.text_reportorder_beforewait), Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                    + formatTime(timeWaitClient) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.priceClientWait)) + "</font>")))
        }

        reportList!!.add(ItemReport(getString(R.string.price_finish_round), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.rounding.toString()))))
        reportList!!.add(ItemReport(getString(R.string.price_finish_mincost), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, order!!.minPrice.toString()))))

        llContainerFinish!!.removeAllViews()
        for (item in reportList!!) {
            val layoutView = layoutInflater!!.inflate(R.layout.item_view_finish, null, false)
            (layoutView.findViewById<View>(R.id.textview_reportorder_label) as TextView).text = item.label
            (layoutView.findViewById<View>(R.id.textview_reportorder_info) as TextView).text = item.desc
            llContainerFinish!!.addView(layoutView)
        }

        if (order!!.typeCost != null && order!!.typeCost == "CASH")
            (findViewById<View>(R.id.textview_reportorder_cash) as TextView).text = getString(R.string.text_reportorder_cash)
        else
            (findViewById<View>(R.id.textview_reportorder_cash) as TextView).text = getString(R.string.text_reportorder_nocash)

        (findViewById<View>(R.id.textview_report_orderdetail_btn1) as TextView).text = getString(R.string.text_reportorder_btn_1) + " " + order!!.tariffLabel

        findViewById<View>(R.id.textview_report_orderdetail_btn1).setOnClickListener {
            val jsonObject = JSONObject()
            try {
                jsonObject.put("code", 0)
                jsonObject.put("info", "OK")
                jsonObject.put("result", JSONArray().put(JSONObject().put("tariffInfo", JSONObject(order!!.tariffData).getJSONObject("tariffInfo"))))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val builder = AlertDialog.Builder(this@ReportOrderDetailActivity)
            builder.setTitle(order!!.tariffLabel + " " + (if (order!!.startPoint == "in") getString(R.string.city) else getString(R.string.track)) + " "
                    + if (order!!.day) getString(R.string.day) else getString(R.string.night))
            builder.setView(orderPagerBorderFragmentRes.getViewTariffs(this@ReportOrderDetailActivity, jsonObject)).setPositiveButton(getString(R.string.button_exit)) { dialog, which -> dialog.dismiss() }.create().show()
        }

        //LayoutInflater layoutInflater = getLayoutInflater();
        val stringBuilder = StringBuilder()

        try {
            val jsonObjectAddress = JSONObject(order!!.address)
            stringBuilder.setLength(0)

            if (jsonObjectAddress.getJSONObject("A").getString("city").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("city") != "null")
                stringBuilder.append(jsonObjectAddress.getJSONObject("A").getString("city") + ", ")

            if (jsonObjectAddress.getJSONObject("A").getString("street").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("street") != "null")
                stringBuilder.append(jsonObjectAddress.getJSONObject("A").getString("street"))

            if (jsonObjectAddress.getJSONObject("A").getString("house").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("house") != "null")
                stringBuilder.append(", " + getString(R.string.text_house) + " " + jsonObjectAddress.getJSONObject("A").getString("house"))

            if (jsonObjectAddress.getJSONObject("A").getString("housing").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("housing") != "null")
                stringBuilder.append(", " + getString(R.string.text_housing) + " " + jsonObjectAddress.getJSONObject("A").getString("housing"))

            if (jsonObjectAddress.getJSONObject("A").getString("porch").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("porch") != "null")
                stringBuilder.append(", " + getString(R.string.text_porch) + " " + jsonObjectAddress.getJSONObject("A").getString("porch"))

            if (jsonObjectAddress.getJSONObject("A").getString("apt").isNotEmpty() && jsonObjectAddress.getJSONObject("A").getString("apt") != "null")
                stringBuilder.append(", " + getString(R.string.text_apt) + " " + jsonObjectAddress.getJSONObject("A").getString("apt"))


            (findViewById<View>(R.id.textview_order_offer_point_a) as TextView).text = stringBuilder.toString()


            if (order!!.comment != null && order!!.comment.isNotEmpty()) {
                findViewById<View>(R.id.textview_orderoffer_comment).visibility = View.VISIBLE
                (findViewById<View>(R.id.textview_orderoffer_comment) as TextView).text = order!!.comment
            }

            for (itemAdress in arrAddresses) {
                try {
                    jsonObjectAddress.getJSONObject(itemAdress)
                    val layoutView = layoutInflater!!.inflate(R.layout.detail_orderoffer_source, null, false)

                    stringBuilder.setLength(0)

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("city").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("city") != "null")
                        stringBuilder.append(jsonObjectAddress.getJSONObject(itemAdress).getString("city") + ", ")

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("street").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("street") != "null")
                        stringBuilder.append(jsonObjectAddress.getJSONObject(itemAdress).getString("street"))

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("house").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("house") != "null")
                        stringBuilder.append(", " + getString(R.string.text_house) + " " + jsonObjectAddress.getJSONObject(itemAdress).getString("house"))

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("housing").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("housing") != "null")
                        stringBuilder.append(", " + getString(R.string.text_housing) + " " + jsonObjectAddress.getJSONObject(itemAdress).getString("housing"))

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("porch").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("porch") != "null")
                        stringBuilder.append(", " + getString(R.string.text_porch) + " " + jsonObjectAddress.getJSONObject(itemAdress).getString("porch"))

                    if (jsonObjectAddress.getJSONObject(itemAdress).getString("apt").isNotEmpty() && jsonObjectAddress.getJSONObject(itemAdress).getString("apt") != "null")
                        stringBuilder.append(", " + getString(R.string.text_apt) + " " + jsonObjectAddress.getJSONObject(itemAdress).getString("apt"))

                    (layoutView.findViewById<View>(R.id.textview_orderoffer_source) as TextView).text = stringBuilder.toString()
                    (findViewById<View>(R.id.linearlayout_orderoffer_second_source) as LinearLayout).addView(layoutView)
                    findViewById<View>(R.id.linearlayout_orderoffer_to).visibility = View.VISIBLE


                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }

            if (order!!.dopOption != null) {

                val jsonArrayOptions = JSONArray(order!!.dopOption)
                val linearLayoutOptions = findViewById<LinearLayout>(R.id.linearlayout_orderoffer_options)
                val views = ArrayList<View>()

                var countOption = 1

                var layoutView: View? = null
                for (j in 0 until jsonArrayOptions.length()) {
                    val jsonObjectOption = jsonArrayOptions.getJSONObject(j)

                    when (countOption) {
                        1 -> {
                            layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_option, null, false)
                            (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option1) as TextView).text = jsonObjectOption.getString("name")

                            if (j == jsonArrayOptions.length() - 1) {
                                views.add(layoutView)
                            }
                        }
                        2 -> {
                            (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option2) as TextView).text = jsonObjectOption.getString("name")
                            views.add(layoutView)
                            countOption = 0
                        }
                    }
                    countOption++
                }

                for (item in views) {
                    linearLayoutOptions.addView(item)
                }

            }


        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe
    fun onEvent(closeActivityEvent: CloseActivityEvent) {
        finish()
    }

    private fun fixPrice(order: Order) {
        reportList!!.add(ItemReport(getString(R.string.text_reportorder_planting_fix), Html.fromHtml(TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, Round.roundFormat(order!!.costOrder, order!!.rounding)))))

        (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = Round.roundFormatDouble(order!!.costOrder + order!!.priceClientWait + order!!.priceCityWaitInOrder + order!!.priceTrackWaitInOrder, order!!.rounding, order!!.roundingType).toString()

        val finalFixCost = Round.roundFormatDouble(order!!.costOrder
                + order!!.priceClientWait
                + order!!.priceCityWaitInOrder
                + order!!.priceTrackWaitInOrder, order!!.rounding, order!!.roundingType)

        when {
            tariffCity.bonusPayment -> {
                bonus = Round.roundFormat10((finalCost
                        + order!!.priceClientWait
                        + order!!.priceCityWaitInOrder
                        + order!!.priceTrackWaitInOrder) - BonusSystem.getBonusCost(finalCost
                        + order!!.priceClientWait
                        + order!!.priceCityWaitInOrder
                        + order!!.priceTrackWaitInOrder, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))

                (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView)
                        .setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>"
                                + Round.roundFormat10(finalCost
                                + order!!.priceClientWait
                                + order!!.priceCityWaitInOrder
                                + order!!.priceTrackWaitInOrder) + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>"
                                + Round.roundFormat10(BonusSystem.getBonusCost(finalCost
                                + order!!.priceClientWait
                                + order!!.priceCityWaitInOrder
                                + order!!.priceTrackWaitInOrder, tariffCity.maxPayment, tariffCity.clientBonusBalance, tariffCity.maxPaymentType, tariffCity.paymentMethod))
                                + "</font>", HtmlTextView.LocalImageGetter())
                (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.written_off), bonus)
            }
            order!!.promoCodeDiscount > 0 -> {
                promo = (finalFixCost - BonusSystem.getPromoCost(finalFixCost, order!!.promoCodeDiscount.toDouble())).toString()

                (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView)
                        .setHtmlFromString("<small><small><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'><strike>"
                                + Round.roundFormatFinal(order!!.costOrder
                                + order!!.priceClientWait
                                + order!!.priceCityWaitInOrder
                                + order!!.priceTrackWaitInOrder, order!!.rounding, order!!.roundingType) + "</strike></font></small></small>"
                                + "  <font color='#2E7D32'>"
                                + BonusSystem.getPromoCost(finalFixCost, order!!.promoCodeDiscount.toDouble())
                                + "</font>", HtmlTextView.LocalImageGetter())
                (findViewById<View>(R.id.textview_reportorder_cost_written_off) as TextView).text = String.format(getString(R.string.promo_mask), order!!.promoCodeDiscount.toString()) + "%"

            }
            else -> (findViewById<View>(R.id.textview_reportorder_cost) as HtmlTextView).text = Round.roundFormatDouble(order!!.costOrder + order!!.priceClientWait + order!!.priceCityWaitInOrder + order!!.priceTrackWaitInOrder, order!!.rounding, order!!.roundingType).toString()
        }

        try {
            if (order!!.timeWaitCityInOrder > 0) {
                reportList!!.add(ItemReport(getString(R.string.text_reportorder_waitbase) + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")),
                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + formatTime(order!!.timeWaitCityInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.timeWaitCityInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order!!.day) jsonTariffCity.getString("wait_price_day") else jsonTariffCity.getString("wait_price_night")))) + "</font>")))
            }

            if (order!!.timeWaitTrackInOrder > 0) {
                reportList!!.add(ItemReport(getString(R.string.text_reportorder_waittrack) + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, if (order!!.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")),
                        Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe.data) + "'>"
                                + formatTime(order!!.timeWaitTrackInOrder) + "</font>" + " " + "<font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain.data) + "'>"
                                + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, formatCost(order!!.timeWaitTrackInOrder.toDouble() / 60000.0 * java.lang.Double.valueOf(if (order!!.day) jsonTariffTrack.getString("wait_price_day") else jsonTariffTrack.getString("wait_price_night")))) + "</font>")))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun formatCost(cost: Double): String {
        return (Math.round(cost * 100).toDouble() / 100).toString()
    }

    private fun formatTime(mSec: Long): String {
        return if (mSec > 0) {
            String.format(Locale.ENGLISH, "%d " + getString(R.string.text_time_min_small) + ", %d " + getString(R.string.text_time_sec_small), TimeUnit.MILLISECONDS.toMinutes(mSec), TimeUnit.MILLISECONDS.toSeconds(mSec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mSec)))
        } else {
            String.format(Locale.ENGLISH, "%d " + getString(R.string.text_time_min_small) + ", %d " + getString(R.string.text_time_sec_small), TimeUnit.MILLISECONDS.toMinutes(0), TimeUnit.MILLISECONDS.toSeconds(0) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(0)))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private inner class ItemReport(val label: String, val desc: Spanned)
}
