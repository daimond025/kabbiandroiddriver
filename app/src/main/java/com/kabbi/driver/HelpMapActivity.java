package com.kabbi.driver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.map.DriverMap;
import com.kabbi.driver.map.DriverMapGoogle;
import com.kabbi.driver.map.DriverMapOsm;
import com.kabbi.driver.map.DriverMapYandex;


public class HelpMapActivity extends BaseActivity implements OnMapReadyCallback {

    double lat, lon;
    BroadcastReceiver broadcastReceiver;

    DriverMap driverMap;
    AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helpmap);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getIntent().getExtras().getString("car_data"));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lat = Double.valueOf(getIntent().getExtras().getString("lat"));
        lon = Double.valueOf(getIntent().getExtras().getString("lon"));

        appPreferences = new AppPreferences(getApplicationContext());

        if (appPreferences.getText("favorite_map").equals("3")) {
            driverMap = new DriverMapYandex("normal");

            driverMap.initMap(getApplicationContext());
            ((FrameLayout) findViewById(R.id.fragment_map)).addView(driverMap.getMapView());
        } else if (appPreferences.getText("favorite_map").equals("2")) {
            driverMap = new DriverMapOsm();
            driverMap.initMap(getApplicationContext());
            ((FrameLayout) findViewById(R.id.fragment_map)).addView(driverMap.getMapView());
        } else if (appPreferences.getText("favorite_map").equals("1")) {
            driverMap = new DriverMapGoogle();
            SupportMapFragment mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_map, mapFragment);
            fragmentTransaction.commit();
            mapFragment.getMapAsync(this);
        } if (appPreferences.getText("favorite_map").equals("0")) {
            driverMap = new DriverMapYandex("narod");
            driverMap.initMap(getApplicationContext());
            ((FrameLayout) findViewById(R.id.fragment_map)).addView(driverMap.getMapView());
        }

        if (!appPreferences.getText("favorite_map").equals("1")) {
            driverMap.setCenterMap(lat, lon);
            driverMap.addMarker(lat, lon, R.drawable.sos_map);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                driverMap.updateCar(intent.getExtras().getDouble(AppParams.COORDS_GPS_LAT), intent.getExtras().getDouble(AppParams.COORDS_GPS_LON), intent.getExtras().getFloat(AppParams.BEARING_GPS));
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(AppParams.BROADCAST_ORDER));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(broadcastReceiver);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        driverMap.initMap(getApplicationContext(), googleMap);

        driverMap.setCenterMap(Double.valueOf(appPreferences.getText("lat")), Double.valueOf(appPreferences.getText("lon")));

        googleMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.sos_map)).draggable(false));

    }
}
