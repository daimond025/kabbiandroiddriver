package com.kabbi.driver.models

class OwnOrder {
    var orderId: String = ""
    var parking: String = ""
    var type: String = ""
    var lon: String = ""
    var lat: String = ""
    var house: String = ""
    var housing: String = ""
    var porch: String = ""
    var city: String = ""
    var street: String = ""
    var apt: String = ""
    var orderTime: String = ""
    var payment: String = ""
    var cityId: String = ""
    var dis: String = ""
    var isDenyRefuse: Boolean = false

    var title: String = ""

    constructor()

    constructor(type: String, title: String) {
        this.type = type
        this.title = title
    }
}
