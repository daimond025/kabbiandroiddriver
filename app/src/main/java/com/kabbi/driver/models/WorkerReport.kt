package com.kabbi.driver.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WorkerReport(@SerializedName("create_time")
                   @Expose
                   val createTime: Long,
                   @SerializedName("address")
                   @Expose
                   val address: Address,
                   @SerializedName("order_id")
                   @Expose
                   val orderId: Long,
                   @SerializedName("earnings")
                   @Expose
                   val earnings: Double?,
                   @SerializedName("summary_cost")
                   @Expose
                   val summaryCost: Double?)