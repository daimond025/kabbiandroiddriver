package com.kabbi.driver.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Position(
        val name: String,
        @SerializedName("position_id")
        val positionId: String,
        @SerializedName("has_car")
        val isHasCar: Boolean) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString()!!,
            source.readString()!!,
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(name)
        writeString(positionId)
        writeInt((if (isHasCar) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Position> = object : Parcelable.Creator<Position> {
            override fun createFromParcel(source: Parcel): Position = Position(source)
            override fun newArray(size: Int): Array<Position?> = arrayOfNulls(size)
        }
    }
}