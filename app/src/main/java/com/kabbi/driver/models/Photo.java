package com.kabbi.driver.models;

import java.io.Serializable;

public class Photo implements Serializable {

    public Photo(String photoId, String photoDescr) {
        this.photoId = photoId;
        this.photoDescr = photoDescr;
    }

    public Photo(String photoId, String photoDescr, String reviewComment, String reviewSrcLink) {
        this.photoId = photoId;
        this.photoDescr = photoDescr;
        this.reviewComment = reviewComment;
        this.reviewSrcLink = reviewSrcLink;
    }

    private String photoId;
    private String photoDescr;
    private String photoPath;
    private String reviewComment;
    private String reviewSrcLink;

    public String getPhotoDescr() {
        return photoDescr;
    }

    public String getPhotoId() {
        return photoId;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getReviewComment() {
        return reviewComment;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "typeId='" + photoId + '\'' +
                ", photoDescr='" + photoDescr + '\'' +
                ", photoPath='" + photoPath + '\'' +
                ", reviewComment='" + reviewComment + '\'' +
                ", reviewSrcLink='" + reviewSrcLink + '\'' +
                '}';
    }
}
