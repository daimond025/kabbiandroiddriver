package com.kabbi.driver.models


class Address {

    var city: String? = null
    var house: String? = null
    var lat: Double = 0.0
    var lon: Double = 0.0
    var porch: String? = null
    var street: String? = null
}
