package com.kabbi.driver.models

import com.google.gson.annotations.SerializedName

data class OpenOrder(
        @SerializedName("order_id")
        var id: Long,
        @SerializedName("position_id")
        var positionId: Int?,
        @SerializedName("order_from")
        var from: From,
        @SerializedName("order_time")
        var time: String?,
        @SerializedName("payment")
        var payment: String,
        @SerializedName("except_car_models")
        var except: String?,
        var type: String = "free",
        var distance: Double?
)

data class From(
        @SerializedName("city_id")
        var cityId: String?,
        @SerializedName("city")
        var city: String?,
        @SerializedName("street")
        var street: String?,
        @SerializedName("housing")
        var housing: String?,
        @SerializedName("house")
        var house: String?,
        @SerializedName("porch")
        var porch: String?,
        @SerializedName("apt")
        var apt: String?,
        @SerializedName("lat")
        var lat: Double,
        @SerializedName("lon")
        var lng: Double,
        @SerializedName("confirmation_code")
        var confirmationCode: String?,
        @SerializedName("comment")
        var comment: String?,
        @SerializedName("phone")
        var phone: String?,
        @SerializedName("parking_id")
        var parkingId: Long?,
        @SerializedName("parking")
        var parking: String?
) {
    override fun toString(): String {
        return "From(cityId=$cityId, city=$city, street=$street, housing=$housing, house=$house, porch=$porch, apt=$apt, lat=$lat, lng=$lng, confirmationCode=$confirmationCode, comment=$comment, phone=$phone, parkingId=$parkingId, parking=$parking)"
    }
}