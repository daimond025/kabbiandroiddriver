package com.kabbi.driver.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class DriverCity(
        @SerializedName("city_id")
        val cityId: String? = null,
        @SerializedName("name")
        val cityName: String? = null,
        @SerializedName("shortname")
        val cityShortName: String? = null,
        val lat: String? = null,
        val lon: String? = null) : Parcelable {

    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(cityId)
        writeString(cityName)
        writeString(cityShortName)
        writeString(lat)
        writeString(lon)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<DriverCity> = object : Parcelable.Creator<DriverCity> {
            override fun createFromParcel(source: Parcel): DriverCity = DriverCity(source)
            override fun newArray(size: Int): Array<DriverCity?> = arrayOfNulls(size)
        }
    }
}
