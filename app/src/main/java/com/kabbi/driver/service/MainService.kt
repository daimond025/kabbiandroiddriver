package com.kabbi.driver.service

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Resources
import android.graphics.Color
import android.graphics.PixelFormat
import android.location.Location
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.WindowManager
import androidx.core.app.NotificationCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.PolyUtil
import com.kabbi.driver.*
import com.kabbi.driver.activity.FakeGpsActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.CurrentOrderDao
import com.kabbi.driver.database.dao.OrderDao
import com.kabbi.driver.database.dao.SoundDao
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.events.orderStatus.OrderStatusEvent
import com.kabbi.driver.events.taximeter.*
import com.kabbi.driver.fragments.ChatFragment
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.ClientTariffParser
import com.kabbi.driver.network.WebService
import com.kabbi.driver.offer.OrderOfferActivity
import com.kabbi.driver.preorder.RemindPreOrderActivity
import com.kabbi.driver.util.GpsTracker
import com.kabbi.driver.util.Taximeter
import com.kabbi.driver.util.Utils
import com.kabbi.driver.util.WebSocket
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONObject
import java.lang.Runnable
import java.util.*

@Suppress("UNUSED_PARAMETER")
class MainService : Service(), GpsTracker.GpsTrackerInterface, WebSocket.DriverChatInterface {

    private val notificationNameMain = "Main"
    private val notificationNameChat = "Chat"
    private val notificationNameOrders = "Orders"
    private val channelMain = "1"
    private val channelChat = "6"
    private val channelOrder = "7"
    private val pingCount = 7
    private val doublePressDelay: Long = 300
    private var notificationForeground = 777
    private lateinit var notificationManager: NotificationManager
    private lateinit var gpsTracker: GpsTracker
    private lateinit var webSocket: WebSocket
    private lateinit var taximeter: Taximeter
    private lateinit var driver: Driver
    private var currentOrder: CurrentOrder? = null
    private var order: Order? = null
    private lateinit var service: com.kabbi.driver.database.entities.Service
    private var getDoAsyncTaskTaximeter: TimerTask? = null
    private var timerTaximeter: Timer? = null
    private var handlerTaskTaximeter: Handler? = null
    private var serverTime: Long = 0
    private var workerLocation: WorkerLocation? = null
    private var hdop = 100.0
    private var parking: Parking? = null
    private var countTaximeter = 3
    private lateinit var appPreferences: AppPreferences
    private var intentOrder: Intent? = null
    private var stage: Int = 0
    private var stageOrder: Int = 0
    private var statusDriver: String? = null
    private var statusDriverEng: String? = null
    private var usedSatellite = 0
    private var visibleSatellite = 0
    private var parkingList: List<Parking>? = null
    private var parkingName = ""
    private var broadcastReceiverUnreaded: BroadcastReceiver? = null
    private var handlerTaskInfo: Handler? = null
    private var runnable: Runnable? = null
    private var falseCount: Int = 0
    private lateinit var mNotificationManager: NotificationManager
    private var builderForeground: NotificationCompat.Builder? = null
    private var windowManager: WindowManager? = null
    private var taximeterIsStarted = false
    private var startCommandIntent: Intent? = null
    private var closeActivity = false
    private lateinit var res: String
    private lateinit var soundDao: SoundDao
    private lateinit var currentOrderDao: CurrentOrderDao
    private lateinit var orderDao: OrderDao
    private var requests: MutableList<OrderStatusEvent> = mutableListOf()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate() {
        super.onCreate()

        val db = AppDatabase.getInstance(applicationContext)
        soundDao = db.soundDao()
        currentOrderDao = db.currentOrderDao()
        orderDao = db.orderDao()

        EventBus.getDefault().register(this)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        res = "android.resource://$packageName/"
        appPreferences = AppPreferences(applicationContext)

        runBlocking {
            withContext(Dispatchers.IO) {
                driver = db.driverDao().getById((appPreferences.getText("driver_id")).toLong())
                service = db.serviceDao().getById((appPreferences.getText("service_id")).toLong())
                parkingList = db.parkingDao().getAllPing(service.id)
            }
        }

        webSocket = WebSocket(applicationContext, this, driver, service)
        webSocket.connect()
        webSocket.emitGetUnreadMessage()

        gpsTracker = GpsTracker(this, applicationContext)
        gpsTracker.startTracking(appPreferences.getText("gps"), appPreferences.getText("gps_server"))

        taximeter = Taximeter()
        intentOrder = Intent(AppParams.BROADCAST_ORDER)

        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        setLocale(applicationContext)

        val typeParam: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_PHONE
        }

        val myParams = WindowManager.LayoutParams(
                DrawerLayout.LayoutParams.WRAP_CONTENT,
                DrawerLayout.LayoutParams.WRAP_CONTENT,
                typeParam,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT)

        myParams.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL

        runBlocking {

            withContext(Dispatchers.IO) {
                currentOrder = currentOrderDao.getCurOrder(driver.id)
                if (currentOrder != null) {
                    order = orderDao.getByOrderId(currentOrder!!.orderId)
                    taximeter.start(order!!, applicationContext, order!!.stage, order!!.statusOrder)
                    taximeterIsStarted = true
                }
            }
        }

        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, getIntentWorkShift(), 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(channelMain, notificationNameMain, NotificationManager.IMPORTANCE_LOW)
            notificationChannel.enableLights(true)
            notificationManager.createNotificationChannel(notificationChannel)
            builderForeground = NotificationCompat.Builder(applicationContext, channelMain)
                    .setSmallIcon(R.drawable.push)
                    .setChannelId(channelMain)
                    .setContentText(getString(R.string.driver_status))
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(false)
        } else {
            @Suppress("DEPRECATION")
            builderForeground = NotificationCompat.Builder(this@MainService)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(service.serviceName)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(getString(R.string.driver_status)))
                    .setContentText(getString(R.string.driver_status))
                    .setContentIntent(pendingIntent)
        }
        startForeground(notificationForeground, builderForeground!!.build())

        timerTaximeter = Timer()
        handlerTaskTaximeter = Handler()
        getDoAsyncTaskTaximeter = object : TimerTask() {
            override fun run() {
                handlerTaskTaximeter!!.post {
                    try {
                        if (workerLocation != null && driver != null) {
                            runBlocking {
                                withContext(Dispatchers.IO) {
                                    parking = AppDatabase.getInstance(applicationContext).parkingDao().getBase(service.id)
                                }
                            }
                            intentOrder!!.putExtra(AppParams.COORDS_GPS_LAT, workerLocation!!.workerLocation.latitude)
                            intentOrder!!.putExtra(AppParams.COORDS_GPS_LON, workerLocation!!.workerLocation.longitude)
                            intentOrder!!.putExtra(AppParams.SPEED_GPS, workerLocation!!.workerLocation.speed * 3.6)
                            intentOrder!!.putExtra(AppParams.BEARING_GPS, workerLocation!!.workerLocation.bearing)
                            intentOrder!!.putExtra(AppParams.ACCURACY_GPS, workerLocation!!.workerLocation.accuracy)
                            intentOrder!!.putExtra(AppParams.POLYGON_GPS, "unknow")
                            intentOrder!!.putExtra("lifetime", workerLocation!!.workerLocation.time)
                            intentOrder!!.putExtra("provider", workerLocation!!.workerLocation.provider)
                            intentOrder!!.putExtra("dop", hdop)

                            if (appPreferences.getText("deny_fakegps") == "1" && isMockLocation(workerLocation!!.workerLocation) > 0) {
                                appPreferences = AppPreferences(applicationContext)
                                if (appPreferences.getText("fake_gps") == "0") {
                                    val intentFakeGps = Intent(applicationContext, FakeGpsActivity::class.java)
                                    intentFakeGps.putExtra("fake_gps", isMockLocation(workerLocation!!.workerLocation))
                                    intentFakeGps.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    intentFakeGps.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(intentFakeGps)
                                }
                            }

                            EventBus.getDefault().post(CheckFakeGpsEvent(isMockLocation(workerLocation!!.workerLocation)))

                            try {
                                val curPoint = LatLng(workerLocation!!.workerLocation.latitude, workerLocation!!.workerLocation.longitude)
                                val listPolygon = ArrayList<LatLng>()
                                if (parking != null) {
                                    val jsonArrayCoords = JSONObject(parking!!.parkingPolygon).getJSONObject("parking_polygone").getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0)

                                    for (k in 0 until jsonArrayCoords.length()) {
                                        val jsonArrayPoint = jsonArrayCoords.getJSONArray(k)
                                        listPolygon.add(LatLng(jsonArrayPoint.getDouble(1), jsonArrayPoint.getDouble(0)))
                                    }
                                    if (PolyUtil.containsLocation(curPoint, listPolygon, false)) {
                                        intentOrder!!.putExtra(AppParams.POLYGON_GPS, "base")
                                    } else {
                                        intentOrder!!.putExtra(AppParams.POLYGON_GPS, "track")
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {
                                EventBus.getDefault().post(TrackingEvent(workerLocation!!.workerLocation.time,
                                        workerLocation!!.workerLocation.provider,
                                        workerLocation!!.workerLocation.accuracy,
                                        taximeter.order, workerLocation!!.workerLocation.latitude, workerLocation!!.workerLocation.longitude))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            taximeter.setPosition(intentOrder!!)

                            if (taximeterIsStarted)
                                taximeter.nextStep()

                            sendBroadcast(intentOrder)

                            try {
                                if (countTaximeter == pingCount) {
                                    val requestParams = LinkedHashMap<String, String>()
                                    requestParams["coordinate_accuracy"] = hdop.toString()
                                    requestParams["tenant_login"] = service.uri
                                    if (workerLocation!!.serverTime == 0L)
                                        requestParams["update_time"] = ""
                                    else
                                        requestParams["update_time"] = workerLocation!!.serverTime.toString()

                                    requestParams["worker_degree"] = workerLocation!!.workerLocation.bearing.toString()
                                    requestParams["worker_lat"] = workerLocation!!.workerLocation.latitude.toString()
                                    requestParams["worker_login"] = driver.callSign
                                    requestParams["worker_lon"] = workerLocation!!.workerLocation.longitude.toString()
                                    requestParams["worker_speed"] = (workerLocation!!.workerLocation.speed * 3.6).toString()
                                    appPreferences.saveText("lat", workerLocation!!.workerLocation.latitude.toString())
                                    appPreferences.saveText("lon", workerLocation!!.workerLocation.longitude.toString())
                                    WebService.setWorkerPosition(requestParams, driver.secretCode)

                                    countTaximeter = 0

                                    if (parkingList == null || parkingList!!.isEmpty()) {
                                        runBlocking {
                                            withContext(Dispatchers.IO) {
                                                parkingList = AppDatabase.getInstance(applicationContext).parkingDao().getAllPing(service.id)
                                            }
                                        }
                                    } else {
                                        val point = LatLng(workerLocation!!.workerLocation.latitude,
                                                workerLocation!!.workerLocation.longitude)

                                        var sendPark = false
                                        for (parking in parkingList!!) {
                                            val jsonPolygon = JSONObject(parking.parkingPolygon)
                                            val jsonArrayCoords = jsonPolygon.getJSONObject("parking_polygone")
                                                    .getJSONObject("geometry")
                                                    .getJSONArray("coordinates")
                                                    .getJSONArray(0)

                                            val listPolygon = ArrayList<LatLng>()
                                            for (k in 0 until jsonArrayCoords.length()) {
                                                val jsonArrayPoint = jsonArrayCoords.getJSONArray(k)
                                                listPolygon.add(LatLng(jsonArrayPoint.getDouble(1), jsonArrayPoint.getDouble(0)))
                                            }

                                            if (PolyUtil.containsLocation(point, listPolygon, false)) {

                                                if (jsonPolygon.getString("parking_type") != "basePolygon" && parking.parkingName != parkingName) {
                                                    parkingName = parking.parkingName ?: ""
                                                    EventBus.getDefault().post(NetworkParkingsListEvent("OK", parking))

                                                    val requestParamsQueue = LinkedHashMap<String, String>()
                                                    requestParamsQueue["parking_id"] = parking.parkingID.toString()
                                                    requestParamsQueue["tenant_login"] = service.uri
                                                    requestParamsQueue["worker_login"] = driver.callSign
                                                    WebService.setParkingQueue(requestParamsQueue, driver.secretCode)
                                                }

                                                sendPark = true
                                            }
                                        }

                                        if (!sendPark) {
                                            EventBus.getDefault().post(NetworkParkingsListEvent("OK", null))

                                            val requestParamsQueue = LinkedHashMap<String, String>()
                                            requestParamsQueue["parking_id"] = ""
                                            requestParamsQueue["tenant_login"] = service.uri
                                            requestParamsQueue["worker_login"] = driver.callSign
                                            WebService.setParkingQueue(requestParamsQueue, driver.secretCode)
                                        }
                                    }
                                }
                                countTaximeter++
                            } catch (e: Exception) {
                                Log.e("MS", e.localizedMessage)
                                e.printStackTrace()
                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
        timerTaximeter!!.schedule(getDoAsyncTaskTaximeter, 0, AppParams.UNIT_TIME)

        broadcastReceiverUnreaded = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (webSocket != null)
                    webSocket.emitGetUnreadMessage()
            }
        }
        registerReceiver(broadcastReceiverUnreaded, IntentFilter(WebSocket.BROADCAST_UNREAD_MESSAGE))

        falseCount = 0
        handlerTaskInfo = Handler()
        runnable = Runnable {
            if (webSocket != null) {
                EventBus.getDefault().post(ConnectedEvent(webSocket.isConnected))
                if (!webSocket.isConnected)
                    falseCount++
                else
                    falseCount = 0

                if (falseCount == 10) {
                    webSocket.disconnect()
                    webSocket.connect()
                    falseCount = 0
                }
            }

            handlerTaskInfo!!.postDelayed(runnable, 3000)
        }

        handlerTaskInfo!!.post(runnable)
        mNotificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    private fun getIntentWorkShift(): Intent {
        val dao = AppDatabase.getInstance(this).workDayDao()
        var workDay: WorkDay? = null

        runBlocking {
            withContext(Dispatchers.IO) {
                workDay = dao.getLastWorkDay(driver.id)
            }
            if (workDay == null) workDay = dao.getLastCloseWorkDay(driver.id)
        }

        val intent = Intent(applicationContext, WorkShiftActivity::class.java)
        intent.putExtra("service_id", service.id)
        intent.putExtra("confirm", false)
        intent.putExtra("stageOrder", 0)
        intent.putExtra("showDialog", "empty")
        intent.putExtra("workday_id", workDay!!.id)
        intent.putExtra("showDialogGps", true)

        (application as App).driver = driver
        (application as App).service = service
        (application as App).workDay = workDay!!

        appPreferences.saveText("driver_id", driver.id.toString())
        appPreferences.saveText("service_id", service.id.toString())
        appPreferences.saveText("workday_id", workDay!!.id.toString())

        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        return intent
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        startCommandIntent = intent
        if (intent == null)
            EventBus.getDefault().post(ShowWindowViewEvent(true))

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()

        stopForeground(true)

        EventBus.getDefault().unregister(this)

        timerTaximeter!!.cancel()
        gpsTracker.stopTracking()

        unregisterReceiver(broadcastReceiverUnreaded)
        handlerTaskInfo!!.removeCallbacks(runnable)
        webSocket.disconnect()
    }

    @Subscribe
    fun onEvent(costChange: CostChange) {
        webSocket.emitCostChange(costChange)
    }

    @Subscribe
    fun onEvent(event: OrderStatusEvent) {
        var outdated: OrderStatusEvent? = null
        requests.forEach { sent ->
            if (sent.status == event.status) {
                outdated = sent
            }
        }
        requests.remove(outdated)
        requests.add(event)
        webSocket.emitOrderStatus(event.status)
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(timeWaitStartEvent: TimeWaitStartEvent) {
        taximeter.timeWaitStart()
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(timeWaitStopEvent: TimeWaitStopEvent) {
        taximeter.timeWaitStop()
    }

    @Subscribe
    fun onEvent(destroyMainActivityEvent: DestroyMainActivityEvent) {
        closeActivity = destroyMainActivityEvent.isDestroy
    }

    @Subscribe
    fun onEvent(taximeterStageEvent: TaximeterStageEvent) {
        taximeter.setStage(taximeterStageEvent.stage, taximeterStageEvent.orderStatus)
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        try {
            orderForIdEvent.json?.let { orderData ->
                orderData.getJSONObject("result").getJSONObject("order_data")
                if (orderData.has("order_id") && taximeter.order.orderId == orderData.getString("order_id")) {
                    try {
                        val clientTariffDao = AppDatabase.getInstance(this).clientTariffDao()
                        CoroutineScope(Dispatchers.Default).launch {

                            var tariffCity: ClientTariff? = null
                            withContext(Dispatchers.IO) {
                                tariffCity = clientTariffDao.getTariff(taximeter.order.id, ClientTariffParser.AREA_CITY)
                            }
                            if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1")
                                taximeter.order.fix = 1
                            else
                                taximeter.order.fix = orderData.getJSONObject("costData").getInt("is_fix")

                            currentOrder?.address = orderData.getString("address")
                            taximeter.order.address = orderData.getString("address")
                            taximeter.order.typeCost = orderData.getString("payment")

                            var bonusPayment = 0
                            try {
                                bonusPayment = Integer.valueOf(orderData.getString("bonus_payment"))
                                try {
                                    if (orderData.isNull("bonusData") || orderData.getJSONArray("bonusData").length() == 0)
                                        bonusPayment = 0
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            if (bonusPayment == 1) {
                                tariffCity!!.bonusPayment = true
                                try {
                                    if (orderData.getJSONObject("bonusData").getString("payment_method") == ClientTariffParser.METHOD_PAYMENT_FULL) {
                                        if (orderData.getJSONObject("bonusData").getString("max_payment_type") == ClientTariffParser.TYPE_MAX_PAYMENT_BONUS) {
                                            tariffCity?.maxPayment = java.lang.Double.valueOf(orderData.getJSONObject("bonusData").getString("client_bonus_balance "))
                                        } else {
                                            tariffCity?.maxPayment = 100.0
                                        }
                                    } else {
                                        tariffCity?.maxPayment = java.lang.Double.valueOf(orderData.getJSONObject("bonusData").getString("max_payment"))
                                    }

                                    tariffCity?.clientBonusBalance = Math.floor(java.lang.Double.valueOf(orderData.getJSONObject("bonusData").getString("client_bonus_balance ")))
                                    tariffCity?.bonusId = orderData.getJSONObject("bonusData").getString("bonus_id")
                                    tariffCity?.paymentMethod = orderData.getJSONObject("bonusData").getString("payment_method")
                                    tariffCity?.maxPaymentType = orderData.getJSONObject("bonusData").getString("max_payment_type")
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            } else {
                                tariffCity!!.bonusPayment = false
                            }
                            withContext(Dispatchers.IO) {
                                clientTariffDao.save(tariffCity!!)
                            }

                            if (orderData.getJSONObject("status").getString("status_group") == "car_at_place") {
                                stage = 1
                                if (taximeter.timeWaitCityClient * taximeter.timeWaitTrackClient < 0) {
                                    taximeter.timeWaitCityClient = 0
                                    taximeter.timeWaitTrackClient = 0
                                }
                                taximeter.setStage(1, AppParams.ORDER_STATUS_ARRIVED)
                                currentOrder?.stage = stage
                                //currentOrder.setTimeBeginWait((new Date()).getTime());
                            } else if (orderData.getJSONObject("status").getString("status_group") == "executing") {
                                stage = 2
                                taximeter.setStage(2, AppParams.ORDER_STATUS_CAR_ASSIGNED)
                                currentOrder?.stage = stage
                                currentOrder?.waitTimeEnd = Date().time
                            }
                            if (!orderData.isNull("comment")) {
                                currentOrder?.comment = orderData.getString("comment")
                                taximeter.order.comment = orderData.getString("comment")
                            }
                            if (workerLocation?.workerLocation?.provider == "network" || orderData.getJSONObject("costData").getInt("is_fix") == 1) {
                                taximeter.order.costOrder = java.lang.Double.valueOf(orderData.getJSONObject("costData").getString("summary_cost"))
                            }
                            try {
                                if (orderData.getJSONArray("options").length() > 0) {
                                    currentOrder?.options = orderData.getJSONArray("options").toString()
                                    taximeter.order.dopOption = orderData.getJSONArray("options").toString()
                                } else {
                                    currentOrder?.options = ""
                                    taximeter.order.dopOption = ""
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {
                                if (orderData.getJSONObject("costData").isNull("additionals_cost")) {
                                    taximeter.order.dopCost = 0.0
                                } else {
                                    taximeter.order.dopCost = java.lang.Double.valueOf(orderData.getJSONObject("costData").getString("additionals_cost"))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            CoroutineScope(Dispatchers.IO).launch {
                                currentOrderDao.save(currentOrder!!)
                            }
                            (application as App).order = taximeter.order
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(workerPositionEvent: NetworkWorkerPositionEvent) {
        serverTime = workerPositionEvent.serverTime
        setLocale(applicationContext)
        statusDriver = ""
        statusDriverEng = workerPositionEvent.workerStatus
        when (workerPositionEvent.workerStatus) {
            "FREE" -> statusDriver = getString(R.string.status_free)
            "ON_ORDER" -> statusDriver = getString(R.string.status_on_order)
            "REQUIRED_INSPECTION_CAR" -> statusDriver = getString(R.string.status_required_inspection)
            "ON_BREAK" -> statusDriver = getString(R.string.status_on_break)
            "OFFER_ORDER" -> statusDriver = getString(R.string.status_offer_order)
            "BLOCKED" -> statusDriver = getString(R.string.status_blocked)
            "SHIFT_IS_CLOSED" -> statusDriver = getString(R.string.status_shift_closed)
        }
        val pendingIntent: PendingIntent
        if (workerPositionEvent.workerStatus != "REQUIRED_INSPECTION_CAR") {
            pendingIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, WorkShiftActivity::class.java), 0)
        } else {
            val intent = Intent(applicationContext, CarInspectionActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            pendingIntent = PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext, CarInspectionActivity::class.java), 0)
            stopService(Intent(applicationContext, MainService::class.java))
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(channelMain, notificationNameMain, NotificationManager.IMPORTANCE_LOW)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builderForeground = NotificationCompat.Builder(applicationContext, channelMain)
                    .setSmallIcon(R.drawable.push)
                    .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
                    .setSound(null)
                    .setChannelId(channelMain)
                    .setContentText(statusDriver)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
        } else {
            @Suppress("DEPRECATION")
            builderForeground = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(service.serviceName)
                    .setContentIntent(pendingIntent)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText("$statusDriver | SAT $usedSatellite/$visibleSatellite | SIG $hdop"))
                    .setContentText("$statusDriver | SAT $usedSatellite/$visibleSatellite | SIG $hdop")
        }
        mNotificationManager.notify(notificationForeground, builderForeground!!.build())

        if (taximeter != null)
            taximeter.setServerTime(serverTime)
    }

    @Subscribe
    fun onEvent(startTaximeterEvent: StartTaximeterEvent) {
        if (taximeterIsStarted) return

        CoroutineScope(Dispatchers.Main).launch {

            taximeterIsStarted = true
            taximeter.start(startTaximeterEvent.order, applicationContext, startTaximeterEvent.stage, startTaximeterEvent.statusOrder)
            withContext(Dispatchers.IO) {
                currentOrder = currentOrderDao.getCurOrder(driver.id)
            }
            order = startTaximeterEvent.order

            stageOrder = 1
        }
    }

    @Subscribe
    fun onEvent(stopTaximeterEvent: StopTaximeterEvent) {
        taximeterIsStarted = false
        taximeter.stop(stopTaximeterEvent.statusOrder)
    }

    override fun onLocationChanged(location: Location?) {
        location?.also {
            workerLocation = WorkerLocation(it, serverTime)
        }
    }

    override fun onGpsStatusChanged(usedSatellite: Int, visibleSatellite: Int) {
        EventBus.getDefault().post(SatelliteInfoEvent(visibleSatellite, usedSatellite))

        this.usedSatellite = usedSatellite
        this.visibleSatellite = visibleSatellite

        if (statusDriver != null) {
            builderForeground!!.setStyle(NotificationCompat.BigTextStyle()
                    .bigText("$statusDriver | SAT $usedSatellite/$visibleSatellite | SIG $hdop"))
                    .setContentText("$statusDriver | SAT $usedSatellite/$visibleSatellite | SIG $hdop")
            mNotificationManager.notify(notificationForeground, builderForeground!!.build())
        }
    }

    override fun onNmeaReceived(hdop: Double) {
        this.hdop = hdop
    }

    override fun onNewMessage(userName: String, time: String, role: String, message: String, typeChat: String, read: Boolean) {
        sendNotification("$userName ($role)", message)
        EventBus.getDefault().post(ChatOnNewMessageEvent(userName, time, role, message, typeChat, read))
    }

    override fun onUnreadMessage(messages: List<HashMap<String, String>>) {
        sendBroadcast(Intent(WebSocket.BROADCAST_UNREAD_MESSAGE_COUNT).putExtra("count", messages.size))
        EventBus.getDefault().post(ChatOnUnreadedMessageEvent(messages))
    }

    override fun onLastMessages(typeChat: String, messages: List<HashMap<String, String>>) {
        EventBus.getDefault().post(ChatOnLastMessagesEvent(typeChat, messages))
    }

    override fun onNewOrder(orderId: String, callsign: String, tenant_login: String, secToClose: Int?, uuid: String, arrival: IntArray) {

        webSocket.emitUuid(uuid)

        if (appPreferences.getText("deny_fakegps") == "0" || isMockLocation(workerLocation!!.workerLocation) == 0) {

            val workDay = getLastWorkDay(driver)
            val intent = Intent(applicationContext, OrderOfferActivity::class.java)
            intent.putExtra("order_id", orderId)
            intent.putExtra("service_id", service.id)
            intent.putExtra("workday_id", workDay.id)
            intent.putExtra("driver_id", driver.id)
            intent.putExtra("free_order", 0)
            intent.putExtra("type_order", "free")
            intent.putExtra("time", Date().time)
            intent.putExtra("source", "main_service")
            intent.putExtra("server_time", 0)
            intent.putExtra("type_source", 1)
            intent.putExtra("delete", false)
            intent.putExtra("code_cancel", AppParams.ORDER_STATUS_CANCEL)
            intent.putExtra("music", true)
            intent.putExtra("arrival", arrival)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

            startActivity(intent)
        }
    }

    override fun onPreOrder(orderId: String, callsign: String, tenant_login: String, secToClose: Int?, uuid: String) {

        val workDay = getLastWorkDay(driver)
        val orderIntent = Intent(applicationContext, OrderOfferActivity::class.java)

        orderIntent.putExtra("order_id", orderId)
        orderIntent.putExtra("service_id", service.id)
        orderIntent.putExtra("workday_id", workDay.id)
        orderIntent.putExtra("free_order", 3)
        orderIntent.putExtra("driver_id", driver.id)
        orderIntent.putExtra("type_order", "reserve_br")
        orderIntent.putExtra("source", "main_service")
        orderIntent.putExtra("time", Date().time)
        orderIntent.putExtra("server_time", 0)
        orderIntent.putExtra("type_source", 3)
        orderIntent.putExtra("delete", false)
        orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_RESERVE_CANCEL)
        orderIntent.putExtra("music", false)
        orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(orderIntent)
    }

    override fun onUpdateOrder(orderId: String, callSign: String, tenant_login: String, uuid: String) {

        CoroutineScope(Dispatchers.Default).launch {
            webSocket.emitUuid(uuid)

            var currentOrder: CurrentOrder? = null
            withContext(Dispatchers.IO) {
                currentOrder = currentOrderDao.getCurOrder(driver.id)
            }

            if (currentOrder != null) {
                EventBus.getDefault().post(PushOrderEvent(orderId, callSign, tenant_login, "order_is_updated"))
                val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
                requestParams["order_id"] = orderId
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign

                WebService.getOrderForId(requestParams, driver.secretCode)
            }
        }
    }

    override fun onRejectOrder(orderId: String, callsign: String, tenant_login: String, uuid: String) {
        webSocket.emitUuid(uuid)
        val intentStatusOrder = Intent(AppParams.BROADCAST_PUSH_STATUSORDER)
        intentStatusOrder.putExtra("status", "order_is_rejected")
        intentStatusOrder.putExtra("order_id", orderId)
        sendBroadcast(intentStatusOrder)
        sendNotification(getString(R.string.text_order_rejected), getString(R.string.text_order_rejected), true, "order_is_rejected")
        EventBus.getDefault().post(PushOrderEvent(orderId, callsign, tenant_login, "order_is_rejected"))

        rejectOrder(orderId)
    }

    override fun onRejectDriverFromOrder(orderId: String, callsign: String, tenant_login: String, uuid: String) {

        webSocket.emitUuid(uuid)

        val intentStatusOrder = Intent(AppParams.BROADCAST_PUSH_STATUSORDER)
        intentStatusOrder.putExtra("status", "order_is_rejected")
        intentStatusOrder.putExtra("order_id", orderId)
        sendBroadcast(intentStatusOrder)
        sendNotification(getString(R.string.text_reject_worker_from_order), getString(R.string.text_reject_worker_from_order), true, "order_is_rejected")
        EventBus.getDefault().post(PushOrderEvent(orderId, callsign, tenant_login, "reject_worker_from_order"))

        rejectOrder(orderId)
    }

    override fun onCompleteOrder(orderId: String, callsign: String, tenant_login: String, uuid: String) {
        webSocket.emitUuid(uuid)
        val intentStatusOrder = Intent(AppParams.BROADCAST_PUSH_STATUSORDER)
        intentStatusOrder.putExtra("status", "order_is_rejected")
        intentStatusOrder.putExtra("order_id", orderId)
        sendBroadcast(intentStatusOrder)
        sendNotification(getString(R.string.text_complete_worker_from_order), getString(R.string.text_complete_worker_from_order), true, "complete_order")
        EventBus.getDefault().post(PushOrderEvent(orderId, callsign, tenant_login, "complete_order"))

        rejectOrder(orderId)
    }

    override fun onResponse(uuid: String, infoCode: String, result: Int, jsonObject: JSONObject) {
        webSocket.emitUuid(uuid)

        val completed = mutableListOf<OrderStatusEvent>()
        requests.forEach { event ->
            if (event.status.uuid == uuid) {
                if (infoCode == "OK") {
                    EventBus.getDefault().post(NetworkOrderStatusEvent(infoCode, event.requestType, result, 0))
                } else {
                    EventBus.getDefault().post(NetworkOrderStatusEvent(infoCode, event.requestType))
                }
                completed.add(event)
            }
        }
        requests.removeAll(completed)

        EventBus.getDefault().post(PushResponseEvent(uuid, infoCode, result, jsonObject))
    }

    override fun onPhotocontrolEvent(uuid: String) {
        webSocket.emitUuid(uuid)
    }

    override fun onOrderListAddItem(type: String, order_id: String, uuid: String) {
        webSocket.emitUuid(uuid)

        if (stageOrder > 0 || statusDriverEng != "FREE")
            return

        if (appPreferences.getText("deny_fakegps") == "0" || isMockLocation(workerLocation!!.workerLocation) == 0) {

            val workDay = getLastWorkDay(driver)
            val orderIntent = Intent(applicationContext, OrderOfferActivity::class.java)
            orderIntent.putExtra("order_id", order_id)
            orderIntent.putExtra("service_id", service.id)
            orderIntent.putExtra("workday_id", workDay.id)
            orderIntent.putExtra("driver_id", driver.id)
            orderIntent.putExtra("free_order", 1)
            orderIntent.putExtra("type_order", type)
            orderIntent.putExtra("time", Date().time)
            orderIntent.putExtra("source", "main_service")
            orderIntent.putExtra("server_time", serverTime)
            orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            orderIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            orderIntent.putExtra("type_source", 0)
            orderIntent.putExtra("delete", false)
            orderIntent.putExtra("code_cancel", AppParams.ORDER_STATUS_ASSIGNED_CANCEL)
            orderIntent.putExtra("music", true)

            startActivity(orderIntent)
        }
    }

    private fun getLastWorkDay(driver: Driver): WorkDay =
            runBlocking {
                withContext(Dispatchers.IO) {
                    AppDatabase.getInstance(applicationContext).workDayDao().getLastWorkDay(driver.id)
                }
            }

    override fun onOrderListDelItem(type: String, order_id: String, uuid: String) {
        webSocket.emitUuid(uuid)
        EventBus.getDefault().post(UpdateOwnOrdersEvent(type, "delete", order_id))
    }

    override fun onConfirmPreOrder(orderId: String, callsign: String, tenant_login: String, secConfirm: Int?, uuid: String) {
        webSocket.emitUuid(uuid)
        val orderIntent = Intent(applicationContext, RemindPreOrderActivity::class.java)
        orderIntent.putExtra("order_id", orderId)
        orderIntent.putExtra("service_id", service.id)
        orderIntent.putExtra("driver_id", driver.id)
        orderIntent.putExtra("sec_confirm", secConfirm)

        orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        orderIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(orderIntent)
    }

    @Subscribe
    fun onEvent(chatEmitGetHistoryMessages: ChatEmitGetHistoryMessages) {
        webSocket.emitGetHistoryMessages(chatEmitGetHistoryMessages.typeChat, chatEmitGetHistoryMessages.timestamp)
    }

    @Subscribe
    fun onEvent(chatEmitGetLastMessages: ChatEmitGetLastMessages) {
        webSocket.emitGetLastMessages(chatEmitGetLastMessages.typeChat)
    }

    @Subscribe
    fun onEvent(chatEmitGetUnreadMessage: ChatEmitGetUnreadedMessage) {
        webSocket.emitGetUnreadMessage()
    }

    @Subscribe
    fun onEvent(chatEmitMessageIsRead: ChatEmitMessageIsRead) {
        webSocket.emitMessageIsRead()
    }

    @Subscribe
    fun onEvent(chatEmitNewMessage: ChatEmitNewMessage) {
        webSocket.emitNewMessage(chatEmitNewMessage.typeChat, chatEmitNewMessage.message)
    }

    @Subscribe
    fun onEvent(deleteAllNotificationEvent: DeleteAllNotificationEvent) {
        if (mNotificationManager != null) {
            mNotificationManager.cancelAll()
        }
    }

    @Subscribe
    fun onEvent(setStageOrderEvent: SetStageOrderEvent) {
        stageOrder = setStageOrderEvent.stageOrder
    }

    //Chat feature
    private fun sendNotification(title: String, msg: String) {
        val mBuilder: NotificationCompat.Builder
        val att: AudioAttributes
        val intent = Intent(applicationContext, ChatFragment::class.java)
        val pendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID_OFFER, intent, 0)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(channelChat, notificationNameChat, NotificationManager.IMPORTANCE_HIGH)
            att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mBuilder = NotificationCompat.Builder(applicationContext, channelChat)
                    .setSmallIcon(R.drawable.push)
                    .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
                    .setSound(null)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setAutoCancel(false)

            if (soundDao.get("ringtone_mirimba_chord").isDefault) {
                notificationChannel.setSound(Uri.parse(res + R.raw.marimba_chord), att)
            } else {
                notificationChannel.setSound(Uri.parse(soundDao.get("ringtone_mirimba_chord").path), att)
            }
            notificationManager.createNotificationChannel(notificationChannel)
            mBuilder.setChannelId(channelChat)

        } else {
            mBuilder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
            try {
                if (soundDao.get("ringtone_mirimba_chord").isDefault) {
                    mBuilder.setSound(Uri.parse(res + R.raw.marimba_chord))
                } else {
                    mBuilder.setSound(Uri.parse(soundDao.get("ringtone_mirimba_chord").path))
                }
            } catch (e: Exception) {
                mBuilder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "marimba_chord", Locale.getDefault().language)))
                e.printStackTrace()
            }
        }

        mNotificationManager.notify(NOTIFICATION_ID_CHAT, mBuilder.build())
    }

    private fun sendNotification(title: String, msg: String, sound: Boolean, command: String) {
        val mBuilder: NotificationCompat.Builder
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val att: AudioAttributes

        val workshiftIntent = Intent(this, WorkShiftActivity::class.java)
        workshiftIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val shiftPendingIntent = PendingIntent.getActivity(this, 0, workshiftIntent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(channelOrder, notificationNameOrders, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)

            att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
            mBuilder = NotificationCompat.Builder(applicationContext, channelOrder)
                    .setSmallIcon(R.drawable.push)
                    .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
                    .setSound(null)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(shiftPendingIntent)
                    .setContentText(msg)
                    .setAutoCancel(false)

            if (sound) {
                when (command) {

                    "order_is_rejected" -> {
                        if (soundDao.get("ringtone_rejected").isDefault) {
                            notificationChannel.setSound(Uri.parse(res +
                                    Utils.getResourceId(applicationContext, "order_is_rejected", Locale.getDefault().language)), att)
                        } else {
                            notificationChannel.setSound(Uri.parse(soundDao.get("ringtone_rejected").path), att)
                        }
                        mBuilder.setContentIntent(getPendingIntent())
                    }

                    "update_free_orders" -> {
                        if (soundDao.get("ringtone_free_order").isDefault) {
                            notificationChannel.setSound(Uri.parse(res
                                    + Utils.getResourceId(applicationContext, "free_order", Locale.getDefault().language)), att)
                        } else {
                            notificationChannel.setSound(Uri.parse(soundDao.get("ringtone_free_order").path), att)
                        }
                    }

                    "update_pre_orders" -> {
                        if (soundDao.get("ringtone_preorder").isDefault) {
                            notificationChannel.setSound(Uri.parse(res
                                    + Utils.getResourceId(applicationContext, "pre_order", Locale.getDefault().language)), att)
                        } else {
                            notificationChannel.setSound(Uri.parse(soundDao.get("ringtone_preorder").path), att)
                        }
                    }

                    else -> {
                        if (soundDao.get("ringtone_mirimba_chord").isDefault) {
                            notificationChannel.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "marimba_chord", Locale.getDefault().language)), att)
                        } else {
                            notificationChannel.setSound(Uri.parse(soundDao.get("ringtone_mirimba_chord").path), att)
                        }
                    }
                }
                notificationManager.createNotificationChannel(notificationChannel)
                mBuilder.setChannelId(channelOrder)
            }
        } else {
            mBuilder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(title)
                    .setContentIntent(shiftPendingIntent)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
            if (sound) {
                when (command) {

                    "order_is_rejected" -> {
                        if (soundDao.get("ringtone_rejected").isDefault) {
                            mBuilder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "order_is_rejected", Locale.getDefault().language)))
                        } else {
                            mBuilder.setSound(Uri.parse(soundDao.get("ringtone_rejected").path))
                        }

                        mBuilder.setContentIntent(getPendingIntent())
                    }

                    "update_free_orders" -> {
                        if (soundDao.get("ringtone_free_order").isDefault) {
                            mBuilder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "free_order", Locale.getDefault().language)))
                        } else {
                            mBuilder.setSound(Uri.parse(soundDao.get("ringtone_free_order").path))
                        }
                    }

                    "update_pre_orders" -> {
                        if (soundDao.get("ringtone_preorder").isDefault) {
                            mBuilder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "pre_order", Locale.getDefault().language)))
                        } else {
                            mBuilder.setSound(Uri.parse(soundDao.get("ringtone_preorder").path))
                        }
                    }

                    else -> {
                        if (soundDao.get("ringtone_mirimba_chord").isDefault) {
                            mBuilder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "marimba_chord", Locale.getDefault().language)))
                        } else {
                            mBuilder.setSound(Uri.parse(soundDao.get("ringtone_mirimba_chord").path))
                        }
                    }
                }
            }
        }
        mBuilder.setAutoCancel(true)
        mNotificationManager.notify(NOTIFICATION_ID_MAIN, mBuilder.build())
    }

    private fun getPendingIntent(): PendingIntent {
        var service: com.kabbi.driver.database.entities.Service? = null
        var workDay = WorkDay()
        runBlocking {
            val db = AppDatabase.getInstance(applicationContext)
            withContext(Dispatchers.IO) {
                service = db.serviceDao().getByDriver(driver.id)
                workDay = db.workDayDao().getByServiceId(service!!.id.toString())
            }
        }
        var intent: Intent? = null
        if (workDay == null || workDay.endWorkDay > 0) {
            intent = Intent(applicationContext, WorkActivity::class.java)
            intent.putExtra("service_id", service!!.id)
            intent.putExtra("end_workshift", false)
        } else if (workDay.endWorkDay == 0L) {
            intent = Intent(applicationContext, WorkShiftActivity::class.java)
            intent.putExtra("showDialogGps", false)
            intent.putExtra("service_id", service!!.id)
            intent.putExtra("confirm", false)
            intent.putExtra("stageOrder", 0)
            intent.putExtra("showDialog", "empty")
            intent.putExtra("workday_id", workDay.id)
        }

        return PendingIntent.getActivity(this, PENDING_INTENT_REJECTED, intent, 0)
    }

    private fun rejectOrder(orderId: String) {
        var reserveOrders: List<Order> = listOf()
        runBlocking {
            withContext(Dispatchers.IO) {
                reserveOrders = orderDao.getReserveOrder(driver.id, AppParams.ORDER_LABEL_RESERVE)
            }
        }
        for (order in reserveOrders) {
            if (order.orderId == orderId)
                orderDao.delete(order)
        }

        if (order != null && order!!.orderId == orderId) {
            EventBus.getDefault().post(StopTaximeterEvent(""))

            runBlocking {
                withContext(Dispatchers.IO) {
                    currentOrderDao.deleteAll(driver.id)
                    orderDao.delete(order!!)
                }
            }
            order = null
            currentOrder = null
            stageOrder = 0
        }
    }

    private fun isMockLocation(location: Location): Int {
        if (Build.VERSION.SDK_INT >= 18) {
            if (location.isFromMockProvider) return 2
        } else {
            @Suppress("DEPRECATION")
            if (Settings.Secure.getString(contentResolver, Settings.Secure.ALLOW_MOCK_LOCATION) == "1")
                return 1
        }
        return 0
    }

    private fun setLocale(context: Context) {
        val conf = context.resources.configuration
        val appPreferences = AppPreferences(context)
        val favLang = if (appPreferences.getText("favorite_lang").isNotEmpty()) {
            appPreferences.getText("favorite_lang")
        } else {
            "0"
        }
        if (AppParams.LANG[Integer.valueOf(favLang)].length > 2) {
            val arrLang = AppParams.LANG[Integer.valueOf(favLang)].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            conf.locale = Locale(arrLang[0], arrLang[1])

        } else {
            conf.locale = Locale(AppParams.LANG[Integer.valueOf(favLang)])
        }
        val metrics = DisplayMetrics()
        val resources = Resources(context.assets, metrics, conf)
        resources.updateConfiguration(conf, metrics)
    }

    private inner class WorkerLocation internal constructor(internal val workerLocation: Location, internal val serverTime: Long)

    companion object {

        const val NOTIFICATION_ID_CHAT = 6
        const val NOTIFICATION_ID_MAIN = 7
        const val NOTIFICATION_ID_OFFER = 8
        const val PENDING_INTENT_REJECTED = 91
    }
}