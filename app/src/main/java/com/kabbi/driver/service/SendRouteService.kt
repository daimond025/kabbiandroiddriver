package com.kabbi.driver.service


import android.app.IntentService
import android.content.Intent
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Route
import com.kabbi.driver.util.AsyncHttpTask
import com.loopj.android.http.RequestParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLEncoder

class SendRouteService : IntentService("SendRouteService"), AsyncHttpTask.AsyncTaskInterface {

    private var routes: List<Route>? = null
    private lateinit var db: AppDatabase

    override fun onCreate() {
        super.onCreate()
        db = AppDatabase.getInstance(this)
        //EventBus.getDefault().register(this);
    }

    override fun onDestroy() {
        //EventBus.getDefault().unregister(this);
        super.onDestroy()
    }

    override fun onHandleIntent(intent: Intent?) {

        val requestParams = RequestParams()

        try {
            var order = Order()
            var driver = Driver()
            runBlocking {
                withContext(Dispatchers.IO) {
                    order = db.orderDao().getByOrderId(intent!!.extras!!.getString("order_id")!!)
                    driver = db.driverDao().getById(order.driver!!)
                    routes = db.routeDao().getRoutes(order.id)
                }
            }

            val jsonOrderRoute = JSONObject()
            val jsonArrayRoutes = JSONArray()

            if (routes!!.size <= 15000) {

                for (route in routes!!) {
                    try {
                        val jsonRoute = JSONObject()
                        jsonRoute.put("lat", route.lat)
                        jsonRoute.put("lon", route.lon)
                        jsonRoute.put("parking", route.parking)
                        jsonRoute.put("speed", route.speed)
                        jsonRoute.put("status_id", route.corn)
                        jsonRoute.put("time", route.datetime.toString())
                        jsonRoute.put("accuracy", route.hdop)
                        jsonArrayRoutes.put(jsonRoute)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                jsonOrderRoute.put("order_route", jsonArrayRoutes)

                requestParams.put("order_id", order.orderId)

                try {
                    requestParams.put("order_route", URLEncoder.encode(jsonOrderRoute.toString(), "UTF-8").replace("+", "%20"))
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }

                requestParams.put("tenant_login", intent?.extras!!.getString("uri"))
                requestParams.put("worker_login", driver.callSign)

                AsyncHttpTask(this, applicationContext).postRoute("set_order_route", requestParams, driver.secretCode)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        if (typeJson == "set_order_route" && jsonObject != null) {
            for (route in routes!!) {
                try {
                    runBlocking {
                        withContext(Dispatchers.IO) {
                            db.routeDao().delete(route)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }
}
