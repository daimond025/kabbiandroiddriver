package com.kabbi.driver.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.kabbi.driver.*
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.SoundDao
import com.kabbi.driver.database.entities.DarkWingDuck
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.util.Utils
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val NOTIFICATION_ID_NOTIFY = 2
    private val NOTIFICATION_ID_BULK = 3
    private val NOTIFICATION_ID_UPDATE = 4
    private val NOTIFICATION_ID_SOS = 5
    private var notificationManager: NotificationManager? = null
    private var intentWorkDay: Intent? = null
    private var intentWorkDayOrdersFree: Intent? = null
    private var intentWorkDayOrdersPree: Intent? = null
    private var intentWorkDayHelp: Intent? = null
    private var intentWorkDayOrder: Intent? = null
    private var intentStatusOrder: Intent? = null
    internal lateinit var appPreferences: AppPreferences
    internal var resources: Resources? = null
    internal lateinit var service: Service
    internal var driver: Driver? = null
    private lateinit var res: String
    private lateinit var soundDao: SoundDao

    override fun onCreate() {
        super.onCreate()
        soundDao = AppDatabase.getInstance(applicationContext).soundDao()
        res = "android.resource://$packageName/"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationChannelId = "Miscellaneous"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(notificationChannelId, "Your Notifications", NotificationManager.IMPORTANCE_HIGH)

            notificationChannel.description = "Description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = longArrayOf(0, 1000, 500, 1000)
            notificationChannel.enableVibration(true)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        // to diaplay notification in DND Mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = notificationManager.getNotificationChannel(notificationChannelId)
            channel.canBypassDnd()
        }

        val params = remoteMessage.data
        val json = JSONObject(params as Map<*, *>)
        val command = json.getString("command")
        val context = applicationContext
        appPreferences = AppPreferences(context)

        val conf = getResources().configuration
        val favLang = if( appPreferences.getText("favorite_lang").isNotEmpty()){ appPreferences.getText("favorite_lang")} else {"0"}

        if (AppParams.LANG[Integer.valueOf(favLang)].length > 2) {
            val arrLang = AppParams.LANG[Integer.valueOf(favLang)].split("_")
            conf.locale = Locale(arrLang[0], arrLang[1])

        } else {
            conf.locale = Locale(AppParams.LANG[Integer.valueOf(favLang)])
        }
        val metrics = DisplayMetrics()
        resources = Resources(assets, metrics, conf)

        if (command != null) {
            appPreferences = AppPreferences(applicationContext)
            intentWorkDay = Intent(AppParams.BROADCAST_PUSH)
            intentWorkDayOrdersFree = Intent(AppParams.BROADCAST_PUSH_FREE)
            intentWorkDayOrdersPree = Intent(AppParams.BROADCAST_PUSH_PRE)
            intentWorkDayHelp = Intent(AppParams.BROADCAST_PUSH_SOS)
            intentWorkDayOrder = Intent(AppParams.BROADCAST_PUSH_ORDER)
            intentStatusOrder = Intent(AppParams.BROADCAST_PUSH_STATUSORDER)
            val jsonObjectHelp: JSONObject?

            when (command) {
                "update_free_orders" -> {
                    sendBroadcast(intentWorkDayOrdersFree)
                    if (json.has("sound") && json.getString("sound") == "new_free.aiff") {
                        if (appPreferences.getText("in_shift") == "1" || appPreferences.getText("push_show_freeorder") == "1") {
                            sendNotificationUpdate(command, json.getString("message"), appPreferences.getText("push_sound_freeorder") == "1")
                        }
                    }
                }

                "update_pre_orders" -> {
                    sendBroadcast(intentWorkDayOrdersPree)
                    if (json.has("sound") && json.getString("sound") == "new_free.aiff" && (appPreferences.getText("in_shift") == "1" || appPreferences.getText("push_show_preorder") == "1")) {
                        sendNotificationUpdate(command, json.getString("message"), appPreferences.getText("push_show_preorder") == "1")
                    }
                }

                "new_balance" -> {
                    val jsonObjectBalance = JSONObject(json.getString("command_params"))

                    CoroutineScope(Dispatchers.IO).launch {
                        val db = AppDatabase.getInstance(applicationContext)
                        service = db.serviceDao().getById((appPreferences.getText("service_id").toLong()))
                        service.balance = jsonObjectBalance.getString("balance")
                        service.id = db.serviceDao().save(service)
                        if (appPreferences.getText("push_balance") == "1")
                            sendNotification(json.getString("tickerText"), json.getString("message"), true, command)
                        intentWorkDay?.putExtra("PUSH", jsonObjectBalance.getString("balance"))
                        sendBroadcast(intentWorkDay)
                    }
                }
                "sos" -> {
                    jsonObjectHelp = JSONObject(json.getString("command_params"))
                    CoroutineScope(Dispatchers.Default).launch {
                        val db = AppDatabase.getInstance(applicationContext)

                        withContext(Dispatchers.IO) {
                            driver = db.driverDao().getById(appPreferences.getText("driver_id").toLong())
                        }

                        driver?.also { driver ->
                            val dao = db.darkWingDuckDao()
                            var duck = dao.get(jsonObjectHelp.getString("worker_callsign"))
                            if (duck == null)
                                duck = DarkWingDuck()
                            duck.driverCallSign = jsonObjectHelp.getString("worker_callsign")
                            duck.address = jsonObjectHelp.getString("address")
                            duck.carData = jsonObjectHelp.getString("car_data")
                            duck.lat = (jsonObjectHelp.getString("lat")).toDouble()
                            duck.lon = (jsonObjectHelp.getString("lon")).toDouble()
                            duck.timerHelp = Calendar.getInstance().time.time
                            duck.driver = driver.id
                            dao.save(duck)

                            if (appPreferences.getText("push_sos") == "1") {
                                sendNotificationSos(json.getString("tickerText"), json.getString("message"), true, jsonObjectHelp)
                            }
                            sendBroadcast(intentWorkDayHelp)
                        }
                    }
                }

                "unsos" -> {
                    jsonObjectHelp = JSONObject(json.getString("command_params"))
                    CoroutineScope(Dispatchers.Default).launch {
                        withContext(Dispatchers.IO) {
                            AppDatabase.getInstance(applicationContext).darkWingDuckDao().delete(jsonObjectHelp.getString("worker_callsign"))
                        }
                        if (appPreferences.getText("push_sos") == "1") {
                            sendNotification(json.getString("tickerText"), json.getString("message"), true, command)
                        }
                        sendBroadcast(intentWorkDayHelp)
                    }
                }

                "remind_pre_order" ->
                    if (appPreferences.getText("push_preorder") == "1")
                        sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "bulk_notify" -> sendNotificationBulk(json.getString("tickerText"), json.getString("message"))

                "new_message" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "remove_from_reserved_pre_order" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "remove_from_reserved_order" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "assign_at_pre_order" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "assign_at_order" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)

                "order_is_updated" ->
                    sendNotification(json.getString("tickerText"), json.getString("message"), true, command)
            }
        }
    }

    private fun sendNotification(title: String, msg: String, sound: Boolean, command: String) {
        var msg = msg
        val builder: NotificationCompat.Builder
        val id: String
        val name: String
        val intent: Intent
        var pendingIntent: PendingIntent? = null

        when (command) {
            "order_is_rejected" -> {
                id = AppParams.CHANNEL_ID_ORDER_REJECTED
                name = AppParams.NOTIFICATION_NAME_ORDER_REJECTED
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "order_is_rejected")
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            "update_free_orders" -> {
                id = AppParams.CHANNEL_ID_UPDATE_FREE_ORDERS
                name = AppParams.NOTIFICATION_NAME_UPDATE_FREE
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "update_free_orders")
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            "order_is_updated" -> {
                id = AppParams.CHANNEL_ID_ORDER_UPDATED
                name = AppParams.NOTIFICATION_NAME_ORDER_UPDATED
            }
            "remove_from_reserved_pre_order" -> {
                id = AppParams.CHANNEL_ID_REMOVED_FROM_R_PRE_ORDER
                name = AppParams.NOTIFICATION_NAME_REMOVED_FROM_R_PRE_ORDER
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "remove_from_reserved_pre_order")
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            "new_balance" -> {
                id = AppParams.CHANNEL_ID_BALANCE
                name = AppParams.NOTIFICATION_NAME_BALANCE
                intent = Intent(this, CardActivity::class.java)
                CoroutineScope(Dispatchers.Default).launch {
                    service = AppDatabase.getInstance(applicationContext).serviceDao().getById(java.lang.Long.valueOf(appPreferences.getText("service_id")))
                    intent.putExtra("service_id", service.id)
                    pendingIntent = PendingIntent.getActivity(this@MyFirebaseMessagingService, 0, intent, 0)
                }
            }
            "unsos" -> {
                id = AppParams.CHANNEL_ID_UNSOS
                name = AppParams.NOTIFICATION_NAME_UNSOS
            }
            "remind_pre_order" -> {
                id = AppParams.CHANNEL_ID_REMIND_PRE_ORDER
                name = AppParams.NOTIFICATION_NAME_REMIND_PRE_ORDER
            }
            "update_pre_orders" -> {
                id = AppParams.CHANNEL_ID_UPDATE_PRE_ORDERS
                name = AppParams.NOTIFICATION_UPDATE_PRE_ORDERS
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "update_pre_orders")
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            "assign_at_pre_order" -> {
                id = AppParams.CHANNEL_ID_ASSIGN_PRE_ORDER
                name = AppParams.NOTIFICATION_ASSIGN_PRE_ORDER
            }
            "assign_at_order" -> {
                id = AppParams.CHANNEL_ID_ASSIGN_ORDER
                name = AppParams.NOTIFICATION_NAME_ASSIGN_ORDER
            }
            else -> {
                id = "_"
                name = "_"
            }
        }
        if (title == msg)
            msg = ""
        if (notificationManager == null) {
            notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = NotificationCompat.Builder(this, name)
            builder.setContentTitle(title)
                    .setSmallIcon(R.drawable.push)
                    .setContentText(msg)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setTicker(title)
        } else {
            builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(title)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
        }

        when (command) {

            "order_is_rejected" -> if (soundDao.get("ringtone_rejected").isDefault) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, res + Utils.getResourceId(applicationContext, "order_is_rejected", Locale.getDefault().language)))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(res
                            + Utils.getResourceId(applicationContext, "rejected", Locale.getDefault().language)))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id,
                            name, soundDao.get("ringtone_rejected").path))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(soundDao.get("ringtone_rejected").path))
                }
            }

            "update_free_orders" -> if (sound) {
                if (soundDao.get("ringtone_free_order").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, res + Utils.getResourceId(applicationContext,
                                "free_order", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res
                                + Utils.getResourceId(applicationContext, "free_order", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_free_order").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_free_order").path))
                    }
                }
            }

            "order_is_updated" -> {
                //do nothing
            }

            "new_balance", "unsos" -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationManager!!.createNotificationChannel(channel(id, name, res + R.raw.marimba_chord))
                builder.setChannelId(id)
            } else {
                builder.setSound(Uri.parse(res + R.raw.marimba_chord))
            }

            "remove_from_reserved_pre_order" ->
                //Удалён с забронированного заказа
                if (soundDao.get("ringtone_rejected").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id, name, res + Utils.getResourceId(applicationContext, "rejected", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res
                                + Utils.getResourceId(applicationContext, "rejected", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_rejected").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_rejected").path))
                    }
                }

            "remind_pre_order" ->
                //Напоминание о предзаказе
                if (soundDao.get("ringtone_remind_preorder").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, res + Utils.getResourceId(applicationContext,
                                "remind_preorder", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res
                                + Utils.getResourceId(applicationContext, "remind_preorder", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_remind_preorder").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_remind_preorder").path))
                    }
                }

            "update_pre_orders" ->
                //Новый предзаказ
                if (soundDao.get("ringtone_preorder").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, res + Utils.getResourceId(applicationContext,
                                "pre_order", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res
                                + Utils.getResourceId(applicationContext, "pre_order", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_preorder").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_preorder").path))
                    }
                }

            "assign_at_pre_order" -> if (soundDao.get("ringtone_own").isDefault) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, res + Utils.getResourceId(applicationContext, "own", Locale.getDefault().language)))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "own", Locale.getDefault().language)))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, soundDao.get("ringtone_own").path))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(soundDao.get("ringtone_own").path))
                }
            }

            "assign_at_order" -> if (soundDao.get("ringtone_own").isDefault) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, res + Utils.getResourceId(applicationContext, "own", Locale.getDefault().language)))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "own", Locale.getDefault().language)))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, soundDao.get("ringtone_own").path))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(soundDao.get("ringtone_own").path))
                }
            }
            else -> if (soundDao.get("ringtone_mirimba_chord").isDefault) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, res + R.raw.marimba_chord))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(res + R.raw.marimba_chord))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(id, name, soundDao.get("ringtone_mirimba_chord").path))
                    builder.setChannelId(id)
                } else {
                    builder.setSound(Uri.parse(soundDao.get("ringtone_mirimba_chord").path))
                }
            }
        }
        notificationManager!!.notify(NOTIFICATION_ID_NOTIFY, builder.build())
    }


    private fun sendNotificationBulk(title: String, msg: String) {
        val intent: Intent
        val pendingIntent: PendingIntent
        val builder: NotificationCompat.Builder
        if (notificationManager == null) {
            notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = NotificationCompat.Builder(this, AppParams.CHANNEL_ID_BULK)
            intent = Intent(this, SplashActivity::class.java)  //test
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
            builder.setContentTitle(title)
                    .setSmallIcon(R.drawable.push)
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
        } else {
            builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(title)
                    .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
            intent = Intent(this, SplashActivity::class.java)
            val contentIntent = PendingIntent.getActivity(this, 0, intent, 0)
            builder.setContentIntent(contentIntent)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager!!.createNotificationChannel(channel(AppParams.CHANNEL_ID_BULK,
                    AppParams.NOTIFICATION_NAME_BULK, res + R.raw.marimba_chord))
            builder.setChannelId(AppParams.CHANNEL_ID_BULK)
        } else {
            builder.setSound(Uri.parse(res + R.raw.marimba_chord))
        }
        Log.d(javaClass.simpleName, "BULK_NOTIFICATION")
        notificationManager!!.notify(NOTIFICATION_ID_BULK, builder.build())
    }

    private fun sendNotificationUpdate(command: String, message: String, sound: Boolean) {
        var intent: Intent
        var contentIntent: PendingIntent? = null
        val builder: NotificationCompat.Builder
        val id: String
        val name: String
        when (command) {
            "update_free_orders" -> {
                id = AppParams.CHANNEL_ID_UPDATE_FREE
                name = AppParams.NOTIFICATION_NAME_UPDATE_FREE
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "update_free_orders")
                intent.putExtra("type", "free")
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            "update_pre_orders" -> {
                id = AppParams.CHANNEL_ID_UPDATE_PRE
                name = AppParams.NOTIFICATION_NAME_UPDATE_PRE
                intent = Intent(this, WorkShiftActivity::class.java)
                intent.putExtra("intent_type", "update_pre_orders")
                intent.putExtra("type", "pre")
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            else -> {
                id = ""
                name = ""
            }
        }
        val db = AppDatabase.getInstance(applicationContext)
        if (driver == null)
            runBlocking {
                    driver = db.driverDao().getById((appPreferences.getText("driver_id").toLong()))
            }

        var workDay: WorkDay? = null
        var service: Service? = null

        runBlocking {
            withContext(Dispatchers.IO){
                workDay = db.workDayDao().getLastWorkDay(driver!!.id)
                workDay?.also{
                    service = db.serviceDao().getById(it.service)
                }
            }
        }

        if (workDay != null) {
            intent = Intent(this, WorkShiftActivity::class.java)
            intent.putExtra("showDialogGps", true)
            intent.putExtra("showDialog", "push")
            intent.putExtra("service_id", service!!.id) //long
            intent.putExtra("workday_id", workDay!!.id) //long
            intent.putExtra("stageOrder", 0) //int
            intent.putExtra("push", command)

            contentIntent = PendingIntent.getActivity(this, 0, intent, 0)
        }

        if (notificationManager == null) {
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = NotificationCompat.Builder(this, name)
            builder.setContentTitle(resources!!.getString(R.string.update_order_title))
                    .setSmallIcon(R.drawable.push)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
        } else {
            builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(resources!!.getString(R.string.update_order_title))
                    .setAutoCancel(true)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(message)).setContentText(message)
                    .setContentIntent(contentIntent)
        }

        when (command) {
            "update_free_orders" -> if (sound) {
                if (soundDao.get("ringtone_free_order").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, res + Utils.getResourceId(applicationContext, "free_order", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "free_order", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_free_order").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_free_order").path))
                    }
                }
            }

            "update_pre_orders" -> if (sound) {
                if (soundDao.get("ringtone_preorder").isDefault) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, res + Utils.getResourceId(applicationContext, "pre_order", Locale.getDefault().language)))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(res + Utils.getResourceId(applicationContext, "pre_order", Locale.getDefault().language)))
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        notificationManager!!.createNotificationChannel(channel(id,
                                name, soundDao.get("ringtone_preorder").path))
                        builder.setChannelId(id)
                    } else {
                        builder.setSound(Uri.parse(soundDao.get("ringtone_preorder").path))
                    }
                }
            }

            else -> builder.setSound(Uri.parse(res + R.raw.pop))
        }

        notificationManager!!.notify(NOTIFICATION_ID_UPDATE, builder.build())
    }

    private fun sendNotificationSos(title: String, msg: String, sound: Boolean, params: JSONObject) {
        val intent = Intent(this, HelpMapActivity::class.java)
        var pendingIntent: PendingIntent? = null
        val builder: NotificationCompat.Builder
        if (notificationManager == null) {
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        try {
            intent.putExtra("lat", params.getString("lat"))
            intent.putExtra("lon", params.getString("lon"))
            intent.putExtra("car_data", params.getString("car_data"))
            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = NotificationCompat.Builder(this, AppParams.CHANNEL_ID_SOS)
            builder.setContentTitle(title)
                    .setSmallIcon(R.drawable.push)
                    .setContentText(msg)
                    .setAutoCancel(false)
                    .setContentIntent(pendingIntent)
        } else {
            builder = NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.push)
                    .setContentTitle(title)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(msg))
                    .setContentText(msg)
            builder.setContentIntent(pendingIntent)
        }
        if (sound) {
            if (soundDao.get("ringtone_sos").isDefault) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(AppParams.CHANNEL_ID_SOS,
                            AppParams.NOTIFICATION_NAME_SOS, res +
                            Utils.getResourceId(applicationContext, "sos", Locale.getDefault().language)))
                    builder.setChannelId(AppParams.CHANNEL_ID_SOS)
                } else {
                    builder.setSound(Uri.parse(res
                            + Utils.getResourceId(applicationContext, "sos", Locale.getDefault().language)))
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager!!.createNotificationChannel(channel(AppParams.CHANNEL_ID_SOS,
                            AppParams.NOTIFICATION_NAME_SOS, soundDao.get("ringtone_sos").path))
                    builder.setChannelId(AppParams.CHANNEL_ID_SOS)
                } else {
                    builder.setSound(Uri.parse(soundDao.get("ringtone_sos").path))
                }
            }
        }
        notificationManager!!.notify(NOTIFICATION_ID_SOS, builder.build())
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    fun channel(id: String, name: String, sound: String): NotificationChannel {
        val notificationChannel = NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH)
        val att = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()
        notificationChannel.setSound(Uri.parse(sound), att)
        return notificationChannel
    }
}