package com.kabbi.driver.offer


import android.app.Activity
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.telephony.TelephonyManager
import android.text.Html
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.snackbar.Snackbar
import com.google.maps.model.LatLng
import com.kabbi.driver.App
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.events.*
import com.kabbi.driver.helper.*
import com.kabbi.driver.helper.AppParams.*
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.Utils
import com.kabbi.driver.util.buildAddress
import kotlinx.android.synthetic.main.activity_order_offer.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class OrderOfferActivity : BaseActivity() {

    internal lateinit var driver: Driver
    internal lateinit var service: Service
    internal lateinit var appPreferences: AppPreferences
    internal lateinit var workDay: WorkDay

    private lateinit var requestType: RequestType
    private lateinit var progressBar: LinearLayout
    private lateinit var buttons: LinearLayout
    private lateinit var timeButtons: LinearLayout
    private lateinit var presenter: OfferPresenter
    private lateinit var mediaPlayer: MediaPlayer

    internal var beginOrder: Boolean = false

    //    internal var progressDialog: ProgressDialog? = null
    internal var bundle: Bundle? = null

    private var jsonObjectOrder: JSONObject? = null
    private var bOrder: Boolean = false
    private var timeToClient = "0"
    private var jsonArrayOptions: JSONArray? = null
    private var parkingStart: Parking? = null
    private var offerSec: Int = 0
    private var backOfferSec: Long = 0
    private var showSearchWorker = 0
    private var confirmationCode = 0
    private var dynamicCalculationTimeToClient = 0
    private var stTimeToClient: String? = null
    private var notificationManager: NotificationManager? = null
    private var typedValueSubscribe: TypedValue? = null
    private var timer: CountDownTimer? = null
    private var countDownTimer: CountDownTimer? = null
    private var serverTime: Long = 0
    private var onCreateOrderId: String = ""
    private var viewGroup: ViewGroup? = null
    private var source: String? = null
    private var orderType: String? = null
    private var freeOrder: Int = 0
    private var arrival: IntArray? = intArrayOf()

    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private val preOrderKey = "reserve_br"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBundle()
        presenter = OfferPresenter(this)
        presenter.init(intent.extras)

        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_order_offer)

        viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

        typedValueSubscribe = TypedValue()

        EventBus.getDefault().register(this)

        timeButtons = findViewById(R.id.linear_layout_order_offer_btn_container)
        timeButtons.visibility = View.INVISIBLE

        buttons = findViewById(R.id.order_offer_button_container)

        progressBar = findViewById(R.id.progress_bar_container)
        val db = AppDatabase.getInstance(this)
        val soundDao = db.soundDao()

        try {
            runBlocking {
                withContext(Dispatchers.IO) {

                    mediaPlayer = if (soundDao.get("ringtone_cute_bells").isDefault) {
                        MediaPlayer.create(this@OrderOfferActivity, R.raw.cute_bells)
                    } else {
                        MediaPlayer.create(this@OrderOfferActivity, Uri.parse(soundDao.get("ringtone_cute_bells").path))
                    }
                }
            }
            mediaPlayer.isLooping = true
        } catch (e: Exception) {
            mediaPlayer = MediaPlayer.create(this, R.raw.cute_bells)
            mediaPlayer.isLooping = true
            e.printStackTrace()
        }

        initButtonTime()

        if (source == "main_service") {

            runBlocking {
                withContext(Dispatchers.IO) {
                    service = db.serviceDao().getById(bundle!!.getLong("service_id"))
                    driver = db.driverDao().getById(bundle!!.getLong("driver_id"))
                    workDay = db.workDayDao().getById(bundle!!.getLong("workday_id"))
                }
            }

            (application as App).service = service
            (application as App).driver = driver
            (application as App).workDay = workDay

        } else {
            service = (application as App).service
            driver = (application as App).driver
            workDay = (application as App).workDay
        }

        bOrder = false
        offerSec = TIME
        try {
            if (bundle!!.getInt("offer_sec") > 0) {
                offerSec = bundle!!.getInt("offer_sec") * 1000
            } else if (appPreferences.getText("offer_sec").isNotEmpty()) {
                offerSec = Integer.valueOf(appPreferences.getText("offer_sec")) * 1000
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager!!.cancel(WorkShiftActivity.INTENT_ORDER_OFFER)

        if (intent.getLongExtra("time", 0) + offerSec < Date().time) {
            CustomToast.showMessage(this, getString(R.string.toast_text_end_time))
            finish()
            logEvent("0", this.javaClass.name, LogEvent.FINISH)
        } else {
            beginOrder = false

            when (orderType) {

                "preorder", "free" -> if (freeOrder == 0 || freeOrder == 1) {
                    findViewById<View>(R.id.relativelayout_btn_rejected).visibility = View.GONE
                } else {
                    findViewById<View>(R.id.relativelayout_btn_cancel).visibility = View.GONE
                    findViewById<View>(R.id.relativelayout_timer).visibility = View.GONE
                }

                "reserve", "free_br" -> {
                    timeButtons.visibility = View.GONE
                    findViewById<View>(R.id.button_order_offer_hold).visibility = View.VISIBLE
                    findViewById<View>(R.id.relativelayout_btn_cancel).visibility = View.GONE
                    findViewById<View>(R.id.relativelayout_timer).visibility = View.GONE
                    findViewById<View>(R.id.linearlayout_orderoffer_phone).visibility = View.GONE
                }

                "reserve_br" -> {
                    findViewById<View>(R.id.relativelayout_timer).visibility = View.GONE
                    findViewById<View>(R.id.linearlayout_orderoffer_phone).visibility = View.VISIBLE
                }
            }

            enableButton(false)

            val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
            requestParams["order_id"] = onCreateOrderId
            requestParams["tenant_login"] = service.uri
            requestParams["worker_login"] = driver.callSign
            WebService.getOrderForId(requestParams, driver.secretCode)

            val title = findViewById<TextView>(R.id.textview_orderoffer_title)
            if (freeOrder == 0 || freeOrder == 1) {
                backTimer(freeOrder)
                title.text = getString(R.string.text_orderoffer_title)
            } else {
                title.text = getString(R.string.text_orderoffer_title2)
            }
        }

        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                val requestParams = LinkedHashMap<String, String>()
                requestParams["request_id"] = presenter.getUuid()
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign
                WebService.getResult(requestParams, driver.secretCode)
            }
        }

        if (orderType == "free" || orderType == "assignedOrders" || orderType == "preorder") {
            if (bundle!!.getBoolean("music") && appPreferences.getText("push_sound") == "1") {
                try {
                    mediaPlayer.start()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onDestroy() {
        try {
            mediaPlayer.pause()
            mediaPlayer.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }

//        progressDialog?.dismiss()
        hideProgressBar()

        EventBus.getDefault().post(DeleteAllNotificationEvent())
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun onBackPressed() {

    }

    private fun initBundle() {
        bundle = intent.extras

        bundle?.apply {
            onCreateOrderId = getString("order_id")!!
            source = getString("source")
            orderType = getString("type_order")
            freeOrder = getInt("free_order")
            arrival = getIntArray("arrival")
        }
    }

    private fun initButtonTime() {
        arrival?.also { times ->
            if (times.isNotEmpty()) {
                timeButtons.removeAllViews()
                for (time in times) {
                    val button = Button(this)
                    button.text = getTimeString(time)
                    button.tag = time
                    button.textSize = 24f
                    button.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f)
                    button.setTextColor(Color.parseColor("#228B22"))
                    button.setOnClickListener { v -> createStartDialog(v) }
                    timeButtons.addView(button)
                }
            }
            return
        }

        val jsonArray = JSONArray(appPreferences.getText("worker_arrival_time"))
        if (jsonArray.length() > 0) {
            for (j in 0 until jsonArray.length()) {
                if (jsonArray.getInt(j).toString().length > 1)
                    (timeButtons.getChildAt(j) as Button).text = jsonArray.getInt(j).toString()
                else
                    (timeButtons.getChildAt(j) as Button).text = String.format("0%s", jsonArray.getInt(j).toString())
                timeButtons.getChildAt(j).tag = jsonArray.getInt(j).toString()
            }
        }
    }

    private fun getTimeString(time: Int): String {
        return when {
            time < 60 -> time.toString()
            else -> {
                val hours = time.div(60)
                val min = time.rem(60)
                String.format(Locale.ROOT, "%s:%s ", hours, min)
            }
        }
    }

    @Subscribe
    fun onEvent(pushOrderEvent: PushOrderEvent) {

        if (pushOrderEvent.command == "reject_worker_from_order" || pushOrderEvent.command == "order_is_rejected") {
            if (onCreateOrderId == pushOrderEvent.orderId) {
                bOrder = true
                finish()
            }
        }
    }

    @Subscribe
    fun onEvent(updateOwnOrdersEvent: UpdateOwnOrdersEvent) {
        if (updateOwnOrdersEvent.type == "order" && updateOwnOrdersEvent.action == "delete")
            if (onCreateOrderId == updateOwnOrdersEvent.orderId && !beginOrder) {
                bOrder = true
                finish()
            }
    }

    @Subscribe
    fun onEvent(workerPositionEvent: NetworkWorkerPositionEvent) {
        serverTime = workerPositionEvent.cityTime
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {

        try {
            this.runOnUiThread {
//                progressDialog?.apply {
//                    if (isShowing) dismiss()
//                }
                hideProgressBar()

                countDownTimer!!.cancel()
            }
            if (pushResponseEvent.uuid == presenter.getUuid()) {
                if (requestType == RequestType.RESERVE) {

                    when (pushResponseEvent.infoCode) {
                        "OK" -> if (pushResponseEvent.result == 1) {
                            val intent = Intent(applicationContext, WorkShiftActivity::class.java)
                            intent.putExtra("service_id", service.id)
                            intent.putExtra("workday_id", bundle!!.getLong("workday_id"))
                            intent.putExtra("time", timeToClient)
                            intent.putExtra("showDialog", "empty")
                            intent.putExtra("showDialogGps", false)

                            if (orderType == "reserve") {
                                intent.putExtra("stageOrder", 0)
                            } else {
                                appPreferences.saveText("require_point_confirmation_code", confirmationCode.toString())
                                appPreferences.saveText("confirm_codes", "")
                                intent.putExtra("stageOrder", 1)
                            }
                            setResult(Activity.RESULT_CANCELED, intent)
                            finish()
                        }

                        "ORDER_IS_BUSY",
                        "EMPTY_DATA_IN_DATABASE",
                        "DRIVER_BLOCKED" -> {
                            showSnackbar(pushResponseEvent.infoCode)
                            finish()
                        }

                        else -> showSnackbar(pushResponseEvent.infoCode)
                    }

                } else if (requestType == RequestType.NORMAL) {
                    when (pushResponseEvent.infoCode) {
                        "OK" -> if (pushResponseEvent.result == 1 && jsonObjectOrder!!.getString("costData").length > 3) {

                            runBlocking {
                                withContext(Dispatchers.IO) {
                                    AppDatabase.getInstance(this@OrderOfferActivity).currentOrderDao().deleteAll(driver.id)
                                }
                            }

                            val options = if (jsonObjectOrder!!.getJSONArray("options").length() > 0) {
                                jsonObjectOrder!!.getJSONArray("options").toString()
                            } else ""

                            val comment = if (jsonObjectOrder!!.getString("comment").isNotEmpty() && jsonObjectOrder!!.getString("comment") != "null") {
                                jsonObjectOrder!!.getString("comment")
                            } else ""

                            val currentOrder = CurrentOrder(
                                    orderId = onCreateOrderId,
                                    timeToClient = timeToClient.toLong() * 60 * 1000,
                                    beginOrderTime = Date().time,
                                    waitTimeBegin = Date().time,
                                    waitTimeEnd = Date().time,
                                    waitTime = 0,
                                    stage = 0,
                                    tariffData = jsonObjectOrder!!.getString("costData"),
                                    address = jsonObjectOrder!!.getString("address"),
                                    options = options,
                                    comment = comment,
                                    phone = "",
                                    driver = driver.id
                            )

                            try {
                                if (jsonObjectOrder!!.getJSONObject("costData").getInt("enable_parking_ratio") == 1) {
                                    val jsonObjectAddress = jsonObjectOrder!!.getJSONObject("address")
                                    parkingStart = Utils.getParkingDopCost((jsonObjectAddress.getJSONObject("A").getString("lat")).toDouble(),
                                            (jsonObjectAddress.getJSONObject("A").getString("lon")).toDouble(), service, this)
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }

                            val order = Order()

                            order.comment = jsonObjectOrder!!.getString("comment")
                            order.workDay = workDay.id

                            if (orderType == preOrderKey)
                                order.pasName = ORDER_LABEL_RESERVE
                            else
                                order.pasName = ORDER_LABEL_NEW

                            order.statusOrder = ORDER_LABEL_NEW
                            order.createDatetime = Date().time
                            order.orderId = onCreateOrderId
                            order.orderIdService = jsonObjectOrder!!.getString("order_number")
                            order.driver = driver.id
                            order.tariffData = jsonObjectOrder!!.getString("costData")
                            order.address = jsonObjectOrder!!.getString("address")
                            order.orderTime = jsonObjectOrder!!.getString("order_time")
                            order.handBrake = jsonObjectOrder!!.getJSONObject("tariff").getString("auto_downtime")

                            try {
                                if (!jsonObjectOrder!!.isNull("clientPassenger") && !jsonObjectOrder!!.isNull("client")) {
                                    order.clientName = ((if (jsonObjectOrder!!.getJSONObject("clientPassenger").isNull("last_name") || jsonObjectOrder!!.getJSONObject("clientPassenger").getString("last_name").isEmpty()) "" else jsonObjectOrder!!.getJSONObject("clientPassenger").getString("last_name"))
                                            + (if (jsonObjectOrder!!.getJSONObject("clientPassenger").isNull("name") || jsonObjectOrder!!.getJSONObject("clientPassenger").getString("name").isEmpty()) "" else " " + jsonObjectOrder!!.getJSONObject("clientPassenger").getString("name"))
                                            + if (jsonObjectOrder!!.getJSONObject("clientPassenger").isNull("second_name") || jsonObjectOrder!!.getJSONObject("clientPassenger").getString("second_name").isEmpty()) "" else " " + jsonObjectOrder!!.getJSONObject("clientPassenger").getString("second_name"))
                                } else {
                                    order.clientName = ((if (jsonObjectOrder!!.getJSONObject("client").isNull("last_name") || jsonObjectOrder!!.getJSONObject("client").getString("last_name").isEmpty()) "" else jsonObjectOrder!!.getJSONObject("client").getString("last_name"))
                                            + (if (jsonObjectOrder!!.getJSONObject("client").isNull("name") || jsonObjectOrder!!.getJSONObject("client").getString("name").isEmpty()) "" else " " + jsonObjectOrder!!.getJSONObject("client").getString("name"))
                                            + if (jsonObjectOrder!!.getJSONObject("client").isNull("second_name") || jsonObjectOrder!!.getJSONObject("client").getString("second_name").isEmpty()) "" else " " + jsonObjectOrder!!.getJSONObject("client").getString("second_name"))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {
                                if (!jsonObjectOrder!!.isNull("phone") && jsonObjectOrder!!.getString("phone").isNotEmpty()) {
                                    order.clientPhone = jsonObjectOrder!!.getString("phone")
                                } else {
                                    order.clientPhone = ""
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {
                                if (!jsonObjectOrder!!.isNull("client_passenger_phone")
                                        && !jsonObjectOrder!!.isNull("phone")
                                        && jsonObjectOrder!!.getString("client_passenger_phone").isNotEmpty()) {
                                    order.clientPhone = jsonObjectOrder!!.getString("client_passenger_phone")
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }

                            order.day = jsonObjectOrder!!.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay") == 1

                            order.tariffLabel = jsonObjectOrder!!.getJSONObject("tariff").getString("name")

                            try {
                                order.dopCost = java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("additionals_cost"))
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            if (!jsonObjectOrder!!.isNull("promo_code_discount")) {
                                order.promoCodeDiscount = jsonObjectOrder!!.getInt("promo_code_discount")
                            }

                            val clientTariffParser = ClientTariffParser()

                            val tariffCity = clientTariffParser.createTariff(jsonObjectOrder!!.getJSONObject("costData").getJSONObject("tariffInfo").getJSONObject("tariffDataCity"),
                                    jsonObjectOrder!!.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay"), ClientTariffParser.AREA_CITY, jsonObjectOrder!!)

                            val tariffTrack = clientTariffParser.createTariff(jsonObjectOrder!!.getJSONObject("costData").getJSONObject("tariffInfo").getJSONObject("tariffDataTrack"),
                                    jsonObjectOrder!!.getJSONObject("costData").getJSONObject("tariffInfo").getInt("isDay"), ClientTariffParser.AREA_TRACK, jsonObjectOrder!!)


                            if (jsonObjectOrder!!.getJSONObject("costData").getInt("is_fix") == 1 && order.promoCodeDiscount > 0) {
                                if (parkingStart != null) {
                                    order.costOrder = Utils.addParkingCost(parkingStart!!, java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost_no_discount")), 0)!!
                                } else {
                                    order.costOrder = java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost_no_discount"))
                                }
                            } else {
                                if (parkingStart != null) {
                                    order.costOrder = Utils.addParkingCost(parkingStart!!, java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost")), 0)!!
                                } else {
                                    order.costOrder = java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost"))
                                }
                            }

                            if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1") {
                                order.fix = 1
                                tariffCity.accrual = ClientTariffParser.ACCRUAL_WIFI
                                tariffTrack.accrual = ClientTariffParser.ACCRUAL_WIFI
                            } else {
                                order.fix = jsonObjectOrder!!.getJSONObject("costData").getInt("is_fix")
                            }

                            order.timeWaitCityClient = tariffCity.waitTime * -60000
                            order.timeWaitTrackClient = tariffTrack.waitTime * -60000

                            if (order.onlyCity) {
                                order.startPoint = "in"
                            } else {
                                order.startPoint = jsonObjectOrder!!.getJSONObject("costData").getString("start_point_location")
                            }

                            if (jsonObjectOrder!!.getJSONObject("costData").getString("start_point_location") == "in") {
                                if (parkingStart != null) {
                                    order.plantingCost = Utils.addParkingCost(parkingStart!!, tariffCity.plantingPrice, 0)!!
                                } else {
                                    order.plantingCost = tariffCity.plantingPrice
                                }
                                order.minPrice = tariffCity.minPrice
                                order.rounding = if (tariffCity.rounding > 0) tariffCity.rounding else 1.0
                                order.roundingType = tariffCity.roundingType

                                when (tariffCity.accrual) {
                                    ClientTariffParser.ACCRUAL_TIME -> order.timeCityInOrder = tariffCity.plantingInclude.toLong() * -60000
                                    ClientTariffParser.ACCRUAL_DISTANCE -> order.disCityInOrder = -tariffCity.plantingInclude
                                    ClientTariffParser.ACCRUAL_MIXED -> {
                                        order.timeCityInOrder = tariffCity.plantingIncludeTime.toLong() * -60000
                                        order.disCityInOrder = -tariffCity.plantingInclude
                                    }
                                    ClientTariffParser.ACCRUAL_INTERVAL -> order.disCityInOrder = -tariffCity.plantingInclude
                                }
                            } else {

                                if (parkingStart != null) {
                                    order.plantingCost = Utils.addParkingCost(parkingStart!!, tariffTrack.plantingPrice, 0)!!
                                } else {
                                    order.plantingCost = tariffTrack.plantingPrice
                                }
                                order.minPrice = tariffTrack.minPrice
                                order.rounding = if (tariffTrack.rounding > 0) tariffTrack.rounding else 1.0
                                order.roundingType = tariffTrack.roundingType

                                when (tariffTrack.accrual) {
                                    ClientTariffParser.ACCRUAL_TIME -> order.timeTrackInOrder = tariffTrack.plantingInclude.toLong() * -60000
                                    ClientTariffParser.ACCRUAL_DISTANCE -> order.disTrackInOrder = -tariffTrack.plantingInclude
                                    ClientTariffParser.ACCRUAL_MIXED -> {
                                        order.timeTrackInOrder = tariffTrack.plantingIncludeTime.toLong() * -60000
                                        order.disTrackInOrder = -tariffTrack.plantingInclude
                                    }
                                    ClientTariffParser.ACCRUAL_INTERVAL -> order.disTrackInOrder = -tariffTrack.plantingInclude
                                }
                            }

                            if (jsonArrayOptions != null)
                                order.dopOption = jsonArrayOptions!!.toString()

                            order.typeCost = jsonObjectOrder!!.getString("payment")

                            try {
                                if (parkingStart != null) {
                                    order.fixCityCost = Utils.addParkingCost(parkingStart!!, (jsonObjectOrder!!.getJSONObject("costData").getString("city_cost")).toDouble(), 0)!!
                                    order.fixOutCityCost = Utils.addParkingCost(parkingStart!!, (jsonObjectOrder!!.getJSONObject("costData").getString("out_city_cost")).toDouble(), 0)!!
                                } else {
                                    order.fixCityCost = java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("city_cost"))
                                    order.fixOutCityCost = java.lang.Double.valueOf(jsonObjectOrder!!.getJSONObject("costData").getString("out_city_cost"))
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            try {
                                order.enableParkingRatio = jsonObjectOrder!!.getJSONObject("costData").getInt("enable_parking_ratio")
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            val intent = Intent(applicationContext, WorkShiftActivity::class.java)
                            intent.putExtra("service_id", service.id)
                            intent.putExtra("workday_id", bundle!!.getLong("workday_id"))
                            intent.putExtra("time", timeToClient)
                            intent.putExtra("showDialogGps", false)
                            intent.putExtra("order_id", order.orderId)

                            order.handBrake = jsonObjectOrder!!.getJSONObject("tariff").getString("auto_downtime")
                            intent.putExtra("showDialog", "empty")

                            try {
                                appPreferences.saveText("require_point_confirmation_code", confirmationCode.toString())
                                appPreferences.saveText("confirm_codes", "")

                                runBlocking {
                                    val db = AppDatabase.getInstance(this@OrderOfferActivity)
                                    withContext(Dispatchers.IO) {
                                        currentOrder.id = db.currentOrderDao().save(currentOrder)
                                        order.curOrderId = currentOrder.id
                                        order.id = db.orderDao().save(order)

                                        tariffCity.order = order.id
                                        tariffTrack.order = order.id

                                        val clientDao = AppDatabase.getInstance(this@OrderOfferActivity).clientTariffDao()

                                        clientDao.save(tariffCity)
                                        clientDao.save(tariffTrack)
                                    }
                                }
                                intent.putExtra("stageOrder", 1)
                            } catch (e: Exception) {
                                intent.putExtra("stageOrder", 0)
                            }

                            EventBus.getDefault().post(CloseActivityEvent())

                            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                            startActivity(intent)
                            finish()
                        } else {
                            this.runOnUiThread { CustomToast.showMessage(applicationContext, "Empty tariff") }
                            finish()
                        }

                        "ORDER_IS_BUSY",
                        "EMPTY_DATA_IN_DATABASE",
                        "DRIVER_BLOCKED" -> {
                            showSnackbar(pushResponseEvent.infoCode)
                            finish()
                        }

                        "SHIFT_IS_CLOSED" -> showSnackbar(pushResponseEvent.infoCode)

                        else -> {
                            showSnackbar(pushResponseEvent.infoCode)
                            countDownTimer!!.cancel()
                            bOrder = false
                            beginOrder = false
                        }
                    }


                } else if (requestType == RequestType.CANCEL_PREORDER || requestType == RequestType.CANCEL_ORDER) {

                    if (pushResponseEvent.result == 1) {
                        setResult(Activity.RESULT_CANCELED, Intent())
                        finish()
                    } else {
                        bOrder = false
                        beginOrder = false
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {

        try {
            if (orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id") != onCreateOrderId)
                return

            jsonObjectOrder = null
            when (orderForIdEvent.status) {

                "OK" -> {
                    jsonObjectOrder = orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data")

                    try {
                        val firstAddr = jsonObjectOrder!!.getJSONObject("address").getJSONObject("A")

                        val text = (DistanceGPS.getDistanceRound(
                                appPreferences.getText("lat").toDouble(),
                                appPreferences.getText("lon").toDouble(),
                                firstAddr.getString("lat").toDouble(),
                                firstAddr.getString("lon").toDouble()).toString() + " " + getString(R.string.text_dis))
                        findViewById<TextView>(R.id.textview_order_offer_dis).text = text

                        showMap(appPreferences.getText("lat").toDouble(), appPreferences.getText("lon").toDouble(), firstAddr.getString("lat").toDouble(), firstAddr.getString("lon").toDouble())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    try {
                        try {
                            val settings = jsonObjectOrder!!.getJSONObject("settings")
                            if (settings.has("dynamic_calculation_time_to_client") && settings.getInt("dynamic_calculation_time_to_client") > 0) {
                                stTimeToClient = jsonObjectOrder!!.getString("time_to_client")
                                val stDistanceToClient = jsonObjectOrder!!.getString("distance_to_client")

                                if (stTimeToClient!!.isNotEmpty() && stTimeToClient != "null") {
                                    dynamicCalculationTimeToClient = jsonObjectOrder!!.getJSONObject("settings").getInt("dynamic_calculation_time_to_client")
                                    timeButtons.visibility = View.GONE
                                    findViewById<View>(R.id.button_order_offer_dynamic).visibility = View.VISIBLE
                                    findViewById<TextView>(R.id.textview_order_offer_dis).text = Html.fromHtml("<big><big><big>$stDistanceToClient</big></big></big>")
                                }
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        if (dynamicCalculationTimeToClient == 0) {
                            if (orderType == "reserve" || orderType == "free_br") {
                                timeButtons.visibility = View.GONE
                            } else {
                                timeButtons.visibility = View.VISIBLE
                            }
                        }

                        showSearchWorker = jsonObjectOrder!!.getJSONObject("settings").getInt("show_search_worker_loader")
                        backOfferSec = offerSec.toLong()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    try {
                        confirmationCode = jsonObjectOrder!!.getJSONObject("settings").getInt("require_point_confirmation_code")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (jsonObjectOrder!!.getString("deny_refuse_order") == "1") {
                        findViewById<View>(R.id.relativelayout_btn_rejected).visibility = View.VISIBLE
                        findViewById<View>(R.id.relativelayout_btn_cancel).visibility = View.GONE
                    }
                    val firstAddr = jsonObjectOrder!!.getJSONObject("address").getJSONObject("A")

                    findViewById<TextView>(R.id.textview_order_offer_point_a).text = buildAddress(LatLng(firstAddr.getDouble("lat"), firstAddr.getDouble("lon")), firstAddr)

                    if (confirmationCode == 1) {
                        try {
                            if (firstAddr.getString("comment").isNotEmpty() && !firstAddr.isNull("comment")) {
                                findViewById<View>(R.id.point_a_comment).visibility = View.VISIBLE
                                (findViewById<View>(R.id.point_a_comment) as TextView).text = firstAddr.getString("comment")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        try {
                            if (firstAddr.getString("phone").isNotEmpty() && !firstAddr.isNull("phone")) {
                                findViewById<View>(R.id.ll_phone_block).visibility = View.VISIBLE
                                (findViewById<View>(R.id.point_a_phone) as TextView).text = firstAddr.getString("phone")
                            }

                            findViewById<View>(R.id.point_a_phone).setOnClickListener {
                                val intent = Intent(Intent.ACTION_DIAL)
                                intent.data = Uri.parse("tel:" + (findViewById<View>(R.id.point_a_phone) as TextView).text)
                                startActivity(intent)
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    if (jsonObjectOrder!!.getString("comment").isNotEmpty() && jsonObjectOrder!!.getString("comment") != "null") {
                        findViewById<View>(R.id.textview_orderoffer_comment).visibility = View.VISIBLE
                        (findViewById<View>(R.id.textview_orderoffer_comment) as TextView).text = jsonObjectOrder!!.getString("comment")
                    }

                    var count = 0
                    (findViewById<View>(R.id.linearlayout_orderoffer_second_source) as LinearLayout).removeAllViews()

                    for (itemAddress in arrAddresses) {
                        try {
                            try {
                                if (count == 0 && appPreferences.getText("show_estimation") == "1") {
                                    try {
                                        findViewById<View>(R.id.textview_orderoffer_random_currency).visibility = View.VISIBLE
                                        if (jsonObjectOrder!!.getJSONObject("costData").getInt("is_fix") == 1) {
                                            (findViewById<View>(R.id.textview_orderoffer_random_currency) as TextView).text = Html.fromHtml(getString(R.string.text_orderoffer_fix_price) + "<br />"
                                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost")) + " "
                                                    + jsonObjectOrder!!.getJSONObject("costData").getString("summary_distance") + " " + getString(R.string.text_dis) + " "
                                                    + jsonObjectOrder!!.getJSONObject("costData").getString("summary_time") + " " + getString(R.string.text_orderoffer_min))
                                        } else {
                                            (findViewById<View>(R.id.textview_orderoffer_random_currency) as TextView).text = Html.fromHtml(getString(R.string.text_orderoffer_random_price) + "<br />"
                                                    + TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost")) + " "
                                                    + jsonObjectOrder!!.getJSONObject("costData").getString("summary_distance") + " " + getString(R.string.text_dis) + " "
                                                    + jsonObjectOrder!!.getJSONObject("costData").getString("summary_time") + " " + getString(R.string.text_orderoffer_min))
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }

                                }
                                if (appPreferences.getText("show_worker_address") == "0")
                                    break

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            val layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_source, null, false)

                            val otherAddr = jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress)
                            (layoutView.findViewById<View>(R.id.textview_orderoffer_source) as TextView).text = buildAddress(LatLng(otherAddr.getDouble("lat"), otherAddr.getDouble("lon")), otherAddr)

                            if (confirmationCode == 1) {
                                try {
                                    if (jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).getString("comment").isNotEmpty() && !jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).isNull("comment")) {
                                        layoutView.findViewById<View>(R.id.point_a_comment).visibility = View.VISIBLE
                                        (layoutView.findViewById<View>(R.id.point_a_comment) as TextView).text = jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).getString("comment")
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                                try {
                                    if (jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).getString("phone").isNotEmpty() && !jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).isNull("phone")) {
                                        layoutView.findViewById<View>(R.id.ll_phone_block).visibility = View.VISIBLE
                                        (layoutView.findViewById<View>(R.id.point_a_phone) as TextView).text = jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress).getString("phone")
                                    }

                                    layoutView.findViewById<View>(R.id.point_a_phone).setOnClickListener {
                                        val intent = Intent(Intent.ACTION_DIAL)
                                        intent.data = Uri.parse("tel:" + (layoutView.findViewById<View>(R.id.point_a_phone) as TextView).text)
                                        startActivity(intent)
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }

                            (findViewById<View>(R.id.linearlayout_orderoffer_second_source) as LinearLayout).addView(layoutView)
                            findViewById<View>(R.id.linearlayout_orderoffer_to).visibility = View.VISIBLE

                            count++
                        } catch (e: JSONException) {
                        }
                    }

                    if (count == 0) findViewById<View>(R.id.tv_to).visibility = View.GONE

                    try {

                        val dopOptions = ArrayList<String>()
                        dopOptions.add(jsonObjectOrder!!.getJSONObject("tariff").getString("name"))

                        if (jsonObjectOrder!!.getString("payment") == "CASH") {
                            dopOptions.add(getString(R.string.text_reportorder_cash))
                        } else {
                            dopOptions.add(getString(R.string.text_reportorder_nocash))
                        }

                        if (jsonObjectOrder!!.getJSONArray("options").length() > 0) {
                            jsonArrayOptions = jsonObjectOrder!!.getJSONArray("options")
                            for (j in 0 until jsonArrayOptions!!.length()) {
                                dopOptions.add(jsonArrayOptions!!.getJSONObject(j).getString("name"))
                            }
                        }

                        if (orderType != "free" && orderType != "assignedOrders" && orderType != "preorder") {
                            val linearLayoutPreTime = findViewById<LinearLayout>(R.id.linearlayout_orderoffer_pretime)
                            linearLayoutPreTime.visibility = View.VISIBLE
                            try {
                                val orderTime = jsonObjectOrder!!.getString("order_time").split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                (linearLayoutPreTime.getChildAt(0) as TextView).text = getText(R.string.text_tariff_pretime).toString() + " " + orderTime[1] + " " + orderTime[0]
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        val linearLayoutOptions = findViewById<LinearLayout>(R.id.linearlayout_orderoffer_options)
                        linearLayoutOptions.removeAllViews()
                        val views = ArrayList<View>()
                        var countOption = 1
                        var layoutView: View? = null
                        for (i in dopOptions.indices) {
                            if (countOption == 1) {
                                layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_option, null, false)
                                (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option1) as TextView).text = dopOptions[i]

                                if (i == dopOptions.size - 1) {
                                    views.add(layoutView)
                                }


                            } else if (countOption == 2) {
                                (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option2) as TextView).text = dopOptions[i]
                                views.add(layoutView)
                                countOption = 0
                            }
                            countOption++
                        }
                        for (item in views) {
                            linearLayoutOptions.addView(item)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    try {
                        if (!jsonObjectOrder!!.isNull("clientPassenger")) {
                            val passenger = jsonObjectOrder!!.getJSONObject("clientPassenger")
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).text =
                                    if (passenger.isNull("last_name")) ""
                                    else
                                        passenger.getString("last_name") + (if (passenger.isNull("second_name")) "" else " " + passenger.getString("second_name")) + if (passenger.isNull("name")) "" else " " + passenger.getString("name")
                        } else if (!jsonObjectOrder!!.isNull("client")) {
                            val client = jsonObjectOrder!!.getJSONObject("client")

                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).text = if (client.isNull("last_name"))
                                ""
                            else
                                client.getString("last_name") +
                                        (if (client.isNull("second_name")) "" else " " + client.getString("second_name")) +
                                        if (client.isNull("name")) "" else " " + client.getString("name")
                        } else {
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).setTextColor(resources.getColor(R.color.red))
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).text = getString(R.string.text_taximeter_order_name_unknow)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val imageButton = findViewById<ImageButton>(R.id.imagebutton_taximeterorder_phone)
                    try {
                        if (!jsonObjectOrder!!.isNull("phone") && jsonObjectOrder!!.getString("phone").isNotEmpty()) {
                            imageButton.setOnClickListener {
                                val telecomManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                                val simState = telecomManager.simState
                                if (simState == TelephonyManager.SIM_STATE_READY) {
                                    val callIntent = Intent(Intent.ACTION_DIAL)
                                    try {
                                        callIntent.data = Uri.parse("tel:+" + jsonObjectOrder!!.getString("phone"))
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }

                                    startActivity(callIntent)
                                } else {
                                    CustomToast.showMessage(applicationContext, getString(R.string.toast_absent_sim))
                                }
                            }
                        } else {
                            imageButton.visibility = View.INVISIBLE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    enableButton(true)
                }
                "ORDER_IS_BUSY" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_order_is_busy))
                    deleteOrder()
                    finish()
                    logEvent("ORDER_IS_BUSY", this.javaClass.name, LogEvent.FINISH)
                }
                "EMPTY_DATA_IN_DATABASE" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_empty))
                    deleteOrder()
                    finish()
                    logEvent("EMPTY_DATA_IN_DATABASE", this.javaClass.name, LogEvent.FINISH)
                }
                "DRIVER_BLOCKED" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked))
                    finish()
                    logEvent("DRIVER_BLOCKED", this.javaClass.name, LogEvent.FINISH)
                }
                "DRIVER_PRE_ORDER_BLOCKED" -> CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    @Subscribe
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {

        if (orderStatusEvent.type == RequestType.NONE && orderStatusEvent.result == 1) {
            return
        }

        if (orderStatusEvent.status == "OK" && (orderStatusEvent.result == 1 || orderStatusEvent.result == 2)) {
        } else if (orderStatusEvent.status == "EMPTY_DATA_IN_DATABASE") {
            CustomToast.showMessage(applicationContext, getString(R.string.text_order_rejected))
            finish()
            logEvent("EMPTY_DATA_IN_DATABASE", this.javaClass.name, LogEvent.FINISH)
        } else {
            showSnackbar(orderStatusEvent.status)
            bOrder = false
        }
    }

    @Subscribe
    fun onEvent(requestResult: NetworkRequestResult) {

        if (requestResult.status == "OK") {
            EventBus.getDefault().post(PushResponseEvent(presenter.getUuid(), requestResult.status, requestResult.result, JSONObject()))
        } else {
            showSnackbar(requestResult.status)
            presenter.clearUuid()

            bOrder = false
            beginOrder = false
        }
    }

    fun createStartDialog(v: View) {

        try {
            val tariff = jsonObjectOrder!!.getJSONObject("tariff").getString("name")
            if (tariff == "Fahrservice" || tariff == "Mietwagen") {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.text_alert_orderoffer_title))
                builder.setCancelable(false)
                builder.setPositiveButton(getString(R.string.text_alert_orderoffer_confirm)) { _, _ ->
                    if (v.tag.toString() != "createOrder")
                        timeOrder(v)
                    else
                        timeOrder(null)
                }
                builder.setNegativeButton(getString(R.string.text_alert_orderoffer_cancel)) { _, _ -> }
                val alert = builder.create()
                alert.show()
                return
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        if (v.tag.toString() != "createOrder")
            timeOrder(v)
        else
            timeOrder(null)
    }

    private fun showProgressBar(text: String) {
        progress_bar_text.text = text
        progressBar.visibility = View.VISIBLE
        timeButtons.visibility = View.GONE
        buttons.visibility = View.GONE

        object : CountDownTimer(20000, 1000) {
            override fun onFinish() {
                hideProgressBar()
            }

            override fun onTick(millisUntilFinished: Long) {
                order_progress_bar.progress = (19 - (millisUntilFinished.toInt() / 1000)) * 5
            }

        }.start()

    }

    private fun hideProgressBar() {

        progressBar.visibility = View.GONE
        timeButtons.visibility = View.VISIBLE
        buttons.visibility = View.VISIBLE
    }

    private fun timeOrder(v: View?) {
        if (InternetConnection.isOnline(applicationContext)) {

            val text = if (showSearchWorker == 1) getString(R.string.dialog_search_worker) else getString(R.string.dialog_create_order)
            showProgressBar(text)

            timeToClient = v?.tag?.toString() ?: stTimeToClient.toString()

            if (!bOrder) {
                requestType = RequestType.NORMAL
                try {
                    findViewById<View>(R.id.button_orderoffer_cancel).isEnabled = false
                    if (timer != null)
                        timer!!.cancel()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (orderType == preOrderKey) {
                    val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH)
                    try {
                        dateFormat.parse(jsonObjectOrder!!.getString("order_time"))?.also { parseDate ->
//                            val tz = TimeZone.getDefault()
//                            if (serverTime == 0L)
//                                serverTime = intent.getLongExtra("server_time", 0)

//                            if (parseDate.time / 1000 - (serverTime - tz.getOffset(parseDate.time) / 1000) <= Integer.valueOf(appPreferences.getText("start_time_by_order")) * 60) {
                            bOrder = true
                            beginOrder = true
                            val requestParams = LinkedHashMap<String, String>()
                            requestParams["order_id"] = onCreateOrderId
                            requestParams["status_new_id"] = ORDER_STATUS_RESERVE_CONFIRM
                            requestParams["tenant_login"] = service.uri
                            requestParams["time_to_client"] = timeToClient
                            requestParams["worker_login"] = driver.callSign

                            initCountTimer(backOfferSec)

                            if (SET_ORDER_PARAMS_VIA_SOCKET) {
                                presenter.setOrderStatus(ORDER_STATUS_RESERVE_CONFIRM, requestType, timeToClient)
                            } else {
                                WebService.setOrderStatus(requestParams, driver.secretCode, presenter.getNewUuid(), requestType)
                            }

//                            } else {
//                                CustomToast.showMessage(applicationContext, String.format(getString(R.string.time_to_order), appPreferences.getText("start_time_by_order")))
//                                progressDialog!!.dismiss()
//
//                                bOrder = false
//                                beginOrder = false
//                                findViewById<View>(R.id.button_orderoffer_cancel).isEnabled = true
//                            }
                        }
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: IllegalStateException) {
                        e.printStackTrace()
                    }
                } else {
                    bOrder = true
                    beginOrder = true

                    val requestParams = LinkedHashMap<String, String>()
                    requestParams["order_id"] = onCreateOrderId
                    requestParams["status_new_id"] = ORDER_STATUS_CONFIRM
                    requestParams["tenant_login"] = service.uri
                    requestParams["time_to_client"] = timeToClient
                    requestParams["worker_login"] = driver.callSign

                    initCountTimer(backOfferSec)

                    if (SET_ORDER_PARAMS_VIA_SOCKET) {
                        presenter.setOrderStatus(ORDER_STATUS_CONFIRM, params = timeToClient)
                    } else {
                        WebService.setOrderStatus(requestParams, driver.secretCode, presenter.getNewUuid(), RequestType.NONE)
                    }
                }
            }
        } else {
            CustomToast.showMessage(applicationContext, getString(R.string.text_internet_access))
        }
    }

    fun holdOrder(v: View) {

        val requestParams = LinkedHashMap<String, String>()
        requestParams["order_id"] = onCreateOrderId
        requestParams["status_new_id"] = bundle!!.getString("code_next")!!
        requestParams["tenant_login"] = service.uri
        requestParams["worker_login"] = driver.callSign

        requestType = RequestType.RESERVE

        initCountTimer(backOfferSec)

        if (SET_ORDER_PARAMS_VIA_SOCKET) {
            presenter.setOrderStatus(bundle!!.getString("code_next")!!, requestType)
        } else {
            WebService.setOrderStatus(requestParams, driver.secretCode, presenter.getNewUuid(), requestType)
        }
    }

    fun closeOrder(v: View) {
        notificationManager!!.cancel(8)
        finish()
    }

    fun rejectOrder(v: View) {
        if (!InternetConnection.isOnline(applicationContext)) {
            CustomToast.showMessage(applicationContext, getString(R.string.text_internet_access))
            return
        }

        if (!bOrder) {
            notificationManager!!.cancel(8)

            beginOrder = true
            bOrder = true

            val requestParams = LinkedHashMap<String, String>()
            requestParams["order_id"] = onCreateOrderId

            val status: String =
                    if (orderType == preOrderKey || orderType == "assignedPreOrders") {
                        requestType = RequestType.CANCEL_PREORDER
                        ORDER_STATUS_RESERVE_CANCEL
                    } else {
                        requestType = RequestType.CANCEL_ORDER
                        bundle!!.getString("code_cancel")!!
                    }

            requestParams["status_new_id"] = status
            requestParams["tenant_login"] = service.uri
            requestParams["worker_login"] = driver.callSign

            val type = if (orderType == preOrderKey) RequestType.CANCEL_PREORDER else RequestType.CANCEL_ORDER

            if (SET_ORDER_PARAMS_VIA_SOCKET) {
                presenter.setOrderStatus(status, type, "driver")
            } else {
                WebService.setOrderStatus(requestParams, driver.secretCode, presenter.getNewUuid(), type)
            }

            if (type == RequestType.CANCEL_PREORDER)
                setResult(Activity.RESULT_CANCELED, Intent())

            finish()
        }
    }

    private fun enableButton(enable: Boolean) {
        if (orderType == "preorder" && source == "main_service") {
            findViewById<View>(R.id.button_order_offer_dynamic).visibility = View.VISIBLE
            timeButtons.visibility = View.GONE
        } else {

            for (i in 0 until timeButtons.childCount) {
                timeButtons.getChildAt(i).isEnabled = enable
            }
            findViewById<View>(R.id.button_orderoffer_rejected).isEnabled = enable
            findViewById<View>(R.id.button_orderoffer_cancel).isEnabled = enable
        }
    }

    private fun backTimer(x: Int) {
        if (x == 1) offerSec *= 3
        if (showSearchWorker == 1) backOfferSec = offerSec.toLong()
        timer = object : CountDownTimer(offerSec.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                findViewById<TextView>(R.id.textview_orderoffer_timer).text = Html.fromHtml("<big><big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + millisUntilFinished / 1000 + "</font></big></big>")
            }

            override fun onFinish() {
                if (!beginOrder) {
                    finish()
                } else {
                    findViewById<TextView>(R.id.textview_orderoffer_timer).text = Html.fromHtml("<big><big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + 0 + "</font></big></big>")
                }
            }
        }.start()
    }

    private fun showSnackbar(status: String) {

        hideProgressBar()

        val message = when (status) {

            EMPTY_RESPONSE -> getString(R.string.contact_support)

            RESPONSE_ERROR -> getString(R.string.response_error)

            "FAIL" -> getString(R.string.request_error)

            "WORKER_ALREADY_HAS_ACTIVE_ORDER" -> getString(R.string.already_has_active_order)

            "DRIVER_PRE_ORDER_BLOCKED" -> getString(R.string.error_driver_blocked)

            "SHIFT_IS_CLOSED" -> getString(R.string.status_shift_closed)

            "DENY_REFUSE_ORDER" -> getString(R.string.status_deny_refuse)

            "FORBIDDEN_ACTION" -> getString(R.string.time_to_order)

            "ORDER_ASSIGN_LIMIT" -> getString(R.string.status_error_limit)

            "ORDER_IS_BUSY" -> getString(R.string.error_order_is_busy)

            "EMPTY_DATA_IN_DATABASE" -> getString(R.string.error_empty)

            "DRIVER_BLOCKED" -> getString(R.string.error_driver_blocked)

            else -> status
        }

        val snackbar = Snackbar.make(findViewById(R.id.textview_orderoffer_timer), message, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("OK") {
            snackbar.dismiss()
        }
        snackbar.show()
    }

    private fun deleteOrder() {
        val dao = AppDatabase.getInstance(this).orderDao()
        runBlocking {
            withContext(Dispatchers.IO) {
                val order = dao.getByOrderId(onCreateOrderId)
                dao.delete(order)
            }
        }
    }

    private fun initCountTimer(offSec: Long?) {
        countDownTimer = object : CountDownTimer(10000 + offSec!!, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                val requestParams = LinkedHashMap<String, String>()
                requestParams["request_id"] = presenter.getUuid()
                requestParams["tenant_login"] = service.uri
                requestParams["worker_login"] = driver.callSign
                WebService.getResult(requestParams, driver.secretCode)
            }
        }.start()
    }

    private fun showMap(driverLat: Double?, driverLon: Double?, addressLat: Double?, addressLon: Double?) {

        val mapURL = String.format("https://maps.googleapis.com/maps/api/staticmap?&size=400x400&markers=%s,%s&markers=%s,%s&key=%s",
                driverLat,
                driverLon,
                addressLat,
                addressLon,
                "AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4")
        findViewById<SimpleDraweeView>(R.id.sdv_static_map).setImageURI(mapURL)
    }
}