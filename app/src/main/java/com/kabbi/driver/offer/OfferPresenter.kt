package com.kabbi.driver.offer

import android.os.Bundle
import com.kabbi.driver.App
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.orderStatus.OrderStatusPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class OfferPresenter(val context: OrderOfferActivity,
                     val application: App = context.application as App,
                     val db: AppDatabase = AppDatabase.getInstance(context)): OrderStatusPresenter() {

    private lateinit var source: String
    private lateinit var service: Service
    private lateinit var workDay: WorkDay

    fun init(bundle: Bundle?) {
        bundle?.apply {
            orderId = getString("order_id") ?: ""
            source = getString("source") ?: ""

            if (source == "main_service") {

                runBlocking {
                    withContext(Dispatchers.IO) {
                        service = db.serviceDao().getById(getLong("service_id"))
                        driver = db.driverDao().getById(getLong("driver_id"))
                        workDay = db.workDayDao().getById(getLong("workday_id"))
                    }
                }
                application.service = service
                application.driver = driver
                application.workDay = workDay

            } else {
                service = application.service
                driver = application.driver
                workDay = application.workDay
            }
        }
    }
}