package com.kabbi.driver.events;

public class NetworkClearDriverInfoEvent {

    private String status;
    private long serviceId;
    private int position;

    public NetworkClearDriverInfoEvent(String status, long serviceId, int position) {
        this.status = status;
        this.serviceId = serviceId;
        this.position = position;
    }

    public String getStatus() {
        return status;
    }

    public long getServiceId() {
        return serviceId;
    }

    public int getPosition() {
        return position;
    }
}
