package com.kabbi.driver.events;

public class NetworkCheckBankCard {
    private String status;

    public NetworkCheckBankCard(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
