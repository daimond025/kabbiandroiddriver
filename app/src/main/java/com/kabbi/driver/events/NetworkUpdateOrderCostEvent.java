package com.kabbi.driver.events;


public class NetworkUpdateOrderCostEvent {

    private String status;
    private int result;

    public NetworkUpdateOrderCostEvent(String status, int result) {
        this.status = status;
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public int getResult() {
        return result;
    }
}
