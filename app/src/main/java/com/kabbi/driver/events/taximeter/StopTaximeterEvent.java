package com.kabbi.driver.events.taximeter;

public class StopTaximeterEvent {
    private String statusOrder;

    public StopTaximeterEvent(String statusOrder) {
        this.statusOrder = statusOrder;
    }

    public String getStatusOrder() {
        return statusOrder;
    }
}
