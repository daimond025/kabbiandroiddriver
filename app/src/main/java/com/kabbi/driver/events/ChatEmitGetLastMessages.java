package com.kabbi.driver.events;

import com.kabbi.driver.database.entities.Driver;

public class ChatEmitGetLastMessages {

    private String typeChat;
    private Driver driver;

    public ChatEmitGetLastMessages(String typeChat, Driver driver) {
        this.typeChat = typeChat;
        this.driver = driver;
    }

    public String getTypeChat() {
        return typeChat;
    }

    public Driver getDriver() {
        return driver;
    }
}
