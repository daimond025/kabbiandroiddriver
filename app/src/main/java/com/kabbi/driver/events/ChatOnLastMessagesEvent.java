package com.kabbi.driver.events;

import java.util.HashMap;
import java.util.List;

public class ChatOnLastMessagesEvent {
    private String typeChat;
    private List<HashMap<String, String>> messages;

    public ChatOnLastMessagesEvent(String typeChat, List<HashMap<String, String>> messages) {
        this.typeChat = typeChat;
        this.messages = messages;
    }

    public String getTypeChat() {
        return typeChat;
    }

    public List<HashMap<String, String>> getMessages() {
        return messages;
    }
}
