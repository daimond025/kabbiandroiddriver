package com.kabbi.driver.events;

public class NetworkUpdateOrderEvent {
    private String status;
    private int result;

    public NetworkUpdateOrderEvent(String status) {
        this.status = status;
    }

    public NetworkUpdateOrderEvent(String status, int result) {
        this.status = status;
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public int getResult() {
        return result;
    }
}
