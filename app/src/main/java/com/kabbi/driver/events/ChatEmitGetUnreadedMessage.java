package com.kabbi.driver.events;

import com.kabbi.driver.database.entities.Driver;

public class ChatEmitGetUnreadedMessage {

    private Driver driver;

    public ChatEmitGetUnreadedMessage(Driver driver) {
        this.driver = driver;
    }

    public Driver getDriver() {
        return driver;
    }
}
