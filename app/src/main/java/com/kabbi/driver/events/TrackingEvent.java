package com.kabbi.driver.events;


import com.kabbi.driver.database.entities.Order;

public class TrackingEvent {

    private long lifetime;
    private String provider;
    private float accuracyGps;
    private Order order;
    private double lat;
    private double lon;

    public TrackingEvent(long lifetime, String provider, float accuracyGps, Order order, double lat, double lon) {
        this.lifetime = lifetime;
        this.provider = provider;
        this.accuracyGps = accuracyGps;
        this.order = order;
        this.lat = lat;
        this.lon = lon;
    }

    public long getLifetime() {
        return lifetime;
    }

    public String getProvider() {
        return provider;
    }

    public float getAccuracyGps() {
        return accuracyGps;
    }

    public Order getOrder() {
        return order;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
