package com.kabbi.driver.events;

public class CloseDialogEvent {

    private String dialog;

    public CloseDialogEvent(String dialog) {
        this.dialog = dialog;
    }

    public String getDialog() {
        return dialog;
    }


}
