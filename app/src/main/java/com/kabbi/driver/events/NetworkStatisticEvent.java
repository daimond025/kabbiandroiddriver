package com.kabbi.driver.events;

public class NetworkStatisticEvent {

    private Double earnings;
    private int countOrders;
    private int workingHours;

    public NetworkStatisticEvent(Double earnings, int countOrders, int workingHours) {
        this.earnings = earnings;
        this.countOrders = countOrders;
        this.workingHours = workingHours;
    }

    public Double getEarnings() {
        return earnings;
    }

    public int getCountOrders() {
        return countOrders;
    }

    public int getWorkingHours() {
        return workingHours;
    }
}
