package com.kabbi.driver.events;

public class ChangeBorderTariffEvent {

    private String tariffId;

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }
}
