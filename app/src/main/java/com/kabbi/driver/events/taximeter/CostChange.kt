package com.kabbi.driver.events.taximeter

import com.google.gson.annotations.SerializedName

data class CostChange(

        @SerializedName("uuid")
        var uuid: String,

        @SerializedName("predv_price")
        var cost: String,

        @SerializedName("order_id")
        var orderId: String,

        @SerializedName("worker_login")
        var worker: String
)