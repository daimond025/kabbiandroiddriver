package com.kabbi.driver.events

class NetworkWorkerPositionEvent {
    var status: String
        private set
    var workerStatus: String
        private set
    var serverTime: Long = 0
        private set
    var cityTime: Long = 0
        private set

    constructor(status: String, workerStatus: String) {
        this.status = status
        this.workerStatus = workerStatus
    }

    constructor(status: String, workerStatus: String, serverTime: Long, cityTime: Long) {
        this.status = status
        this.workerStatus = workerStatus
        this.serverTime = serverTime
        this.cityTime = cityTime
    }

}