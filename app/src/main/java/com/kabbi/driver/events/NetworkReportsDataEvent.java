package com.kabbi.driver.events;

import com.kabbi.driver.models.Address;
import com.kabbi.driver.models.WorkerReport;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class NetworkReportsDataEvent {

    private JSONArray data;
    private int currentPage;
    private int lastPage;
    private int from;
    private int perPage;
    private int to;
    private int total;
    private ArrayList<WorkerReport> workerReportList;

    public NetworkReportsDataEvent(JSONArray data, int currentPage, int lastPage, int perPage, int total) {
        this.data = data;
        this.currentPage = currentPage;
        this.lastPage = lastPage;
        this.perPage = perPage;
        this.total = total;
    }

    public ArrayList<WorkerReport> getData() {
        workerReportList = new ArrayList<>();
        for (int i = 0; i < data.length(); i++) {
            try {
                Address address = new Address();
                address.setCity(data.getJSONObject(i).getJSONObject("address").getJSONObject("A").getString("city"));
                address.setStreet(data.getJSONObject(i).getJSONObject("address").getJSONObject("A").getString("street"));

                WorkerReport workerReport = new WorkerReport(
                        data.getJSONObject(i).getLong("create_time"),
                        address,
                        data.getJSONObject(i).getLong("order_id"),
                        data.getJSONObject(i).getDouble("earnings"),
                        data.getJSONObject(i).getDouble("summary_cost")
                );
                workerReportList.add(workerReport);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return workerReportList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public int getFrom() {
        return from;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getTo() {
        return to;
    }

    public int getTotal() {
        return total;
    }
}