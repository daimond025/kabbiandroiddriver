package com.kabbi.driver.events;

public class SatelliteInfoEvent {

    private int satelliteCount;
    private int satelliteCoustUsed;

    public SatelliteInfoEvent(int satelliteCount, int satelliteCoustUsed) {
        this.satelliteCount = satelliteCount;
        this.satelliteCoustUsed = satelliteCoustUsed;
    }

    public int getSatelliteCount() {
        return satelliteCount;
    }

    public int getSatelliteCoustUsed() {
        return satelliteCoustUsed;
    }
}
