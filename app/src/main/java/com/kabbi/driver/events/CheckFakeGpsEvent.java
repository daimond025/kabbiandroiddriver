package com.kabbi.driver.events;

public class CheckFakeGpsEvent {

    int code;

    public CheckFakeGpsEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
