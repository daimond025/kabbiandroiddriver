package com.kabbi.driver.events;


public class CarEvent {

    private double lat;
    private double lon;
    private float bearing;

    public CarEvent(double lat, double lon, float bearing) {
        this.lat = lat;
        this.lon = lon;
        this.bearing = bearing;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public float getBearing() {
        return bearing;
    }
}
