package com.kabbi.driver.events;


import com.kabbi.driver.database.entities.Order;

public class NetworkSendRoute {
    private String status;
    private Order order;

    public NetworkSendRoute(String status) {
        this.status = status;
    }

    public NetworkSendRoute(String status, Order order) {
        this.status = status;
        this.order = order;
    }

    public String getStatus() {
        return status;
    }

    public Order getOrder() {
        return order;
    }
}
