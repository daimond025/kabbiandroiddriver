package com.kabbi.driver.events

import com.kabbi.driver.models.OpenOrder

class NetworkOrdersListPretimeEvent(val status: String, val type: String, val orders: MutableList<OpenOrder>)
