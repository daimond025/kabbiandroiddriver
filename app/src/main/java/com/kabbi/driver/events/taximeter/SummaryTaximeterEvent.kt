package com.kabbi.driver.events.taximeter

import com.kabbi.driver.database.entities.Order

class SummaryTaximeterEvent {
    var summaryCost = 0.0
    lateinit var order: Order
    var timeWaitClient: Long = 0
    var area: String = ""
    var stage = 0
    var testData = ""
}