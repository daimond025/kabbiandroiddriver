package com.kabbi.driver.events;

public class DriverEvent {

    private String type;
    private String data;

    public DriverEvent(String type, String data) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public String getData() {
        return data;
    }
}
