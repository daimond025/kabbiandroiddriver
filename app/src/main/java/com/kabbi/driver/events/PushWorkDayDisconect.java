package com.kabbi.driver.events;

public class PushWorkDayDisconect {

    private String newDeviceInfo;

    public PushWorkDayDisconect(String newDeviceInfo) {
        this.newDeviceInfo = newDeviceInfo;
    }

    public String getNewDeviceInfo() {
        return newDeviceInfo;
    }
}
