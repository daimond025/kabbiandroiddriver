package com.kabbi.driver.events;

import com.kabbi.driver.helper.AppParams;

public class NetworkOrderStatusEvent {

    private String status;
    private AppParams.RequestType type;
    private int result;
    private long cityTime;

    public NetworkOrderStatusEvent(String status) {
        this.status = status;
    }

    public NetworkOrderStatusEvent(String status, AppParams.RequestType type) {
        this.status = status;
        this.type = type;
    }

    public NetworkOrderStatusEvent(String status, AppParams.RequestType type, int result, long cityTime) {
        this.status = status;
        this.type = type;
        this.result = result;
        this.cityTime = cityTime;
    }

    public String getStatus() {
        return status;
    }

    public AppParams.RequestType getType() {
        return type;
    }

    public int getResult() {
        return result;
    }

    public long getCityTime() {
        return cityTime;
    }
}
