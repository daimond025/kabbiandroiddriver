package com.kabbi.driver.events;

public class UpdateOwnOrdersEvent {

    String type;
    String action;
    String orderId;

    public UpdateOwnOrdersEvent(String type, String action, String orderId) {
        this.type = type;
        this.action = action;
        this.orderId = orderId;
    }

    public String getType() {
        return type;
    }

    public String getAction() {
        return action;
    }

    public String getOrderId() {
        return orderId;
    }
}
