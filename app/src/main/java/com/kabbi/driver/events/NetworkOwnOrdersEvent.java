package com.kabbi.driver.events;

import com.kabbi.driver.models.OwnOrder;

import java.util.List;

public class NetworkOwnOrdersEvent {
    private String status;
    private List<OwnOrder> assignedOrders;
    private List<OwnOrder> assignedPreOrders;
    private List<OwnOrder> activeOrders;

    public NetworkOwnOrdersEvent(String status) {
        this.status = status;
    }

    public NetworkOwnOrdersEvent(String status, List<OwnOrder> assignedOrders, List<OwnOrder> assignedPreOrders, List<OwnOrder> activeOrders) {
        this.status = status;
        this.assignedOrders = assignedOrders;
        this.assignedPreOrders = assignedPreOrders;
        this.activeOrders = activeOrders;
    }

    public String getStatus() {
        return status;
    }

    public List<OwnOrder> getAssignedOrders() {
        return assignedOrders;
    }

    public List<OwnOrder> getAssignedPreOrders() {
        return assignedPreOrders;
    }

    public List<OwnOrder> getActiveOrders() {
        return activeOrders;
    }
}
