package com.kabbi.driver.events.taximeter;


import com.kabbi.driver.database.entities.Order;

public class StartTaximeterEvent {
    private Order order;
    private int stage;
    private String statusOrder;

    public StartTaximeterEvent(Order order, int stage, String statusOrder) {
        this.order = order;
        this.stage = stage;
        this.statusOrder = statusOrder;
    }

    public Order getOrder() {
        return order;
    }

    public int getStage() {
        return stage;
    }

    public String getStatusOrder() {
        return statusOrder;
    }
}
