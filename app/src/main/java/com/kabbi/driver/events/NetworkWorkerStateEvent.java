package com.kabbi.driver.events;


public class NetworkWorkerStateEvent {

    private String status;
    private int result;

    public NetworkWorkerStateEvent(String status) {
        this.status = status;
    }

    public NetworkWorkerStateEvent(String status, int result) {
        this.status = status;
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public int getResult() {
        return result;
    }
}
