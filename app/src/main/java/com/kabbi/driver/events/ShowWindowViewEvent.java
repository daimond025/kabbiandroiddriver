package com.kabbi.driver.events;

public class ShowWindowViewEvent {

    private boolean show;

    public ShowWindowViewEvent(boolean show) {
        this.show = show;
    }

    public boolean isShow() {
        return show;
    }
}
