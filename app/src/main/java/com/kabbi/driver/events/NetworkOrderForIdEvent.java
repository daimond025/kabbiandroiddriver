package com.kabbi.driver.events;

import org.json.JSONObject;

public class NetworkOrderForIdEvent {

    private String status;
    private JSONObject json;

    public NetworkOrderForIdEvent(String status) {
        this.status = status;
    }

    public NetworkOrderForIdEvent(String status, JSONObject json) {
        this.status = status;
        this.json = json;
    }

    public String getStatus() {
        return status;
    }

    public JSONObject getJson() {
        return json;
    }
}
