package com.kabbi.driver.events;

public class NetworkCreateBankCard {
    private String url;
    private String orderId;
    private String status;

    public NetworkCreateBankCard(String url, String orderId, String status) {
        this.url = url;
        this.orderId = orderId;
        this.status = status;
    }

    public NetworkCreateBankCard(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getStatus() {
        return status;
    }
}
