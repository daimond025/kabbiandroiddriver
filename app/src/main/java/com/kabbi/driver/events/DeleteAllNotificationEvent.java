package com.kabbi.driver.events;


public class DeleteAllNotificationEvent {

    private String test;

    public DeleteAllNotificationEvent() {
    }

    public DeleteAllNotificationEvent(String test) {
        this.test = test;
    }

    public String getTest() {
        return test;
    }
}
