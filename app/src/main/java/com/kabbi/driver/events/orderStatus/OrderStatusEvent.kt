package com.kabbi.driver.events.orderStatus

import com.kabbi.driver.helper.AppParams

data class OrderStatusEvent(
        val status: OrderStatus,
        val requestType: AppParams.RequestType?)