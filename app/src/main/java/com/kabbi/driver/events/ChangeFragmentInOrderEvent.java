package com.kabbi.driver.events;

public class ChangeFragmentInOrderEvent {

    private int itemSelected;

    public ChangeFragmentInOrderEvent(int itemSelected) {
        this.itemSelected = itemSelected;
    }

    public int getItemSelected() {
        return itemSelected;
    }
}
