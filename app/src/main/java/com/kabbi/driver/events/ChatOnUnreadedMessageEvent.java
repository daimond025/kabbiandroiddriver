package com.kabbi.driver.events;

import java.util.HashMap;
import java.util.List;

public class ChatOnUnreadedMessageEvent {

    private List<HashMap<String, String>> messages;

    public ChatOnUnreadedMessageEvent(List<HashMap<String, String>> messages) {
        this.messages = messages;
    }

    public List<HashMap<String, String>> getMessages() {
        return messages;
    }
}
