package com.kabbi.driver.events.orderStatus

import com.google.gson.annotations.SerializedName

data class OrderStatus(

        @SerializedName("uuid")
        var uuid: String = "",

        @SerializedName("order_id")
        var orderId: String = "",

        @SerializedName("worker_login")
        var worker: String = "",

        @SerializedName("lang")
        var lang: String? = null,

        @SerializedName("tenant_login")
        var tenant: String? = null,

        @SerializedName("status_id")
        var status: String = "",

        @SerializedName("time_to_client")
        var arrival: String? = null,

        @SerializedName("detail_order_data")
        var details: String? = null,

        @SerializedName("order_refuse")
        var refuse: String? = null
)