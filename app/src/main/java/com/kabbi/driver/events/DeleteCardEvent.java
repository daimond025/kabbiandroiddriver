package com.kabbi.driver.events;

public class DeleteCardEvent {
    private String pan;

    public DeleteCardEvent(String pan) {
        this.pan = pan;
    }

    public String getPan() {
        return pan;
    }
}
