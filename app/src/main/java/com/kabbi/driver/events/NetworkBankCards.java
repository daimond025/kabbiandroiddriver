package com.kabbi.driver.events;

import java.util.List;

public class NetworkBankCards {
    private List<String> cards;
    private String status;

    public NetworkBankCards(List<String> cards, String status) {
        this.cards = cards;
        this.status = status;
    }

    public List<String> getCards() {
        return cards;
    }

    public String getStatus() {
        return status;
    }
}
