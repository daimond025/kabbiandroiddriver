package com.kabbi.driver.events;


import java.util.Map;

public class NetworkAutocompleteEvent {

    private Map<String, String> address;

    public NetworkAutocompleteEvent(Map<String, String> address) {
        this.address = address;
    }

    public Map<String, String> getAddress() {
        return address;
    }
}
