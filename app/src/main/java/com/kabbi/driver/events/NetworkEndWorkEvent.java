package com.kabbi.driver.events;


public class NetworkEndWorkEvent {

    private String status;

    public NetworkEndWorkEvent(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
