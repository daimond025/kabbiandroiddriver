package com.kabbi.driver.events;

public class ChatOnNewMessageEvent {
    private String userName;
    private String time;
    private String role;
    private String message;
    private String typeChat;
    private boolean read;

    public ChatOnNewMessageEvent(String userName, String time, String role, String message, String typeChat, boolean read) {
        this.userName = userName;
        this.time = time;
        this.role = role;
        this.message = message;
        this.typeChat = typeChat;
        this.read = read;
    }

    public String getUserName() {
        return userName;
    }

    public String getTime() {
        return time;
    }

    public String getRole() {
        return role;
    }

    public String getMessage() {
        return message;
    }

    public String getTypeChat() {
        return typeChat;
    }

    public boolean isRead() {
        return read;
    }
}
