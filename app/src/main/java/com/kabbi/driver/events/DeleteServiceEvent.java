package com.kabbi.driver.events;

public class DeleteServiceEvent {

    private long serviceId;
    private int position;

    public DeleteServiceEvent(long serviceId, int position) {
        this.serviceId = serviceId;
        this.position = position;
    }

    public long getServiceId() {
        return serviceId;
    }

    public int getPosition() {
        return position;
    }
}
