package com.kabbi.driver.events;

import com.kabbi.driver.database.entities.Driver;

public class ChatEmitMessageIsRead {

    private Driver driver;

    public ChatEmitMessageIsRead(Driver driver) {
        this.driver = driver;
    }

    public Driver getDriver() {
        return driver;
    }
}
