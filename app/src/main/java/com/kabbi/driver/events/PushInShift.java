package com.kabbi.driver.events;


public class PushInShift {
    int type;
    String command;

    public PushInShift(int type, String command) {
        this.type = type;
        this.command = command;
    }

    public int getType() {
        return type;
    }

    public String getCommand() {
        return command;
    }
}


