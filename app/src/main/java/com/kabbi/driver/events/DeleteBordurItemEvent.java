package com.kabbi.driver.events;

public class DeleteBordurItemEvent {
    private int position;

    public DeleteBordurItemEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
