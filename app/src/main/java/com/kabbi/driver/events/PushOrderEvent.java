package com.kabbi.driver.events;


public class PushOrderEvent {

    private String orderId;
    private String callsign;
    private String tenant_login;
    private String command;
    private Integer secToClose;

    public PushOrderEvent(String orderId, String callsign, String tenant_login, String command) {
        this.orderId = orderId;
        this.callsign = callsign;
        this.tenant_login = tenant_login;
        this.command = command;
        this.secToClose = 0;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getCallsign() {
        return callsign;
    }

    public String getTenant_login() {
        return tenant_login;
    }

    public String getCommand() {
        return command;
    }

    public Integer getSecToClose() {
        return secToClose;
    }
}
