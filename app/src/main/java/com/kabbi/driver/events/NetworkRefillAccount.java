package com.kabbi.driver.events;

public class NetworkRefillAccount {
    private String status;
    private String balance;
    private String currency;

    public NetworkRefillAccount(String status, String balance, String currency) {
        this.status = status;
        this.balance = balance;
        this.currency = currency;
    }

    public NetworkRefillAccount(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
