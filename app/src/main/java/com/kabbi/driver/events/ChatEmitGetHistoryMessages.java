package com.kabbi.driver.events;

import com.kabbi.driver.database.entities.Driver;

public class ChatEmitGetHistoryMessages {

    private String typeChat;
    private long timestamp;
    private Driver driver;

    public ChatEmitGetHistoryMessages(String typeChat, long timestamp, Driver driver) {
        this.typeChat = typeChat;
        this.timestamp = timestamp;
        this.driver = driver;
    }

    public String getTypeChat() {
        return typeChat;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Driver getDriver() {
        return driver;
    }
}
