package com.kabbi.driver.events;


import org.json.JSONObject;

public class PushResponseEvent {

    private String uuid;
    private String infoCode;
    private int result;
    private JSONObject jsonObject;

    public PushResponseEvent(String uuid, String infoCode, int result, JSONObject jsonObject) {
        this.uuid = uuid;
        this.infoCode = infoCode;
        this.result = result;
        this.jsonObject = jsonObject;
    }

    public String getUuid() {
        return uuid;
    }

    public String getInfoCode() {
        return infoCode;
    }

    public int getResult() {
        return result;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }
}
