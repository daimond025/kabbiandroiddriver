package com.kabbi.driver.events;


import com.kabbi.driver.database.entities.Parking;

public class NetworkParkingsListEvent {

    private String status;
    private Parking parking;

    public NetworkParkingsListEvent(String status, Parking parking) {
        this.status = status;
        this.parking = parking;
    }

    public String getStatus() {
        return status;
    }

    public Parking getParking() {
        return parking;
    }
}
