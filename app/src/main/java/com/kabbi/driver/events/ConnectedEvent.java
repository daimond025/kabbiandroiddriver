package com.kabbi.driver.events;

public class ConnectedEvent {

    private boolean connected;

    public ConnectedEvent(boolean connected) {
        this.connected = connected;
    }

    public boolean isConnected() {
        return connected;
    }
}
