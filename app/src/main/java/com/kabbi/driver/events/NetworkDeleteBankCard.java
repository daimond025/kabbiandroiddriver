package com.kabbi.driver.events;

public class NetworkDeleteBankCard {
    private String status;

    public NetworkDeleteBankCard(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
