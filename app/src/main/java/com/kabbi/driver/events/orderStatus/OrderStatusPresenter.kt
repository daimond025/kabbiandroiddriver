package com.kabbi.driver.events.orderStatus

import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.helper.AppParams.*
import com.kabbi.driver.util.UuidProvider
import org.greenrobot.eventbus.EventBus

abstract class OrderStatusPresenter : UuidProvider() {

    lateinit var orderId: String
    lateinit var driver: Driver

    open fun setOrderStatus(status: String, type: RequestType? = null, params: String? = null) {

        val orderStatus = OrderStatus(
                uuid = getUuid(),
                orderId = orderId,
                worker = driver.callSign,
                lang = "de",
                status = status)

        when (status) {

            ORDER_STATUS_CONFIRM -> orderStatus.arrival = params

            ORDER_STATUS_RESERVE_CONFIRM -> orderStatus.arrival = params

            ORDER_STATUS_CANCEL -> orderStatus.refuse = params

            ORDER_STATUS_COMPLETED -> orderStatus.details = params

            ORDER_STATUS_ASSIGNED_CANCEL -> {
            }

            ORDER_STATUS_RESERVE_CANCEL -> {
            }

            ORDER_STATUS_CANCEL_IN_WORK -> {
            }

            ORDER_STATUS_ARRIVED -> {
            }

            ORDER_STATUS_CAR_ASSIGNED -> {
            }

            ORDER_STATUS_STOP_ORDER -> {
            }
        }

        EventBus.getDefault().post(OrderStatusEvent(orderStatus, type))
    }
}