package com.kabbi.driver.events;

public class SendRouteToServer {

    private long id;
    private int position;

    public SendRouteToServer(long id, int position) {
        this.id = id;
        this.position = position;
    }

    public long getId() {
        return id;
    }

    public int getPosition() {
        return position;
    }
}
