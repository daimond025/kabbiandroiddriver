package com.kabbi.driver.events.taximeter;

public class DestroyMainActivityEvent {
    private boolean destroy;

    public DestroyMainActivityEvent(boolean destroy) {
        this.destroy = destroy;
    }

    public boolean isDestroy() {
        return destroy;
    }
}
