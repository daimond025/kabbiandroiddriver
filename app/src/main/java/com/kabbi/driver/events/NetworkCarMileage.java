package com.kabbi.driver.events;


public class NetworkCarMileage {

    private String status;
    private String currentMileage;

    public NetworkCarMileage(String status, String currentMileage) {
        this.status = status;
        this.currentMileage = currentMileage;
    }

    public String getStatus() {
        return status;
    }

    public String getCurrentMileage() {
        return currentMileage;
    }
}
