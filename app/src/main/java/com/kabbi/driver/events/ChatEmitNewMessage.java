package com.kabbi.driver.events;

import com.kabbi.driver.database.entities.Driver;

public class ChatEmitNewMessage {

    private String typeChat;
    private String message;
    private Driver driver;

    public ChatEmitNewMessage(String typeChat, String message, Driver driver) {
        this.typeChat = typeChat;
        this.message = message;
        this.driver = driver;
    }

    public String getTypeChat() {
        return typeChat;
    }

    public String getMessage() {
        return message;
    }

    public Driver getDriver() {
        return driver;
    }
}
