package com.kabbi.driver.events;

public class SetStageOrderEvent {
    int stageOrder;

    public SetStageOrderEvent(int stageOrder) {
        this.stageOrder = stageOrder;
    }

    public int getStageOrder() {
        return stageOrder;
    }
}
