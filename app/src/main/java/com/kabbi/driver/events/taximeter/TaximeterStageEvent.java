package com.kabbi.driver.events.taximeter;

public class TaximeterStageEvent {
    private int stage;
    private String orderStatus;

    public TaximeterStageEvent(int stage, String orderStatus) {
        this.stage = stage;
        this.orderStatus = orderStatus;
    }

    public int getStage() {
        return stage;
    }

    public String getOrderStatus() {
        return orderStatus;
    }
}
