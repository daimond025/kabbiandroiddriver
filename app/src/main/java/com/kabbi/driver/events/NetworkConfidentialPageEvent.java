package com.kabbi.driver.events;


public class NetworkConfidentialPageEvent {

    private String status;
    private String page;

    public NetworkConfidentialPageEvent(String status, String page) {
        this.status = status;
        this.page = page;
    }

    public String getStatus() {
        return status;
    }

    public String getPage() {
        return page;
    }
}
