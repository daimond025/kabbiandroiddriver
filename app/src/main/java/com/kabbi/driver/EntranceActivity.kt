package com.kabbi.driver

import android.Manifest
import android.annotation.TargetApi
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.TypedValue
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.kabbi.driver.adapters.ServiceAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.Sound
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.DeleteServiceEvent
import com.kabbi.driver.events.NetworkClearDriverInfoEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.service.MainService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class EntranceActivity : BaseActivity(), View.OnClickListener, AdapterView.OnItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private lateinit var llNotice: LinearLayout
    internal lateinit var listView: ListView
    private lateinit var serviceAdapter: ServiceAdapter
    internal lateinit var appPreferences: AppPreferences
    private var services: List<Service> = arrayListOf()
    internal var list: MutableList<Map<String, Long>> = mutableListOf()
    internal lateinit var typedValueMain: TypedValue
    internal lateinit var typedValueSubscribe: TypedValue
    private lateinit var mLocationRequest: LocationRequest
    private var mGoogleApiClient: GoogleApiClient? = null
    internal var result: PendingResult<LocationSettingsResult>? = null
    private var workDayLast: WorkDay? = null
    private lateinit var service: Service
    private lateinit var driver: Driver
    private var workDay: WorkDay? = null
    private lateinit var db: AppDatabase
    private val job: Job = Job()
    private var scope: CoroutineScope = CoroutineScope(Dispatchers.Main + job)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isStoragePermissionGranted()

        appPreferences = AppPreferences(applicationContext)
        db = AppDatabase.getInstance(this)
        setTheme(AppParams.THEME_DEFAULT)
        appPreferences.saveText("theme", AppParams.THEME_DEFAULT.toString())
        setContentView(R.layout.activity_entrance)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener(this)

        llNotice = findViewById(R.id.addition_notice)
        val tvNotice = findViewById<TextView>(R.id.textview_entrance_notice)
        tvNotice.text = AppParams.WORKER_REG_DESCRIPTION
        val btnBeginDriver = findViewById<Button>(R.id.button_entrance_begindriver)
        btnBeginDriver.text = AppParams.WORKER_REG_BUTTON
        btnBeginDriver.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(AppParams.WORKER_REG_URL))
            startActivity(intent)
        }

        services = ArrayList()
        list = ArrayList()

        runBlocking {
            withContext(Dispatchers.IO) {
                services = db.serviceDao().getAll()
            }
        }
        for (item in services) {
            val map = HashMap<String, Long>()
            map["driver"] = item.driver!!
            map["service"] = item.id
            list.add(map)
        }

        if (AppParams.USE_WORKER_REG && list.size == 0) {
            llNotice.visibility = View.VISIBLE
        }

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        if (services.isNotEmpty()) {

            findViewById<View>(R.id.textview_entrance_infoswipe).visibility = View.VISIBLE
            findViewById<View>(R.id.textview_entrance_info).visibility = View.GONE

            serviceAdapter = ServiceAdapter(applicationContext, list, typedValueMain.data, typedValueSubscribe.data)
            listView = findViewById(R.id.listview_service)
            listView.divider = null
            listView.dividerHeight = 0
            listView.adapter = serviceAdapter

            listView.onItemClickListener = this
        }

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        mGoogleApiClient?.connect()


        if (Build.VERSION.SDK_INT >= 23) {
            someMethod()
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    fun someMethod() {
        if (!Settings.canDrawOverlays(this)) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
            startActivityForResult(intent, 243)
        }
    }

    public override fun onResume() {
        super.onResume()
        val resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(applicationContext)
        if (resultCode != ConnectionResult.SUCCESS) {
            val requestCode = 1
            GoogleApiAvailability.getInstance().getErrorDialog(this, resultCode, requestCode).show()
        }
    }

    @Subscribe
    fun onEvent(deleteServiceEvent: DeleteServiceEvent) {

        var service: Service? = null
        var driver: Driver? = null
        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@EntranceActivity)
                service = db.serviceDao().getById(deleteServiceEvent.serviceId)
                driver = db.driverDao().getById(service!!.driver!!)
            }
        }

        val paramsDriver = LinkedHashMap<String, String>()

        try {
            paramsDriver["tenant_login"] = service?.uri!!
            paramsDriver["worker_login"] = driver?.callSign!!
            WebService.clearInformationAfterWorkerDelete(paramsDriver, driver?.secretCode, deleteServiceEvent.serviceId, deleteServiceEvent.position)
        } catch (e: KotlinNullPointerException) {
            EventBus.getDefault().post(NetworkClearDriverInfoEvent("OK", deleteServiceEvent.serviceId, deleteServiceEvent.position))
        }
    }

    @Subscribe
    fun onEvent(clearDriverInfoEvent: NetworkClearDriverInfoEvent) {
        if ("OK" == clearDriverInfoEvent.status) {

            runBlocking {
                withContext(Dispatchers.IO) {
                    val db = AppDatabase.getInstance(this@EntranceActivity)
                    val service = db.serviceDao().getById(clearDriverInfoEvent.serviceId)
                    db.driverDao().delete(service.driver!!)
                    db.serviceDao().delete(service.id)
                }
            }
            list.removeAt(clearDriverInfoEvent.position)
            serviceAdapter = ServiceAdapter(applicationContext, list, typedValueMain.data, typedValueSubscribe.data)
            listView.adapter = serviceAdapter
            if (AppParams.USE_WORKER_REG && list.size == 0) {
                llNotice.visibility = View.VISIBLE
            }
            if (services.isEmpty()) {
                findViewById<View>(R.id.textview_entrance_info).visibility = View.VISIBLE
                findViewById<View>(R.id.textview_entrance_infoswipe).visibility = View.INVISIBLE
            }
        } else {
            CustomToast.showMessage(applicationContext, clearDriverInfoEvent.status)
        }
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        job.cancel()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onClick(v: View) {
        val intent = Intent(applicationContext, RegistrationActivity::class.java)
        intent.putExtra("service_id", 0.toLong())
        startActivity(intent)
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        appPreferences.saveText("car_id", "")
        appPreferences.saveText("drivertariff_id", "")
        appPreferences.saveText("city_id", "")
        appPreferences.saveText("driver_id", "")
        appPreferences.saveText("parking", "")
        appPreferences.saveText("navigation", getString(R.string.dialog_nav_google))

        appPreferences.saveText("step_1", "0")
        appPreferences.saveText("step_2", "0")
        appPreferences.saveText("step_3", "0")
        appPreferences.saveText("step_4", "0")

        appPreferences.saveText("fake_gps", "0")

        appPreferences.saveText("require_point_confirmation_code", "0")
        appPreferences.saveText("confirmation_order_id", "")
        appPreferences.saveText("confirm_codes", "")

        if (appPreferences.getText("favorite_map").isEmpty())
            appPreferences.saveText("favorite_map", AppParams.MAP)
        if (appPreferences.getText("push_balance").isEmpty())
            appPreferences.saveText("push_balance", "1")
        if (appPreferences.getText("push_preorder").isEmpty())
            appPreferences.saveText("push_preorder", "1")
        if (appPreferences.getText("push_sos").isEmpty())
            appPreferences.saveText("push_sos", "1")
        if (appPreferences.getText("push_sound").isEmpty())
            appPreferences.saveText("push_sound", "1")
        if (appPreferences.getText("favorite_lang").isEmpty())
            appPreferences.saveText("favorite_lang", AppParams.DEFAULT_LANG)
        if (appPreferences.getText("far_navi").isEmpty())
            appPreferences.saveText("far_navi", "0")

        if (appPreferences.getText("push_sound_freeorder").isEmpty())
            appPreferences.saveText("push_sound_freeorder", "1")
        if (appPreferences.getText("push_sound_preorder").isEmpty())
            appPreferences.saveText("push_sound_preorder", "1")

        if (appPreferences.getText("push_show_freeorder").isEmpty())
            appPreferences.saveText("push_show_freeorder", "1")
        if (appPreferences.getText("push_show_preorder").isEmpty())
            appPreferences.saveText("push_show_preorder", "1")

        if (appPreferences.getText("in_shift").isEmpty())
            appPreferences.saveText("in_shift", "0")

        if (appPreferences.getText("gps").isEmpty())
            appPreferences.saveText("gps", "0")
        if (appPreferences.getText("gps_server").isEmpty())
            appPreferences.saveText("gps_server", "0")


        if (appPreferences.getText("allow_cancel_order").isEmpty())
            appPreferences.saveText("allow_cancel_order", "0")
        if (appPreferences.getText("allow_cancel_order_after_time").isEmpty())
            appPreferences.saveText("allow_cancel_order_after_time", "20")

        if (appPreferences.getText("show_chat_with_dispatcher_before_start_shift").isEmpty())
            appPreferences.saveText("show_chat_with_dispatcher_before_start_shift", "1")
        if (appPreferences.getText("show_chat_with_dispatcher_on_shift").isEmpty())
            appPreferences.saveText("show_chat_with_dispatcher_on_shift", "1")
        if (appPreferences.getText("show_general_chat_on_shift").isEmpty())
            appPreferences.saveText("show_general_chat_on_shift", "1")
        if (appPreferences.getText("deny_fakegps").isEmpty())
            appPreferences.saveText("deny_fakegps", "0")
        if (appPreferences.getText("allow_worker_to_create_order").isEmpty())
            appPreferences.saveText("allow_worker_to_create_order", "1")
        if (appPreferences.getText("deny_setting_arrival_status_early").isEmpty())
            appPreferences.saveText("deny_setting_arrival_status_early", "0")

        if (appPreferences.getText("min_distance_to_set_arrival_status").isEmpty())
            appPreferences.saveText("min_distance_to_set_arrival_status", "500")
        if (appPreferences.getText("start_time_by_order").isEmpty())
            appPreferences.saveText("start_time_by_order", "30")
        if (appPreferences.getText("require_password_every_time_log_in_application").isEmpty())
            appPreferences.saveText("require_password_every_time_log_in_application", "0")
        if (appPreferences.getText("print_check").isEmpty())
            appPreferences.saveText("print_check", "0")
        if (appPreferences.getText("control_own_car_mileage").isEmpty())
            appPreferences.saveText("control_own_car_mileage", "0")
        if (appPreferences.getText("show_driver_privacy_policy").isEmpty())
            appPreferences.saveText("show_driver_privacy_policy", "0")
        if (appPreferences.getText("is_view_queue").isEmpty()) {
            appPreferences.saveText("is_view_queue", "1")
        }
        if (appPreferences.getText("allow_reserve_urgent_orders").isEmpty()) {
            appPreferences.saveText("allow_reserve_urgent_orders", "1")
        }
        if (appPreferences.getText("allow_edit_cost_order").isEmpty()) {
            appPreferences.saveText("allow_edit_cost_order", "1")
        }

        scope.launch {
            getSounds()
        }

        val textView = view.findViewById<TextView>(R.id.textview_detail_list_service)
        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@EntranceActivity)
                service = db.serviceDao().getById((textView.getTag(R.id.tag_service_id).toString()).toLong())
                driver = db.driverDao().getById(service.driver!!)
                workDay = db.workDayDao().getByServiceId(textView.getTag(R.id.tag_service_id).toString())
                workDayLast = db.workDayDao().getLastWorkDay(driver.id)
            }
        }

        if (workDayLast != null && workDay != null) {
            if (workDay!!.id == workDayLast!!.id) {
                requestLocationWrapper()
            } else {
                CustomToast.showMessage(this, getString(R.string.text_shiftwork))
            }

        } else if (workDayLast == null && (workDay == null || workDay!!.endWorkDay != 0L)) {

            if (appPreferences.getText("require_password_every_time_log_in_application") == "1") {
                confirmPassword(java.lang.Long.valueOf(textView.getTag(R.id.tag_service_id).toString()))
            } else {

                val intentEntrance = Intent(applicationContext, WorkActivity::class.java)
                intentEntrance.putExtra("service_id", java.lang.Long.valueOf(textView.getTag(R.id.tag_service_id).toString()))
                intentEntrance.putExtra("end_workshift", false)

                (application as App).driver = driver
                (application as App).service = service

                appPreferences.saveText("driver_id", driver.id.toString())
                appPreferences.saveText("service_id", service.id.toString())

                startActivity(intentEntrance)
                finish()
            }
        } else {
            CustomToast.showMessage(this, getString(R.string.text_shiftwork))
        }
    }

    private suspend fun getSounds() = withContext(Dispatchers.IO) {
        val soundDao = db.soundDao()
        val soundList = soundDao.getAll()
        if (soundList.isEmpty()) {
            val prefix = "android.resource://$packageName/"
            soundDao.saveAll(Sound("ringtone_pop", "Pop", prefix + R.raw.pop, isDefault = true, isShortSound = true),
                    Sound("ringtone_cute_bells", "Cute bells", prefix + R.raw.cute_bells, isDefault = true, isShortSound = true),
                    Sound("ringtone_free_order", "Free order", prefix + R.raw.free_order, isDefault = true, isShortSound = true),
                    Sound("ringtone_mirimba_chord", "Mirimba chord", prefix + R.raw.marimba_chord, isDefault = true, isShortSound = false),
                    Sound("ringtone_own", "Own", prefix + R.raw.own, isDefault = true, isShortSound = true),
                    Sound("ringtone_preorder", "Preorder", prefix + R.raw.pre_order, isDefault = true, isShortSound = true),
                    Sound("ringtone_rejected", "Rejected", prefix + R.raw.rejected, isDefault = true, isShortSound = true),
                    Sound("ringtone_remind_preorder", "Remind preorder", prefix + R.raw.remind_preorder, isDefault = true, isShortSound = true),
                    Sound("ringtone_sos", "Sos", prefix + R.raw.sos, isDefault = true, isShortSound = true))
        }
    }

    override fun onBackPressed() {
        val alertDialog = AlertDialog.Builder(this@EntranceActivity)
        alertDialog.setTitle(getString(R.string.activities_MainActivity_exit_title))
        alertDialog.setMessage(getString(R.string.activities_MainActivity_exit_question))
        alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_exit_answer_yes)) { _, _ ->
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
        alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_exit_answer_no)) { dialog, _ -> dialog.cancel() }
        alertDialog.show()
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
                .setAlwaysShow(true)

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result?.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        }
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    private fun confirmPassword(service_id: Long) {
        val viewDialog = layoutInflater.inflate(R.layout.fragment_dialog_edittext, null)
        AlertDialog.Builder(this@EntranceActivity)
                .setView(viewDialog)
                .setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel)) { dialog, _ -> dialog.dismiss() }
                .setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok)) { dialog, _ ->
                    val password = viewDialog.findViewById<EditText>(R.id.et_password).text.toString()
                    var driverPassword = ""

                    CoroutineScope(Dispatchers.Main).launch {
                        withContext(Dispatchers.IO) {
                            val db = AppDatabase.getInstance(this@EntranceActivity)
                            service = db.serviceDao().getById(service_id)
                            driverPassword = db.driverDao().getById(service.driver!!).password!!
                        }

                        if (password == driverPassword) {

                            val intentEntrance = Intent(applicationContext, WorkActivity::class.java)
                            intentEntrance.putExtra("service_id", service_id)
                            intentEntrance.putExtra("end_workshift", false)

                            (application as App).driver = driver
                            (application as App).service = service

                            appPreferences.saveText("driver_id", driver.id.toString())
                            appPreferences.saveText("service_id", service.id.toString())

                            startActivity(intentEntrance)
                            finish()
                        } else {
                            CustomToast.showMessage(applicationContext, getString(R.string.password_fail))
                        }
                        dialog.dismiss()
                    }
                }
                .create()
                .show()
    }

    private fun startWorkDay() {
        val intent = Intent(applicationContext, WorkShiftActivity::class.java)
        intent.putExtra("service_id", service.id)
        intent.putExtra("confirm", false)
        intent.putExtra("stageOrder", 0)
        intent.putExtra("showDialog", "empty")
        intent.putExtra("workday_id", workDay!!.id)
        intent.putExtra("showDialogGps", true)

        (application as App).driver = driver
        (application as App).service = service
        (application as App).workDay = workDay!!


        stopService(Intent(applicationContext, MainService::class.java))

        startService(Intent(applicationContext, MainService::class.java).putExtra("driver_id", driver.id))

        appPreferences.saveText("driver_id", driver.id.toString())
        appPreferences.saveText("service_id", service.id.toString())
        appPreferences.saveText("workday_id", workDay!!.id.toString())

        startActivity(intent)
        finish()
    }


    private fun requestLocationWrapper() {
        val hasWriteLocPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (hasWriteLocPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request), DialogInterface.OnClickListener { _, _ ->
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE)
                })
                return
            }
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE)
            return
        }
        startWorkDay()
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel), null)
                .setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok), okListener)
                .create()
                .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WorkShiftActivity.PERMISSION_PINGSERVICE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startWorkDay()
            } else {
                CustomToast.showMessage(this, getString(R.string.activities_MainActivity_permission_rejected))
            }

            WorkShiftActivity.PERMISSION_LOGGING -> if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                CustomToast.showMessage(this, getString(R.string.activities_MainActivity_permission_rejected_file))
            }

            WorkShiftActivity.PERMISSION_WINDOW -> {
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request_file), DialogInterface.OnClickListener { _, _ ->
                        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WorkShiftActivity.PERMISSION_LOGGING)
                    })
                    return
                }
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WorkShiftActivity.PERMISSION_LOGGING)
            }
        }
    }

    companion object {

        internal const val REQUEST_LOCATION = 199
    }
}
