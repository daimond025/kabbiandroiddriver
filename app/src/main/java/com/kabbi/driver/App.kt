package com.kabbi.driver

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.facebook.drawee.backends.pipeline.Fresco
import com.github.anrwatchdog.ANRWatchDog
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import org.greenrobot.eventbus.EventBus

class App : Application() {
   lateinit var service: Service
   lateinit var driver: Driver
   lateinit var order: Order
   lateinit var workDay: WorkDay

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        ANRWatchDog().setReportMainThreadOnly().start()
        EventBus.builder()
                .logNoSubscriberMessages(false)
                .sendNoSubscriberEvent(false)
                .installDefaultEventBus()
        Fresco.initialize(this)
    }

    override fun attachBaseContext(base: Context) {
        MultiDex.install(base)
        super.attachBaseContext(base)
    }

    companion object {
        lateinit var appContext: Context
            private set
    }
}