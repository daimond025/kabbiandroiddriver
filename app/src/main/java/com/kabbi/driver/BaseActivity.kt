package com.kabbi.driver

import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import java.util.*

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val appPreferences = AppPreferences(this)
        if (appPreferences.getText("favorite_lang").isNotEmpty()) {
            var locale: Locale? = null
            if (AppParams.LANG[Integer.valueOf(appPreferences.getText("favorite_lang"))].length > 2) {
                try {
                    val arrLang = AppParams.LANG[Integer.valueOf(appPreferences.getText("favorite_lang"))].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    locale = Locale(arrLang[0], arrLang[1])
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else {
                locale = Locale(AppParams.LANG[Integer.valueOf(appPreferences.getText("favorite_lang"))])
            }
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
        }
    }

    fun logEvent(id: String, name: String, event: String) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name)
        firebaseAnalytics.logEvent(event, bundle)
    }
}
