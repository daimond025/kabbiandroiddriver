package com.kabbi.driver


import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.events.CloseActivityEvent
import com.kabbi.driver.fragments.ReportTabFragment
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class ReportWorkDayActivity : BaseActivity() {

    internal var tabCount = 4
    internal var titles: MutableList<String> = arrayListOf()
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var tabLayout: TabLayout
    internal var driver: Driver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_report_workday)

        EventBus.getDefault().register(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_reports)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        titles = ArrayList()
        titles.add(getString(R.string.text_title_report_shift))
        titles.add(getString(R.string.text_title_report_week))
        titles.add(getString(R.string.text_title_report_month))
        titles.add(getString(R.string.text_title_report_interval))

        runBlocking {
            withContext(Dispatchers.IO){
                driver = AppDatabase.getInstance(this@ReportWorkDayActivity).driverDao().getById(intent.extras!!.getLong("driver_id"))
            }
        }

        viewPager = findViewById(R.id.report_pager)
        pagerAdapter = ReportAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter

        tabLayout = findViewById(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)

        val myColorStateList = ColorStateList(
                arrayOf(intArrayOf(android.R.attr.state_enabled), // enabled
                        intArrayOf(-android.R.attr.state_checked), // unchecked
                        intArrayOf(android.R.attr.state_pressed)  // pressed
                ),
                intArrayOf(Color.WHITE, Color.WHITE, Color.WHITE)
        )
        tabLayout.tabTextColors = myColorStateList
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe
    fun onEvent(closeActivityEvent: CloseActivityEvent) {
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private inner class ReportAdapter(fm: FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            var fragment: Fragment? = null
            val args = Bundle()
            args.putLong("driver_id", driver!!.id)

            when (position) {
                0 -> {
                    fragment = ReportTabFragment()
                    args.putInt("number_page", 1)
                }
                1 -> {
                    fragment = ReportTabFragment()
                    args.putInt("number_page", 2)
                }
                2 -> {
                    fragment = ReportTabFragment()
                    args.putInt("number_page", 3)
                }
                3 -> {
                    fragment = ReportTabFragment()
                    args.putInt("number_page", 4)
                }
            }
            fragment!!.arguments = args

            return fragment
        }

        override fun getCount(): Int {
            return tabCount
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titles[position]
        }
    }
}
