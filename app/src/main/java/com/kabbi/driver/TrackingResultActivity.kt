package com.kabbi.driver


import android.graphics.Color
import android.os.Bundle
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Route
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.map.DriverMap
import com.kabbi.driver.map.DriverMapGoogle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext


class TrackingResultActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var driverMap: DriverMap
    internal var order: Order? = null
    private var orderID: Int = 0
    private lateinit var rectLine: PolylineOptions
    internal lateinit var map: GoogleMap
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)

        orderID = intent.extras!!.getInt("orderID")
        db = AppDatabase.getInstance(this)
        runBlocking{
            withContext(Dispatchers.IO) {
                db.orderDao().getById(orderID.toLong())
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        driverMap = DriverMapGoogle()
        val mapFragment = SupportMapFragment.newInstance()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.fragment_map, mapFragment)
        fragmentTransaction.commit()
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        driverMap.initMap(applicationContext, googleMap)
        map = googleMap

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(56.844219, 53.242045), AppParams.ZOOM.toFloat()))

        var routes = listOf<Route>()
        runBlocking{
            withContext(Dispatchers.IO) {
                routes = db.routeDao().getRoutes(order!!.id)
            }
        }
        Log.i("ROUTES_SIZE", routes.size.toString())
        if (routes.size > 0) {
            rectLine = PolylineOptions().color(Color.parseColor("#0080FC"))
            for (route in routes) {
                Log.i("ORDER", route.speed.toString())
                Log.i("ORDER", route.lat.toString())
                Log.i("ORDER", route.lon.toString())
                rectLine.add(LatLng(route.lat, route.lon))
            }
            googleMap.addPolyline(rectLine)
        }
    }
}
