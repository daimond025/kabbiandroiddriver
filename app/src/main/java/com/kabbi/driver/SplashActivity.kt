package com.kabbi.driver

import android.content.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.service.MainService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class SplashActivity : AppCompatActivity() {

    private var appPreferences: AppPreferences? = null
    private var regid: String? = null
    private var mRegistrationBroadcastReceiver: BroadcastReceiver? = null
    private lateinit var service: Service
    private lateinit var driver: Driver
    private var workDayLast: WorkDay? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val db = AppDatabase.getInstance(this)

        if (!isTaskRoot) {
            finish()
            return
        }
        appPreferences = AppPreferences(this)
        runBlocking {
            withContext(Dispatchers.IO){
                workDayLast = db.workDayDao().getLastWorkDayEmpty()
            }
        }

        if (appPreferences!!.getText("theme") != AppParams.THEME_DEFAULT.toString() && appPreferences!!.getText("theme_flag") != "1") {
            appPreferences!!.saveText("theme", AppParams.THEME_DEFAULT.toString())
            appPreferences!!.saveText("theme_flag", "1")
        }

        if (workDayLast != null) {
            runBlocking {
                withContext(Dispatchers.IO){

                    service = db.serviceDao().getById(workDayLast!!.service)
                    service.also {
                        it.driver?.also {driverId ->
                            driver = AppDatabase.getInstance(this@SplashActivity).driverDao().getById(driverId)
                        }
                    }
                }
            }
            requestLocationWrapper()
        } else if (workDayLast == null) {
            if (appPreferences!!.getText("theme").isEmpty()) {
                appPreferences!!.saveText("theme", AppParams.THEME_DEFAULT.toString())
            }

            if (appPreferences!!.getText("favorite_lang").isEmpty()) {
                val lang = Locale.getDefault().language
                var count = 0
                for (item in AppParams.LANG) {
                    if (item == lang) {
                        appPreferences!!.saveText("favorite_lang", AppParams.DEFAULT_LANG)
                        break
                    }
                    count++
                }
            }

            val res = resources
            val dm = res.displayMetrics
            val conf = res.configuration
            if (appPreferences!!.getText("favorite_lang").isNotEmpty()) {
                if (AppParams.LANG[Integer.valueOf(appPreferences!!.getText("favorite_lang"))].length > 2) {
                    try {
                        val arrLang = AppParams.LANG[Integer.valueOf(appPreferences!!.getText("favorite_lang"))].split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        conf.locale = Locale(arrLang[0], arrLang[1])
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    conf.locale = Locale(AppParams.LANG[Integer.valueOf(appPreferences!!.getText("favorite_lang"))])
                }
            } else {
                conf.locale = Locale(AppParams.LANG[0])
            }
            res.updateConfiguration(conf, dm)

            mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val sentToken = sharedPreferences.getBoolean("sentTokenToServer", false)
                    if (sentToken) {
                        try {
                            regid = intent.extras!!.getString("token")
                            appPreferences!!.saveText("device_token", regid)
                            val intentEntrance = Intent(applicationContext, EntranceActivity::class.java)
                            startActivity(intentEntrance)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
            Handler().postDelayed({
                val intent = Intent(applicationContext, EntranceActivity::class.java)
                startActivity(intent)
            }, 3000)
        } else {
            CustomToast.showMessage(this, getString(R.string.text_shiftwork))
        }
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver!!, IntentFilter("registrationComplete"))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver!!)
    }

    private fun startWorkDay() {
        val intent = Intent(applicationContext, WorkShiftActivity::class.java)
        intent.putExtra("service_id", service.id)
        intent.putExtra("confirm", false)
        intent.putExtra("stageOrder", 0)
        intent.putExtra("showDialog", "empty")
        intent.putExtra("workday_id", workDayLast!!.id)
        intent.putExtra("showDialogGps", true)

        (application as App).driver = driver
        (application as App).service = service
        (application as App).workDay = workDayLast!!

        stopService(Intent(applicationContext, MainService::class.java))
        startService(Intent(applicationContext, MainService::class.java).putExtra("driver_id", driver.id))
        appPreferences!!.saveText("driver_id", driver.id.toString())
        appPreferences!!.saveText("service_id", service.id.toString())
        appPreferences!!.saveText("workday_id", workDayLast!!.id.toString())
        appPreferences!!.saveText("in_shift", "1")
        startActivity(intent)
        finish()
    }

    private fun requestLocationWrapper() {
        val hasWriteLocPermission = ContextCompat.checkSelfPermission(this@SplashActivity, android.Manifest.permission.ACCESS_FINE_LOCATION)
        if (hasWriteLocPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this@SplashActivity, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request), DialogInterface.OnClickListener { _, _ ->
                    ActivityCompat.requestPermissions(this@SplashActivity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE) })
                return
            }
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), WorkShiftActivity.PERMISSION_PINGSERVICE)
            return
        }
        startWorkDay()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == WorkShiftActivity.PERMISSION_PINGSERVICE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startWorkDay()
            } else {
                CustomToast.showMessage(this, getString(R.string.activities_MainActivity_permission_rejected))
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@SplashActivity)
                .setMessage(message)
                .setPositiveButton(getString(R.string.activities_MainActivity_permission_dialog_ok), okListener)
                .setNegativeButton(getString(R.string.activities_MainActivity_permission_dialog_cancel), null)
                .create()
                .show()
    }
}
