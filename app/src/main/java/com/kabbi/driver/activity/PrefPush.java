package com.kabbi.driver.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;


public class PrefPush extends AppCompatActivity {

    public static final int INTENT_RINGTONE = 15;
    public static final int INTENT_MUSIC = 16;

    private AppPreferences appPreferences;
    private CheckBox cbBalance, cbPreOrder, cbSos, cbSound,
            cbFreeOrderSound, cbPreOrderSound, cbFreeOrderShow, cbPreOrderShow;
    private TextView tvChangeMusic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferences = new AppPreferences(this);
        setTheme(Integer.valueOf(appPreferences.getText("theme")));
        setContentView(R.layout.activity_prefpush);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getString(R.string.title_toolbar_prefpush));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        cbBalance = (CheckBox) findViewById(R.id.checkbox_profile_prefpush_balance);
        cbPreOrder = (CheckBox) findViewById(R.id.checkbox_profile_prefpush_preorder);
        cbSos = (CheckBox) findViewById(R.id.checkbox_profile_prefpush_sos);
        cbSound = (CheckBox) findViewById(R.id.checkbox_profile_prefpush_sound);
        cbFreeOrderSound = (CheckBox) findViewById(R.id.checkbox_freeorder);
        cbPreOrderSound = (CheckBox) findViewById(R.id.checkbox_preorder);
        cbFreeOrderShow = (CheckBox) findViewById(R.id.checkbox_freeorder_show);
        cbPreOrderShow = (CheckBox) findViewById(R.id.checkbox_preorder_show);

        tvChangeMusic = (TextView) findViewById(R.id.tv_change_music);


        if (appPreferences.getText("push_balance").equals("1") || appPreferences.getText("push_balance").equals("")) {
            cbBalance.setChecked(true);
            appPreferences.saveText("push_balance", "1");
        } else {
            cbBalance.setChecked(false);
        }
        if (appPreferences.getText("push_preorder").equals("1") || appPreferences.getText("push_preorder").equals("")) {
            cbPreOrder.setChecked(true);
            appPreferences.saveText("push_preorder", "1");
        } else {
            cbPreOrder.setChecked(false);
        }
        if (appPreferences.getText("push_sos").equals("1") || appPreferences.getText("push_sos").equals("")) {
            cbSos.setChecked(true);
            appPreferences.saveText("push_sos", "1");
        } else {
            cbSos.setChecked(false);
        }
        if (appPreferences.getText("push_sound").equals("1") || appPreferences.getText("push_sound").equals("")) {
            cbSound.setChecked(true);
            appPreferences.saveText("push_sound", "1");
        } else {
            cbSound.setChecked(false);
        }
        if (appPreferences.getText("push_sound_freeorder").equals("1") || appPreferences.getText("push_sound_freeorder").equals("")) {
            cbFreeOrderSound.setChecked(true);
            appPreferences.saveText("push_sound_freeorder", "1");
        } else {
            cbFreeOrderSound.setChecked(false);
        }
        if (appPreferences.getText("push_sound_preorder").equals("1") || appPreferences.getText("push_sound_preorder").equals("")) {
            cbPreOrderSound.setChecked(true);
            appPreferences.saveText("push_sound_preorder", "1");
        } else {
            cbPreOrderSound.setChecked(false);
        }
        if (appPreferences.getText("push_show_freeorder").equals("1") || appPreferences.getText("push_show_freeorder").equals("")) {
            cbFreeOrderShow.setChecked(true);
            appPreferences.saveText("push_show_freeorder", "1");
        } else {
            cbFreeOrderShow.setChecked(false);
        }
        if (appPreferences.getText("push_show_preorder").equals("1") || appPreferences.getText("push_show_preorder").equals("")) {
            cbPreOrderShow.setChecked(true);
            appPreferences.saveText("push_show_preorder", "1");
        } else {
            cbPreOrderShow.setChecked(false);
        }

        cbBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbBalance.isChecked())
                    appPreferences.saveText("push_balance", "1");
                else
                    appPreferences.saveText("push_balance", "0");
            }
        });
        cbPreOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbPreOrder.isChecked())
                    appPreferences.saveText("push_preorder", "1");
                else
                    appPreferences.saveText("push_preorder", "0");
            }
        });
        cbSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbSos.isChecked())
                    appPreferences.saveText("push_sos", "1");
                else
                    appPreferences.saveText("push_sos", "0");
            }
        });
        cbSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbSound.isChecked())
                    appPreferences.saveText("push_sound", "1");
                else
                    appPreferences.saveText("push_sound", "0");
            }
        });
        cbFreeOrderSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbFreeOrderSound.isChecked())
                    appPreferences.saveText("push_sound_freeorder", "1");
                else
                    appPreferences.saveText("push_sound_freeorder", "0");
            }
        });
        cbPreOrderSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbPreOrderSound.isChecked())
                    appPreferences.saveText("push_sound_preorder", "1");
                else
                    appPreferences.saveText("push_sound_preorder", "0");
            }
        });
        cbFreeOrderShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbFreeOrderShow.isChecked())
                    appPreferences.saveText("push_show_freeorder", "1");
                else
                    appPreferences.saveText("push_show_freeorder", "0");
            }
        });
        cbPreOrderShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbPreOrderShow.isChecked())
                    appPreferences.saveText("push_show_preorder", "1");
                else
                    appPreferences.saveText("push_show_preorder", "0");
            }
        });

        tvChangeMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ChangeSoundsActivity.class));
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case INTENT_RINGTONE:
                    Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    if (uri != null) {
                        Log.d("RINGTONE", uri.toString());
                        appPreferences.saveText("ringtone", uri.toString());
                    }
                    break;
                case INTENT_MUSIC:

                    break;
                default:
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
