package com.kabbi.driver.activity

import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.App
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.NetworkConfidentialPageEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class ConfidentialPageActivity : BaseActivity() {
    private var webView: WebView? = null
    private var appPreferences: AppPreferences? = null
    private var progressDialog: ProgressDialog? = null
    private lateinit var service: Service
    private lateinit var driver: Driver
    private var isRedirected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_webview)
        setTitle(R.string.activities_WebViewActivity_title)
        EventBus.getDefault().register(this)
        initToolbar()
        initViews()
        initVars()
    }

    private fun initToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.title = "  " + getString(R.string.confidential_title)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViews() {
        val webChromeClient = WebChromeClient()
        val client: WebViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView?, url: String?) {
                if (progressDialog != null) progressDialog!!.cancel()
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                url?.also{mUrl ->
                    if (mUrl.contains("payment_success") || mUrl.contains("errors")) {
                        isRedirected = true
                    }
                }
            }
        }
        webView = findViewById<View>(R.id.webview_card) as WebView
        webView?.also{view ->
            view.webViewClient = client
            view.webChromeClient = webChromeClient
            view.settings.javaScriptEnabled = true
        }

        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(getString(R.string.activities_CardActivity_text_load))
        progressDialog!!.show()
    }

    private fun initVars() {
        service = (application as App).service
        driver = (application as App).driver

        val params: MutableMap<String, String> = LinkedHashMap()
        params["position_id"] = appPreferences!!.getText("position_id")
        params["tenant_login"] = service.uri
        params["worker_city_id"] = appPreferences!!.getText("city_id")
        params["worker_login"] = driver.callSign
        WebService.getConfidentialPage(params, driver.secretCode)
    }

    @Subscribe
    fun onEvent(networkConfidentialPageEvent: NetworkConfidentialPageEvent) {
        progressDialog!!.dismiss()
        if (networkConfidentialPageEvent.status == "OK") {
            val webPageString = networkConfidentialPageEvent.page
            if (webPageString.isNotEmpty()) {
                webView!!.loadDataWithBaseURL(null, webPageString, "text/html", "UTF-8", null)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}