package com.kabbi.driver.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.kabbi.driver.App
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.WorkActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.CheckFakeGpsEvent
import com.kabbi.driver.events.NetworkEndWorkEvent
import com.kabbi.driver.events.SatelliteInfoEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.service.MainService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class FakeGpsActivity : BaseActivity() {

    private var appPreferences: AppPreferences? = null
    private var tvSatellites: TextView? = null
    private var service: Service? = null
    private var driver: Driver? = null
    private var workDay: WorkDay? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_fakegps)

        val buttonOpenSettings = findViewById<Button>(R.id.btn_opensettings)
        val btnCloseShift = findViewById<Button>(R.id.btn_close_shift)
        val btnReloadService = findViewById<Button>(R.id.btn_reload_service)
        val tvDesc = findViewById<TextView>(R.id.tv_fakegps_desc)
        tvSatellites = findViewById(R.id.tv_sattelites)

        EventBus.getDefault().register(this)


        workDay = (application as App).workDay
        driver = (application as App).driver
        service = (application as App).service
        val versionFlag = intent.getIntExtra("fake_gps", 1)

        appPreferences!!.saveText("fake_gps", "1")

        if (versionFlag == 2) {
            buttonOpenSettings.visibility = View.INVISIBLE
            tvDesc.text = getString(R.string.off_fakegps_18)
        } else {
            tvDesc.text = getString(R.string.off_fakegps_15)
        }

        buttonOpenSettings.setOnClickListener { startActivity(Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS)) }
        btnCloseShift.setOnClickListener {
            val requestParams = LinkedHashMap<String, String>()
            requestParams["car_mileage"] = "0"
            requestParams["tenant_login"] = service!!.uri
            requestParams["worker_login"] = driver!!.callSign
            WebService.endWork(requestParams, driver!!.secretCode)
        }
        btnReloadService.setOnClickListener {
            stopService(Intent(applicationContext, MainService::class.java))
            startService(Intent(applicationContext, MainService::class.java).putExtra("driver_id", driver!!.id))
        }

    }

    override fun onDestroy() {
        appPreferences!!.saveText("fake_gps", "0")
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun onBackPressed() {

    }

    @Subscribe
    fun onEvent(checkFakeGpsEvent: CheckFakeGpsEvent) {
        if (checkFakeGpsEvent.code == 0) finish()
    }

    @Subscribe
    fun onEvent(endWorkEvent: NetworkEndWorkEvent) {
        if (endWorkEvent.status == "OK") {
            workDay!!.endWorkDay = Calendar.getInstance().time.time
            runBlocking {
                withContext(Dispatchers.IO) {
                    AppDatabase.getInstance(this@FakeGpsActivity).workDayDao().save(workDay!!)
                }
            }

            try {
                stopService(Intent(applicationContext, MainService::class.java))
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            try {
                appPreferences!!.saveText("in_shift", "0")

                val intent = Intent(applicationContext, WorkActivity::class.java)
                intent.putExtra("service_id", service!!.id)
                startActivity(intent)
                finish()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        }
    }

    @SuppressLint("StringFormatInvalid")
    @Subscribe
    fun onEvent(satelliteInfoEvent: SatelliteInfoEvent) {
        try {
            tvSatellites!!.text = String.format(getString(R.string.search_sattelite), " " + satelliteInfoEvent.satelliteCoustUsed + "/" + satelliteInfoEvent.satelliteCount)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
