package com.kabbi.driver.activity

import android.app.Activity
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.daimajia.swipe.SwipeLayout
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.SoundDao
import com.kabbi.driver.database.entities.Sound
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.*

class ChangeSoundsActivity : BaseActivity() {
    private lateinit var llContainer: LinearLayout
    private lateinit var sounds: List<Sound>
    private lateinit var selectedSound: Sound
    private lateinit var soundDao: SoundDao
    private val job = Job()
    private var scope = CoroutineScope(Dispatchers.Main + job)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        soundDao = AppDatabase.getInstance(this).soundDao()
        val appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_changemusic)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_changemusic)

        @Suppress("DEPRECATION")
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        llContainer = findViewById(R.id.ll_container_music)

        scope.launch {
            withContext(Dispatchers.IO) {
                sounds = soundDao.getAll()
            }
            updateListSounds(sounds)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                INTENT_RINGTONE -> {
                    val uri = data!!.getParcelableExtra<Uri>(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)
                    if (uri != null) {
                        selectedSound.name = RingtoneManager.getRingtone(applicationContext, uri).getTitle(applicationContext)
                        selectedSound.path = uri.toString()
                        selectedSound.isDefault = false
                        scope.launch {
                            withContext(Dispatchers.IO) {
                                soundDao.save(selectedSound)
                                sounds = soundDao.getAll()
                            }
                            updateListSounds(sounds)
                        }
                    }
                }

                INTENT_MUSIC -> {
                }
            }
        }
    }

    private fun updateListSounds(sounds: List<Sound>) {
        llContainer.removeAllViews()
        if (sounds.isNotEmpty()) {
            for (sound in sounds) {
                val layoutView = layoutInflater.inflate(R.layout.item_nested, null, false)
                val swipeLayout = layoutView.findViewById<SwipeLayout>(R.id.test_swipe_swipe)
                val tvSound = layoutView.findViewById<TextView>(R.id.tv_sound)
                swipeLayout.setOnClickListener { swipeLayout.toggle() }
                layoutView.findViewById<View>(R.id.tv_sound_default).setOnClickListener {
                    sound.path = getDefaultPath(sound.desc)
                    sound.isDefault = true
                    scope.launch {
                        withContext(Dispatchers.IO) {
                            soundDao.save(sound)
                        }
                    }
                    swipeLayout.toggle()
                    tvSound.text = getLabel(sound.desc) + "\n" + getString(R.string.standart_sound)
                }
                layoutView.findViewById<View>(R.id.tv_sound_choose).setOnClickListener {
                    val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone")
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, null as Uri?)
                    startActivityForResult(intent, INTENT_RINGTONE)
                    selectedSound = sound
                    swipeLayout.toggle()
                }
                tvSound.text = getLabel(sound.desc) + "\n" + if (sound.isDefault) getString(R.string.standart_sound) else sound.name
                val dividerView = layoutInflater.inflate(R.layout.divider, null, false)
                llContainer.addView(layoutView)
                llContainer.addView(dividerView)
            }
        }
    }

    private fun getDefaultPath(desc: String): String {
        val prefix = "android.resource://$packageName/"

        return when (desc) {
            "ringtone_pop" -> return prefix + R.raw.pop
            "ringtone_cute_bells" -> return prefix + R.raw.cute_bells
            "ringtone_free_order" -> return prefix + R.raw.free_order
            "ringtone_mirimba_chord" -> return prefix + R.raw.marimba_chord
            "ringtone_own" -> return prefix + R.raw.own
            "ringtone_preorder" -> return prefix + R.raw.pre_order
            "ringtone_rejected" -> return prefix + R.raw.rejected
            "ringtone_remind_preorder" -> return prefix + R.raw.remind_preorder
            "ringtone_sos" -> return prefix + R.raw.sos
            else -> ""
        }
    }

    private fun getLabel(desc: String): String {
        return when (desc) {
            "ringtone_pop" -> return getString(R.string.ringtone_pop)
            "ringtone_cute_bells" -> return getString(R.string.ringtone_cute_bells)
            "ringtone_free_order" -> return getString(R.string.ringtone_free_order)
            "ringtone_mirimba_chord" -> return getString(R.string.ringtone_mirimba_chord)
            "ringtone_own" -> return getString(R.string.ringtone_own)
            "ringtone_preorder" -> return getString(R.string.ringtone_preorder)
            "ringtone_rejected" -> return getString(R.string.ringtone_rejected)
            "ringtone_remind_preorder" -> return getString(R.string.ringtone_remind_preorder)
            "ringtone_sos" -> return getString(R.string.ringtone_sos)
            else -> ""
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        const val INTENT_RINGTONE = 15
        const val INTENT_MUSIC = 16
    }

    override fun onStop() {
        job.cancel()
        super.onStop()
    }
}