package com.kabbi.driver.activity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kabbi.driver.R;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.util.CustomToast;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DeviceListActivity extends AppCompatActivity {

    private AppPreferences appPreferences;
    private TypedValue typedValueMain, typedValueSubscribe;
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private List<BluetoothDevice> mDevices;
    private ListView listVew;
    private Set<BluetoothDevice> mPairedDevices;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appPreferences = new AppPreferences(this);
        setTheme(Integer.valueOf(appPreferences.getText("theme")));
        setContentView(R.layout.activity_devicelist);

        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();
        initToolbar();
        listVew = (ListView) findViewById(R.id.lv_devices);
        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        mDevices = new ArrayList<>();

        listVew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CustomToast.showMessage(getApplicationContext(), mDevices.get(position).getName());
                setResult(RESULT_OK, new Intent().putExtra("address", mDevices.get(position).getAddress()));
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        if (mBluetoothAdapter == null) {
            CustomToast.showMessage(this, getString(R.string.profile_bluetooth_empty));
        } else if (mBluetoothAdapter.isEnabled()) {
            mPairedDevices = mBluetoothAdapter.getBondedDevices();
            if (mPairedDevices.size() > 0) {
                for (BluetoothDevice mDevice : mPairedDevices) {
                    mPairedDevicesArrayAdapter.add(mDevice.getName() + "\n" + mDevice.getAddress());
                    mDevices.add(mDevice);
                }
                listVew.setAdapter(mPairedDevicesArrayAdapter);
            }

        } else {
            CustomToast.showMessage(this, getString(R.string.profile_bluetooth_off));
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getString(R.string.devices));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
