package com.kabbi.driver.activity;


import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.fragments.MapDialogFragment;
import com.kabbi.driver.fragments.NavigatorChangeDialogFragment;
import com.kabbi.driver.fragments.NavigatorDialogFragment;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;

import java.util.Locale;

public class PrefMap extends AppCompatActivity {

    TextView tvMap, tvNav;
    CheckBox checkBox;
    AppPreferences appPreferences;
    private TypedValue typedValueMain, typedValueSubscribe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferences = new AppPreferences(this);
        setTheme(Integer.valueOf(appPreferences.getText("theme")));
        setContentView(R.layout.activity_prefmap);

        typedValueMain = new TypedValue();
        typedValueSubscribe = new TypedValue();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("  " + getString(R.string.title_toolbar_prefmap));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        tvMap = findViewById(R.id.textview_profile_prefmap_map);
        tvNav = findViewById(R.id.textview_profile_prefmap_nav);
        checkBox = findViewById(R.id.checkbox_profile_prefmap_bearing);

        if (appPreferences.getText("bearing_map").equals("1")) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        String navMap = AppParams.getMapLabel(getApplicationContext(), Integer.valueOf(appPreferences.getText("favorite_map")));
        if (Locale.getDefault().getLanguage().equals("fa")) {
            switch (AppParams.getMapLabel(getApplicationContext(), Integer.valueOf(appPreferences.getText("favorite_map")))) {
                case "Google":
                    navMap = getString(R.string.dialog_nav_google);
                    break;
                case "Open Street Map":
                    navMap = getString(R.string.dialog_nav_osm);
                    break;
            }
        }

        tvMap.setText(Html.fromHtml("<font color='"+String.format("#%06X", 0xFFFFFF & typedValueMain.data)+"'>"
                + getString(R.string.text_profile_map) + "</font><br /><small><font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                + navMap
                + "</font></small>"));

        tvNav.setText(Html.fromHtml("<font color='"+String.format("#%06X", 0xFFFFFF & typedValueMain.data)+"'>"
                + getString(R.string.text_profile_prefmap_nav) + "</font><br /><small><font color='" + String.format("#%06X", 0xFFFFFF & typedValueSubscribe.data) + "'>"
                + appPreferences.getText("navigation")
                + "</font></small>"));


        tvMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new MapDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "dialogFragmentMap");
            }
        });

        tvNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new NavigatorDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), "dialogFragmentNav");
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    appPreferences.saveText("bearing_map", "1");
                } else {
                    appPreferences.saveText("bearing_map", "0");
                }
            }
        });

        (findViewById(R.id.fl_far_navi)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new NavigatorChangeDialogFragment().show(getSupportFragmentManager(), "dialogFragment_navi");
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
