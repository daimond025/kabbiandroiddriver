package com.kabbi.driver.activity


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Parking
import com.kabbi.driver.database.entities.Route
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.map.DriverMap
import com.kabbi.driver.map.DriverMapGoogle
import com.kabbi.driver.map.DriverMapOsm
import com.kabbi.driver.map.DriverMapYandex
import kotlinx.coroutines.*
import java.util.*

class RouteMap : AppCompatActivity(), OnMapReadyCallback {

    internal var routes: List<Route> = arrayListOf()
    private lateinit var driverMap: DriverMap
    internal lateinit var appPreferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_routemap)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_routemap)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        when {
            appPreferences.getText("favorite_map") == "3" -> {
                driverMap = DriverMapYandex("normal")

                driverMap.initMap(applicationContext)
                (findViewById<View>(R.id.fragment_map) as FrameLayout).addView(driverMap.mapView)
            }
            appPreferences.getText("favorite_map") == "2" -> {
                driverMap = DriverMapOsm()
                driverMap.initMap(applicationContext)
                (findViewById<View>(R.id.fragment_map) as FrameLayout).addView(driverMap.mapView)
            }
            appPreferences.getText("favorite_map") == "1" -> {
                driverMap = DriverMapGoogle()
                val mapFragment = SupportMapFragment.newInstance()
                val fragmentTransaction = supportFragmentManager.beginTransaction()
                fragmentTransaction.add(R.id.fragment_map, mapFragment)
                fragmentTransaction.commit()
                mapFragment.getMapAsync(this)
            }
        }
        if (appPreferences.getText("favorite_map") == "0") {
            driverMap = DriverMapYandex("narod")
            driverMap.initMap(applicationContext)
            (findViewById<View>(R.id.fragment_map) as FrameLayout).addView(driverMap.mapView)
        }

        routes = ArrayList()
        val db = AppDatabase.getInstance(this)
        runBlocking {
            withContext(Dispatchers.IO){
                val order = db.orderDao().getById(intent.extras!!.getLong("order_id"))
                routes = db.routeDao().getRoutes(order.id)
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        driverMap.initMap(applicationContext, googleMap)
        if (routes.isNotEmpty()) {
            driverMap.setCenterMap(routes[0].lat, routes[0].lon)
            CoroutineScope(Dispatchers.Main).launch {
                var parking: Parking? = null
                withContext(Dispatchers.IO) {
                    val db = AppDatabase.getInstance(this@RouteMap)
                    val service = db.serviceDao().getById(intent.extras!!.getLong("service_id"))
                    parking = db.parkingDao().getBase(service.id)
                }
                driverMap.setPathRoutes(routes, parking)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
