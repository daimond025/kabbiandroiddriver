package com.kabbi.driver.activity.card

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.browser.customtabs.CustomTabsIntent
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.adapters.CardAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.*
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class CardProfitListActivity : BaseActivity() {
    private var appPreferences: AppPreferences? = null
    private val swipeRefreshLayout: androidx.swiperefreshlayout.widget.SwipeRefreshLayout? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var service: Service? = null
    private var driver: Driver? = null
    private var listView: ListView? = null
    private var cards: MutableList<String>? = null
    private var cardAdapter: CardAdapter? = null
    private var fab: FloatingActionButton? = null
    private var progressDialog: ProgressDialog? = null
    private var checkProgressDialog: ProgressDialog? = null
    private var orderId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_cardlist)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@CardProfitListActivity)
                service = db.serviceDao().getById(intent.extras!!.getLong("service_id"))
                driver = db.driverDao().getById(service!!.driver!!)
            }
        }
        cards = ArrayList()

        checkProgressDialog = ProgressDialog(this)
        checkProgressDialog!!.setMessage(getString(R.string.activities_CardActivity_check_text_load))

        EventBus.getDefault().register(this)
        initToolbar()
        initViews()
        getBankCards()
    }

    private fun getBankCards() {
        val paramsBankCard = LinkedHashMap<String, String>()
        paramsBankCard["tenant_login"] = service!!.uri
        paramsBankCard["worker_login"] = driver!!.callSign

        WebService.getBankCards(paramsBankCard, driver!!.secretCode)
    }

    @Subscribe
    fun onEvent(createBankCard: NetworkCreateBankCard) {
        try {
            if (progressDialog!!.isShowing)
                progressDialog!!.dismiss()
            if (Build.VERSION.SDK_INT <= 19) {
                if (createBankCard.status == "OK") {
                    orderId = createBankCard.orderId
                    val webPageString = createBankCard.url
                    if (webPageString.isNotEmpty()) {

                        val builder = CustomTabsIntent.Builder()
                        builder.setToolbarColor(Color.WHITE)
                        builder.setShowTitle(true)
                        val customTabsIntent = builder.build()
                        try {
                            customTabsIntent.launchUrl(this, Uri.parse("googlechrome://navigate?url=$webPageString"))
                        } catch (e: Exception) {
                            customTabsIntent.launchUrl(this, Uri.parse(webPageString))
                        }

                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    @Subscribe
    fun onEvent(bankCards: NetworkBankCards) {
        cards!!.clear()
        if (bankCards.cards != null && bankCards.cards.size > 0)
            cards = bankCards.cards
        cardAdapter = CardAdapter(applicationContext, cards, typedValueMain!!.data, typedValueSubscribe!!.data)
        listView!!.adapter = cardAdapter
    }

    @Subscribe
    fun onEvent(deleteCardEvent: DeleteCardEvent) {
        if (InternetConnection.isOnline(applicationContext)) {
            progressDialog = ProgressDialog.show(this@CardProfitListActivity, null, getString(R.string.delete_card), false)
            val paramsBankCard = LinkedHashMap<String, String>()
            paramsBankCard["pan"] = deleteCardEvent.pan
            paramsBankCard["tenant_login"] = service!!.uri
            paramsBankCard["worker_login"] = driver!!.callSign
            val uuidObject = UUID.randomUUID()
            WebService.deleteProfitBankCard(paramsBankCard, driver!!.secretCode, uuidObject.toString())
        } else {
            CustomToast.showMessage(applicationContext, getString(R.string.text_internet_access))
        }
    }

    @Subscribe
    fun onEvent(deleteBankCard: NetworkDeleteBankCard) {
        try {
            if (progressDialog != null)
                progressDialog!!.dismiss()
            if (deleteBankCard.status == "OK") {
                getBankCards()
            } else {
                CustomToast.showMessage(applicationContext, deleteBankCard.status)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.your_cards)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViews() {
        listView = findViewById(R.id.lv_cards)
        listView!!.setOnItemClickListener { parent, view, position, id ->
            setResult(Activity.RESULT_OK, Intent().putExtra("pan", (view.findViewById<View>(R.id.textview_detail_list_card) as TextView).text.toString()))
            finish()
        }
        fab = findViewById(R.id.fab)
        fab!!.setOnClickListener { v ->

            if (Build.VERSION.SDK_INT > 19) {
                val webViewIntent = Intent(applicationContext, WebViewProfitActivity::class.java)
                webViewIntent.putExtra("service_id", service!!.id)
                startActivityForResult(webViewIntent, CardProfitActivity.PAYMENT_LIST_REQ_CODE)
            } else {
                progressDialog = ProgressDialog.show(this@CardProfitListActivity, null, getString(R.string.activities_CardActivity_text_load), false)

                val paramsBankCard = LinkedHashMap<String, String>()
                paramsBankCard["tenant_login"] = service!!.uri
                paramsBankCard["worker_login"] = driver!!.callSign
                val uuidObject = UUID.randomUUID()
                WebService.createProfitBankCard(paramsBankCard, driver!!.secretCode, uuidObject.toString())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CardProfitActivity.PAYMENT_LIST_REQ_CODE && resultCode == Activity.RESULT_OK) {
            val returnKey = data!!.getStringExtra(WebViewProfitActivity.RESULT_SUCCESS_KEY)
            if (returnKey != null && returnKey == WebViewProfitActivity.RESULT_SUCCESS_CARD) {
                val paramsBankCard = LinkedHashMap<String, String>()
                paramsBankCard["tenant_login"] = service!!.uri
                paramsBankCard["worker_login"] = driver!!.callSign
                WebService.getBankCards(paramsBankCard, driver!!.secretCode)
                CustomToast.showMessage(applicationContext,
                        getString(R.string.activities_PaymentActivity_add_card_success))
            }
        } else {
            CustomToast.showMessage(applicationContext,
                    getString(R.string.activities_PaymentActivity_add_card_error))
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK, Intent().putExtra("pan", ""))
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        if (orderId != null) {
            checkProgressDialog!!.show()
            val paramsBankCard = LinkedHashMap<String, String>()
            paramsBankCard["order_id"] = orderId!!
            paramsBankCard["tenant_login"] = service!!.uri
            paramsBankCard["worker_login"] = driver!!.callSign
            val uuidObject = UUID.randomUUID()
            WebService.checkBankCards(paramsBankCard, driver!!.secretCode, uuidObject.toString())
        }
    }

    @Subscribe
    fun onEvent(checkBankCard: NetworkCheckBankCard) {
        when (checkBankCard.status) {
            "OK" -> {
                val paramsBankCard = LinkedHashMap<String, String>()
                paramsBankCard["tenant_login"] = service!!.uri
                paramsBankCard["worker_login"] = driver!!.callSign
                WebService.getBankCards(paramsBankCard, driver!!.secretCode)
                orderId = null
            }
            "REQUEST_PROCESSING" -> {

                CustomToast.showMessage(applicationContext,
                        getString(R.string.activities_PaymentActivity_add_card_wait))

                val paramsBankCard = LinkedHashMap<String, String>()
                paramsBankCard["tenant_login"] = service!!.uri
                paramsBankCard["worker_login"] = driver!!.callSign
                WebService.getBankCards(paramsBankCard, driver!!.secretCode)
                orderId = null
            }
            else -> {
                CustomToast.showMessage(applicationContext,
                        getString(R.string.activities_PaymentActivity_add_card_error))
            }
        }
        checkProgressDialog!!.dismiss()
    }
}
