package com.kabbi.driver.activity.card

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.NetworkBankCards
import com.kabbi.driver.events.NetworkCreateBankCard
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class WebViewProfitActivity : BaseActivity() {

    private var webView: WebView? = null
    private var progressDialog: ProgressDialog? = null
    private var checkProgressDialog: ProgressDialog? = null
    private var service: Service? = null
    private var driver: Driver? = null
    private var appPreferences: AppPreferences? = null
    private var isRedirected: Boolean = false
    private var orderId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_webview)
        setTitle(R.string.activities_WebViewActivity_title)
        EventBus.getDefault().register(this)

        initToolbar()
        initViews()
        initVars()
    }


    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.add_card)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViews() {
        val webChromeClient = WebChromeClient()
        val client = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                if (progressDialog != null) progressDialog!!.cancel()

            }

            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
                if (url.contains("payment_success") || url.contains("errors")) {
                    isRedirected = true
                }
            }
        }
        webView = findViewById(R.id.webview_card)
        if (webView != null) {
            webView!!.webViewClient = client
            webView!!.webChromeClient = webChromeClient
            webView!!.settings.javaScriptEnabled = true
        }
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(getString(R.string.activities_CardActivity_text_load))
        progressDialog!!.show()
        checkProgressDialog = ProgressDialog(this)
        checkProgressDialog!!.setMessage(getString(R.string.activities_CardActivity_check_text_load))
    }

    private fun initVars() {

        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@WebViewProfitActivity)
                service = db.serviceDao().getById(intent.extras!!.getLong("service_id"))
                driver = db.driverDao().getById(service!!.driver!!)
            }
        }

        val paramsBankCard = LinkedHashMap<String, String>()
        paramsBankCard["tenant_login"] = service!!.uri
        paramsBankCard["worker_login"] = driver!!.callSign
        val uuidObject = UUID.randomUUID()
        WebService.createProfitBankCard(paramsBankCard, driver!!.secretCode, uuidObject.toString())
    }

    @Subscribe
    fun onEvent(createBankCard: NetworkCreateBankCard) {
        progressDialog!!.dismiss()
        if (createBankCard.status == "OK") {
            orderId = createBankCard.orderId
            val webPageString = createBankCard.url
            if (!webPageString.isEmpty()) {
                //Log.d("URL_CARD", webPageString);
                webView!!.loadUrl(webPageString)
            }
        } else {
            setResult(Activity.RESULT_CANCELED, Intent())
            finish()
        }
    }

    @Subscribe
    fun onEvent(networkBankCards: NetworkBankCards) {
        val returnIntent = Intent()
        if (networkBankCards.status == "OK" && networkBankCards.cards.size > 0) {
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_CARD)
            setResult(Activity.RESULT_OK, returnIntent)
        } else {
            CustomToast.showMessage(applicationContext, networkBankCards.status)
            setResult(Activity.RESULT_CANCELED, returnIntent)
        }
        checkProgressDialog!!.cancel()
        CustomToast.showMessage(this, "END_CHECK")
        finish()
    }

    override fun onBackPressed() {
        if (isRedirected) {
            checkProgressDialog!!.show()
            val paramsBankCard = LinkedHashMap<String, String>()
            paramsBankCard["tenant_login"] = service!!.uri
            paramsBankCard["worker_login"] = driver!!.callSign
            WebService.getProfitBankCard(paramsBankCard, driver!!.secretCode)
        } else {
            val returnIntent = Intent()
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_NULL)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    companion object {

        val RESULT_SUCCESS_KEY = "card_activity.result_success_type"
        val RESULT_SUCCESS_CARD = "card_activity.result_success_card"
        val RESULT_SUCCESS_NULL = "card_activity.result_success_null"
    }

}
