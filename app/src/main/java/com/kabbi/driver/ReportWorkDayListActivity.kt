package com.kabbi.driver

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import com.kabbi.driver.adapters.WorkDayAdapter
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.CloseActivityEvent
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class ReportWorkDayListActivity : BaseActivity(), AdapterView.OnItemClickListener {

    private var workDays: MutableList<WorkDay> = mutableListOf()
    private var workDayIds: ArrayList<String> = arrayListOf()
    internal lateinit var adapter: WorkDayAdapter
    private lateinit var lvReportWorkDay: ListView
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_report_workdaylist)
        EventBus.getDefault().register(this)
        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_workday)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        lvReportWorkDay = findViewById(R.id.listview_report_workday)
        workDays = ArrayList()
        workDayIds = intent.getStringArrayListExtra("workday_ids")

        for (id in workDayIds) {
            runBlocking {
                withContext(Dispatchers.IO){
                  val day =  AppDatabase.getInstance(this@ReportWorkDayListActivity).workDayDao().getById(id.toLong())
                    workDays.add(day)
                }
            }
        }

        if (workDays.size > 0) {
            adapter = WorkDayAdapter(applicationContext, workDays, typedValueMain!!.data, typedValueSubscribe!!.data)
            lvReportWorkDay.onItemClickListener = this
            lvReportWorkDay.adapter = adapter
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe
    fun onEvent(closeActivityEvent: CloseActivityEvent) {
        finish()
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val intent = Intent(applicationContext, ReportOrderListActivity::class.java)
        intent.putExtra("workday_id", java.lang.Long.valueOf(view.tag.toString()))
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}


