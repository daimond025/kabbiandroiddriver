package com.kabbi.driver.network.responses

import com.google.gson.annotations.SerializedName

data class BalanceResponse(
        var status: String,
        @SerializedName("worker_balance")
        val balance: String,
        val currency: String)