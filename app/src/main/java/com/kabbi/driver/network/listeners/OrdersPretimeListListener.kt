package com.kabbi.driver.network.listeners

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.kabbi.driver.events.NetworkOrdersListPretimeEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.DistanceGPS
import com.kabbi.driver.models.OpenOrder
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import java.util.*

object OrdersPretimeListListener {

    private lateinit var appPreferences: AppPreferences

    fun onRequestSuccess(response: ApiResponse, context: Context, type: String) = CoroutineScope(Dispatchers.IO).launch {
        try {
            appPreferences = AppPreferences(context)

            if (response.info == "OK") {

                response.result?.also { result ->
                    val orders = mutableListOf<OpenOrder>()
                    val jsonArray = result.asJsonObject.getAsJsonArray("orders_open_list").toString()
                    val jsonArrayBusy = result.asJsonObject.getAsJsonArray("worker_pre_orders").toString()

                    orders.addAll(getItems(jsonArray, "reserve"))
                    orders.addAll(getItems(jsonArrayBusy, "reserve_br"))

                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkOrdersListPretimeEvent(response.info, type, orders))
                    }
                }

            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOrdersListPretimeEvent(response.info, type, ArrayList()))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getItems(stringArray: String, type: String): List<OpenOrder> {

        try {
            val ordersArray = Gson().fromJson(stringArray, Array<OpenOrder>::class.java)
            val orders = ordersArray.toMutableList()

            Log.d("orders", orders.toString() + orders.isEmpty())

            if (orders.isEmpty()) {
                return listOf()
            }

            for (order in orders) {
                try {
                    order.distance = DistanceGPS.getDistanceRound(order.from.lat, order.from.lng, appPreferences.getText("lat").toDouble(), appPreferences.getText("lon").toDouble())
                } catch (e: Exception) {
                }
                order.type = type
            }
            return orders

        } catch (e: Exception) {
            return listOf()
//            this.orders.addAll(orders)
        }
    }
}
