package com.kabbi.driver.network.responses

import com.google.gson.annotations.SerializedName

data class AuthWorkerResponse(
        var status: String? = null,
        @SerializedName("company_name")
        var companyName: String? = null,
        @SerializedName("company_logo")
        var companyLogo: String? = null,
        @SerializedName("worker_secret_key")
        var workerSecretKey: String? = null
)