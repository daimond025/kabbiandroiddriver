package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkRefillAccount
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object RefillAccountListener {
    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkRefillAccount("FAIL"))
        }
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")

            if (info == "OK") {
                val result = jsonObject.getJSONObject("result")
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkRefillAccount(info, result.getString("worker_balance"), result.getString("currency")))
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkRefillAccount(info))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}