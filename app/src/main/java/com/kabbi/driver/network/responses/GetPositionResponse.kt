package com.kabbi.driver.network.responses

import com.kabbi.driver.models.Position

data class GetPositionResponse(
        var status: String,
        var positions: List<Position> = arrayListOf())