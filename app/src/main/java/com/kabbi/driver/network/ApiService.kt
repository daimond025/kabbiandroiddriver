package com.kabbi.driver.network

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.http.*


interface ApiService {

    @GET("/v3/api/auth_worker")
    suspend fun authWorker(@QueryMap params: Map<String?, String?>?): ApiResponse

    @GET("/v3/api/get_worker_balance")
    suspend fun getBalance(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: LinkedHashMap<String?, String?>?): ApiResponse

    @GET("/v3/api/get_worker_cities")
    suspend fun getCities(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: LinkedHashMap<String?, String?>?): ApiResponse

    @GET("/v3/api/get_worker_positions")
    suspend fun getPositions(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: LinkedHashMap<String, String>?): ApiResponse

    @GET("/v3/api/get_worker_profile")
    suspend fun getProfile(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/worker_start_work")
    suspend fun startWorkDay(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: LinkedHashMap<String, String>?): ApiResponse

    @GET("/v3/api/get_parkings_list")
    suspend fun getParkingList(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @QueryMap params: Map<String?, String?>?): JsonObject?

    @GET("/v3/api/get_orders_open_list")
    suspend fun getOrdersOpenList(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String?, String?>): ApiResponse

    @GET("/v3/api/get_orders_pretime_list")
    suspend fun getOrdersPretimeList(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String?, String?>?): ApiResponse

    @GET("/v3/api/get_order_for_id")
    suspend fun getOrderForId(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String?, String?>?): JsonObject?


    @GET("/v3/api/get_bank_cards")
    suspend fun getBankCards(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @QueryMap params: Map<String, String>): ApiResponse

    @GET("/v3/api/get_own_orders")
    suspend fun getOwnOrders(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String, String>): JsonObject?

    @GET("/v3/api/get_request_result")
    suspend fun getRequestResult(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @QueryMap params: Map<String, String>): JsonObject?

    @GET("/v3/api/get_profit_bank_cards")
    suspend fun getProfitBankCards(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @QueryMap params: Map<String, String>): ApiResponse

    @GET("/v3/api/get_confidential_page")
    suspend fun getConfidentialPage(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @QueryMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/worker_statistic")
    suspend fun getWorkerStatistic(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/worker_report")
    suspend fun getWorkerReport(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/set_worker_position")
    suspend fun setWorkerPosition(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/clear_information_after_worker_delete")
    suspend fun clearInformationAfterWorkerDelete(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/set_order_status")
    suspend fun setOrderStatus(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("Request-Id") uuid: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/worker_end_work")
    suspend fun endWork(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/set_worker_state")
    suspend fun setWorkerState(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/set_order_raw_calc")
    suspend fun setOrderRawCalc(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @Multipart
    @POST("/v3/api/set_order_route_file")
    suspend fun setOrderRouteFile(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Part("order_id") orderId: String?,
            @Part("tenant_login") tenantLogin: String?,
            @Part("worker_login") workerLogin: String?,
            @Part("order_route") file: MultipartBody.Part?): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/set_order_route")
    suspend fun setOrderRoute(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/set_signal_sos")
    suspend fun setSignalSos(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/unset_signal_sos")
    suspend fun unsetSignalSos(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/update_order")
    suspend fun updateOrder(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("Request-Id") uuid: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/update_order_cost")
    suspend fun updateOrderCost(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("Request-Id") uuid: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/create_bank_card")
    suspend fun createBankCard(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/check_bank_card")
    suspend fun checkBankCard(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/refill_account")
    suspend fun refillAccount(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/delete_bank_card")
    suspend fun deleteBankCard(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse


    @FormUrlEncoded
    @POST("/v3/api/set_parking_queue")
    suspend fun setParkingQueue(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse

    @FormUrlEncoded
    @POST("/v3/api/set_car_mileage")
    suspend fun setCarMileage(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Connection") connection: String?,
            @Header("version") version: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/create_profit_bank_card")
    suspend fun createProfitBankCard(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): JsonObject?

    @FormUrlEncoded
    @POST("/v3/api/delete_profit_bank_card")
    suspend fun deleteProfitBankCard(
            @Header("signature") signature: String?,
            @Header("worker-device") workerDevice: String?,
            @Header("lang") lang: String?,
            @Header("User-Agent") userAgent: String?,
            @Header("Request-Id") uuid: String?,
            @Header("Connection") connection: String?,
            @FieldMap params: Map<String, String>): ApiResponse
}