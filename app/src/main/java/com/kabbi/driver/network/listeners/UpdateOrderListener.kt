package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkUpdateOrderEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object UpdateOrderListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkUpdateOrderEvent("FAIL"))
        }
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")

            withContext(Dispatchers.Main) {
                if (info == "OK") {
                    EventBus.getDefault().post(NetworkUpdateOrderEvent(info, jsonObject.getJSONObject("result").getInt("set_result")))
                } else {
                    EventBus.getDefault().post(NetworkUpdateOrderEvent(info))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}