package com.kabbi.driver.network

import android.content.Context
import android.os.Build
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.kabbi.driver.BuildConfig
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppParams.HOST_NEW
import com.kabbi.driver.helper.AppParams.VERSION_APP
import com.kabbi.driver.helper.HashMD5
import com.kabbi.driver.models.DriverCity
import com.kabbi.driver.models.Position
import com.kabbi.driver.network.listeners.*
import com.kabbi.driver.network.responses.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

object WebService {

    private const val STATUS_OK = "OK"

    private var client: ApiService
    private var userAgent = "driver/${BuildConfig.VERSION_CODE} (android ${Build.VERSION.RELEASE}; ${Build.MANUFACTURER} ${Build.MODEL})/${VERSION_APP}"
    private const val connection = "Keep-Alive"
    private const val device = "android"
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        val builder = OkHttpClient().newBuilder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = builder
                .connectTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
//                .addInterceptor(logging)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(HOST_NEW)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        client = retrofit.create(ApiService::class.java)
    }

    suspend fun authWorker(token: String?, tenantLogin: String?, workerLogin: String?, workerPass: String?): AuthWorkerResponse {

        val params: LinkedHashMap<String?, String?> = linkedMapOf()

        params["device_token"] = token
        params["tenant_login"] = tenantLogin
        params["worker_login"] = workerLogin
        params["worker_password"] = workerPass

        return try {
            val response = client.authWorker(params)

            if (response.info == STATUS_OK) {
                val event = Gson().fromJson(response.result, AuthWorkerResponse::class.java)
                event.status = response.info
                event
            } else {
                AuthWorkerResponse(response.info)
            }
        } catch (e: JsonSyntaxException) {
            AuthWorkerResponse("JSON/NULL")
        } catch (e: Exception) {
            AuthWorkerResponse("FAIL")
        }
    }

    suspend fun getBalance(params: LinkedHashMap<String?, String?>, secretCode: String?): BalanceResponse? {

        val signature = HashMD5.getSignature(params, secretCode)

        return try {
            val response = client.getBalance(signature, device, language(), userAgent, connection, "3.16.0", params)

            if (response.info == STATUS_OK) {
                val event = Gson().fromJson(response.result, BalanceResponse::class.java)
                event.status = response.info
                event
            } else {
                null
            }
        } catch (e: Exception) {
            null
        }
    }

    suspend fun getCities(params: LinkedHashMap<String?, String?>, secretCode: String?): WorkerCitiesResponse? {

        val signature = HashMD5.getSignature(params, secretCode)

        return try {
            val response = client.getCities(signature, "android", language(), userAgent, connection, "3.16.0", params)

            val event = WorkerCitiesResponse(response.info)
            var lastCity: DriverCity? = null

            response.result?.asJsonObject?.also { result ->
                val cities = Gson().fromJson(result.get("cities"), Array<DriverCity>::class.java).asList()

                for (city in cities) {
                    if (result.get("last_city_id").asString == city.cityId) {
                        lastCity = city
                    }
                }

                if (cities.isNotEmpty()) {
                    if (lastCity == null)
                        lastCity = cities[0]
                } else {
                    event.status = "EMPTY"
                }

                event.driverCities = cities
                event.driverCity = lastCity
            }

            event

        } catch (e: Exception) {
            null
        }
    }

    suspend fun getPositions(params: LinkedHashMap<String, String>, secretCode: String?): GetPositionResponse? {

        val signature = HashMD5.getSignature(params, secretCode)

        return try {

            val response = client.getPositions(signature, device, language(), userAgent, connection, "3.18.0", params)
            val event = GetPositionResponse(response.info)

            response.result?.asJsonObject?.let { result ->
                Gson().fromJson(result.get("positions"), Array<Position>::class.java).asList()
            }?.also { positions ->
                event.positions = positions
            }

            if (event.positions.isNullOrEmpty()) {
                event.status = "EMPTY"
            }

            event

        } catch (e: Exception) {
            null
        }
    }

    fun getProfile(context: Context, params: LinkedHashMap<String, String>, secretCode: String?) = scope.launch {

        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getProfile(signature, device, language(), userAgent, connection, "3.32.0", params)?.also { result ->
                WorkerProfileListener.onRequestSuccess(result, context)
            }
        } catch (e: Exception) {
            Log.w("getParkingList", e.localizedMessage ?: "")
        }
    }

    suspend fun startWorkDay(params: LinkedHashMap<String, String>, secretCode: String?): StartWorkResponse {

        val signature = HashMD5.getSignature(params, secretCode)

        return try {
            val response = client.startWorkDay(signature, device, language(), userAgent, connection, "3.32.0", params)

            if (response.info == STATUS_OK) {
                val event = Gson().fromJson(response.result, StartWorkResponse::class.java)
                event.status = response.info
                event
            } else {
                StartWorkResponse(response.info)
            }
        } catch (e: JsonSyntaxException) {
            StartWorkResponse("JSON/NULL")
        } catch (e: Exception) {
            StartWorkResponse("FAIL")
        }
    }

    fun getParkingList(context: Context, params: LinkedHashMap<String?, String?>, secretCode: String?, serviceId: Long, point: LatLng) = scope.launch {

        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getParkingList(signature, "android", language(), userAgent, "Keep-Alive", params)?.also { result ->
                ParkingListListener.onRequestSuccess(result, serviceId, point, context)
            }
        } catch (e: Exception) {
            Log.w("getParkingList", e.localizedMessage ?: "")
        }
    }

    fun getOrdersList(context: Context, params: LinkedHashMap<String?, String?>, secretCode: String?, type: String) = scope.launch {

        val signature = HashMD5.getSignature(params, secretCode)

        try {
            if (type == "free") {
                val result = client.getOrdersOpenList(signature, "android", language(), userAgent, "Keep-Alive", "3.16.0", params)
                OrdersOpenListListener.onRequestSuccess(result, context, type)
            } else {
                val result = client.getOrdersPretimeList(signature, "android", language(), userAgent, "Keep-Alive", "3.16.0", params)
                OrdersPretimeListListener.onRequestSuccess(result, context, type)
            }

        } catch (e: Exception) {
        }
    }

    fun getOrderForId(params: Map<String?, String?>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getOrderForId(signature, "android", language(), userAgent, "Keep-Alive", "3.17.0", params)?.also { result ->
                OrderForIdListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
        }
    }

    fun getBankCards(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            val result = client.getBankCards(signature, "android", language(), userAgent, "Keep-Alive", params)
            GetBankCardsListener.onRequestSuccess(result)
        } catch (e: Exception) {
            GetBankCardsListener.onRequestFailure()
            e.printStackTrace()
        }

    }

    fun checkBankCards(params: Map<String, String>, secretCode: String?, uuid: String) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            val result = client.checkBankCard(signature, "android", language(), userAgent, uuid, "Keep-Alive", "3.16.0", params)
            CheckBankCardListener.onRequestSuccess(result)
        } catch (e: Exception) {
            CheckBankCardListener.onRequestFailure()
            e.printStackTrace()
        }

    }

    fun getOwnOrders(context: Context, params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getOwnOrders(signature, "android", language(), userAgent, "Keep-Alive", "3.14.0", params)?.also { result ->
                OwnOrdersListener.onRequestSuccess(result, context)
            }
        } catch (e: Exception) {
            OwnOrdersListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun getResult(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getRequestResult(signature, "android", language(), userAgent, "Keep-Alive", "3.16.0", params)?.also { result ->
                RequestResultListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            RequestResultListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun getProfitBankCard(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            val result = client.getProfitBankCards(signature, "android", language(), userAgent, "Keep-Alive", params)
            GetBankCardsListener.onRequestSuccess(result)
        } catch (e: Exception) {
            GetBankCardsListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun getConfidentialPage(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getConfidentialPage(signature, "android", language(), userAgent, "Keep-Alive", params)?.also { result ->
                ConfidentialPageListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            ConfidentialPageListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun getWorkerStatistic(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getWorkerStatistic(signature, "android", language(), userAgent, "Keep-Alive", "3.16.0", params)?.also { result ->
                WorkerStatisticListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getWorkerReport(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.getWorkerReport(signature, "android", language(), userAgent, "Keep-Alive", "3.16.0", params)?.also { result ->
                WorkerReportsListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setWorkerPosition(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.setWorkerPosition(signature, "android", language(), userAgent, "Keep-Alive", "3.12.0", params)?.also { result ->
                WorkerPositionListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun clearInformationAfterWorkerDelete(params: Map<String, String>, secretCode: String?, serviceId: Long, position: Int) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            val result = client.clearInformationAfterWorkerDelete(signature, "android", language(), userAgent, "Keep-Alive", "3.18.0", params)
            ClearInformationAfterWorkerDeleteListener.onRequestSuccess(result, serviceId, position)
        } catch (e: Exception) {
            ClearInformationAfterWorkerDeleteListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun setOrderStatus(params: Map<String, String>, secretCode: String?, uuid: String?, type: AppParams.RequestType) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.setOrderStatus(signature, "android", language(), userAgent, "Keep-Alive", uuid, "3.12.0", params)?.also { result ->
                OrderStatusListener.onRequestSuccess(result, type)
            }
        } catch (e: Exception) {
            OrderStatusListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun endWork(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            val result = client.endWork(signature, "android", language(), userAgent, "Keep-Alive", "3.32.0", params)
            EndWorkListener.onRequestSuccess(result)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setWorkerState(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.setWorkerState(signature, "android", language(), userAgent, "Keep-Alive", params)?.also { result ->
                WorkerStateListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setOrderCalc(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        try {
            client.setOrderRawCalc(signature, "android", language(), userAgent, "Keep-Alive", params)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setOrderRouteFile(params: Map<String, String>, secretCode: String?, file: File?, order: Order) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)
        val orderId = params["order_id"]
        val tenantLogin = params["tenant_login"]
        val workerLogin = params["worker_login"]
        val part = file?.run {
            MultipartBody.Part.createFormData("file", file.name, file.asRequestBody(file.extension.toMediaTypeOrNull()))
        }
        try {
            val result = client.setOrderRouteFile(signature, "android", language(), userAgent, "Keep-Alive", orderId, tenantLogin, workerLogin, part)
            RouteListener.onRequestSuccess(result, order)
        } catch (e: Exception) {
            RouteListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun setSos(params: Map<String, String>, secretCode: String?, request: String) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            when (request) {
                "set_order_raw_calc" ->
                    client.setSignalSos(signature, "android", language(), userAgent, "Keep-Alive", params)
                "set_order_route" ->
                    client.unsetSignalSos(signature, "android", language(), userAgent, "Keep-Alive", params)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateOrder(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.updateOrder(signature, "android", language(), userAgent, "Keep-Alive", uuid, "3.3.0", params)?.also { result ->
                UpdateOrderListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            UpdateOrderListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun updateOrderCost(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.updateOrderCost(signature, "android", language(), userAgent, "Keep-Alive", uuid, "3.3.0", params)?.also { result ->
                UpdateOrderCostListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            UpdateOrderCostListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun createBankCard(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.createBankCard(signature, "android", language(), userAgent, uuid, "Keep-Alive", params)?.also { result ->
                CreateBankCardListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            CreateBankCardListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun refillAccount(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.refillAccount(signature, "android", language(), userAgent, uuid, "Keep-Alive", params)?.also { result ->
                RefillAccountListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            RefillAccountListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun createProfitBankCard(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.createProfitBankCard(signature, "android", language(), userAgent, uuid, "Keep-Alive", params)?.also { result ->
                CreateBankCardListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            CreateBankCardListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun setCarMileage(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.setCarMileage(signature, "android", language(), userAgent, "Keep-Alive", "3.32.0", params)?.also { result ->
                CarMileageListener.onRequestSuccess(result)
            }
        } catch (e: Exception) {
            CarMileageListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun deleteBankCard(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            val result = client.deleteBankCard(signature, "android", language(), userAgent, uuid, "Keep-Alive", params)
            DeleteBankCardListener.onRequestSuccess(result)
        } catch (e: Exception) {
            DeleteBankCardListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun deleteProfitBankCard(params: Map<String, String>, secretCode: String?, uuid: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            val result = client.deleteProfitBankCard(signature, "android", language(), userAgent, uuid, "Keep-Alive", params)
            DeleteBankCardListener.onRequestSuccess(result)
        } catch (e: Exception) {
            DeleteBankCardListener.onRequestFailure()
            e.printStackTrace()
        }
    }

    fun setParkingQueue(params: Map<String, String>, secretCode: String?) = scope.launch {
        val signature = HashMD5.getSignature(params, secretCode)

        try {
            client.setParkingQueue(signature, "android", language(), userAgent, "Keep-Alive", params)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun language(): String {
        return Locale.getDefault().language
    }
}




