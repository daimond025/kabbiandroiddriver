package com.kabbi.driver.network.listeners

import com.kabbi.driver.events.NetworkDeleteBankCard
import com.kabbi.driver.network.ApiResponse
import org.greenrobot.eventbus.EventBus

object DeleteBankCardListener {

    fun onRequestFailure() {
        EventBus.getDefault().post(NetworkDeleteBankCard("FAIL"))
    }

    fun onRequestSuccess(response: ApiResponse) {
        EventBus.getDefault().post(NetworkDeleteBankCard(response.info))
    }
}