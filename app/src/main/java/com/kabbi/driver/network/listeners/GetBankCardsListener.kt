package com.kabbi.driver.network.listeners

import com.kabbi.driver.events.NetworkBankCards
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import java.util.*

object GetBankCardsListener {
    fun onRequestFailure() = CoroutineScope(Dispatchers.Main).launch {
        EventBus.getDefault().post(NetworkBankCards(null, "FAIL"))
    }

    fun onRequestSuccess(response: ApiResponse) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val cards: MutableList<String> = ArrayList()
            if (response.info == "OK") {
                response.result?.also {
                    val array = it.asJsonArray
                    val count = array.size()
                    for (i in 0 until count) {
                        cards.add(array.get(i).asString)
                    }
                }
            }

            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkBankCards(cards, response.info))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}