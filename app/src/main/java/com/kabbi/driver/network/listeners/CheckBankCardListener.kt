package com.kabbi.driver.network.listeners

import com.kabbi.driver.events.NetworkCheckBankCard
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

object CheckBankCardListener {
    fun onRequestFailure() = CoroutineScope(Dispatchers.Main).launch {
        EventBus.getDefault().post(NetworkCheckBankCard("FAIL"))
    }

    fun onRequestSuccess(response: ApiResponse) = CoroutineScope(Dispatchers.IO).launch {
        try {
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkCheckBankCard(response.info))
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}