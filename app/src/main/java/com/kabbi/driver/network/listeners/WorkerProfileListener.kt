package com.kabbi.driver.network.listeners

import android.content.Context
import com.google.gson.JsonObject
import com.kabbi.driver.database.entities.Car
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.DriverTariff
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.network.responses.GetProfileResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject
import java.util.*

object WorkerProfileListener {

    fun onRequestSuccess(response: JsonObject, context: Context) = CoroutineScope(Dispatchers.IO).launch {
        val appPreferences = AppPreferences(context)

        try {
            val jsonObject = JSONObject(response.toString())
            if (jsonObject.getString("info") == "OK") {
                val driver = Driver()
                val carList: MutableList<Car> = ArrayList()

                driver.cityID = jsonObject.getJSONObject("result").getString("worker_city_id")
                driver.exp = jsonObject.getJSONObject("result").getString("worker_experience")
                driver.name = jsonObject.getJSONObject("result").getString("worker_fio")
                driver.phone = jsonObject.getJSONObject("result").getString("worker_phone")
                driver.email = jsonObject.getJSONObject("result").getString("worker_email")
                driver.photo = jsonObject.getJSONObject("result").getString("worker_photo")

                if (!jsonObject.getJSONObject("result").isNull("promo_code")) {
                    driver.promoCode = jsonObject.getJSONObject("result").getString("promo_code")
                }

                val carArray = jsonObject.getJSONObject("result").getJSONArray("worker_cars")

                for (j in 0 until carArray.length()) {
                    val jsonCar = carArray.getJSONObject(j)
                    val car = Car(
                            "",
                            jsonCar.getString("worker_car_descr"),
                            "",
                            "",
                            jsonCar.getString("worker_car_class"),
                            jsonCar.getString("worker_car_id"),
                            jsonCar.getBoolean("is_company_car"),
                            null)
                    carList.add(car)
                }

                val jsonDriverTariffs = jsonObject.getJSONObject("result").getJSONObject("worker_tariffs")
                val jsonArrayActive = jsonDriverTariffs.getJSONArray("active")
                val jsonArrayToBuy = jsonDriverTariffs.getJSONArray("to_buy")

                val driverTariffList: MutableList<DriverTariff> = ArrayList()

                if (jsonArrayActive.length() > 0) {
                    for (j in 0 until jsonArrayActive.length()) {
                        val jsonTariff = jsonArrayActive.getJSONObject(j)
                        driverTariffList.add(createDriverTariff("active", jsonTariff))
                    }
                }

                if (jsonArrayToBuy.length() > 0) {
                    for (j in 0 until jsonArrayToBuy.length()) {
                        val jsonTariff = jsonArrayToBuy.getJSONObject(j)
                        driverTariffList.add(createDriverTariff("to_buy", jsonTariff))
                    }
                }

                try {
                    appPreferences.saveText("offer_sec", jsonObject.getJSONObject("result").getString("order_offer_sec"))
                    appPreferences.saveText("gps_server", jsonObject.getJSONObject("result").getString("allow_work_without_gps"))
                    appPreferences.saveText("allow_cancel_order", jsonObject.getJSONObject("result").getString("allow_cancel_order"))
                    appPreferences.saveText("allow_cancel_order_after_time", jsonObject.getJSONObject("result").getString("allow_cancel_order_after_time"))
                    appPreferences.saveText("show_chat_with_dispatcher_before_start_shift", jsonObject.getJSONObject("result").getString("show_chat_with_dispatcher_before_start_shift"))
                    appPreferences.saveText("show_chat_with_dispatcher_on_shift", jsonObject.getJSONObject("result").getString("show_chat_with_dispatcher_on_shift"))
                    appPreferences.saveText("show_general_chat_on_shift", jsonObject.getJSONObject("result").getString("show_general_chat_on_shift"))
                    appPreferences.saveText("start_time_by_order", jsonObject.getJSONObject("result").getString("start_time_by_order"))
                    appPreferences.saveText("worker_arrival_time", jsonObject.getJSONObject("result").getJSONArray("worker_arrival_time").toString())
                    appPreferences.saveText("allow_refill_balance_by_bank_card", jsonObject.getJSONObject("result").getString("allow_refill_balance_by_bank_card"))
                    appPreferences.saveText("allow_edit_order", jsonObject.getJSONObject("result").getString("allow_edit_order"))
                    appPreferences.saveText("show_estimation", jsonObject.getJSONObject("result").getString("show_estimation"))
                    appPreferences.saveText("show_worker_address", jsonObject.getJSONObject("result").getString("show_worker_address"))
                    appPreferences.saveText("show_urgent_order_time", jsonObject.getJSONObject("result").getString("show_urgent_order_time"))
                    appPreferences.saveText("hand_brake", "1")
                    appPreferences.saveText("deny_fakegps", jsonObject.getJSONObject("result").getString("deny_fakegps"))
                    appPreferences.saveText("allow_worker_to_create_order", jsonObject.getJSONObject("result").getString("allow_worker_to_create_order"))
                    appPreferences.saveText("deny_setting_arrival_status_early", jsonObject.getJSONObject("result").getString("deny_setting_arrival_status_early"))
                    appPreferences.saveText("min_distance_to_set_arrival_status", jsonObject.getJSONObject("result").getString("min_distance_to_set_arrival_status"))
                    appPreferences.saveText("require_password_every_time_log_in_application", jsonObject.getJSONObject("result").getString("require_password_every_time_log_in_application"))
                    appPreferences.saveText("print_check", jsonObject.getJSONObject("result").getString("print_check"))
                    appPreferences.saveText("control_own_car_mileage", jsonObject.getJSONObject("result").getString("control_own_car_mileage"))
                    appPreferences.saveText("show_driver_privacy_policy", jsonObject.getJSONObject("result").getString("show_driver_privacy_policy"))
                    appPreferences.saveText("is_view_queue", jsonObject.getJSONObject("result").getString("is_view_queue"))
                    appPreferences.saveText("allow_reserve_urgent_orders", jsonObject.getJSONObject("result").getString("allow_reserve_urgent_orders"))
                    appPreferences.saveText("allow_edit_cost_order", jsonObject.getJSONObject("result").getString("allow_edit_cost_order"))
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                appPreferences.saveText("city_id", jsonObject.getJSONObject("result").getString("worker_city_id"))
                withContext(Dispatchers.Main){
                    EventBus.getDefault().post(GetProfileResponse(jsonObject.getString("info"),
                            carList,
                            driverTariffList,
                            driver,
                            jsonObject.getJSONObject("result").getString("worker_city_name")))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    @Throws(JSONException::class)
    private fun createDriverTariff(type: String, jsonTariff: JSONObject): DriverTariff {
        var activeShiftCount: String? = "0"
        try {
            if (jsonTariff.getString("worker_tariff_type") == "SUBSCRIPTION") {
                activeShiftCount = jsonTariff.getString("worker_count_active_shift")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return DriverTariff(
                type,
                activeShiftCount!!,
                jsonTariff.getString("worker_tariff_type"),
                jsonTariff.getString("worker_car_class"),
                jsonTariff.getString("worker_tariff_id"),
                jsonTariff.getString("worker_tariff_period_type"),
                jsonTariff.getString("worker_tariff_period_info"),
                jsonTariff.getString("worker_tariff_name"),
                jsonTariff.getString("worker_tariff_cost"),
                jsonTariff.getString("worker_order_comission"),
                jsonTariff.getString("worker_order_comission_type"),
                jsonTariff.getString("worker_count_shift"),
                null)
    }

}