package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkStatisticEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

object WorkerStatisticListener {

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            if (jsonObject.getString("info") == "OK") {
                val data = jsonObject.getJSONObject("result").getJSONObject("data")
                val earnings = data.getDouble("earnings")
                val count = data.getInt("count_orders")
                val hours = data.getInt("working_hours")

                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkStatisticEvent(earnings, count, hours))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}