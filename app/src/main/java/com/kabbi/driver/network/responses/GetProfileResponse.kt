package com.kabbi.driver.network.responses

import com.kabbi.driver.database.entities.Car
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.DriverTariff

data class GetProfileResponse(
        var status: String,
        var carList: List<Car>,
        var driverTariffList: List<DriverTariff>,
        var driver: Driver,
        var city: String)