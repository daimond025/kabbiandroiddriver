package com.kabbi.driver.network.listeners

import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.events.NetworkSendRoute
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

object RouteListener {
    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkSendRoute("FAIL"))
        }
    }

    fun onRequestSuccess(response: ApiResponse, order: Order) = CoroutineScope(Dispatchers.IO).launch {
        try {
            withContext(Dispatchers.Main) {
                if (response.info == "OK")
                    EventBus.getDefault().post(NetworkSendRoute(response.info, order))
                else
                    EventBus.getDefault().post(NetworkSendRoute(response.info))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}