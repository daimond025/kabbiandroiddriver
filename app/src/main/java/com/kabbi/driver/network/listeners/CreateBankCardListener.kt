package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkCreateBankCard
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object CreateBankCardListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkCreateBankCard("FAIL"))
        }
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")

            if (info == "OK") {
                val result = jsonObject.getJSONObject("result")
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkCreateBankCard(result.getString("url"), result.getString("orderId"), info))
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkCreateBankCard(info))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}