package com.kabbi.driver.network.listeners

import android.content.Context
import com.google.gson.Gson
import com.kabbi.driver.events.NetworkOrdersListEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.DistanceGPS
import com.kabbi.driver.models.OpenOrder
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

object OrdersOpenListListener {

    fun onRequestSuccess(response: ApiResponse, context: Context, type: String) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val appPreferences = AppPreferences(context)

            if (response.info == "OK") {

                response.result?.also {

                    val ordersArray = Gson().fromJson(it.asJsonObject.getAsJsonArray("orders_open_list").toString(), Array<OpenOrder>::class.java)
                    val orders = ordersArray.toMutableList()

                    for (order in orders) {
                        order.distance = DistanceGPS.getDistanceRound(order.from.lat, order.from.lng, appPreferences.getText("lat").toDouble(), appPreferences.getText("lon").toDouble())
                        order.type = "free"
                    }

                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkOrdersListEvent(response.info, type, orders))
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
