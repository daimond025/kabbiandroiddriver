package com.kabbi.driver.network.listeners


import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonObject
import com.google.maps.android.PolyUtil
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Parking
import com.kabbi.driver.events.NetworkParkingsListEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import java.util.*

object ParkingListListener {

    fun onRequestSuccess(response: JsonObject, serviceId: Long, point: LatLng, context: Context) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())

            if (jsonObject.getString("info") == "OK") {

                val jsonArrayPolygons = jsonObject.getJSONObject("result").getJSONArray("parkings_list")
                val dao = AppDatabase.getInstance(context).parkingDao()
                dao.deleteAll(serviceId)

                var park = false

                for (j in 0 until jsonArrayPolygons.length()) {
                    val jsonPolygon = jsonArrayPolygons.getJSONObject(j)

                    val coordinatesJsonArray = jsonPolygon.getJSONObject("parking_polygone").getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0)

                    val listPolygon = ArrayList<LatLng>()
                    for (k in 0 until coordinatesJsonArray.length()) {
                        val jsonArrayPoint = coordinatesJsonArray.getJSONArray(k)
                        listPolygon.add(LatLng(jsonArrayPoint.getDouble(1), jsonArrayPoint.getDouble(0)))
                    }

                    val parking = Parking()
                    parking.parkingID = jsonPolygon.getLong("parking_id")
                    parking.parkingType = jsonPolygon.getString("parking_type")
                    parking.parkingPolygon = jsonPolygon.toString()
                    parking.parkingName = jsonPolygon.getString("parking_name")
                    parking.service = serviceId

                    parking.inDistrCoefficientType = jsonPolygon.getString("in_distr_coefficient_type")
                    parking.outDistrCoefficientType = jsonPolygon.getString("out_distr_coefficient_type")
                    parking.inDistrict = jsonPolygon.getDouble("in_district")
                    parking.outDistrict = jsonPolygon.getDouble("out_district")
                    dao.save(parking)

                    if (PolyUtil.containsLocation(point, listPolygon, false) && jsonPolygon.getString("parking_type") != "basePolygon") {
                        park = true
                        withContext(Dispatchers.Main) {
                            EventBus.getDefault().post(NetworkParkingsListEvent(jsonObject.getString("info"), parking))
                        }
                    }
                }

                if (!park) {
                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkParkingsListEvent(jsonObject.getString("info"), null))
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
