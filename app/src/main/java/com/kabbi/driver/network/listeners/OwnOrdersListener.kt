package com.kabbi.driver.network.listeners

import android.content.Context
import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkOwnOrdersEvent
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.DistanceGPS
import com.kabbi.driver.models.OwnOrder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject
import java.util.*

object OwnOrdersListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkOwnOrdersEvent("FAIL"))
        }
    }

    fun onRequestSuccess(response: JsonObject, context: Context) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val appPreferences = AppPreferences(context)
            val jsonObject = JSONObject(response.toString())

            val assignedOrders: MutableList<OwnOrder> = ArrayList()
            val assignedPreOrders: MutableList<OwnOrder> = ArrayList()
            val activeOrders: MutableList<OwnOrder> = ArrayList()

            if (jsonObject.getString("info") == "OK") {
                val result = jsonObject.getJSONObject("result")
                val assignedOrdersJsonArray = result.getJSONArray("assignedOrders")
                val assignedPreOrdersJsonArray = result.getJSONArray("assignedPreOrders")
                val activeOrdersJsonArray = result.getJSONArray("activeOrders")
                for (j in 0 until assignedOrdersJsonArray.length()) {
                    val ownOrder = OwnOrder()
                    ownOrder.type = "assignedOrders"
                    val `object` = assignedOrdersJsonArray.getJSONObject(j)
                    ownOrder.orderId = `object`.getString("order_id")
                    ownOrder.orderTime = `object`.getString("order_time")
                    ownOrder.payment = `object`.getString("payment")
                    ownOrder.isDenyRefuse = `object`.getBoolean("deny_refuse")
                    val from = `object`.getJSONObject("order_from")
                    ownOrder.cityId = from.getString("city_id")
                    ownOrder.lat = from.getString("lat")
                    ownOrder.lon = from.getString("lon")
                    ownOrder.city = from.getString("city")
                    ownOrder.street = from.getString("street")
                    ownOrder.house = from.getString("house")
                    ownOrder.housing = from.getString("housing")
                    ownOrder.porch = from.getString("porch")
                    ownOrder.apt = from.getString("apt")
                    ownOrder.parking = from.getString("parking")
                    try {
                        ownOrder.dis = DistanceGPS.getDistanceRound(
                                java.lang.Double.valueOf(from.getString("lat")),
                                java.lang.Double.valueOf(from.getString("lon")),
                                java.lang.Double.valueOf(appPreferences.getText("lat")),
                                java.lang.Double.valueOf(appPreferences.getText("lon"))).toString()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        ownOrder.dis = "ex"
                    }
                    assignedOrders.add(ownOrder)
                }

                for (j in 0 until assignedPreOrdersJsonArray.length()) {
                    val ownOrder = OwnOrder()
                    ownOrder.type = "assignedPreOrders"
                    val `object` = assignedPreOrdersJsonArray.getJSONObject(j)
                    ownOrder.orderId = `object`.getString("order_id")
                    ownOrder.orderTime = `object`.getString("order_time")
                    ownOrder.payment = `object`.getString("payment")
                    ownOrder.isDenyRefuse = `object`.getBoolean("deny_refuse")
                    val from = `object`.getJSONObject("order_from")
                    ownOrder.cityId = from.getString("city_id")
                    ownOrder.lat = from.getString("lat")
                    ownOrder.lon = from.getString("lon")
                    ownOrder.city = from.getString("city")
                    ownOrder.street = from.getString("street")
                    ownOrder.house = from.getString("house")
                    ownOrder.housing = from.getString("housing")
                    ownOrder.porch = from.getString("porch")
                    ownOrder.apt = from.getString("apt")
                    ownOrder.parking = from.getString("parking")
                    try {
                        ownOrder.dis = DistanceGPS.getDistanceRound(
                                java.lang.Double.valueOf(from.getString("lat")),
                                java.lang.Double.valueOf(from.getString("lon")),
                                java.lang.Double.valueOf(appPreferences.getText("lat")),
                                java.lang.Double.valueOf(appPreferences.getText("lon"))).toString()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        ownOrder.dis = "ex"
                    }
                    assignedPreOrders.add(ownOrder)
                }

                for (j in 0 until activeOrdersJsonArray.length()) {
                    val ownOrder = OwnOrder()
                    ownOrder.type = "activeOrders"
                    val `object` = activeOrdersJsonArray.getJSONObject(j)
                    ownOrder.orderId = `object`.getString("order_id")
                    ownOrder.orderTime = `object`.getString("order_time")
                    ownOrder.payment = `object`.getString("payment")
                    ownOrder.isDenyRefuse = `object`.getBoolean("deny_refuse")
                    val from = `object`.getJSONObject("order_from")
                    ownOrder.cityId = from.getString("city_id")
                    ownOrder.lat = from.getString("lat")
                    ownOrder.lon = from.getString("lon")
                    ownOrder.city = from.getString("city")
                    ownOrder.street = from.getString("street")
                    ownOrder.house = from.getString("house")
                    ownOrder.housing = from.getString("housing")
                    ownOrder.porch = from.getString("porch")
                    ownOrder.apt = from.getString("apt")
                    ownOrder.parking = from.getString("parking")
                    try {
                        ownOrder.dis = DistanceGPS.getDistanceRound(
                                java.lang.Double.valueOf(from.getString("lat")),
                                java.lang.Double.valueOf(from.getString("lon")),
                                java.lang.Double.valueOf(appPreferences.getText("lat")),
                                java.lang.Double.valueOf(appPreferences.getText("lon"))).toString()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        ownOrder.dis = "ex"
                    }
                    activeOrders.add(ownOrder)
                }
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOwnOrdersEvent(jsonObject.getString("info"), assignedOrders, assignedPreOrders, activeOrders))
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOwnOrdersEvent(jsonObject.getString("info")))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

}