package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkCarMileage
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object CarMileageListener {

    fun onRequestFailure() {
        EventBus.getDefault().post(NetworkCarMileage("FAIL", ""))
    }

    fun onRequestSuccess(response: JsonObject) {
        try {
            val jsonObject = JSONObject(response.toString())

            when (val info = jsonObject.getString("info")) {
                "INVALID_CAR_MILEAGE" -> EventBus.getDefault().post(NetworkCarMileage(info, jsonObject.getJSONObject("result").getString("current_mileage")))
                else -> EventBus.getDefault().post(NetworkCarMileage(info, ""))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}