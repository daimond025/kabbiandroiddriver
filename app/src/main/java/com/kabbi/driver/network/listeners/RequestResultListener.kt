package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkRequestResult
import com.kabbi.driver.helper.AppParams.EMPTY_RESPONSE
import com.kabbi.driver.helper.AppParams.RESPONSE_ERROR
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object RequestResultListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkRequestResult("FAIL", 0))
        }
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())

            if (jsonObject.getString("info") == "OK") {

                if (!jsonObject.getJSONObject("result").isNull("response")) {
                    val resp = jsonObject.getJSONObject("result").getJSONObject("response")
                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkRequestResult(resp.getString("info"), resp.getJSONObject("result").getInt("set_result")))
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkRequestResult(EMPTY_RESPONSE, 0))
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkRequestResult(jsonObject.getString("info"), 0))
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkRequestResult(RESPONSE_ERROR, 0))
            }
        }
    }
}
