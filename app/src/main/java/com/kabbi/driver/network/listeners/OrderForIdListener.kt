package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkOrderForIdEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

object OrderForIdListener {
    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())

            if (jsonObject.getString("info") == "OK") {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOrderForIdEvent(jsonObject.getString("info"), jsonObject))
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOrderForIdEvent(jsonObject.getString("info")))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}