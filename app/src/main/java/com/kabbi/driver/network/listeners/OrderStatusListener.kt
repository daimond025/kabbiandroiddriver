package com.kabbi.driver.network.listeners

import android.util.Log
import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkOrderStatusEvent
import com.kabbi.driver.helper.AppParams
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object OrderStatusListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkOrderStatusEvent("FAIL"))
        }
    }

    fun onRequestSuccess(response: JsonObject, type: AppParams.RequestType) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")

            if (info == "OK") {
                val result = jsonObject.getJSONObject("result")
                val cityTime = if (result.has("city_time")) result.getLong("city_time") else 0
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOrderStatusEvent(info, type, result.getInt("set_result"), cityTime))
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkOrderStatusEvent(info, type))
                }
            }
        } catch (e: Exception) {
            Log.e("OrderStatusListener", e.message ?: "")
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkOrderStatusEvent(AppParams.RESPONSE_ERROR))
            }
        }
    }

}