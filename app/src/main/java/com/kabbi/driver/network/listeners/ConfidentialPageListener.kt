package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkConfidentialPageEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

object ConfidentialPageListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.Main).launch {
        EventBus.getDefault().post(NetworkConfidentialPageEvent("FAIL", null))
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkConfidentialPageEvent("OK", jsonObject.getJSONObject("result").getString("result")))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}