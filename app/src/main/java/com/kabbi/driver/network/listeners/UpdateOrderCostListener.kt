package com.kabbi.driver.network.listeners


import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkUpdateOrderCostEvent
import com.kabbi.driver.helper.AppParams
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object UpdateOrderCostListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkUpdateOrderCostEvent("FAIL", 0))
        }
    }

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {

        try {
            val jsonObject = JSONObject(response.toString())

            if (jsonObject.getString("info") == "OK") {
                if (jsonObject.getJSONObject("result").isNull("set_result") || jsonObject.getJSONObject("result").getInt("set_result") != 1) {
                    withContext(Dispatchers.Main) {
                        EventBus.getDefault().post(NetworkUpdateOrderCostEvent(AppParams.EMPTY_RESPONSE, 0))
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkUpdateOrderCostEvent(jsonObject.getString("info"), 0))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkUpdateOrderCostEvent(AppParams.RESPONSE_ERROR, 0))
            }
        }
    }
}
