package com.kabbi.driver.network.listeners

import com.kabbi.driver.App
import com.kabbi.driver.events.NetworkEndWorkEvent
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

object EndWorkListener {

    fun onRequestSuccess(response: ApiResponse) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val event = NetworkEndWorkEvent(response.info)

            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(event)
            }

            if (event.status.equals("ok", ignoreCase = true)) {
                App.appContext.cacheDir.deleteRecursively()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}