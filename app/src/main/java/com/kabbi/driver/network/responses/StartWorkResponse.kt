package com.kabbi.driver.network.responses

import com.google.gson.annotations.SerializedName

data class StartWorkResponse(
        var status: String,
        @SerializedName("worker_shift_id")
        var workerShiftId: String? = null
)