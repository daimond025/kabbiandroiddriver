package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkWorkerStateEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object WorkerStateListener {

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")

            withContext(Dispatchers.Main) {
                if (info == "OK") {
                    EventBus.getDefault().post(NetworkWorkerStateEvent(info, jsonObject.getJSONObject("result").getInt("set_result")))
                } else {
                    EventBus.getDefault().post(NetworkWorkerStateEvent(info))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}