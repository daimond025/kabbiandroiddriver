package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkReportsDataEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject

object WorkerReportsListener {

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val jsonResult = jsonObject.getJSONObject("result")

            if (jsonObject.getString("info") == "OK") {
                withContext(Dispatchers.Main) {
                    EventBus.getDefault().post(NetworkReportsDataEvent(jsonResult.getJSONArray("data"),
                            jsonResult.getInt("current_page"), jsonResult.getInt("last_page"),
                            jsonResult.getInt("per_page"), jsonResult.getInt("total")))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}