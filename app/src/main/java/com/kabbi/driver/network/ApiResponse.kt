package com.kabbi.driver.network

import com.google.gson.JsonElement

data class ApiResponse(
        val code: Int?,
        val info: String,
        val result: JsonElement?
)