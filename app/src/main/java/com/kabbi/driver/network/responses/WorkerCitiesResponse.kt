package com.kabbi.driver.network.responses

import com.kabbi.driver.models.DriverCity

data class WorkerCitiesResponse(
        var status: String,
        var driverCities: List<DriverCity> = arrayListOf(),
        var driverCity: DriverCity? = null)