package com.kabbi.driver.network.listeners

import com.kabbi.driver.events.NetworkClearDriverInfoEvent
import com.kabbi.driver.network.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException

object ClearInformationAfterWorkerDeleteListener {

    fun onRequestFailure() = CoroutineScope(Dispatchers.IO).launch {
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(NetworkClearDriverInfoEvent("FAIL", 0, 0))
        }
    }

    fun onRequestSuccess(response: ApiResponse, serviceId: Long, position: Int) = CoroutineScope(Dispatchers.IO).launch {
        try {
            withContext(Dispatchers.Main) {
                EventBus.getDefault().post(NetworkClearDriverInfoEvent(response.info, serviceId, position))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}