package com.kabbi.driver.network.listeners

import com.google.gson.JsonObject
import com.kabbi.driver.events.NetworkWorkerPositionEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

object WorkerPositionListener {

    fun onRequestSuccess(response: JsonObject) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val jsonObject = JSONObject(response.toString())
            val info = jsonObject.getString("info")
            val result = jsonObject.getJSONObject("result")

            withContext(Dispatchers.Main) {
                if (info == "OK") {
                    EventBus.getDefault().post(NetworkWorkerPositionEvent(info,
                            result.getString("worker_status"),
                            result.getLong("server_time"),
                            result.getLong("city_time")))
                } else {
                    EventBus.getDefault().post(NetworkWorkerPositionEvent(info, info))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }
}