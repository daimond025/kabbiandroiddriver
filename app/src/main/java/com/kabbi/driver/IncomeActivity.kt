package com.kabbi.driver

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.fragments.IncomeFragment
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class IncomeActivity : BaseActivity() {

    internal lateinit var toolbar: Toolbar
    var driver: Driver? = null
        internal set
    var service: Service? = null
        internal set

    internal lateinit var appPreferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(applicationContext)

        service = (application as App).service
        driver = (application as App).driver

        if (service == null || driver == null) {
            try {
                runBlocking {
                    withContext(Dispatchers.IO) {
                        val db = AppDatabase.getInstance(this@IncomeActivity)
                        service = db.serviceDao().getById(java.lang.Long.valueOf(appPreferences.getText("service_id")))
                        driver = db.driverDao().getById(java.lang.Long.valueOf(appPreferences.getText("driver_id")))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finish()
            }

        }

        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_income)

        appPreferences.saveText("service_id", service!!.id.toString())
        appPreferences.saveText("driver_id", driver!!.id.toString())

        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_income)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val fragmentManager = supportFragmentManager

        val incomeFragment = IncomeFragment()
        val fragmentTransaction = fragmentManager.beginTransaction()
        val args = Bundle()
        args.putString("service", service!!.uri)
        args.putString("driver", driver!!.callSign)
        args.putString("driver_code", driver!!.secretCode)
        incomeFragment.arguments = args
        fragmentTransaction.replace(R.id.income_container, incomeFragment)
        fragmentTransaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}