package com.kabbi.driver


import android.app.Activity
import android.app.NotificationManager
import android.content.*
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.fragments.DispChatFragment
import com.kabbi.driver.fragments.NavigationDrawerFragment
import com.kabbi.driver.fragments.ProfileFragment
import com.kabbi.driver.fragments.WorkFragment
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.models.DriverCity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class WorkActivity : BaseActivity(), NavigationDrawerFragment.NavigationDrawerCallbacks, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private var fragmentNumber: Int = 0
    internal lateinit var toolbar: Toolbar
    var driver: Driver? = null
        internal set
    var service: Service? = null
        internal set
    internal lateinit var appPreferences: AppPreferences
    private var fragmentChat: Fragment? = null
    /**
     * Fragment managing the behaviors, interactions and presentation of the
     * navigation drawer.
     */
    private var mNavigationDrawerFragment: NavigationDrawerFragment? = null
    /**
     * Used to store the last screen title. For use in
     * [.restoreActionBar].
     */
    private var mTitle: CharSequence? = null
    private var broadcastReceiverLang: BroadcastReceiver? = null
    private var cities: List<DriverCity>? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(applicationContext)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        service = (application as App).service
        driver = (application as App).driver

        if (service == null || driver == null) {
            try {
                runBlocking {
                    withContext(Dispatchers.IO) {
                        val db = AppDatabase.getInstance(this@WorkActivity)
                        service = db.serviceDao().getById(java.lang.Long.valueOf(appPreferences.getText("service_id")))
                        driver = db.driverDao().getById(java.lang.Long.valueOf(appPreferences.getText("driver_id")))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                finish()
            }
        }

        cities = ArrayList()

        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_work)

        appPreferences.saveText("service_id", service!!.id.toString())
        appPreferences.saveText("driver_id", driver!!.id.toString())

        fragmentNumber = 0

        mNavigationDrawerFragment = supportFragmentManager.findFragmentById(R.id.navigation_drawer) as NavigationDrawerFragment?

        mTitle = getString(R.string.text_menu_field_authrize1)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setTitle("")

        mNavigationDrawerFragment!!.setUp(R.id.navigation_drawer, findViewById(R.id.drawer_layout))

        val args = Bundle()
        args.putLong("driver_id", driver!!.id.toLong())
        try {
            if (intent.extras!!.getBoolean("end_workshift")) {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle(getString(R.string.dialog_endworkday1))
                alertDialog.setPositiveButton(getString(R.string.button_confirm)) { dialog, _ -> dialog.dismiss() }

                alertDialog.create().show()
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.cancelAll()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
        mGoogleApiClient!!.connect()
    }

    override fun onStart() {
        super.onStart()
        broadcastReceiverLang = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                toolbar.title = "  " + getString(R.string.text_menu_field_authrize3)
                toolbar.setTitleTextColor(resources.getColor(R.color.white))
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiverLang!!, IntentFilter(AppParams.BROADCAST_LANG))
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiverLang!!)
    }

    override fun onNavigationDrawerItemSelected(position: Int) {
        Log.d("WORK_ACTIVITY", "onNavigationDrawerItemSelected # $position")
        val args = Bundle()

        val fragmentManager = supportFragmentManager
        onSectionAttached(position)

        if (mNavigationDrawerFragment != null) {
            mNavigationDrawerFragment!!.setMenuItem(position)
        }
        when (position) {
            1 -> {
                fragmentNumber = 0
                val fragmentWork = WorkFragment()
                fragmentManager.beginTransaction().replace(R.id.container, fragmentWork).commit()
            }

            2 -> {
                fragmentNumber = 1
                val fragmentProfile = ProfileFragment()
                args.putBoolean("autorize", false)
                fragmentProfile.arguments = args
                fragmentManager.beginTransaction().replace(R.id.container, fragmentProfile).commit()
            }

            3 -> {
                fragmentNumber = 2
                fragmentChat = DispChatFragment()
                fragmentManager.beginTransaction().replace(R.id.container, fragmentChat!!).commit()
            }

            4 -> {
                fragmentNumber = 3
                try {
                    if (fragmentChat != null)
                        fragmentChat!!.onDestroy()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val intent = Intent(this, EntranceActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun onSectionAttached(number: Int) {
        when (number) {
            1 -> mTitle = getString(R.string.text_menu_field_authrize1)
            2 -> mTitle = getString(R.string.text_menu_field_authrize3)
            3 -> mTitle = getString(R.string.text_menu_field_dispchat)
        }
    }

    private fun restoreActionBar() {
        val actionBar = supportActionBar
        actionBar!!.navigationMode = ActionBar.NAVIGATION_MODE_STANDARD
        actionBar.setDisplayShowTitleEnabled(true)
        toolbar.title = "  " + mTitle!!
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (!mNavigationDrawerFragment!!.isDrawerOpen) {
            restoreActionBar()
            return true
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun onBackPressed() {
        mNavigationDrawerFragment!!.openDrawer(R.id.navigation_drawer)
    }

    override fun onConnected(bundle: Bundle?) {
        val mMLocationRequest = LocationRequest.create()
        mMLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mMLocationRequest.interval = (30 * 1000).toLong()
        mMLocationRequest.fastestInterval = (5 * 1000).toLong()


        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(mMLocationRequest)
        builder.setAlwaysShow(true)

        val result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback { result1 ->
            val status = result1.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                    status.startResolutionForResult(
                            this@WorkActivity,
                            REQUEST_LOCATION)
                } catch (ignored: IntentSender.SendIntentException) {
                }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ProfileFragment.RESULT_PRINTER && resultCode == Activity.RESULT_OK) {
            try {
                appPreferences.saveText("device_printer", data!!.getStringExtra("address"))
                findViewById<TextView>(R.id.textview_profile_printer).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>"
                        + getString(R.string.profile_printer) + "<br>" + data.getStringExtra("address") + "</font></big>")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    companion object {
        const val REQUEST_LOCATION = 5
    }
}
