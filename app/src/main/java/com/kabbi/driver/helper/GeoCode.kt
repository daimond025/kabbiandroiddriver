package com.kabbi.driver.helper


import android.content.Context
import com.kabbi.driver.util.AsyncHttpTask
import com.loopj.android.http.RequestParams
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class GeoCode(context: Context, map: Int, lat: String, lon: String) : AsyncHttpTask.AsyncTaskInterface {

    var lat: String = ""
    var lon: String = ""
    var city: String? = null
    var street: String? = null
    var house: String? = null
    var housing: String? = null
    var porch: String? = null
    var map: Int = 1
    private val address = HashMap<String, String>()

    init {

        val url: String
        val asyncHttpTask = AsyncHttpTask(this, context)
        val requestParams = RequestParams()
        when (AppParams.MAPS[map]) {
            "Yandex", "Yandex narod" -> {
                url = "https://geocode-maps.yandex.ru/1.x/"
                requestParams.add("geocode", "$lon,$lat")
                requestParams.add("format", "json")
                requestParams.add("results", "1")
                requestParams.add("lang", Locale.getDefault().language + "_" + Locale.getDefault().country)
                asyncHttpTask.getGeoJson(url, requestParams)
            }
            "Open Street Map" -> {
                url = "http://nominatim.openstreetmap.org/search"
                requestParams.add("q", "$lat,$lon")
                requestParams.add("format", "json")
                requestParams.add("addressdetails", "1")
                requestParams.add("limit", "1")
                asyncHttpTask.getGeoJsonArray(url, requestParams)
            }
            "Google" -> {
                url = "https://maps.googleapis.com/maps/api/geocode/json"
                requestParams.add("address", "$lat,$lon")
                requestParams.add("language", Locale.getDefault().language)
                requestParams.add("key", "AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4")
                asyncHttpTask.getGeoJson(url, requestParams)
            }
        }
    }

    override fun doPostExecute(jsonObject: JSONObject?, typeJson: String) {
        try {
            if (jsonObject == null) return

            when (AppParams.MAPS[map]) {
                "Yandex", "Yandex narod" -> {
                    val jsonObjectYandex = jsonObject.getJSONObject("response").getJSONObject("GeoObjectCollection").getJSONArray("featureMember").getJSONObject(0).getJSONObject("GeoObject")

                    city = jsonObjectYandex.getJSONObject("metaDataProperty").getJSONObject("GeocoderMetaData").getJSONObject("AddressDetails").getJSONObject("Country").getJSONObject("AdministrativeArea")
                            .getJSONObject("SubAdministrativeArea").getJSONObject("Locality").getString("LocalityName")

                    val arrAdr = jsonObjectYandex.getString("name").split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (arrAdr.size > 1) {
                        street = arrAdr[0]
                        house = arrAdr[1]
                    } else {
                        street = arrAdr[0]
                        house = ""
                    }
                }
                "Open Street Map" -> {
                    val jsonObjectOsm = jsonObject.getJSONObject("address")

                    city = jsonObjectOsm.getString("city")
                    street = jsonObjectOsm.getString("road")
                    house = jsonObjectOsm.getString("house_number")
                }
                "Google" -> {
                    val jsonArrayGoogle = jsonObject.getJSONArray("results").getJSONObject(0).getJSONArray("address_components")
                    for (j in 0 until jsonArrayGoogle.length()) {
                        val jsonComponent = jsonArrayGoogle.getJSONObject(j)

                        when (jsonComponent.getJSONArray("types").getString(0)) {
                            "street_number" -> house = jsonComponent.getString("short_name")
                            "route" -> street = jsonComponent.getString("short_name")
                            "locality" -> city = jsonComponent.getString("short_name")
                        }
                    }
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    fun getAddress(): HashMap<String, String> {
        address["lat"] = lat
        address["lon"] = lon

        return address
    }
}
