package com.kabbi.driver.helper;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.kabbi.driver.BuildConfig;
import com.kabbi.driver.R;

import java.util.UUID;

public class AppParams {

    public static final Boolean SET_ORDER_PARAMS_VIA_SOCKET = false;

    public static final String HOST = "https://da2.kabbi.com:8088/v3/api";
    public static final String HOST_NEW = "https://da2.kabbi.com:8088";
    public static final String CHAT_SERVER = "https://ch2.kabbi.com:3003";

    public static final String URL_GEOCODE = "https://geo.kabbi.com/v1/autocomplete";

    public static final String LOCAION = "GPS";

    public static final String TENANT = "taxi";

    public static final int VERSION_APP = BuildConfig.VERSION_CODE;

    public static final int COUNT_ADDRESS = 19;

    /* ------------------------------------------------------------------------------------------ */

    public static final long UNIT_TIME = 1000;

    public static final int TIME = 15000;

    /**
     * Theme default
     */
    public static final int THEME_DEFAULT = R.style.AppThemeLight;

    /**
     * Zoom map
     */
    public static final int ZOOM = 17;
    public static final boolean USE_WORKER_REG = false;
    public static final String WORKER_REG_URL = "https://www.kabbi.com";
    public static final String WORKER_REG_DESCRIPTION = "Если у вас нет аккаунта, зарегистрируйтесь водителем";
    public static final String WORKER_REG_BUTTON = "Стать водителем";
    /**
     * Favorite_map: 3-google, 2-osm, 1-yandex, 0-yandex narod
     */
    public static String MAP = "1";
    public static String DEFAULT_LANG = "0";
    /**
     * Notification parameters
     */
    public static String CHANNEL_ID_UPDATE_FREE = "40";
    public static String CHANNEL_ID_UPDATE_PRE = "41";
    public static String CHANNEL_ID_ORDER_REJECTED = "42";
    public static String CHANNEL_ID_UPDATE_FREE_ORDERS = "43";
    public static String CHANNEL_ID_ORDER_UPDATED = "44";
    public static String CHANNEL_ID_REMOVED_FROM_R_PRE_ORDER = "45";
    public static String CHANNEL_ID_BALANCE = "46";
    public static String CHANNEL_ID_UNSOS = "47";
    public static String CHANNEL_ID_REMIND_PRE_ORDER = "48";
    public static String CHANNEL_ID_UPDATE_PRE_ORDERS = "49";
    public static String CHANNEL_ID_ASSIGN_PRE_ORDER = "50";
    public static String CHANNEL_ID_ASSIGN_ORDER = "51";
    public static String CHANNEL_ID_BULK = "3";
    public static String CHANNEL_ID_SOS = "5";
    public static String NOTIFICATION_NAME_SOS = "SOS";
    public static String NOTIFICATION_NAME_BULK = "Bulk";
    public static String NOTIFICATION_NAME_UPDATE_FREE = "Update Free Orders";
    public static String NOTIFICATION_NAME_UPDATE_PRE = "Update pre orders";
    public static String NOTIFICATION_NAME_ORDER_REJECTED = "Order is rejected";
    public static String NOTIFICATION_NAME_ORDER_UPDATED = "Order is updated";
    public static String NOTIFICATION_NAME_REMOVED_FROM_R_PRE_ORDER = "Removed from reserved pre order";
    public static String NOTIFICATION_NAME_BALANCE = "New balance";
    public static String NOTIFICATION_NAME_UNSOS = "Unsos";
    public static String NOTIFICATION_NAME_REMIND_PRE_ORDER = "Remind pre order";
    public static String NOTIFICATION_UPDATE_PRE_ORDERS = "Update pre orders";
    public static String NOTIFICATION_ASSIGN_PRE_ORDER = "Assign at pre order";
    public static String NOTIFICATION_NAME_ASSIGN_ORDER = "Assign at order";
    /**
     * IntentFilter name
     */
    public static String BROADCAST_ORDER = "com.kabbi.driver.order";
    public static String BROADCAST_PUSH = "com.kabbi.driver.push";
    public static String BROADCAST_PUSH_FREE = "com.kabbi.driver.pushfree";
    public static String BROADCAST_PUSH_PRE = "com.kabbi.driver.pushpre";
    public static String BROADCAST_PUSH_ORDER = "com.kabbi.driver.pushorder";
    public static String BROADCAST_PUSH_SOS = "com.kabbi.driver.pushsos";
    public static String BROADCAST_DIALOG = "com.kabbi.driver.dialog";
    public static String BROADCAST_PREORDER = "com.kabbi.driver.preorder";
    public static String BROADCAST_PUSH_STATUSORDER = "com.kabbi.driver.statusorder";
    public static String BROADCAST_LANG = "com.kabbi.driver.lang";
    /**
     * Params for broadcast receivers
     */
    public static String COORDS_GPS_LAT = "coords_gps_lat";
    public static String COORDS_GPS_LON = "coords_gps_lon";
    public static String SPEED_GPS = "speed_gps";
    public static String BEARING_GPS = "bearing_gps";
    public static String POLYGON_GPS = "polygon_gps";
    public static String ACCURACY_GPS = "accuracy_gps";
    /**
     * Range accuracy in meters
     */
    public static int ACCURACY_GPS_HIGHT = 5;
    public static int ACCURACY_GPS_MIDLE = 10;
    /**
     * Codes for requests
     */
    public static String ORDER_STATUS_CANCEL = "3";
    public static String ORDER_STATUS_CANCEL_IN_WORK = "120";
    public static String ORDER_STATUS_CONFIRM = "17";
    public static String ORDER_STATUS_COMPLETED = "37";
    public static String ORDER_STATUS_COMPLETED_FALSE = "38";
    public static String ORDER_STATUS_ARRIVED = "26";
    public static String ORDER_STATUS_CAR_ASSIGNED = "36";
    public static String ORDER_STATUS_COMPLETED_NOCASH = "106";
    public static String ORDER_STATUS_STOP_ORDER = "110";
    public static String ORDER_STATUS_RESERVE = "7";
    public static String ORDER_STATUS_RESERVE_CONFIRM = "55";
    public static String ORDER_STATUS_RESERVE_XZ = "113";
    public static String ORDER_STATUS_RESERVE_CANCEL = "10";
    public static String ORDER_STATUS_ASSIGNED_CANCEL = "115";
    public static String ORDER_STATUS_RESERVECONFIRM_TRUE = "119";
    public static String ORDER_STATUS_RESERVECONFIRM_FALSE = "118";
    public static String ORDER_LABEL_RESERVE = "reserve";
    public static String ORDER_LABEL_NEW = "new";
    public static int REQUEST_TYPE_NORMAL = 0;

    public static String EMPTY_RESPONSE = "response_is_empty";
    public static String RESPONSE_ERROR = "response_is_incorrect";

    public static String[] MAPS = {"Open Street Map", "Google"};
    public static String[] LANG = {"de", "en"};
    public static String[] NAVI = {"waze", "google"};
    public static String[] LANG_SOUND = {"de", "en"};

    public static String getLangLabel(Context context, int index) {
        String[] LANG = {context.getString(R.string.language_de),
                context.getString(R.string.language_en)
//                context.getString(R.string.language_ru)
        };
        return LANG[index];
    }

    public static String getMapLabel(Context context, int index) {
        String[] MAPS = {"Open Street Map", "Google"};
        return MAPS[index];
    }

    public static String getNavi(Context context, int index) {
        String[] NAVI = {context.getString(R.string.navi_waze), "maps.me"};
        return NAVI[index];
    }

    /**
     * Return  unique identifier device
     */
    public static String getUDID(Context context) {
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        UUID deviceUUID = new UUID(androidId.hashCode(), ((long) Build.VERSION.RELEASE.hashCode()) | Build.MODEL.hashCode());
        return deviceUUID.toString();
    }

    public static String colorGrey(Context context) {
        return "#" + (Integer.toHexString(context.getResources().getColor(R.color.grey_text))).substring(2, 8);
    }

    public static String colorBlack(Context context) {
        return "#" + (Integer.toHexString(context.getResources().getColor(R.color.black))).substring(2, 8);
    }

    public static String colorGreen(Context context) {
        return "#" + (Integer.toHexString(context.getResources().getColor(R.color.green))).substring(2, 8);
    }

    public static String colorRed(Context context) {
        return "#" + (Integer.toHexString(context.getResources().getColor(R.color.red))).substring(2, 8);
    }

    public static String colorOrange(Context context) {
        return "#" + (Integer.toHexString(context.getResources().getColor(R.color.orange_main))).substring(2, 8);
    }

    public enum RequestType {
        BACK, CANCEL, CANCEL_ORDER, CANCEL_PREORDER, CONFIRM_PREORDER, NORMAL, REJECT_PREORDER, RESERVE, UPDATE, NONE
    }

    public class LogEvent{
        public static final String FINISH = "finish";
    }
}

