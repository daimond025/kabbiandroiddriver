package com.kabbi.driver.helper

import android.util.Log

object BonusSystem {

    fun getBonusCost(cost: Double, max_payment: Double, client_bonus_balance: Double, max_payment_type: String, payment_method: String): Double {
        var updateCost = cost
        val downCost: Double
        if (payment_method == ClientTariffParser.METHOD_PAYMENT_FULL) {
            if (updateCost <= client_bonus_balance)
                updateCost = 0.0
        } else {
            when (max_payment_type) {
                ClientTariffParser.TYPE_MAX_PAYMENT_PERCENT -> {
                    downCost = updateCost / 100 * max_payment
                    if (downCost > client_bonus_balance)
                        updateCost -= client_bonus_balance
                    else
                        updateCost -= downCost
                }
                ClientTariffParser.TYPE_MAX_PAYMENT_BONUS -> if (updateCost < max_payment) {
                    if (updateCost < client_bonus_balance) {
                        updateCost = 0.0
                    } else {
                        updateCost -= client_bonus_balance
                    }
                } else {
                    updateCost -= if (updateCost > client_bonus_balance) {
                        client_bonus_balance
                    } else {
                        max_payment
                    }
                }
            }
        }
        return updateCost
    }

    fun getPromoCost(cost: Double, max_payment: Double): Double {
        var updateCost = cost
        val downCost: Double
        if (max_payment >= 100) {
            updateCost = 0.0
        } else {
            Log.d("PROMO", "$updateCost $max_payment")

            downCost = updateCost / 100 * max_payment
            updateCost -= downCost

            Log.d("PROMO", "$updateCost $max_payment $downCost $updateCost")
        }
        return updateCost
    }
}
