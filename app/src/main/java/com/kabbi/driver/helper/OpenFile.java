package com.kabbi.driver.helper;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class OpenFile {

    Context context;

    public OpenFile (Context context) {
        this.context = context;



    }

    public String getJson() {
        return ReadFile();
    }


    private String ReadFile() {

        StringBuilder sbr = new StringBuilder();

        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader br = new BufferedReader(new InputStreamReader(assetManager.open("json.json")));
            String str = "";

            while ((str = br.readLine()) != null){
                sbr.append(str);
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return sbr.toString();
    }
}