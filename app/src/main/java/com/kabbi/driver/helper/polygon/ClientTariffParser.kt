//package com.kabbi.driver.helper.polygon
//
//import com.kabbi.driver.models.ClientTariff
//import org.json.JSONArray
//import org.json.JSONException
//import org.json.JSONObject
//
//class ClientTariffParser {
//
//    fun createTariff(jsonObject: JSONObject, day: Int, area: String, jsonObjectOrder: JSONObject): ClientTariff {
//        val clientTariff = ClientTariff()
//        var bonus_payment = 0
//        try {
//            clientTariff.accrual = jsonObject.getString("accrual")
//            clientTariff.setTariffId(jsonObject.getString("tariff_id"))
//            clientTariff.setArea(area)
//            //clientTariff.setOrder(order);
//            if (day == 1) {
//                clientTariff.setPlantingPrice(java.lang.Double.valueOf(jsonObject.getString("planting_price_day")))
//                clientTariff.setPlantingInclude(java.lang.Double.valueOf(jsonObject.getString("planting_include_day")))
//                clientTariff.setMinPrice(java.lang.Double.valueOf(jsonObject.getString("min_price_day")))
//                clientTariff.setSupplyPrice(java.lang.Double.valueOf(jsonObject.getString("supply_price_day")))
//                clientTariff.setWaitTime(java.lang.Long.valueOf(jsonObject.getString("wait_time_day")))
//                clientTariff.setWaitDrivingTime(java.lang.Long.valueOf(jsonObject.getString("wait_driving_time_day")))
//                clientTariff.setWaitPrice(java.lang.Double.valueOf(jsonObject.getString("wait_price_day")))
//                clientTariff.setSpeedDowntime(java.lang.Double.valueOf(jsonObject.getString("speed_downtime_day")))
//                clientTariff.setRounding(if (java.lang.Double.valueOf(jsonObject.getString("rounding_day")) > 0) java.lang.Double.valueOf(jsonObject.getString("rounding_day")) else 1)
//                clientTariff.setNextCostUnit(jsonObject.getString("next_cost_unit_day"))
//                clientTariff.setRoundingType(jsonObject.getString("rounding_type_day"))
//
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_INTERVAL) {
//                    val jsonArrayInterval = jsonObject.getJSONArray("next_km_price_day")
//                    val jsonArrayIntervalNew = JSONArray()
//                    for (j in 0 until jsonArrayInterval.length()) {
//                        val jsonItem = jsonArrayInterval.getJSONObject(j)
//                        val jsonItemNew = JSONObject()
//                        jsonItemNew.put("price", java.lang.Double.valueOf(jsonItem.getString("price")))
//                        if (jsonItem.getJSONArray("interval").length() > 1) {
//                            jsonItemNew.put("interval", JSONArray()
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day")))
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(1)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day"))))
//                        } else {
//                            jsonItemNew.put("interval", JSONArray()
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day"))))
//                        }
//                        jsonArrayIntervalNew.put(jsonItemNew)
//                    }
//                    clientTariff.setNextKmPriceArray(jsonArrayIntervalNew.toString())
//                } else {
//                    clientTariff.setNextKmPrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_day")))
//                }
//
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_MIXED) {
//                    clientTariff.setNextTimePrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_day_time")))
//                    clientTariff.setPlantingIncludeTime(java.lang.Double.valueOf(jsonObject.getString("planting_include_day_time")))
//                }
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_TIME) {
//                    clientTariff.setNextTimePrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_day")))
//                }
//            } else {
//                clientTariff.setPlantingPrice(java.lang.Double.valueOf(jsonObject.getString("planting_price_night")))
//                clientTariff.setPlantingInclude(java.lang.Double.valueOf(jsonObject.getString("planting_include_night")))
//                clientTariff.setMinPrice(java.lang.Double.valueOf(jsonObject.getString("min_price_night")))
//                clientTariff.setSupplyPrice(java.lang.Double.valueOf(jsonObject.getString("supply_price_night")))
//                clientTariff.setWaitTime(java.lang.Long.valueOf(jsonObject.getString("wait_time_night")))
//                clientTariff.setWaitDrivingTime(java.lang.Long.valueOf(jsonObject.getString("wait_driving_time_night")))
//                clientTariff.setWaitPrice(java.lang.Double.valueOf(jsonObject.getString("wait_price_night")))
//                clientTariff.setSpeedDowntime(java.lang.Double.valueOf(jsonObject.getString("speed_downtime_night")))
//                clientTariff.setRounding(if (java.lang.Double.valueOf(jsonObject.getString("rounding_night")) > 0) java.lang.Double.valueOf(jsonObject.getString("rounding_night")) else 1)
//                clientTariff.setNextCostUnit(jsonObject.getString("next_cost_unit_night"))
//                clientTariff.setRoundingType(jsonObject.getString("rounding_type_night"))
//
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_INTERVAL) {
//                    val jsonArrayInterval = jsonObject.getJSONArray("next_km_price_night")
//                    val jsonArrayIntervalNew = JSONArray()
//                    for (j in 0 until jsonArrayInterval.length()) {
//                        val jsonItem = jsonArrayInterval.getJSONObject(j)
//                        val jsonItemNew = JSONObject()
//                        jsonItemNew.put("price", java.lang.Double.valueOf(jsonItem.getString("price")))
//                        if (jsonItem.getJSONArray("interval").length() > 1) {
//                            jsonItemNew.put("interval", JSONArray()
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_night")))
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(1)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_night"))))
//                        } else {
//                            jsonItemNew.put("interval", JSONArray()
//                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_night"))))
//                        }
//                        jsonArrayIntervalNew.put(jsonItemNew)
//                    }
//                    clientTariff.setNextKmPriceArray(jsonArrayIntervalNew.toString())
//                } else {
//                    clientTariff.setNextKmPrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_night")))
//                }
//
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_MIXED) {
//                    clientTariff.setNextTimePrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_night_time")))
//                    clientTariff.setPlantingIncludeTime(java.lang.Double.valueOf(jsonObject.getString("planting_include_night_time")))
//                }
//                if (jsonObject.getString("accrual") == ClientTariff.ACCRUAL_TIME) {
//                    clientTariff.setNextTimePrice(java.lang.Double.valueOf(jsonObject.getString("next_km_price_night")))
//                }
//            }
//
//            if (jsonObject.getString("area") == "AIRPORT")
//                clientTariff.setOrderType(ClientTariff.ORDER_TYPE_AIRPORT)
//            else if (jsonObject.getString("area") == "STATION")
//                clientTariff.setOrderType(ClientTariff.ORDER_TYPE_STATION)
//            else
//                clientTariff.setOrderType(ClientTariff.ORDER_TYPE_STANDART)
//
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//
//        try {
//            bonus_payment = Integer.valueOf(jsonObjectOrder.getString("bonus_payment"))
//
//            try {
//                if (jsonObjectOrder.isNull("bonusData") || jsonObjectOrder.getJSONArray("bonusData").length() == 0)
//                    bonus_payment = 0
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        if (bonus_payment == 1) {
//            clientTariff.isBonusPayment = true
//            try {
//                if (jsonObjectOrder.getJSONObject("bonusData").getString("payment_method") == ClientTariff.METHOD_PAYMENT_FULL) {
//                    if (jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type") == ClientTariff.TYPE_MAX_PAYMENT_BONUS) {
//                        clientTariff.maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance "))
//                    } else {
//                        clientTariff.maxPayment = 100.0
//                    }
//                } else {
//                    clientTariff.maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("max_payment"))
//                }
//
//                clientTariff.clientBonusBalance = Math.floor(java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance ")))
//                clientTariff.bonusId = jsonObjectOrder.getJSONObject("bonusData").getString("bonus_id")
//                clientTariff.paymentMethod = jsonObjectOrder.getJSONObject("bonusData").getString("payment_method")
//                clientTariff.maxPaymentType = jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type")
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//
//        } else {
//            clientTariff.isBonusPayment = false
//        }
//        return clientTariff
//    }
//}