package com.kabbi.driver.helper;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.view.View;

import com.kabbi.driver.R;

import java.util.Locale;

public class TypefaceSpanEx extends MetricAffectingSpan {
    private final Typeface mTypeface;

    public TypefaceSpanEx(Typeface typeface) {
        mTypeface = typeface;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        apply(ds, mTypeface);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        apply(paint, mTypeface);
    }

    private static void apply(Paint paint, Typeface tf) {
        int oldStyle;

        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();

        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(tf);
    }

    public static String getCurrencyStr(Context context, View view, String cost) {
        AppPreferences appPreferences = new AppPreferences(context);

        //if (ViewCompat.getLayoutDirection(view) == ViewCompat.LAYOUT_DIRECTION_RTL) {
        if (Locale.getDefault().getLanguage().equals("fa") || Locale.getDefault().getLanguage().equals("ar")) {
            // The view has RTL layout
            if (appPreferences.getText("currency").equals("RUB")) {
                return "P " + cost;
            } else {
                return appPreferences.getText("currency") + " " + cost;
            }
        } else {
            // The view has LTR layout
            if (appPreferences.getText("currency").equals("RUB")) {
                return cost + " P";
            } else {
                return cost + " " + appPreferences.getText("currency");
            }
        }

    }

    public static String getOnlyCurrencyStr(Context context) {
        AppPreferences appPreferences = new AppPreferences(context);
        if (appPreferences.getText("currency").equals("RUB")) {
            final Typeface roubleSupportedTypeface =
                    Typeface.createFromAsset(context.getAssets(), "fonts/rouble2.ttf");

            SpannableStringBuilder resultSpan = new SpannableStringBuilder(context.getString(R.string.main_price));
            for (int i = 0; i < resultSpan.length(); i++) {
                if (resultSpan.charAt(i) == '\u20BD') {
                    TypefaceSpanEx roubleTypefaceSpan = new TypefaceSpanEx(roubleSupportedTypeface);
                    resultSpan.setSpan(roubleTypefaceSpan, i, i + 1, 0);
                }
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                return "P";

            return resultSpan.toString();
        } else {
            return appPreferences.getText("currency");
        }
    }
}
