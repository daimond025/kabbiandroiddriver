package com.kabbi.driver.helper;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Round {

    //ROUND FLOOR CEIL

    public static String roundFormatFinal(double number, double round, String roundType) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);

        double formatDouble;
        if (round >= 1) {
            decimalFormat.applyPattern("###.##");
            switch (roundType) {
                case "FLOOR":
                    formatDouble = Math.round(Math.floor(number / round) * round);
                    break;
                case "CEIL":
                    formatDouble = Math.round(Math.ceil(number / round) * round);
                    break;
                default:
                    formatDouble = Math.round(Math.round(number / round) * round);
            }

            return decimalFormat.format(formatDouble).replace(",", ".");
        } else {
            decimalFormat.applyPattern("###.00");
            BigDecimal numberBD = new BigDecimal(number);
            BigDecimal roundBD = new BigDecimal(round);
            BigDecimal resultBD = numberBD.divide(roundBD, 2);
            switch (roundType) {
                case "FLOOR":
                    formatDouble = Math.floor(resultBD.doubleValue()) * round;

                    break;
                case "CEIL":
                    formatDouble = Math.ceil(resultBD.doubleValue()) * round;

                    break;
                default:
                    formatDouble = Math.round(number / round) * round;
            }

            return decimalFormat.format(formatDouble).replace(",", ".");
        }
    }

    public static double roundFormatDouble(double number, double round, String roundType) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        double formatDouble;
        if (round >= 1) {
            decimalFormat.applyPattern("###.##");
            switch (roundType) {
                case "FLOOR":
                    formatDouble = Math.round(Math.floor(number / round) * round);
                    break;
                case "CEIL":
                    formatDouble = Math.round(Math.ceil(number / round) * round);
                    break;
                default:
                    formatDouble = Math.round(Math.round(number / round) * round);
            }
            return Double.valueOf(decimalFormat.format(formatDouble).replace(",", "."));
        } else {
            BigDecimal numberBD = new BigDecimal(number);
            BigDecimal roundBD = new BigDecimal(round);
            BigDecimal resultBD = numberBD.divide(roundBD, 2);
            decimalFormat.applyPattern("###.00");
            switch (roundType) {
                case "FLOOR":
                    formatDouble = Math.floor(resultBD.doubleValue()) * round;
                    break;
                case "CEIL":
                    formatDouble = Math.ceil(resultBD.doubleValue()) * round;
                    break;
                default:
                    formatDouble = Math.round(number / round) * round;
            }
            return Double.valueOf(decimalFormat.format(formatDouble).replace(",", "."));
        }
    }

    public static String roundFormat(double number, double round) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        if (round % 1 != 0)
            decimalFormat.applyPattern("##0.00");
        else
            decimalFormat.applyPattern("###");
        return decimalFormat.format(number).replace(",", ".");
    }

    public static String roundFormat10(double number) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        decimalFormat.applyPattern("##0.00");
        if (number > 0)
            return decimalFormat.format(number).replace(",", ".");
        else
            return "0";
    }

    public static double roundFormatDis(double dis) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        decimalFormat.applyPattern("###.###");
        return Double.valueOf(decimalFormat.format(dis).replace(",", "."));
    }

    public static String roundLocale(double number, String currency) {
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
        switch (currency) {
            case "RUB":
                decimalFormat.applyPattern("###");
                return decimalFormat.format(number);
            case "BYR":
                decimalFormat.applyPattern("###");
                double formatDouble = Math.round(Math.round(number / 1000) * 1000);
                return decimalFormat.format(formatDouble);
            default:
                decimalFormat.applyPattern("###.##");
                return decimalFormat.format(number).replace(",", ".");
        }
    }

}
