package com.kabbi.driver.helper

import com.kabbi.driver.database.entities.ClientTariff
import com.kabbi.driver.myOrders.OrderRaw
import com.kabbi.driver.myOrders.PriceDay
import com.kabbi.driver.myOrders.TariffData
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class ClientTariffParser {

    companion object {
        val AREA_CITY = "CITY"
        val AREA_TRACK = "TRACK"
        val ACCRUAL_TIME = "TIME"
        val ACCRUAL_DISTANCE = "DISTANCE"
        val ACCRUAL_FIX = "FIX"
        val ACCRUAL_MIXED = "MIXED"
        val ACCRUAL_INTERVAL = "INTERVAL"
        val ACCRUAL_WIFI = "WIFI"

        val COST_UNIT_30 = "30_minute"
        val COST_UNIT_60 = "1_hour"

        val METHOD_PAYMENT_PARTIAL = "PARTIAL"
        val METHOD_PAYMENT_FULL = "FULL"
        val TYPE_MAX_PAYMENT_PERCENT = "PERCENT"
        val TYPE_MAX_PAYMENT_BONUS = "BONUS"
    }

    private var minPrice = 0.0
    private var bonus_payment = 0
    private var accrual = ""
    private var tariffId = ""
    private var nextCostUnit = ""
    private var roundingType = ""
    private var plantingPrice = 0.0
    private var plantingInclude = 0.0
    private var supplyPrice = 0.0
    private var waitPrice = 0.0
    private var speedDowntime = 0.0
    private var rounding = 0.0
    private var nextKmPrice = 0.0
    private var nextTimePrice = 0.0
    private var plantingIncludeTime = 0.0
    private var maxPayment = 0.0
    private var clientBonusBalance = 0.0
    private var waitTime = 0L
    private var waitDrivingTime = 0L
    private var nextKmPriceArray = ""
    private var orderType = ""
    private var bonusId = ""
    private var paymentMethod = ""
    private var maxPaymentType = ""
    private var isBonusPayment = false

    private val ORDER_TYPE_STANDART = "STANDART"
    private val ORDER_TYPE_AIRPORT = "AIRPORT"
    private val ORDER_TYPE_STATION = "STATION"

    fun createTariff(data: TariffData, day: Int, area: String, orderRaw: OrderRaw): ClientTariff {
        accrual = data.accrual!!
        tariffId = data.tariff_id!!.toString()
        if (day == 1) {
            plantingPrice = data.planting_price_day!!.toDouble()
            plantingInclude = data.planting_include_day!!.toDouble()
            minPrice = data.min_price_day!!.toDouble()
            supplyPrice = data.supply_price_day!!.toDouble()
            waitTime = data.wait_time_day!!
            waitDrivingTime = data.wait_driving_time_day!!
            waitPrice = data.wait_price_day!!
            speedDowntime = data.speed_downtime_day!!.toDouble()
            rounding = if (data.rounding_day!! > 0) data.rounding_day else 1.0
            nextCostUnit = data.next_cost_unit_day!!
            roundingType = data.rounding_type_day!!

            if (data.accrual == ACCRUAL_INTERVAL) {

                val newPrices = mutableListOf<PriceDay>()

                for (priceDay in data.next_km_price_day) {
                    val newPrice = PriceDay()
                    val newIntervals = mutableListOf<String>()

                    newPrice.price = priceDay.price

                    if (priceDay.interval.size > 1) {
                        newIntervals.add((priceDay.interval[0].toDouble() - data.planting_include_day).toString())
                        newIntervals.add((priceDay.interval[1].toDouble() - data.planting_include_day).toString())
                    } else {
                        newIntervals.add((priceDay.interval[0].toDouble() - data.planting_include_day).toString())
                    }

                    newPrice.interval = newIntervals.toTypedArray()
                    newPrices.add(newPrice)
                }
                nextKmPriceArray = newPrices.toString()

            } else {
                nextKmPrice = data.next_km_price_day[0].price.toDouble()
            }

            if (data.accrual == ACCRUAL_MIXED) {
                nextTimePrice = data.next_km_price_day_time?.toDouble() ?: 0.0
                plantingIncludeTime = data.planting_include_day_time?.toDouble() ?: 0.0
            }
            if (data.accrual == ACCRUAL_TIME) {
                nextTimePrice = data.next_km_price_day[0].price.toDouble()
            }
        } else {

            plantingPrice = data.planting_price_night!!.toDouble()
            plantingInclude = data.planting_include_night!!.toDouble()
            minPrice = data.min_price_night!!.toDouble()
            supplyPrice = data.supply_price_night!!.toDouble()
            waitTime = data.wait_time_night!!
            waitDrivingTime = data.wait_driving_time_night!!
            waitPrice = data.wait_price_night!!
            speedDowntime = data.speed_downtime_night!!.toDouble()
            rounding = if (data.rounding_night!! > 0) data.rounding_night else 1.0
            nextCostUnit = data.next_cost_unit_night!!
            roundingType = data.rounding_type_night!!

            if (data.accrual == ACCRUAL_INTERVAL) {

                val newPrices = mutableListOf<PriceDay>()

                for (priceDay in data.next_km_price_day) {
                    val newPrice = PriceDay()
                    val newIntervals = mutableListOf<String>()

                    newPrice.price = priceDay.price

                    if (priceDay.interval.size > 1) {
                        newIntervals.add((priceDay.interval[0].toDouble() - data.planting_include_night).toString())
                        newIntervals.add((priceDay.interval[1].toDouble() - data.planting_include_night).toString())
                    } else {
                        newIntervals.add((priceDay.interval[0].toDouble() - data.planting_include_night).toString())
                    }

                    newPrice.interval = newIntervals.toTypedArray()
                    newPrices.add(newPrice)
                }
                nextKmPriceArray = newPrices.toString()

            } else {
                nextKmPrice = data.next_km_price_night[0].price.toDouble()
            }

            if (data.accrual == ACCRUAL_MIXED) {
                nextTimePrice = data.next_km_price_day_time?.toDouble() ?: 0.0
                plantingIncludeTime = data.planting_include_day_time?.toDouble() ?: 0.0
            }
            if (data.accrual == ACCRUAL_TIME) {
                nextTimePrice = data.next_km_price_night[0].price.toDouble()
            }
        }

        orderType = when (data.area) {
            "AIRPORT" -> (ORDER_TYPE_AIRPORT)
            "STATION" -> (ORDER_TYPE_STATION)
            else -> (ORDER_TYPE_STANDART)
        }

        bonus_payment = orderRaw.bonus_payment?.run { toInt() } ?: 0

        if (orderRaw.bonusData.isNullOrEmpty())
            bonus_payment = 0

        if (bonus_payment == 1) {
            isBonusPayment = true
            println("bonusPayment")
//            try {
//                if (orderRaw.bonusData.payment_method == METHOD_PAYMENT_FULL) {
//                    if (jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type") == TYPE_MAX_PAYMENT_BONUS) {
//                        maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance "))
//                    } else {
//                        maxPayment = 100.0
//                    }
//                } else {
//                    maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("max_payment"))
//                }
//
//                clientBonusBalance = Math.floor(java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance ")))
//                bonusId = jsonObjectOrder.getJSONObject("bonusData").getString("bonus_id")
//                paymentMethod = jsonObjectOrder.getJSONObject("bonusData").getString("payment_method")
//                maxPaymentType = jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type")
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }

        } else {
            isBonusPayment = false
        }


        return ClientTariff(accrual,
                area,
                plantingPrice,
                plantingInclude,
                plantingIncludeTime,
                nextKmPrice,
                nextKmPriceArray,
                minPrice,
                supplyPrice,
                waitTime,
                waitDrivingTime,
                waitPrice,
                speedDowntime,
                rounding,
                orderType,
                nextCostUnit,
                nextTimePrice,
                null,
                tariffId,
                isBonusPayment,
                maxPayment,
                clientBonusBalance,
                bonusId,
                paymentMethod,
                maxPaymentType,
                roundingType)
    }

    fun createTariff(jsonObject: JSONObject, day: Int, area: String, jsonObjectOrder: JSONObject): ClientTariff {

        try {
            accrual = jsonObject.getString("accrual")
            tariffId = (jsonObject.getString("tariff_id"))
            if (day == 1) {
                plantingPrice = jsonObject.getString("planting_price_day").toDouble()
                plantingInclude = jsonObject.getString("planting_include_day").toDouble()
                minPrice = jsonObject.getString("min_price_day").toDouble()
                supplyPrice = jsonObject.getString("supply_price_day").toDouble()
                waitTime = jsonObject.getString("wait_time_day").toLong()
                waitDrivingTime = jsonObject.getString("wait_driving_time_day").toLong()
                waitPrice = (java.lang.Double.valueOf(jsonObject.getString("wait_price_day")))
                speedDowntime = (java.lang.Double.valueOf(jsonObject.getString("speed_downtime_day")))
                rounding = if (jsonObject.getString("rounding_day").toDouble() > 0) jsonObject.getString("rounding_day").toDouble() else 1.0
                nextCostUnit = jsonObject.getString("next_cost_unit_day")
                roundingType = jsonObject.getString("rounding_type_day")

                if (jsonObject.getString("accrual") == ACCRUAL_INTERVAL) {
                    val jsonArrayInterval = jsonObject.getJSONArray("next_km_price_day")
                    val jsonArrayIntervalNew = JSONArray()
                    for (j in 0 until jsonArrayInterval.length()) {
                        val jsonItem = jsonArrayInterval.getJSONObject(j)
                        val jsonItemNew = JSONObject()
                        jsonItemNew.put("price", java.lang.Double.valueOf(jsonItem.getString("price")))
                        if (jsonItem.getJSONArray("interval").length() > 1) {
                            jsonItemNew.put("interval", JSONArray()
                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day")))
                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(1)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day"))))
                        } else {
                            jsonItemNew.put("interval", JSONArray()
                                    .put(java.lang.Double.valueOf(jsonItem.getJSONArray("interval").getString(0)) - java.lang.Double.valueOf(jsonObject.getString("planting_include_day"))))
                        }
                        jsonArrayIntervalNew.put(jsonItemNew)
                    }
                    nextKmPriceArray = jsonArrayIntervalNew.toString()
                } else {
                    nextKmPrice = jsonObject.getJSONArray("next_km_price_day").getJSONObject(0).getString("price").toDouble()
                }

                if (jsonObject.getString("accrual") == ACCRUAL_MIXED) {
                    nextTimePrice = jsonObject.getString("next_km_price_day_time").toDouble()
                    plantingIncludeTime = jsonObject.getString("planting_include_day_time").toDouble()
                }
                if (jsonObject.getString("accrual") == ACCRUAL_TIME) {
                    nextTimePrice = jsonObject.getJSONArray("next_km_price_day").getJSONObject(0).getString("price").toDouble()
                }
            } else {
                plantingPrice = (java.lang.Double.valueOf(jsonObject.getString("planting_price_night")))
                plantingInclude = (java.lang.Double.valueOf(jsonObject.getString("planting_include_night")))
                minPrice = (java.lang.Double.valueOf(jsonObject.getString("min_price_night")))
                supplyPrice = (java.lang.Double.valueOf(jsonObject.getString("supply_price_night")))
                waitTime = (java.lang.Long.valueOf(jsonObject.getString("wait_time_night")))
                waitDrivingTime = (java.lang.Long.valueOf(jsonObject.getString("wait_driving_time_night")))
                waitPrice = (java.lang.Double.valueOf(jsonObject.getString("wait_price_night")))
                speedDowntime = (java.lang.Double.valueOf(jsonObject.getString("speed_downtime_night")))
                rounding = (if (java.lang.Double.valueOf(jsonObject.getString("rounding_night")) > 0) java.lang.Double.valueOf(jsonObject.getString("rounding_night")) else 1.0)
                nextCostUnit = (jsonObject.getString("next_cost_unit_night"))
                roundingType = (jsonObject.getString("rounding_type_night"))

                if (jsonObject.getString("accrual") == ACCRUAL_INTERVAL) {
                    val jsonArrayInterval = jsonObject.getJSONArray("next_km_price_night")
                    val jsonArrayIntervalNew = JSONArray()
                    for (j in 0 until jsonArrayInterval.length()) {
                        val jsonItem = jsonArrayInterval.getJSONObject(j)
                        val jsonItemNew = JSONObject()
                        jsonItemNew.put("price", java.lang.Double.valueOf(jsonItem.getString("price")))
                        if (jsonItem.getJSONArray("interval").length() > 1) {
                            jsonItemNew.put("interval", JSONArray()
                                    .put(jsonItem.getJSONArray("interval").getString(0).toDouble() - jsonObject.getString("planting_include_night").toDouble())
                                    .put(jsonItem.getJSONArray("interval").getString(1).toDouble() - jsonObject.getString("planting_include_night").toDouble()))
                        } else {
                            jsonItemNew.put("interval", JSONArray()
                                    .put(jsonItem.getJSONArray("interval").getString(0).toDouble() - jsonObject.getString("planting_include_night").toDouble()))
                        }
                        jsonArrayIntervalNew.put(jsonItemNew)
                    }
                    nextKmPriceArray = (jsonArrayIntervalNew.toString())
                } else {
                    nextKmPrice = jsonObject.getJSONArray("next_km_price_night").getJSONObject(0).getString("price").toDouble()
                }

                if (jsonObject.getString("accrual") == ACCRUAL_MIXED) {
                    nextTimePrice = jsonObject.getString("next_km_price_day_time").toDouble()
                    plantingIncludeTime = jsonObject.getString("planting_include_day_time").toDouble()
                }
                if (jsonObject.getString("accrual") == ACCRUAL_TIME) {
                    nextTimePrice = jsonObject.getJSONArray("next_km_price_night").getJSONObject(0).getString("price").toDouble()
                }
            }

            orderType = when {
                jsonObject.getString("area") == "AIRPORT" -> (ORDER_TYPE_AIRPORT)
                jsonObject.getString("area") == "STATION" -> (ORDER_TYPE_STATION)
                else -> (ORDER_TYPE_STANDART)
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        try {
            bonus_payment = Integer.valueOf(jsonObjectOrder.getString("bonus_payment"))

            try {
                if (jsonObjectOrder.isNull("bonusData") || jsonObjectOrder.getJSONArray("bonusData").length() == 0)
                    bonus_payment = 0
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (bonus_payment == 1) {
            isBonusPayment = true
            try {
                if (jsonObjectOrder.getJSONObject("bonusData").getString("payment_method") == METHOD_PAYMENT_FULL) {
                    if (jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type") == TYPE_MAX_PAYMENT_BONUS) {
                        maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance "))
                    } else {
                        maxPayment = 100.0
                    }
                } else {
                    maxPayment = java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("max_payment"))
                }

                clientBonusBalance = Math.floor(java.lang.Double.valueOf(jsonObjectOrder.getJSONObject("bonusData").getString("client_bonus_balance ")))
                bonusId = jsonObjectOrder.getJSONObject("bonusData").getString("bonus_id")
                paymentMethod = jsonObjectOrder.getJSONObject("bonusData").getString("payment_method")
                maxPaymentType = jsonObjectOrder.getJSONObject("bonusData").getString("max_payment_type")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            isBonusPayment = false
        }
        return ClientTariff(accrual,
                area,
                plantingPrice,
                plantingInclude,
                plantingIncludeTime,
                nextKmPrice,
                nextKmPriceArray,
                minPrice,
                supplyPrice,
                waitTime,
                waitDrivingTime,
                waitPrice,
                speedDowntime,
                rounding,
                orderType,
                nextCostUnit,
                nextTimePrice,
                null,
                tariffId,
                isBonusPayment,
                maxPayment,
                clientBonusBalance,
                bonusId,
                paymentMethod,
                maxPaymentType,
                roundingType)
    }
}