package com.kabbi.driver.helper;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class AppPreferences {

    private static final String APP_SHARED_PREFS = AppPreferences.class.getSimpleName();

    private SharedPreferences _sharedPreferences;
    private Editor _prefsEditor;

    public AppPreferences(Context context) {
        this._sharedPreferences = context.getSharedPreferences(APP_SHARED_PREFS, Activity.MODE_PRIVATE);
    }

    public String getText(String key) {
        return _sharedPreferences.getString(key, "");
    }

    public void saveText(String key, String text) {
        _prefsEditor = _sharedPreferences.edit();
        _prefsEditor.putString(key, text);
        _prefsEditor.apply();
    }

    public void clear() {
        _prefsEditor = _sharedPreferences.edit();
        _prefsEditor.clear();
        _prefsEditor.apply();
    }

}
