package com.kabbi.driver.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesMap {

    private Context context;
    public boolean orientation_north;
    public boolean night_mode;
    public String favorite_map;

    public PreferencesMap(Context context) {
        this.context = context;
        initPrefs();
    }

    private void initPrefs() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        orientation_north = prefs.getBoolean("orientation_north", false);
        night_mode = prefs.getBoolean("night_mode", false);
        favorite_map = prefs.getString("favourite_map", "1");
    }


}
