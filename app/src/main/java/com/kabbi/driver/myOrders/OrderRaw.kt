@file:Suppress("ArrayInDataClass")

package com.kabbi.driver.myOrders

import com.google.gson.Gson

data class OrderRaw(

        val address: Addresses?,
        val bonus_payment: String?,
        val bonusData: Array<BonusData>,
        val client: ClientRaw?,
        val clientPassenger: ClientRaw?,
        val client_passenger_phone: String?,
        val comment: String?,
        val costData: CostData?,
        val options: Array<String?>,
        val order_id: String?,
        val order_number: String,
        val order_time: String,
        val payment: String,
        val phone: String?,
        val promo_code_discount: String?,
        val settings: SettingsRaw?,
        val tariff: TariffRaw?
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}


data class Addresses(
        val A: AddressRaw?,
        val B: AddressRaw?,
        val C: AddressRaw?,
        val E: AddressRaw?,
        val F: AddressRaw?,
        val G: AddressRaw?
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}


data class BonusData(
        val payment_method: String?
)


data class AddressRaw(
        val city_id: String?,
        val city: String?,
        val street: String?,
        val housing: String?,
        val house: String?,
        val porch: String?,
        val apt: String?,
        val lat: String?,
        val lon: String?,
        val confirmation_code: String?,
        val comment: String?,
        val phone: String?,
        val parking_id: Long?,
        val parking: String?
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}


data class ClientRaw(

        val last_name: String?,
        val name: String?,
        val second_name: String?,
        val lang: String?,
        val email: String?,
        val send_to_email: String?
)


data class CostData(

        val tariffInfo: TariffInfo,
        val additionals_cost: String?,
        val summary_time: String?,
        val summary_distance: String?,
        val city_time: String?,
        val city_distance: String?,
        val city_cost: String?,
        val out_city_time: String?,
        val out_city_distance: String?,
        val out_city_cost: String?,
        val enable_parking_ratio: Int?,
        val is_fix: Int?,
        val summary_cost: String?,
        val summary_cost_no_discount: String?,
        val start_point_location: String?
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}


data class TariffInfo(

        val isDay: Int,
        val tariffType: String?,
        val tariffDataCity: TariffData,
        val tariffDataTrack: TariffData
)


data class TariffRaw(
        val name: String?,
        val valtariff_id: String?,
        val class_id: String?,
        val group_id: String?,
        val description: String?,
        val sort: String?,
        val auto_downtime: String?,
        val enabled_site: String?,
        val enabled_app: String?,
        val enabled_operator: String?,
        val enabled_bordur: String?,
        val enabled_cabinet: String?,
        val logo: String?,
        val position_id: String?,
        val type: String?,
        val `class`: Classs?
)


data class Classs(
        val class_id: String?,
        val `class`: String?
)


data class TariffData(

        val option_id: Int?,
        val tariff_id: Long?,
        val accrual: String?,
        val area: String?,
        val planting_price_day: Int?,
        val planting_price_night: Int?,
        val planting_include_day: Int?,
        val planting_include_night: Int?,
        val next_km_price_day: Array<PriceDay>,
        val next_km_price_night: Array<PriceDay>,
        val min_price_day: Int?,
        val min_price_night: Int?,
        val second_min_price_day: Int?,
        val second_min_price_night: Int?,
        val supply_price_day: Int?,
        val supply_price_night: Int?,
        val wait_time_day: Long?,
        val wait_time_night: Long?,
        val wait_driving_time_day: Long?,
        val wait_driving_time_night: Long?,
        val wait_price_day: Double?,
        val wait_price_night: Double?,
        val speed_downtime_day: Int?,
        val speed_downtime_night: Int?,
        val rounding_day: Double?,
        val rounding_night: Double?,
        val tariff_type: String?,
        val start_day: String?,
        val end_day: String?,
        val enabled_parking_ratio: Int?,
        val calculation_fix: Int?,
        val next_cost_unit_day: String?,
        val next_km_price_day_time: Int?,
        val planting_include_day_time: Int?,
        val rounding_type_day: String?,
        val next_cost_unit_night: String?,
        val rounding_type_night: String?
)


data class PriceDay(
        var interval: Array<String> = arrayOf(),
        var price: String = ""
)


data class SettingsRaw(
        val require_point_confirmation_code: String?
)