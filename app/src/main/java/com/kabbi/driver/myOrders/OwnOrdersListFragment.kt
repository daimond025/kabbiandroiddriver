package com.kabbi.driver.myOrders

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.LocationManager
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.core.view.ViewCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.kabbi.driver.R
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.adapters.OwnOrderAdapter
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.events.*
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.models.OwnOrder
import com.kabbi.driver.network.WebService
import com.kabbi.driver.offer.OrderOfferActivity
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.buildOrder
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class OwnOrdersListFragment : androidx.fragment.app.ListFragment() {

    private var orders: MutableList<OwnOrder>? = null
    private lateinit var driver: Driver
    private var adapter: OwnOrderAdapter? = null
    private lateinit var service: Service
    private lateinit var workDay: WorkDay
    private var appPreferences: AppPreferences? = null
    private var update: Boolean = false
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var typedValueBg: TypedValue? = null
    private lateinit var orderRaw: OrderRaw
    private var countDownTimer: CountDownTimer? = null
    private var progressDialog: ProgressDialog? = null
    private var uuid = ""
    private var orderId = ""

    private fun canListViewScrollUp(listView: ListView): Boolean {
        return if (android.os.Build.VERSION.SDK_INT >= 16) {
            // For ICS and above we can call canScrollVertically() to determine this
            ViewCompat.canScrollVertically(listView, -1)
        } else {
            listView.childCount > 0 && (listView.firstVisiblePosition > 0 || listView.getChildAt(0).top < listView.paddingTop)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activity = activity as WorkShiftActivity

        driver = activity.driver!!
        service = activity.service!!
        workDay = activity.workDay!!

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
        typedValueBg = TypedValue()

        appPreferences = AppPreferences(activity.applicationContext)
        update = false
        orders = ArrayList()
        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
                try {
                    uuid = ""
                    if (progressDialog != null && progressDialog!!.isShowing)
                        progressDialog!!.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val listFragmentView = super.onCreateView(inflater, container, savedInstanceState)
        swipeRefreshLayout = ListFragmentSwipeRefreshLayout(activity!!.applicationContext)
        swipeRefreshLayout.addView(listFragmentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        swipeRefreshLayout.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        swipeRefreshLayout.setColorSchemeColors(Color.parseColor(AppParams.colorOrange(activity!!.applicationContext)))
        swipeRefreshLayout.setOnRefreshListener {
            if (InternetConnection.isOnline(activity!!)) {
                swipeRefreshLayout.isRefreshing = true
                updateOrders()
            } else {
                swipeRefreshLayout.isRefreshing = false
                CustomToast.showMessage(activity, getString(R.string.text_internet_access))
            }
        }
        return swipeRefreshLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.listView.setBackgroundColor(typedValueBg!!.data)
    }

    override fun onResume() {
        super.onResume()
        updateOrders()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        when (v.getTag(R.id.tag_order_type).toString()) {

            "activeOrders" -> {
                val activity = (activity as WorkShiftActivity)
                when {

                    activity.stageOrder == 0 -> {
                        progressDialog = ProgressDialog.show(activity, null, getString(R.string.dialog_create_order), false)
                        createOrder(v.getTag(R.id.tag_order_id).toString())
                    }

                    activity.stageOrder != 0 -> activity.onNavigationDrawerItemSelected(1)

                    activity.provider == LocationManager.NETWORK_PROVIDER -> CustomToast.showMessage(activity, getString(R.string.text_gpscoords_fail))
                }
            }

            "assignedOrders" -> {
                val intent = getOrderIntent(v)
                intent.putExtra("free_order", 2)
                intent.putExtra("type_order", v.getTag(R.id.tag_order_type).toString())
                intent.putExtra("code_cancel", AppParams.ORDER_STATUS_ASSIGNED_CANCEL)
                activity!!.startActivityForResult(intent, WorkShiftActivity.INTENT_ORDER_OFFER)
            }

            "assignedPreOrders" -> {
                val intent = getOrderIntent(v)
                intent.putExtra("free_order", 3)
                intent.putExtra("type_order", "reserve_br")
                intent.putExtra("code_cancel", AppParams.ORDER_STATUS_RESERVE_CANCEL)
                activity!!.startActivityForResult(intent, WorkShiftActivity.INTENT_ORDER_OFFER)
            }
        }
    }

    private fun getOrderIntent(v: View): Intent {

        val orderIntent = Intent(activity!!.applicationContext, OrderOfferActivity::class.java)
        orderIntent.putExtra("order_id", v.getTag(R.id.tag_order_id).toString())
        orderIntent.putExtra("service_id", service.id)
        orderIntent.putExtra("workday_id", workDay.id)

        orderIntent.putExtra("delete", java.lang.Boolean.valueOf(v.getTag(R.id.tag_order_delete).toString()))
        orderIntent.putExtra("music", false)

        orderIntent.putExtra("source", "activity")
        orderIntent.putExtra("time", Date().time)
        orderIntent.putExtra("server_time", (activity as WorkShiftActivity).serverTime)
        orderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        if ((activity as WorkShiftActivity).stageOrder == 0) {
            orderIntent.putExtra("type_source", 0)
        } else {
            orderIntent.putExtra("type_source", 3)
        }
        return orderIntent
    }

    private fun createOrder(orderId: String) {
        this.orderId = orderId
        uuid = UUID.randomUUID().toString()
        countDownTimer!!.start()

        val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
        requestParams["order_id"] = orderId
        requestParams["tenant_login"] = service.uri
        requestParams["worker_login"] = driver.callSign
        WebService.getOrderForId(requestParams, driver.secretCode)
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        when (orderForIdEvent.status) {
            "OK" -> try {
                orderRaw = Gson().fromJson(orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").toString(), OrderRaw::class.java)

                if (orderId == orderRaw.order_id) {
                    val requestParams = LinkedHashMap<String, String>()
                    requestParams["order_id"] = orderId
                    requestParams["status_new_id"] = AppParams.ORDER_STATUS_CONFIRM
                    requestParams["tenant_login"] = service.uri
                    requestParams["time_to_client"] = "0"
                    requestParams["worker_login"] = driver.callSign
                    WebService.setOrderStatus(requestParams, driver.secretCode, uuid, AppParams.RequestType.NORMAL)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

            "ORDER_IS_BUSY" -> activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.error_order_is_busy)) }
            "EMPTY_DATA_IN_DATABASE" -> CustomToast.showMessage(context, getString(R.string.error_empty))
            "DRIVER_BLOCKED" -> CustomToast.showMessage(context, getString(R.string.error_driver_blocked))
            "DRIVER_PRE_ORDER_BLOCKED" -> CustomToast.showMessage(context, getString(R.string.error_driver_blocked))
        }
    }

    @Subscribe
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {
        if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 2) {
            EventBus.getDefault().post(PushResponseEvent(uuid, orderStatusEvent.status, 1, JSONObject()))
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {

        activity!!.runOnUiThread {
            if (progressDialog != null && progressDialog!!.isShowing)
                progressDialog!!.dismiss()
            countDownTimer!!.cancel()
        }

        if (pushResponseEvent.uuid == uuid) {
            when (pushResponseEvent.infoCode) {

                "OK" -> buildOrder(pushResponseEvent.result, orderId, orderRaw, activity as WorkShiftActivity)

                "ORDER_IS_BUSY" -> activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.error_order_is_busy)) }

                "EMPTY_DATA_IN_DATABASE" -> activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.error_empty)) }

                "DRIVER_BLOCKED" -> activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.error_driver_blocked)) }

                "SHIFT_IS_CLOSED" -> activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.status_shift_closed)) }

                "DRIVER_PRE_ORDER_BLOCKED" -> {
                    activity!!.runOnUiThread { CustomToast.showMessage(context, getString(R.string.error_driver_blocked)) }
                    countDownTimer!!.cancel()
                }

                else -> countDownTimer!!.cancel()
            }
        }
    }

    @Subscribe
    fun onEvent(ownOrdersEvent: NetworkOwnOrdersEvent) {
        when (ownOrdersEvent.status) {
            "OK" -> {
                try {
                    orders!!.clear()

                    if (ownOrdersEvent.activeOrders.size > 0) {
                        orders!!.add(OwnOrder("title", getString(R.string.titlelist_current_order)))
                        for (ownOrder in ownOrdersEvent.activeOrders) {
                            orders!!.add(ownOrder)
                        }
                    }

                    if (ownOrdersEvent.assignedOrders.size > 0) {
                        orders!!.add(OwnOrder("title", getString(R.string.titlelist_unknow_order)))
                        for (ownOrder in ownOrdersEvent.assignedOrders) {
                            orders!!.add(ownOrder)
                        }
                    }

                    if (ownOrdersEvent.assignedPreOrders.size > 0) {
                        orders!!.add(OwnOrder("title", getString(R.string.titlelist_pre_order)))
                        Log.d("OWN_ORDERS", "count@3_2 " + ownOrdersEvent.assignedPreOrders.size)
                        for (ownOrder in ownOrdersEvent.assignedPreOrders) {
                            orders!!.add(ownOrder)
                        }
                    }

                    adapter = OwnOrderAdapter(activity!!, orders!!, typedValueMain!!.data, typedValueSubscribe!!.data, typedValueBg!!.data)
                    listAdapter = adapter
                    if (swipeRefreshLayout.isRefreshing)
                        swipeRefreshLayout.isRefreshing = false
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                update = false
            }
        }

        if (swipeRefreshLayout.isRefreshing)
            swipeRefreshLayout.isRefreshing = false
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(updateOwnOrdersEvent: UpdateOwnOrdersEvent) {
        updateOrders()
    }

    private fun updateOrders() {
        update = true
        val requestParams = LinkedHashMap<String, String>()
        requestParams["tenant_login"] = service.uri
        requestParams["worker_login"] = driver.callSign

        WebService.getOwnOrders(activity!!, requestParams, driver.secretCode)
    }

    private inner class ListFragmentSwipeRefreshLayout(context: Context) : SwipeRefreshLayout(context) {

        override fun canChildScrollUp(): Boolean {
            val listView = listView
            return if (listView.visibility == View.VISIBLE) {
                canListViewScrollUp(listView)
            } else {
                false
            }
        }
    }
}