package com.kabbi.driver.preorder

import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.events.orderStatus.OrderStatusPresenter

class PreorderPresenter(orderId: String, driver: Driver) : OrderStatusPresenter() {

    init {
        this.orderId = orderId
        this.driver = driver
    }
}