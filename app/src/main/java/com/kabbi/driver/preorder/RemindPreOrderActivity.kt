package com.kabbi.driver.preorder

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.telephony.TelephonyManager
import android.text.Html
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.snackbar.Snackbar
import com.kabbi.driver.App
import com.kabbi.driver.BaseActivity
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.*
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppParams.ORDER_STATUS_RESERVECONFIRM_FALSE
import com.kabbi.driver.helper.AppParams.ORDER_STATUS_RESERVECONFIRM_TRUE
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.DistanceGPS
import com.kabbi.driver.helper.TypefaceSpanEx
import com.kabbi.driver.network.WebService
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.buildAddressFromJson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class RemindPreOrderActivity : BaseActivity() {

    private lateinit var driver: Driver
    private lateinit var service: Service
    private lateinit var requestType: AppParams.RequestType
    private lateinit var presenter: PreorderPresenter
    private var appPreferences: AppPreferences? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var jsonObjectOrder: JSONObject? = null
    private var jsonArrayOptions: JSONArray? = null
    private var onCreateOrderId: String = ""
    private var mediaPlayer: MediaPlayer? = null
    private var timer: CountDownTimer? = null
    private var countDownTimer: CountDownTimer? = null
    private val arrAddresses = arrayOf("B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")
    private var bOrder = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        EventBus.getDefault().register(this)

        onCreateOrderId = intent.extras!!.getString("order_id")!!

        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_remindpreorder)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()
        service = (application as App).service
        driver = (application as App).driver

        presenter = PreorderPresenter(onCreateOrderId, driver)

        val requestParams = kotlin.collections.LinkedHashMap<String?, String?>()
        requestParams["order_id"] = onCreateOrderId
        requestParams["tenant_login"] = service.uri
        requestParams["worker_login"] = driver.callSign

        WebService.getOrderForId(requestParams, driver.secretCode)

        mediaPlayer = MediaPlayer.create(this, R.raw.cute_bells)
        mediaPlayer!!.isLooping = true
        mediaPlayer!!.start()
        backTimer(intent.extras!!.getInt("sec_confirm"))

        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                val params = LinkedHashMap<String, String>()
                params["request_id"] = presenter.getUuid()
                params["tenant_login"] = service.uri
                params["worker_login"] = driver.callSign
                WebService.getResult(params, driver.secretCode)
            }
        }
    }

    override fun onDestroy() {
        mediaPlayer!!.stop()
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    fun confirmOrder(v: View) {
        if (!bOrder) {
            val requestParams = LinkedHashMap<String, String>()
            requestParams["order_id"] = onCreateOrderId
            requestParams["status_new_id"] = ORDER_STATUS_RESERVECONFIRM_TRUE
            requestParams["tenant_login"] = service.uri
            requestParams["worker_login"] = driver.callSign

            val uuid = presenter.getNewUuid()
            countDownTimer!!.start()
            bOrder = true
            requestType = AppParams.RequestType.CONFIRM_PREORDER

            if (AppParams.SET_ORDER_PARAMS_VIA_SOCKET) {
                presenter.setOrderStatus(ORDER_STATUS_RESERVECONFIRM_TRUE, requestType)
            } else {
                WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
            }
        }
    }

    fun rejectOrder(v: View) {
        val requestParams = LinkedHashMap<String, String>()
        requestParams["order_id"] = onCreateOrderId
        requestParams["status_new_id"] = ORDER_STATUS_RESERVECONFIRM_FALSE
        requestParams["tenant_login"] = service.uri
        requestParams["worker_login"] = driver.callSign

        val uuid = presenter.getNewUuid()
        countDownTimer!!.start()
        bOrder = true
        requestType = AppParams.RequestType.REJECT_PREORDER

        if (AppParams.SET_ORDER_PARAMS_VIA_SOCKET) {
            presenter.setOrderStatus(ORDER_STATUS_RESERVECONFIRM_FALSE, requestType)
        } else {
            WebService.setOrderStatus(requestParams, driver.secretCode, uuid, requestType)
        }
    }

    @Subscribe
    fun onEvent(requestResult: NetworkRequestResult) {
        if (requestResult.status == "OK") {
            EventBus.getDefault().post(PushResponseEvent(presenter.getUuid(), requestResult.status, requestResult.result, JSONObject()))
        } else {
            try {
                showSnackbar(requestResult.status)
                presenter.clearUuid()
                bOrder = false
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showSnackbar(status: String) {

        val message = when (status) {
            AppParams.EMPTY_RESPONSE -> getString(R.string.contact_support)
            AppParams.RESPONSE_ERROR -> getString(R.string.response_error)
            else -> getString(R.string.request_error)
        }

        val snackbar = Snackbar.make(findViewById(R.id.relativelayout_btn_rejected), message, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("OK") {
            snackbar.dismiss()
        }
        snackbar.show()
    }

    @Subscribe
    fun onEvent(pushOrderEvent: PushOrderEvent) {
        if (pushOrderEvent.orderId == onCreateOrderId) {
            when (pushOrderEvent.command) {
                "reject_worker_from_order" -> finish()
                "order_list_del_item" -> finish()
                "complete_order" -> finish()
                "order_is_rejected" -> finish()
            }
        }
    }

    @Subscribe
    fun onEvent(orderStatusEvent: NetworkOrderStatusEvent) {
        if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 1) {
            requestType = orderStatusEvent.type
        } else if (orderStatusEvent.status == "OK" && orderStatusEvent.result == 2) {
            EventBus.getDefault().post(PushResponseEvent(presenter.getUuid(), orderStatusEvent.status, 1, JSONObject()))
        } else if (orderStatusEvent.status == "EMPTY_DATA_IN_DATABASE") {
            CustomToast.showMessage(applicationContext, getString(R.string.text_order_rejected))
        } else {
            showSnackbar(orderStatusEvent.status)
            bOrder = false
        }
    }

    @Subscribe
    fun onEvent(pushResponseEvent: PushResponseEvent) {
        try {
            this.runOnUiThread {
                countDownTimer!!.cancel()
                timer!!.cancel()
            }
            if (pushResponseEvent.uuid == presenter.getUuid()) {
                when (pushResponseEvent.infoCode) {
                    "OK" -> {
                    }
                    "ORDER_IS_BUSY" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.error_order_is_busy)) }
                    "EMPTY_DATA_IN_DATABASE" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.error_empty)) }
                    "DRIVER_BLOCKED" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked)) }
                    "DRIVER_PRE_ORDER_BLOCKED" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked)) }
                    "SHIFT_IS_CLOSED" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.status_shift_closed)) }
                    "DENY_REFUSE_ORDER" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.status_deny_refuse)) }
                    "FORBIDDEN_ACTION" -> this.runOnUiThread { CustomToast.showMessage(applicationContext, getString(R.string.status_error)) }
                }
                finish()
            }
            bOrder = false
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(orderForIdEvent: NetworkOrderForIdEvent) {
        val layoutInflater = layoutInflater

        try {
            if (orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data").getString("order_id") != onCreateOrderId)
                return
            jsonObjectOrder = null

            when (orderForIdEvent.status) {
                "OK" -> {

                    jsonObjectOrder = orderForIdEvent.json.getJSONObject("result").getJSONObject("order_data")
                    if (jsonObjectOrder!!.getString("deny_refuse_order") == "1") {
                        findViewById<View>(R.id.relativelayout_btn_rejected).visibility = View.VISIBLE
                        findViewById<View>(R.id.relativelayout_btn_cancel).visibility = View.GONE
                    }

                    val pointA = buildAddressFromJson(jsonObjectOrder!!.getJSONObject("address").getJSONObject("A"))

                    showMap(appPreferences?.getText("lat")?.toDouble(), appPreferences?.getText("lon")?.toDouble(), pointA.lat, pointA.lon)

                    findViewById<TextView>(R.id.textview_order_offer_point_a).text = pointA.stringValue()

                    try {
                        findViewById<TextView>(R.id.textview_order_offer_dis).text = Html.fromHtml("<big><big><big>"
                                + DistanceGPS.getDistanceRound(appPreferences!!.getText("lat").toDouble(), appPreferences!!.getText("lon").toDouble(),
                                pointA.lat, pointA.lon).toString() + " " + getString(R.string.text_dis) + "</big></big></big>")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (jsonObjectOrder!!.getString("comment").isNotEmpty() && jsonObjectOrder!!.getString("comment") != "null") {
                        val comment = findViewById<TextView>(R.id.textview_orderoffer_comment)
                        comment.visibility = View.VISIBLE
                        comment.text = jsonObjectOrder!!.getString("comment")
                    }

                    var count = 0

                    findViewById<LinearLayout>(R.id.linearlayout_orderoffer_second_source).removeAllViews()

                    for (itemAddress in arrAddresses) {
                        try {
                            try {
                                if (count == 0 && appPreferences!!.getText("show_estimation") == "1") {
                                    try {
                                        val currency = findViewById<TextView>(R.id.textview_orderoffer_random_currency)
                                        currency.visibility = View.VISIBLE
                                        currency.text = Html.fromHtml(getString(R.string.text_orderoffer_random_price) + "<br />"
                                                + TypefaceSpanEx.getCurrencyStr(applicationContext,
                                                this.findViewById<ViewGroup>(android.R.id.content).getChildAt(0), jsonObjectOrder!!.getJSONObject("costData").getString("summary_cost")) + " "
                                                + jsonObjectOrder!!.getJSONObject("costData").getString("summary_distance") + " " + getString(R.string.text_dis) + " "
                                                + jsonObjectOrder!!.getJSONObject("costData").getString("summary_time") + " " + getString(R.string.text_orderoffer_min))
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }

                                if (appPreferences!!.getText("show_worker_address") == "0")
                                    break

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            val mPoint = buildAddressFromJson(jsonObjectOrder!!.getJSONObject("address").getJSONObject(itemAddress))
                            val layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_source, null, false)

                            layoutView.findViewById<TextView>(R.id.textview_orderoffer_source).text = mPoint.stringValue()
                            findViewById<LinearLayout>(R.id.linearlayout_orderoffer_second_source).addView(layoutView)
                            findViewById<View>(R.id.linearlayout_orderoffer_to).visibility = View.VISIBLE

                            count++
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                    try {
                        val dopOptions = ArrayList<String>()
                        dopOptions.add(jsonObjectOrder!!.getJSONObject("tariff").getString("name"))
                        if (jsonObjectOrder!!.getString("payment") == "CASH") {
                            dopOptions.add(getString(R.string.text_reportorder_cash))
                        } else {
                            dopOptions.add(getString(R.string.text_reportorder_nocash))
                        }
                        if (jsonObjectOrder!!.getJSONArray("options").length() > 0) {
                            jsonArrayOptions = jsonObjectOrder!!.getJSONArray("options")
                            for (j in 0 until jsonArrayOptions!!.length()) {
                                dopOptions.add(jsonArrayOptions!!.getJSONObject(j).getString("name"))
                            }
                        }
                        if (intent.extras!!.getString("type_order") != "free" && intent.extras!!.getString("type_order") != "assignedOrders") {
                            val linearLayoutPreTime = findViewById<LinearLayout>(R.id.linearlayout_orderoffer_pretime)
                            linearLayoutPreTime.visibility = View.VISIBLE
                            try {
                                val orderTime = jsonObjectOrder!!.getString("order_time").split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                                (linearLayoutPreTime.getChildAt(0) as TextView).text = getText(R.string.text_tariff_pretime).toString() + " " + orderTime[1] + " " + orderTime[0]
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        val linearLayoutOptions = findViewById<LinearLayout>(R.id.linearlayout_orderoffer_options)
                        linearLayoutOptions.removeAllViews()
                        val views = ArrayList<View>()
                        var countOption = 1
                        var layoutView: View? = null
                        for (i in dopOptions.indices) {
                            when (countOption) {
                                1 -> {
                                    layoutView = layoutInflater.inflate(R.layout.detail_orderoffer_option, null, false)
                                    (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option1) as TextView).text = dopOptions[i]

                                    if (i == dopOptions.size - 1) {
                                        views.add(layoutView)
                                    }
                                }
                                2 -> {
                                    (layoutView!!.findViewById<View>(R.id.textview_orderoffer_option2) as TextView).text = dopOptions[i]
                                    views.add(layoutView)
                                    countOption = 0
                                }
                            }
                            countOption++
                        }
                        for (item in views) {
                            linearLayoutOptions.addView(item)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    try {
                        if (!jsonObjectOrder!!.isNull("client")) {
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).text = if (jsonObjectOrder!!.getJSONObject("client").isNull("last_name"))
                                ""
                            else
                                jsonObjectOrder!!.getJSONObject("client").getString("last_name") +
                                        (if (jsonObjectOrder!!.getJSONObject("client").isNull("second_name")) "" else " " + jsonObjectOrder!!.getJSONObject("client").getString("second_name")) +
                                        if (jsonObjectOrder!!.getJSONObject("client").isNull("name")) "" else " " + jsonObjectOrder!!.getJSONObject("client").getString("name")
                        } else {
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).setTextColor(resources.getColor(R.color.red))
                            (findViewById<View>(R.id.textview_taximeterorder_phone) as TextView).text = getString(R.string.text_taximeter_order_name_unknow)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val imageButton = findViewById<ImageButton>(R.id.imagebutton_taximeterorder_phone)
                    try {
                        if (!jsonObjectOrder!!.isNull("phone") && jsonObjectOrder!!.getString("phone").isNotEmpty()) {
                            imageButton.setOnClickListener {
                                val telecomManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                                val simState = telecomManager.simState
                                if (simState == TelephonyManager.SIM_STATE_READY) {
                                    val callIntent = Intent(Intent.ACTION_DIAL)
                                    try {
                                        callIntent.data = Uri.parse("tel:+" + jsonObjectOrder!!.getString("phone"))
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }

                                    startActivity(callIntent)
                                } else {
                                    CustomToast.showMessage(applicationContext, getString(R.string.toast_absent_sim))
                                }
                            }
                        } else {
                            imageButton.visibility = View.INVISIBLE
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    enableButton(true)
                }
                "ORDER_IS_BUSY" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_order_is_busy))
                    try {
                        deleteOrder()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                    finish()
                }
                "EMPTY_DATA_IN_DATABASE" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_empty))
                    try {
                        deleteOrder()
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }

                    finish()
                }
                "DRIVER_BLOCKED" -> {
                    CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked))
                    finish()
                }
                "DRIVER_PRE_ORDER_BLOCKED" -> CustomToast.showMessage(applicationContext, getString(R.string.error_driver_blocked))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private fun deleteOrder() {
        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@RemindPreOrderActivity)
                val order = db.orderDao().getByOrderId(onCreateOrderId)
                db.orderDao().delete(order)
            }
        }
    }

    private fun backTimer(confirm_sec: Int) {
        timer = object : CountDownTimer((confirm_sec * 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {

                (findViewById<View>(R.id.textview_orderoffer_timer) as TextView).text = Html.fromHtml("<big><big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data)
                        + "'>" + (millisUntilFinished / 1000).toString() + "</font></big></big>")
            }

            override fun onFinish() {
                try {
                    (findViewById<View>(R.id.textview_orderoffer_timer) as TextView).text = Html.fromHtml("<big><big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data)
                            + "'>" + 0 + "</font></big></big>")

                    onBackPressed()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }.start()
    }

    override fun onBackPressed() {
        finish()
    }

    private fun enableButton(enable: Boolean) {
        findViewById<View>(R.id.button_orderoffer_rejected).isEnabled = enable
        findViewById<View>(R.id.button_orderoffer_cancel).isEnabled = enable
    }

    private fun showMap(driverLat: Double?, driverLon: Double?, addressLat: Double?, addressLon: Double?) {

        val mapURL = String.format("https://maps.googleapis.com/maps/api/staticmap?&size=400x400&markers=%s,%s&markers=%s,%s&key=%s",
                driverLat,
                driverLon,
                addressLat,
                addressLon,
                "AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4")
        findViewById<SimpleDraweeView>(R.id.sdv_static_map).setImageURI(mapURL)
    }
}
