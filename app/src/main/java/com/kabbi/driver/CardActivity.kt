package com.kabbi.driver

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.browser.customtabs.CustomTabsIntent
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.NetworkBankCards
import com.kabbi.driver.events.NetworkCheckBankCard
import com.kabbi.driver.events.NetworkCreateBankCard
import com.kabbi.driver.events.NetworkRefillAccount
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.network.WebService
import com.kabbi.driver.network.responses.BalanceResponse
import com.kabbi.driver.util.CustomToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.*

class CardActivity : BaseActivity() {

    private var appPreferences: AppPreferences? = null
    private lateinit var service: Service
    private lateinit var driver: Driver
    private lateinit var btnYourCard: Button
    private lateinit var btnAddCard: Button
    private lateinit var btnAddCash: Button
    private lateinit var edtCash: EditText
    private lateinit var tvCurrency: TextView
    private lateinit var tvCurrencyBalance: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var progressDialog: ProgressDialog
    private lateinit var checkProgressDialog: ProgressDialog
    private var orderId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_card)

        progressDialog = ProgressDialog(this)
        checkProgressDialog = ProgressDialog(this)
        checkProgressDialog.setMessage(getString(R.string.activities_CardActivity_check_text_load))

        runBlocking {
            withContext(Dispatchers.IO) {
                val db = AppDatabase.getInstance(this@CardActivity)
                service = db.serviceDao().getById(java.lang.Long.valueOf(appPreferences!!.getText("service_id")))
                driver = db.driverDao().getById(service.driver!!)
            }
        }

        initToolbar()
        initViews()

        val paramsBankCard = LinkedHashMap<String, String>()
        paramsBankCard["tenant_login"] = service.uri
        paramsBankCard["worker_login"] = driver.callSign
        WebService.getBankCards(paramsBankCard, driver.secretCode)

        getBalance()
    }

    override fun onResume() {
        super.onResume()
        if (orderId != null) {
            checkProgressDialog.show()
            val paramsBankCard = LinkedHashMap<String, String>()
            paramsBankCard["order_id"] = orderId!!
            paramsBankCard["tenant_login"] = service.uri
            paramsBankCard["worker_login"] = driver.callSign
            val uuidObject = UUID.randomUUID()
            WebService.checkBankCards(paramsBankCard, driver.secretCode, uuidObject.toString())
        }
    }

    private fun getBalance() {

        val params = LinkedHashMap<String?, String?>()
        params["tenant_login"] = service.uri
        params["worker_city_id"] = appPreferences!!.getText("city_id")
        params["worker_login"] = driver.callSign

        runBlocking {
            withContext(Dispatchers.IO) {
                WebService.getBalance(params, driver.secretCode)
            }?.also { balance ->
                onBalanceResponse(balance)
            }
        }
    }

    @Subscribe
    fun onEvent(checkBankCard: NetworkCheckBankCard) {
        if (checkBankCard.status == "OK") {
            getBankCards()
            orderId = null
        } else if (checkBankCard.status == "REQUEST_PROCESSING") {
            CustomToast.showMessage(applicationContext,
                    getString(R.string.activities_PaymentActivity_add_card_wait))

            val countDownTimer = object : CountDownTimer(5000, 1000) {
                override fun onTick(millisUntilFinished: Long) {

                }

                override fun onFinish() {
                    getBankCards()
                    orderId = null
                }
            }
            countDownTimer.start()

        } else {
            CustomToast.showMessage(applicationContext, getString(R.string.activities_PaymentActivity_add_card_error))
        }

        CustomToast.showMessage(applicationContext, checkBankCard.status)
        checkProgressDialog.dismiss()
    }

    private fun onBalanceResponse(balanceResponse: BalanceResponse) {
        try {
            tvCurrency.text = balanceResponse.currency
            tvCurrencyBalance.text = balanceResponse.balance + " " + balanceResponse.currency
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Subscribe
    fun onEvent(bankCards: NetworkBankCards) {
        if (bankCards.status == "OK" && bankCards.cards != null && bankCards.cards.size > 0) {
            var card = bankCards.cards[0]
            for (item in bankCards.cards) {
                if (appPreferences!!.getText("card") == item) {
                    card = item
                    break
                }
            }
            appPreferences!!.saveText("card", card)
            btnYourCard.text = card
            btnYourCard.visibility = View.VISIBLE
            (btnAddCash.parent as View).visibility = View.VISIBLE
            btnAddCard.visibility = View.GONE
        } else {
            appPreferences!!.saveText("card", "")
            btnYourCard.visibility = View.GONE
            (btnAddCash.parent as View).visibility = View.GONE
            btnAddCard.visibility = View.VISIBLE
        }
    }

    @Subscribe
    fun onEvent(refillAccount: NetworkRefillAccount) {
        if (refillAccount.status == "OK") {
            try {
                tvCurrencyBalance.text = refillAccount.balance + " " + refillAccount.currency
                edtCash.setText("")
                CustomToast.showMessage(this, getString(R.string.paid_success))
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            try {
                CustomToast.showMessage(this, refillAccount.status)
            } catch (e: Exception) {

            }

        }
        btnAddCash.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    fun onClickAddCard(v: View) {
        if (Build.VERSION.SDK_INT > 19) {
            val webViewIntent = Intent(applicationContext, WebViewActivity::class.java)
            webViewIntent.putExtra("service_id", service.id)
            startActivityForResult(webViewIntent, PAYMENT_ACT_REQ_CODE)
        } else {
            progressDialog.setMessage(getString(R.string.activities_CardActivity_text_load))
            progressDialog.show()

            val paramsBankCard = LinkedHashMap<String, String>()
            paramsBankCard["tenant_login"] = service.uri
            paramsBankCard["worker_login"] = driver.callSign
            val uuidObject = UUID.randomUUID()
            WebService.createBankCard(paramsBankCard, driver.secretCode, uuidObject.toString())
        }
    }

    @Subscribe
    fun onEvent(createBankCard: NetworkCreateBankCard) {
        try {
            if (progressDialog.isShowing)
                progressDialog.dismiss()
            if (Build.VERSION.SDK_INT <= 19) {
                if (createBankCard.status == "OK") {
                    orderId = createBankCard.orderId
                    val webPageString = createBankCard.url
                    if (webPageString.isNotEmpty()) {
                        val builder = CustomTabsIntent.Builder()
                        builder.setToolbarColor(Color.WHITE)
                        builder.setShowTitle(true)
                        val customTabsIntent = builder.build()
                        try {
                            customTabsIntent.launchUrl(this, Uri.parse("googlechrome://navigate?url=$webPageString"))
                        } catch (e: Exception) {
                            customTabsIntent.launchUrl(this, Uri.parse(webPageString))
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun onClickYourCard(v: View) {
        val webViewIntent = Intent(applicationContext, CardListActivity::class.java)
        webViewIntent.putExtra("service_id", service.id)
        startActivityForResult(webViewIntent, PAYMENT_LIST_REQ_CODE)
    }

    fun onClickAddCash(v: View) {
        appPreferences = AppPreferences(this)
        if (InternetConnection.isOnline(applicationContext)) {
            if (edtCash.text.toString().trim().isNotEmpty()) {

                btnAddCash.visibility = View.GONE
                progressBar.visibility = View.VISIBLE

                val paramsBankCard = LinkedHashMap<String, String>()
                paramsBankCard["currency"] = service.currency!!
                paramsBankCard["pan"] = appPreferences!!.getText("card")
                paramsBankCard["sum"] = edtCash.text.toString()
                paramsBankCard["tenant_login"] = service.uri
                paramsBankCard["worker_login"] = driver.callSign
                val uuidObject = UUID.randomUUID()
                WebService.refillAccount(paramsBankCard, driver.secretCode, uuidObject.toString())
            } else {
                CustomToast.showMessage(applicationContext, getString(R.string.edittext_valid_required))
            }
        } else {
            CustomToast.showMessage(applicationContext, getString(R.string.text_internet_access))
        }
    }

    private fun getBankCards() {
        val paramsBankCard = LinkedHashMap<String, String>()
        paramsBankCard["tenant_login"] = service.uri
        paramsBankCard["worker_login"] = driver.callSign
        WebService.getBankCards(paramsBankCard, driver.secretCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PAYMENT_ACT_REQ_CODE && resultCode == Activity.RESULT_OK) {
            val returnKey = data!!.getStringExtra(WebViewActivity.RESULT_SUCCESS_KEY)
            if (returnKey != null && returnKey == WebViewActivity.RESULT_SUCCESS_CARD) {
                getBankCards()
                CustomToast.showMessage(applicationContext,
                        getString(R.string.activities_PaymentActivity_add_card_success))
            }
        } else if (requestCode == PAYMENT_LIST_REQ_CODE && resultCode == Activity.RESULT_OK) {
            appPreferences!!.saveText("card", data!!.getStringExtra("pan"))
            getBankCards()
        } else if (requestCode == PAYMENT_ACT_REQ_CODE) {
            CustomToast.showMessage(applicationContext,
                    getString(R.string.activities_PaymentActivity_add_card_error))
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun initViews() {
        btnAddCard = findViewById(R.id.btn_add_card)
        btnYourCard = findViewById(R.id.btn_your_card)
        btnAddCash = findViewById(R.id.btn_addition_cash)
        edtCash = findViewById(R.id.edittext_cash)
        tvCurrency = findViewById(R.id.tv_currency)
        tvCurrencyBalance = findViewById(R.id.tv_currency_balance)
        progressBar = findViewById(R.id.pbHeaderProgress)

        btnYourCard.visibility = View.GONE
        (btnAddCash.parent as View).visibility = View.GONE
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_your_balance)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val PAYMENT_ACT_REQ_CODE = 6642
        const val PAYMENT_LIST_REQ_CODE = 6643
    }
}