package com.kabbi.driver.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.WorkDay
import com.kabbi.driver.helper.TypefaceSpanEx
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class WorkDayAdapter(internal var ctx: Context, internal var element: List<WorkDay>, private val colorMainText: Int, private val colorSubText: Int) : BaseAdapter() {

    internal var lInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return element.size
    }

    override fun getItem(position: Int): Any {
        return element[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = lInflater.inflate(R.layout.detail_list_workday, parent, false)

        val startDate = Date(element[position].startWorkDay)
        val endDate = Date(element[position].endWorkDay)

        val month = SimpleDateFormat("dd MMMM"/*, new Locale("ru", "RU")*/)  // С локалью не подгружаются переводы месяцев :(
        val time = SimpleDateFormat("HH:mm")
        view.tag = element[position].id
        (view.findViewById<View>(R.id.textview_detail_list_workday_date) as TextView).text = month.format(startDate)
        (view.findViewById<View>(R.id.textview_detail_list_workday_betweentime) as TextView).text = time.format(startDate) + "-" + time.format(endDate)

        var orders = listOf<Order>()

        runBlocking {
            withContext(Dispatchers.IO){
                orders =  AppDatabase.getInstance(this@WorkDayAdapter.ctx).orderDao().getByWorkDay(element[position].id)
            }
        }
        var sumPrice = 0.0

        for (order in orders) {
            if(order.finalOrderCost.isNotBlank()) {
                sumPrice += order.finalOrderCost.toDouble()
            }
        }
        view.findViewById<TextView>(R.id.textview_detail_list_workday_size).text = orders.size.toString()
        view.findViewById<TextView>(R.id.textview_detail_list_workday_cost) .text = TypefaceSpanEx.getCurrencyStr(ctx, parent, sumPrice.toString())

        return view
    }
}
