package com.kabbi.driver.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.models.Photo;

import java.util.List;

public class PhotoReviewAdapter extends RecyclerView.Adapter<PhotoReviewAdapter.ViewHolder> {

    private List<Photo> photoList;

    public PhotoReviewAdapter(Context context, List<Photo> photoList) {
        this.photoList = photoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_review, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = holder.getAdapterPosition();
        Photo photo = photoList.get(pos);
        //holder.imageView.set(photo.getReviewSrcLink());
        holder.textView.setText(photo.getReviewComment());
    }

    @Override
    public int getItemCount() {
        return photoList == null ? 0 : photoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_review_item);
            textView = itemView.findViewById(R.id.tv_review_item);
        }
    }
}
