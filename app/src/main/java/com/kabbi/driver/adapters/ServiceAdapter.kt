package com.kabbi.driver.adapters


import android.content.Context
import android.net.Uri
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.BaseSwipeAdapter
import com.facebook.drawee.view.SimpleDraweeView
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.DeleteServiceEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

class ServiceAdapter(private val ctx: Context, private val element: List<Map<String, Long>>, private val colorMainText: Int, private val colorSubText: Int) : BaseSwipeAdapter() {
    private var swipeLayout: SwipeLayout? = null
    private var simpleDraweeView: SimpleDraweeView? = null

    override fun getCount(): Int {
        return element.size
    }

    override fun getItem(position: Int): Any {
        return element[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

    override fun generateView(position: Int, parent: ViewGroup): View {
        val view = LayoutInflater.from(ctx).inflate(R.layout.detail_list_service, null)
        swipeLayout = view.findViewById(getSwipeLayoutResourceId(position))
        simpleDraweeView = view.findViewById(R.id.imageview_detail_list_service)
        return view
    }

    override fun fillValues(position: Int, view: View) {

        CoroutineScope(Dispatchers.Main).launch {
            val db = AppDatabase.getInstance(ctx)
            var service: Service? = null
            var driver: Driver? = null
            withContext(Dispatchers.IO) {
                service = db.serviceDao().getById(element[position]["service"] ?: error("Empty service id"))
                driver = db.driverDao().getById(element[position]["driver"] ?: error("Empty driver id"))
            }

            val uri = Uri.parse(service!!.logo)
            simpleDraweeView!!.setImageURI(uri)

            view.findViewById<View>(R.id.delete).setOnClickListener { EventBus.getDefault().post(DeleteServiceEvent(service!!.id, position)) }

            (view.findViewById<View>(R.id.textview_detail_list_service) as TextView).text = Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and colorMainText) + "'>"
                    + service?.serviceName + "</font><br>"
                    + "<small><font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + driver?.callSign + ", " + driver?.name + "</font></small><br>"
                    + "<small><font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + service?.comment + "</font></small>")

            view.findViewById<View>(R.id.textview_detail_list_service).setTag(R.id.tag_service_id, service?.id)
        }
    }
}
