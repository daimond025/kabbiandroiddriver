package com.kabbi.driver.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.models.Options;

import java.util.List;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.MyViewHolder> {

    private List<Options> optionsList;

    public OptionsAdapter(Context context, List<Options> modelList) {
        optionsList = modelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_options_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Options option = optionsList.get(position);
        holder.textView.setText(option.getOptionName());
        holder.checkBox.setChecked(option.isChecked());
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (option.isChecked()) {
                    option.setChecked(false);
                } else {
                    option.setChecked(true);
                }
                holder.checkBox.setChecked(option.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return optionsList == null ? 0 : optionsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private TextView textView;
        private CheckBox checkBox;

        private MyViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.ll_item);
            textView = itemView.findViewById(R.id.rv_item);
            checkBox = itemView.findViewById(R.id.cb_item);
        }
    }
}