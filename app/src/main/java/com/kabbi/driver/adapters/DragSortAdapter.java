//package com.kabbi.driver.adapters;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//
//import com.kabbi.driver.R;
//import com.kabbi.driver.models.cont.DragSortItem;
//
//import java.util.List;
//import java.util.Map;
//
//public class DragSortAdapter extends BaseAdapter {
//
//    Context ctx;
//    LayoutInflater lInflater;
//    List<DragSortItem> element;
//    Map<Long, Integer[]> elementInfo;
//    int colorTextMain, colorTextSub;
//
//    public DragSortAdapter(Context context, List<DragSortItem> elem, Map<Long, Integer[]> elemInfo, int colorTextMain, int colorTextSub) {
//        ctx = context;
//        element = elem;
//        elementInfo = elemInfo;
//        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.colorTextMain = colorTextMain;
//        this.colorTextSub = colorTextSub;
//    }
//
//    @Override
//    public int getCount() {
//        return element.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return element.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View view = lInflater.inflate(R.layout.list_item_handle_right, parent, false);
//
//        ((TextView) view.findViewById(R.id.text)).setText(element.get(position).getAddress());
//
//        return view;
//    }
//}
