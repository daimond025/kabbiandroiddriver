package com.kabbi.driver.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.DarkWingDuck
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.DistanceGPS
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class HelpAdapter(internal var ctx: Context, private val elements: List<DarkWingDuck>) : BaseAdapter() {

    internal var lInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val appPreferences: AppPreferences = AppPreferences(ctx)

    override fun getCount(): Int {
        return elements.size
    }

    override fun getItem(position: Int): Any {
        return elements[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = lInflater.inflate(R.layout.detail_list_help, parent, false)

        view.findViewById<TextView>(R.id.textview_detail_list_help).text = elements[position].carData

        var callSign = ""

        val db = AppDatabase.getInstance(ctx)
        runBlocking {
            withContext(Dispatchers.IO) {
                callSign = db.driverDao().getById(elements[position].driver!!).callSign
            }
        }

        if (callSign == elements[position].driverCallSign) {
            view.findViewById<TextView>(R.id.textview_detail_list_helpop).text = ctx.getString(R.string.text_help_order)
        } else {
            view.findViewById<TextView>(R.id.textview_detail_list_helpop).text = DistanceGPS.getDistanceRound((appPreferences.getText("lat")).toDouble(),
                    java.lang.Double.valueOf(appPreferences.getText("lat")),
                    elements[position].lat,
                    elements[position].lon).toString() + " " + ctx.getString(R.string.text_dis)
        }

        (view.findViewById<View>(R.id.textview_detail_list_helptime) as TextView).text = getDate(elements[position].timerHelp)

        view.setTag(R.id.tag_car_lat, elements[position].lat.toString())
        view.setTag(R.id.tag_car_lon, elements[position].lon.toString())
        view.setTag(R.id.tag_car_desc, elements[position].carData)

        return view
    }

    private fun getDate(timeStamp: Long): String {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")
        return dateFormat.format(Date(timeStamp))
    }
}
