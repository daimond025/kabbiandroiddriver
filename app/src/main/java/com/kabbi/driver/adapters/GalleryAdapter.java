package com.kabbi.driver.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.models.Photo;

import java.io.File;
import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private ArrayList<Photo> photoList;
    private Context context;
    private File file;
    private GalleryAdapter.reCaptureListener callback;

    public GalleryAdapter(Context context, GalleryAdapter.reCaptureListener callback, ArrayList<Photo> photoList) {
        this.context = context;
        this.callback = callback;
        this.photoList = photoList;
    }

    public interface reCaptureListener {
        void reCapture(Photo photo);
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, final int position) {
        try {
            final int pos = holder.getAdapterPosition();
            holder.title.setText(getPhotoDescription(photoList.get(holder.getAdapterPosition())));
            file = new File(photoList.get(pos).getPhotoPath());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inDither = false;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            options.inSampleSize = 3;
            options.inPurgeable = true;

            Bitmap icon = BitmapFactory.decodeFile(file.getAbsolutePath(), options);

            //holder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.img.setImageBitmap(icon);
            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.reCapture(photoList.get(pos));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            img = itemView.findViewById(R.id.img);
        }
    }

    private String getPhotoDescription(Photo photo) {
        String description = "";
        switch (photo.getPhotoDescr()) {
            case "front":
                description = context.getString(R.string.pc_car_front);
                break;
            case "back":
                description = context.getString(R.string.pc_car_back);
                break;
            case "right_side":
                description = context.getString(R.string.pc_car_right_side);
                break;
            case "left_side":
                description = context.getString(R.string.pc_car_left_side);
                break;
            case "front_interior":
                description = context.getString(R.string.pc_car_front_interior);
                break;
            case "back_interior":
                description = context.getString(R.string.pc_car_back_interior);
                break;
            case "truck":
                description = context.getString(R.string.pc_car_truck);
                break;
            case "documents":
                description = context.getString(R.string.pc_car_documents);
                break;
        }
        return description;
    }


    public static class ItemDecorationGalleryColumns extends RecyclerView.ItemDecoration {

        private int mSizeGridSpacingPx;
        private int mGridSize;

        private boolean mNeedLeftSpacing = false;

        public ItemDecorationGalleryColumns(int gridSpacingPx, int gridSize) {
            mSizeGridSpacingPx = gridSpacingPx;
            mGridSize = gridSize;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int frameWidth = (int) ((parent.getWidth() - (float) mSizeGridSpacingPx * (mGridSize - 1)) / mGridSize);
            int padding = parent.getWidth() / mGridSize - frameWidth;
            int itemPosition = ((RecyclerView.LayoutParams) view.getLayoutParams()).getViewAdapterPosition();

            if (itemPosition < mGridSize) {
                outRect.top = 0;
            } else {
                outRect.top = mSizeGridSpacingPx;
            }
            if (itemPosition % mGridSize == 0) {
                outRect.left = 0;
                outRect.right = padding;
                mNeedLeftSpacing = true;
            } else if ((itemPosition + 1) % mGridSize == 0) {
                mNeedLeftSpacing = false;
                outRect.right = 0;
                outRect.left = padding;
            } else if (mNeedLeftSpacing) {
                mNeedLeftSpacing = false;
                outRect.left = mSizeGridSpacingPx - padding;
                if ((itemPosition + 2) % mGridSize == 0) {
                    outRect.right = mSizeGridSpacingPx - padding;
                } else {
                    outRect.right = mSizeGridSpacingPx / 2;
                }
            } else if ((itemPosition + 2) % mGridSize == 0) {
                mNeedLeftSpacing = false;
                outRect.left = mSizeGridSpacingPx / 2;
                outRect.right = mSizeGridSpacingPx - padding;
            } else {
                mNeedLeftSpacing = false;
                outRect.left = mSizeGridSpacingPx / 2;
                outRect.right = mSizeGridSpacingPx / 2;
            }
            outRect.bottom = 0;
        }
    }
}