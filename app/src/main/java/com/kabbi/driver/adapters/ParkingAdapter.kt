package com.kabbi.driver.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Parking

class ParkingAdapter(var ctx: Context, var element: List<Parking>, var elementInfo: Map<Long, Array<Int>>, colorTextMain: Int) : BaseAdapter() {
   
    var lInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    var colorTextMain: Int = colorTextMain
   
    override fun getCount(): Int {
        return element.size
    }

    override fun getItem(position: Int): Any {
        return element[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = lInflater.inflate(R.layout.detail_list_parking, parent, false)
        view.findViewById<TextView>(R.id.textview_detail_list_parking).setTextColor(colorTextMain)
        view.findViewById<TextView>(R.id.textview_detail_list_parking_countauto).setTextColor(colorTextMain)
        view.findViewById<TextView>(R.id.textview_detail_list_parking).text = element[position].parkingName
        view.tag = element[position].parkingID

        try {
            view.findViewById<TextView>(R.id.textview_detail_list_parking_countauto).text = ctx.getString(R.string.text_parkinglist_auto) + ": " + elementInfo[element[position].parkingID]!![0].toString()
        } catch (e: Exception) {
            view.findViewById<TextView>(R.id.textview_detail_list_parking_countauto).text = ctx.getString(R.string.text_parkinglist_auto) + ": error"
        }
        try {
            view.findViewById<TextView>(R.id.textview_detail_list_parking_countorder).text = ctx.getString(R.string.text_parkinglist_order) + ": " + elementInfo[element[position].parkingID]!![1].toString()
        } catch (e: Exception) {
            view.findViewById<TextView>(R.id.textview_detail_list_parking_countorder).text = ctx.getString(R.string.text_parkinglist_order) + ": error"
        }
        return view
    }

}