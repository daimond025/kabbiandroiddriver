package com.kabbi.driver.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.kabbi.driver.R;
import com.kabbi.driver.events.DeleteCardEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class CardAdapter extends BaseSwipeAdapter {
    private Context ctx;
    private List<String> element;
    private SwipeLayout swipeLayout;
    private int colorMainText, colorSubText;


    public CardAdapter(Context context, List<String> elem, int colorMainText, int colorSubText) {
        ctx = context;
        element = elem;
        this.colorMainText = colorMainText;
        this.colorSubText = colorSubText;
    }

    @Override
    public int getCount() {
        return element.size();
    }

    @Override
    public Object getItem(int position) {
        return element.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.detail_list_card, null);
        swipeLayout = (SwipeLayout) view.findViewById(getSwipeLayoutResourceId(position));
        return view;
    }

    @Override
    public void fillValues(final int position, View view) {

        view.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new DeleteCardEvent(element.get(position)));
            }
        });

        ((TextView) view.findViewById(R.id.textview_detail_list_card)).setText(Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF & colorMainText) + "'>"
                + element.get(position) + "</font></big>"));
    }
}
