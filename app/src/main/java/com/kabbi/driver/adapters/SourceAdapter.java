package com.kabbi.driver.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.kabbi.driver.R;
import com.kabbi.driver.events.DeleteBordurItemEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

public class SourceAdapter extends BaseSwipeAdapter {

    private Context ctx;
    private List<HashMap<String, String>> element;


    public SourceAdapter(Context context, List<HashMap<String, String>> elem) {
        ctx = context;
        element = elem;
    }

    @Override
    public int getCount() {
        return element.size();
    }

    @Override
    public Object getItem(int position) {
        return element.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(ctx).inflate(R.layout.detail_list_sourceborder, null);
    }

    @Override
    public void fillValues(final int position, View view) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(element.get(position).get("street"));

        view.findViewById(R.id.delete).setOnClickListener(view1 -> EventBus.getDefault().post(new DeleteBordurItemEvent(position)));

        try {
            if (element.get(position).get("house").length() > 0)
                stringBuilder.append(", " + ctx.getString(R.string.text_house) + " " + element.get(position).get("house"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (element.get(position).get("housing").length() > 0)
                stringBuilder.append(", " + ctx.getString(R.string.text_housing) + " " + element.get(position).get("housing"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        try {
            if (element.get(position).get("porch").length() > 0)
                stringBuilder.append(", " + ctx.getString(R.string.text_porch) + " " + element.get(position).get("porch"));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        ((TextView) view.findViewById(R.id.textview_detail_list_sourceborder)).setText(stringBuilder.toString());
    }
}