package com.kabbi.driver.adapters;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.kabbi.driver.R;

import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<String[]> element;
    private int colorMainText, colorSubText;

    public MenuAdapter(Context context, ArrayList<String[]> elem, int colorMainText, int colorSubText) {
        ctx = context;
        element = elem;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.colorMainText = colorMainText;
        this.colorSubText = colorSubText;
    }

    @Override
    public int getCount() {
        return element.size();
    }

    @Override
    public Object getItem(int position) {
        return element.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        if (position == 0) {
            view = lInflater.inflate(R.layout.detail_list_menu_header, parent, false);
            view.setEnabled(false);
            view.setOnClickListener(null);

            if (element.get(position).length > 2) {
                ((SimpleDraweeView) view.findViewById(R.id.imageview_backmenu)).setImageURI(Uri.parse(element.get(position)[0]));

                ((TextView) view.findViewById(R.id.textview_backmenu)).setText(Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF & colorMainText) + "'><big>" + element.get(position)[1] + "</big></font><br />"
                        + "<font color='" + String.format("#%06X", 0xFFFFFF & colorSubText) + "'>" + element.get(position)[2] + "</font>"));
            }

        } else {
            view = lInflater.inflate(R.layout.detail_list_menu, parent, false);

            ImageView img = view.findViewById(R.id.menu_item_img);
            TextView text = view.findViewById(R.id.menu_item);
            TextView text_count = view.findViewById(R.id.menu_item_countmes);

            img.setImageResource(Integer.valueOf(element.get(position)[0]));

            if (Integer.valueOf(element.get(position)[3]) > 0) {
                text.setText(element.get(position)[1] + " " + element.get(position)[3]);

                text_count.setVisibility(View.VISIBLE);
                text_count.setText(Html.fromHtml("<small><small><small>" + element.get(position)[3] + "</small></small></small>"));
            }

            text.setText(element.get(position)[1]);


            text.setTextColor(Color.parseColor(element.get(position)[2]));
            view.setEnabled(true);
            int mCurrentSelectedPosition = 1;
            if (position != mCurrentSelectedPosition) {
                view.setBackgroundColor(ctx.getResources().getColor(android.R.color.transparent));
                text.setTypeface(Typeface.DEFAULT);
            }
        }
        return view;
    }
}
