package com.kabbi.driver.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kabbi.driver.R;
import com.kabbi.driver.models.WorkerReport;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class IncomeWeekTabAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface onLoadMoreListener {
        void onLoadMore(int page);
    }

    private List<WorkerReport> workerReports;
    private onLoadMoreListener listener;
    public int currentPage;
    public int countPage;
    private Context context;
    private static final int ITEM = 1;
    private static final int HAS_NEXT = 0;
    private static final int EMPTY_STATE = 2;

    public IncomeWeekTabAdapter(Context context, List<WorkerReport> workerReportsList, int currentPage, int countPage) {
        this.context = context;
        this.workerReports = workerReportsList;
        this.currentPage = currentPage;
        this.countPage = countPage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_income_order, parent, false);
            viewHolder = new ContentViewHolder(view);
        } else if (viewType == HAS_NEXT) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_income_nav, parent, false);
            viewHolder = new ProgressViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_clear_completed_orders, parent, false);
            viewHolder = new EmptyViewHolder(view);
        }
        return viewHolder;
    }

    // Prevent wrong RecyclerView positions and adapter crashes
    public static class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Activity activity, int horizontal, boolean b) {
            super(activity, horizontal, b);
        }

        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ContentViewHolder) {
            position = holder.getAdapterPosition();
            WorkerReport workerReport = workerReports.get(position);
            try {
                DateFormat format = new SimpleDateFormat("dd.MM.yyyy, HH:mm", new Locale("ru"));
                format.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
                Date dates = new Date(workerReport.getCreateTime() * 1000);

                ((ContentViewHolder) holder).tvDate.setText(format.format(dates));
                if (!workerReport.getAddress().getStreet().equals(context.getString(R.string.to_coord))&&!workerReport.getAddress().getStreet().equals("border_order")) {
                    ((ContentViewHolder) holder).tvAddress.setText(String.format("%s, %s", workerReport.getAddress().getCity(), workerReport.getAddress().getStreet()));
                } else {
                    ((ContentViewHolder) holder).tvAddress.setText(context.getString(R.string.to_coord));
                }

                ((ContentViewHolder) holder).tvOrder.setText(String.format("%s %s", context.getString(R.string.income_order_cost), String.valueOf(workerReport.getSummaryCost())/*, context.getString(R.string.main_price)*/));
                ((ContentViewHolder) holder).tvIncome.setText(String.format("%s %s", context.getString(R.string.text_reporttab_4), String.format(new Locale("ru"), "%.2f", workerReport.getEarnings())/*, context.getString(R.string.main_price)*/));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } else if (holder instanceof ProgressViewHolder) {
            if (currentPage == countPage && countPage > 1) {
                ((ProgressViewHolder) holder).btnPrev.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) holder).btnNext.setVisibility(View.GONE);
            } else if (currentPage > 1 && currentPage < countPage) {
                ((ProgressViewHolder) holder).btnPrev.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) holder).btnNext.setVisibility(View.VISIBLE);
            } else if (currentPage == 1 && countPage > 1) {
                ((ProgressViewHolder) holder).btnPrev.setVisibility(View.GONE);
                ((ProgressViewHolder) holder).btnNext.setVisibility(View.VISIBLE);
            }

            ((ProgressViewHolder) holder).btnPrev.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPage--;
                    listener.onLoadMore(currentPage);
                }
            });
            ((ProgressViewHolder) holder).btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPage++;
                    listener.onLoadMore(currentPage);
                }
            });
        }
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate, tvAddress, tvOrder, tvIncome;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvOrder = itemView.findViewById(R.id.tv_order);
            tvIncome = itemView.findViewById(R.id.tv_income);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout btnPrev;
        private LinearLayout btnNext;

        public ProgressViewHolder(View itemView) {
            super(itemView);
            btnPrev = itemView.findViewById(R.id.ll_back);
            btnNext = itemView.findViewById(R.id.ll_forward);
        }
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (workerReports.size() == 0) {
            return EMPTY_STATE;
        } else if (position == workerReports.size() - 1 && (workerReports.get(workerReports.size() - 1)) == null) {
            return HAS_NEXT;
        } else {
            return ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return workerReports.size() == 0 ? 1 : workerReports.size();
    }

    public onLoadMoreListener getListener() {
        return listener;
    }

    public void setListener(onLoadMoreListener listener) {
        this.listener = listener;
    }

    public static class DividerCustomItemDecoration extends RecyclerView.ItemDecoration {

        private Drawable divider;
        private int sidesMargin;

        public DividerCustomItemDecoration(Context context) {
            divider = ContextCompat.getDrawable(context, R.drawable.divider);
            sidesMargin = (int) context.getResources().getDimension(R.dimen.space_medium_m);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = sidesMargin;
            int right = parent.getWidth() - sidesMargin;

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }
}