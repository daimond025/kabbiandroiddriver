package com.kabbi.driver.adapters

import android.app.Activity
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.maps.model.LatLng
import com.kabbi.driver.R
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.models.OwnOrder
import com.kabbi.driver.util.buildAddress
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class OwnOrderAdapter(
        private val activity: Activity,
        private val ownOrders: List<OwnOrder>,
        private val colorMainText: Int,
        private val colorSubText: Int,
        private val colorBg: Int) : BaseAdapter() {

    private val lInflater: LayoutInflater
    private val context: Context = activity.applicationContext
    private val dateFormat: SimpleDateFormat
    private val stringBuilderTime: StringBuilder
    private val appPreferences: AppPreferences

    init {
        this.lInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US)
        stringBuilderTime = StringBuilder()
        appPreferences = AppPreferences(context)
    }

    override fun getCount(): Int {
        return ownOrders.size
    }

    override fun getItem(position: Int): Any {
        return ownOrders[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View

        if (ownOrders[position].type == "title") {
            view = lInflater.inflate(R.layout.detail_list_title, parent, false)

            val detailText = view.findViewById<TextView>(R.id.textview_detail_list_order)
            detailText.text = Html.fromHtml("<b><big>" + ownOrders[position].title + "</big></b>")

            view.setTag(R.id.tag_order_type, ownOrders[position].type)
            return view
        } else {
            view = lInflater.inflate(R.layout.detail_list_order, parent, false)
            stringBuilderTime.setLength(0)
            val detailText = view.findViewById<TextView>(R.id.textview_detail_list_order)

            val order = ownOrders[position]

            view.findViewById<TextView>(R.id.textview_detail_list_order_dis).text = ownOrders[position].dis

            try {
                detailText.text = buildAddress(LatLng(order.lat.toDouble(), order.lon.toDouble()), null)
            } catch (e: Exception) {
                detailText.text = order.street
            }

            val stringBuilder = StringBuilder()
            if (order.type == "assignedPreOrders") {
                val detailsText = view.findViewById<TextView>(R.id.textview_detail_list_order_cash)
                detailsText.text = Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + activity.resources.getString(R.string.text_order_reserve) + "</font>")
                detailsText.setTextColor(colorSubText)
                detailsText.visibility = View.VISIBLE
                stringBuilder.append(activity.getString(R.string.text_order_reserve) + " ")
            } else {
                view.findViewById<View>(R.id.textview_detail_list_order_cash).visibility = View.GONE
            }
            if (order.payment != "CASH") {
                stringBuilder.append(context.getString(R.string.text_nocash))
                view.findViewById<TextView>(R.id.textview_detail_list_order_cash).text = Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + stringBuilder.toString() + "</font>")
            }

            if (order.type == "assignedPreOrders") {
                view.findViewById<TextView>(R.id.textview_detail_list_order_time).text = order.orderTime
            } else if (order.type == "activeOrders") {
                view.findViewById<TextView>(R.id.textview_detail_list_order_time).text = context.getString(R.string.text_type_order)
            } else {
                if (order.type == "assignedOrders" && appPreferences.getText("show_urgent_order_time") == "0") {
                    view.findViewById<TextView>(R.id.textview_detail_list_order_time).text = context.getString(R.string.text_type_order)
                } else {
                    try {

                        var parseDateSec: Long = 0
                        val dateSec = Date().time

                        try {
                            val parseDate = dateFormat.parse(order.orderTime)
                            parseDateSec = parseDate.time
                        } catch (e: ParseException) {
                            e.printStackTrace()
                        }

                        val hours: String
                        val color: String
                        val greenDateSec: Long
                        if (parseDateSec > dateSec) {
                            greenDateSec = parseDateSec - dateSec
                            color = AppParams.colorGreen(context)
                        } else {
                            greenDateSec = dateSec - parseDateSec
                            color = AppParams.colorRed(context)
                        }
                        val lHours = greenDateSec / 3600000
                        if (lHours <= 24) {
                            hours = lHours.toString()
                        } else {
                            hours = (lHours % 24).toString()
                            stringBuilderTime.append((greenDateSec / 86400000).toString() + context.getString(R.string.text_time_day) + ".")
                        }
                        stringBuilderTime.append(" " + hours + context.getString(R.string.text_time_hour) + ".")
                        stringBuilderTime.append(" " + greenDateSec % 3600000 / 60000 + context.getString(R.string.text_time_min_small) + ".")
                        (view.findViewById<View>(R.id.textview_detail_list_order_time) as TextView).text = Html.fromHtml("<font color='" + color + "'>" + stringBuilderTime.toString().trim() + "</font>")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            view.setTag(R.id.tag_order_id, order.orderId)
            view.setTag(R.id.tag_order_type, order.type)
            view.setTag(R.id.tag_order_delete, order.isDenyRefuse)
            return view
        }
    }
}
