package com.kabbi.driver.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.HashMD5
import com.loopj.android.http.RequestParams
import cz.msebera.android.httpclient.client.ClientProtocolException
import cz.msebera.android.httpclient.client.HttpClient
import cz.msebera.android.httpclient.client.methods.HttpGet
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.*

class AddressAdapter(
        var ctx: Context,
        var driver: Driver,
        private val service: Service,
        part: String, cityId: String
) : BaseAdapter(), Filterable {

    var lInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private var part: String
    private val cityId: String
    private var mResult: List<HashMap<String, String>>
    private val client: HttpClient = HttpClientBuilder.create().build()
    private val appPreferences: AppPreferences
    fun updateAddresses(part: String) {
        this.part = part
        filter
    }

    override fun getCount(): Int {
        return mResult.size
    }

    override fun getItem(position: Int): Any {
        return mResult[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = convertView ?: lInflater.inflate(R.layout.item_dropdown, parent, false)
        view.setTag(R.id.tag_cityid, mResult[position]["city_id"])
        view.setTag(R.id.tag_label, mResult[position]["label"])
        view.setTag(R.id.tag_lat, mResult[position]["lat"])
        view.setTag(R.id.tag_lon, mResult[position]["lon"])
        view.setTag(R.id.tag_house, mResult[position]["house"])
        view.setTag(R.id.tag_housing, mResult[position]["housing"])
        view.setTag(R.id.tag_building, mResult[position]["building"])
        view.setTag(R.id.tag_porch, mResult[position]["porch"])
        view.setTag(R.id.tag_type, mResult[position]["type"])
        val builderAddress = StringBuilder()
        if (mResult[position]["city"] != null && mResult[position]["city"]!!.isNotEmpty()) builderAddress.append(mResult[position]["city"]).append(", ")
        builderAddress.append(mResult[position]["label"])
        (view.findViewById<View>(R.id.text1) as TextView).text = builderAddress.toString()
        return view
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val filterResults = FilterResults()
                if (constraint != null) {
                    try {
                        val result = jSON
                        val jsonObject = JSONObject(result)
                        val items: MutableList<HashMap<String, String>> = ArrayList()
                        if (jsonObject.getJSONArray("results").length() > 0) {
                            val jsonArrayStreet = jsonObject.getJSONArray("results")
                            for (j in 0 until jsonArrayStreet.length()) {
                                try {
                                    val jsonObjectStreet = jsonArrayStreet.getJSONObject(j)
                                    items.add(HashMap<String, String>().run {
                                        put("city_id", cityId)
                                        put("city", jsonObjectStreet.getJSONObject("address").getString("city"))
                                        put("label", jsonObjectStreet.getJSONObject("address").getString("label"))
                                        put("lat", jsonObjectStreet.getJSONObject("address").getString("lat"))
                                        put("lon", jsonObjectStreet.getJSONObject("address").getString("lon"))
                                        this
                                    })
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                            filterResults.values = items
                            filterResults.count = items.size
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    mResult = results.values as List<HashMap<String, String>>
                    notifyDataSetChanged()
                } else {
                    notifyDataSetInvalidated()
                }
            }
        }
    }

    //TODO rewrite this old shit
    private val jSON: String
        get() {
            val requestParams = RequestParams()
            requestParams.add("city_id", cityId)
            requestParams.add("focus.point.lat", appPreferences.getText("lat"))
            requestParams.add("focus.point.lon", appPreferences.getText("lon"))
            requestParams.add("format", "gootax")
            if (appPreferences.getText("favorite_lang").isNotEmpty()) {
                try {
                    requestParams.add("lang", AppParams.LANG[Integer.valueOf(appPreferences.getText("favorite_lang"))])
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                requestParams.add("lang", Locale.getDefault().language)
            }
            requestParams.add("tenant_domain", service.uri)
            requestParams.add("type_app", "driver")
            try {
                requestParams.add("text", URLEncoder.encode(part, "UTF-8"))
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
            requestParams.add("hash", HashMD5.getHash(requestParams.toString() + driver.secretCode))
            val stringBuilder = StringBuilder()
            val httpGet = HttpGet(AppParams.URL_GEOCODE + "?" + requestParams.toString())
            httpGet.setHeader("Content-Type", "application/json; charset=utf-8")
            try {
                val response = client.execute(httpGet)
                val statusLine = response.statusLine
                val statusCode = statusLine.statusCode
                if (statusCode == 200) {
                    val entity = response.entity
                    val content = entity.content
                    val reader = BufferedReader(InputStreamReader(content))
                    var line: String?
                    while (reader.readLine().also { line = it } != null) {
                        stringBuilder.append(line)
                    }
                    reader.close()
                    content.close()
                }
            } catch (e: ClientProtocolException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return stringBuilder.toString()
        }

    init {
        this.part = part
        this.cityId = cityId
        appPreferences = AppPreferences(ctx)
        mResult = ArrayList()
    }
}