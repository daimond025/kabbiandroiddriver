package com.kabbi.driver.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.kabbi.driver.R;
import com.kabbi.driver.ReportOrderDetailActivity;
import com.kabbi.driver.database.entities.Order;
import com.kabbi.driver.events.SendRouteToServer;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class RouteAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    List<Order> element;

    public RouteAdapter(Context context, List<Order> elem) {
        ctx = context;
        element = elem;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return element.size();
    }

    @Override
    public Object getItem(int position) {
        return element.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = lInflater.inflate(R.layout.detail_list_route, parent, false);

        TextView textViewInfo = view.findViewById(R.id.tv_routeinfo);

        textViewInfo.setText("№ " + element.get(position).getOrderIdService());
        textViewInfo.setOnClickListener(v -> {
            Intent intent = new Intent(ctx, ReportOrderDetailActivity.class);
            intent.putExtra("order_id", element.get(position).getId());
            ctx.startActivity(intent);
        });

        ImageButton btnSend = view.findViewById(R.id.ib_sendroute);

        btnSend.setOnClickListener(v -> EventBus.getDefault().post(new SendRouteToServer(element.get(position).getId(), position)));

        return view;
    }
}
