package com.kabbi.driver.adapters;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.view.View;

import com.kabbi.driver.fragments.IncomeWeekTabFragment;

import java.util.List;

public class IncomeWeekFragmentAdapter extends FragmentPagerAdapter {

    private List<String> titlesList;
    private List<String> reqDates;
    private final int COUNT_INCOME_FRAGMENT = 4;

    public IncomeWeekFragmentAdapter(FragmentManager fm, List<String> titles, List<String> reqDates) {
        super(fm);
        this.titlesList = titles;
        this.reqDates = reqDates;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        Fragment fragment = new IncomeWeekTabFragment();

        if (position == 0) {
            args.putString("date_start", reqDates.get(1));
            args.putString("date_end", reqDates.get(0));
        } else if (position == 1) {
            args.putString("date_start", reqDates.get(3));
            args.putString("date_end", reqDates.get(2));
        } else if (position == 2) {
            args.putString("date_start", reqDates.get(5));
            args.putString("date_end", reqDates.get(4));
        } else if (position == 3) {
            args.putString("date_start", reqDates.get(7));
            args.putString("date_end", reqDates.get(6));
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        if(object != null){
            return ((Fragment)object).getView() == view;
        }else{
            return false;
        }
    }

    @Override
    public int getCount() {
        return COUNT_INCOME_FRAGMENT;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titlesList.get(position);
    }
}