package com.kabbi.driver.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.helper.TypefaceSpanEx
import org.json.JSONObject

import java.text.DecimalFormat

class ReportOrderAdapter(internal var ctx: Context, private val element: List<Order>) : BaseAdapter() {
    internal var lInflater: LayoutInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return element.size
    }

    override fun getItem(position: Int): Any {
        return element[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        @SuppressLint("ViewHolder")
        val view = lInflater.inflate(R.layout.detail_list_reportorder, parent, false)
        val order = element[position]

        view.tag = order.id
        try {
            val jsonAddress = JSONObject(order.address).getJSONObject("A")

            val city = if (jsonAddress.isNull("city") || jsonAddress.getString("city").isEmpty()) "" else jsonAddress.getString("city") + ", "
            val street = if (jsonAddress.isNull("street") || jsonAddress.getString("street").isEmpty()) "" else jsonAddress.getString("street")
            val house = if (jsonAddress.isNull("house") || jsonAddress.getString("house").isEmpty()) "" else ", " + jsonAddress.getString("house")

            (view.findViewById<View>(R.id.textview_detail_list_reportorder_source) as TextView).text = String.format("%s%s%s", city, street, house)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (order.orderIdService.isNotEmpty()) {
            (view.findViewById<View>(R.id.textview_detail_list_alltime) as TextView).text = String.format("№%s", order.orderIdService)
        } else {
            (view.findViewById<View>(R.id.textview_detail_list_alltime) as TextView).text = String.format("№%s", order.orderId)
        }

        if (order.finalOrderCost.isNotEmpty()) {
            val costFormat = DecimalFormat("#.##")
            (view.findViewById<View>(R.id.textview_detail_list_ordercost) as TextView).text = TypefaceSpanEx.getCurrencyStr(ctx, parent, costFormat.format(java.lang.Double.valueOf(order.finalOrderCost)))
        }

        (view.findViewById<View>(R.id.textview_detail_list_startdate) as TextView).text = order.orderTime

        return view
    }
}