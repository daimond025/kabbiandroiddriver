package com.kabbi.driver.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.google.maps.model.LatLng
import com.kabbi.driver.R
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.models.OpenOrder
import com.kabbi.driver.util.buildAddress
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class OrderAdapter(
        private val activity: Activity,
        private var orders: MutableList<OpenOrder>,
        private val colorSubText: Int) : BaseAdapter() {

    internal var lInflater: LayoutInflater
    internal var ctx: Context = activity.applicationContext
    private val dateFormat: SimpleDateFormat
    private val stringBuilderTime: StringBuilder
    private val appPreferences: AppPreferences

    init {
        lInflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US)
        stringBuilderTime = StringBuilder()
        appPreferences = AppPreferences(ctx)
    }

    override fun getCount(): Int {
        return orders.size
    }

    override fun getItem(position: Int): Any {
        return orders[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: lInflater.inflate(R.layout.detail_list_order, parent, false)
        stringBuilderTime.setLength(0)

        val detailsText = view.findViewById<TextView>(R.id.textview_detail_list_order)
        val order = orders[position]

        try {
            detailsText.text = order.from.street
        } catch (e: Exception) {
            detailsText.text = buildAddress(LatLng(order.from.lat, order.from.lng), null)
        }

        val stringBuilder = StringBuilder()
        if (order.type == "reserve_br") {
            view.findViewById<TextView>(R.id.textview_detail_list_order_cash).text = Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + activity.getString(R.string.text_order_reserve) + "</font>")
            stringBuilder.append(activity.getString(R.string.text_order_reserve) + " ")
        }
        if (order.payment != "CASH") {
            stringBuilder.append(activity.getString(R.string.text_nocash))
            view.findViewById<TextView>(R.id.textview_detail_list_order_cash).text = Html.fromHtml("<font color='" + String.format("#%06X", 0xFFFFFF and colorSubText) + "'>" + stringBuilder.toString() + "</font>")
        }
        view.findViewById<TextView>(R.id.textview_detail_list_order_dis).text = order.distance.toString() + " " + activity.getString(R.string.text_dis)
        if (order.type == "free") {
            if (appPreferences.getText("show_urgent_order_time") == "0") {
                view.findViewById<TextView>(R.id.textview_detail_list_order_time).text = activity.getString(R.string.text_type_order)
            } else {
                try {
                    var parseDateSec: Long = 0
                    val dateSec = Date().time

                    try {
                        val parseDate = dateFormat.parse(order.time)
                        parseDateSec = parseDate.time
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                    val hours: String
                    var color: String? = null
                    var greenDateSec: Long = 0
                    if (parseDateSec > dateSec) {
                        greenDateSec = parseDateSec - dateSec
                        color = AppParams.colorGreen(ctx)
                    } else {
                        greenDateSec = dateSec - parseDateSec
                        color = AppParams.colorRed(ctx)
                    }

                    val lHours = greenDateSec / 3600000
                    if (lHours <= 24) {
                        hours = lHours.toString()
                    } else {
                        hours = (lHours % 24).toString()
                        stringBuilderTime.append((greenDateSec / 86400000).toString() + ctx.getString(R.string.text_time_day) + ".")
                    }

                    stringBuilderTime.append(" " + hours + " " + ctx.getString(R.string.text_time_hour))
                    stringBuilderTime.append(" " + greenDateSec % 3600000 / 60000 + " " + ctx.getString(R.string.text_time_min_small))
                    (view.findViewById<View>(R.id.textview_detail_list_order_time) as TextView).text = Html.fromHtml("<font color='" + color + "'>" + stringBuilderTime.toString().trim { it <= ' ' } + "</font>")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        } else {
            (view.findViewById<View>(R.id.textview_detail_list_order_time) as TextView).text = order.time
        }
        view.setTag(R.id.tag_order_id, order.id)
        view.setTag(R.id.tag_order_type, order.type)
        return view
    }
}
