package com.kabbi.driver


import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.text.Html
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.DriverTariff
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.TypefaceSpanEx
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*


class TariffActivity : BaseActivity() {

    internal lateinit var inflater: LayoutInflater
    internal lateinit var appPreferences: AppPreferences
    private lateinit var onceTariffs: MutableList<DriverTariff>
    private lateinit var subTariffs: MutableList<DriverTariff>
    private lateinit var activeTariffs: MutableList<DriverTariff>
    private lateinit var linearLayoutOnce: LinearLayout
    private lateinit var linearLayoutSub: LinearLayout
    private lateinit var linearLayoutActive: LinearLayout
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null
    private var viewGroup: ViewGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(applicationContext)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_tariff)

        viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_abonement)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        linearLayoutActive = findViewById(R.id.linearlayout_tariff1)
        linearLayoutOnce = findViewById(R.id.linearlayout_tariff2)
        linearLayoutSub = findViewById(R.id.linearlayout_tariff3)
        inflater = layoutInflater

        setup(this)
    }

    private fun setup(context: Context) = runBlocking {

        val db = AppDatabase.getInstance(context)
        val driverTariffDao = db.driverTariffDao()
        var driver = Driver()

        var driverTariffs: List<DriverTariff> = arrayListOf()
        withContext(Dispatchers.IO) {
            driver =  db.driverDao().getById(intent.extras!!.getLong("driver_id"))
            driverTariffs = driverTariffDao.getDriverTariffs(driver.id)
        }
        onceTariffs = ArrayList()
        subTariffs = ArrayList()
        activeTariffs = ArrayList()

        for (tariff in driverTariffs) {
            when {
                tariff.tariff == "active" -> activeTariffs.add(tariff)
                tariff.tariffType == "ONCE" -> onceTariffs.add(tariff)
                tariff.tariffType == "SUBSCRIPTION" -> subTariffs.add(tariff)
            }
        }

        if (activeTariffs.size > 0) {

            for ((_, activeShiftCount, _, _, tariffId, tariffPeriodType, tariffPeriodInfo, tariffName, tariffCost, orderFee, orderFeeType, shiftCount) in activeTariffs) {

                val tariffView = inflater.inflate(R.layout.detail_layout_tariff, null, false)

                var countShift = ""
                if (shiftCount != "null" && shiftCount.isNotEmpty()) {
                    countShift = "<br>" + getString(R.string.text_tariff_countshift) + ": " + activeShiftCount + "/" + shiftCount
                }

                val periodType = if (tariffPeriodType == "HOURS") getString(R.string.text_time_hour) else ""

                (tariffView.findViewById<View>(R.id.textview_tariff_detail) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + tariffName
                        + "</font></big><br><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + getString(R.string.text_tariff_period) + ": " + tariffPeriodInfo + " " + periodType + countShift + "</font>")


                val fee: String = if (orderFeeType == "PERCENT") {
                    orderFee + "%" + " " + getString(R.string.text_tariff_commision)
                } else {
                    TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, orderFee) + " " + getString(R.string.text_tariff_commision)
                }

                (tariffView.findViewById<View>(R.id.textview_tariff_detail_green) as TextView).text = Html.fromHtml(TypefaceSpanEx.getCurrencyStr(context, viewGroup, tariffCost)
                        + "<br>" + fee)

                tariffView.tag = tariffId

                (linearLayoutActive.getChildAt(1) as LinearLayout).addView(tariffView)

                linearLayoutActive.visibility = View.VISIBLE
            }
        }

        if (onceTariffs.size > 0) {
            for ((_, _, _, _, tariffId, tariffPeriodType, tariffPeriodInfo, tariffName, tariffCost, orderFee, orderFeeType, shiftCount) in onceTariffs) {

                val tariffView = inflater.inflate(R.layout.detail_layout_tariff, null, false)

                var countShift = ""
                if (shiftCount != "null" && shiftCount.isNotEmpty()) {
                    countShift = "<br>" + getString(R.string.text_tariff_countshift) + ": " + shiftCount
                }

                val periodType = if (tariffPeriodType == "HOURS") getString(R.string.text_time_hour) else ""

                (tariffView.findViewById<View>(R.id.textview_tariff_detail) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + tariffName
                        + "</font></big><br><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + getString(R.string.text_tariff_period) + ": " + tariffPeriodInfo + " " + periodType + countShift + "</font>")

                val fee: String = if (orderFeeType == "PERCENT") {
                    orderFee + "%" + " " + getString(R.string.text_tariff_commision)
                } else {
                    TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, orderFee) + " " + getString(R.string.text_tariff_commision)
                }

                (tariffView.findViewById<View>(R.id.textview_tariff_detail_green) as TextView).text = Html.fromHtml(TypefaceSpanEx.getCurrencyStr(context, viewGroup, tariffCost)
                        + "<br>" + fee)

                tariffView.tag = tariffId

                (linearLayoutOnce.getChildAt(1) as LinearLayout).addView(tariffView)

                linearLayoutOnce.visibility = View.VISIBLE
            }
        }

        if (subTariffs.size > 0) {
            for ((_, _, _, _, tariffId, tariffPeriodType, tariffPeriodInfo, tariffName, tariffCost, orderFee, orderFeeType, shiftCount) in subTariffs) {

                val tariffView = inflater.inflate(R.layout.detail_layout_tariff, null, false)

                var countShift = ""
                if (shiftCount != "null" && shiftCount.isNotEmpty()) {
                    countShift = "<br>" + getString(R.string.text_tariff_countshift) + ": " + shiftCount
                }

                val periodType = if (tariffPeriodType == "HOURS") getString(R.string.text_time_hour) else ""

                (tariffView.findViewById<View>(R.id.textview_tariff_detail) as TextView).text = Html.fromHtml("<big><font color='" + String.format("#%06X", 0xFFFFFF and typedValueMain!!.data) + "'>" + tariffName
                        + "</font></big><br><font color='" + String.format("#%06X", 0xFFFFFF and typedValueSubscribe!!.data) + "'>" + getString(R.string.text_tariff_period) + ": " + tariffPeriodInfo + " " + periodType + countShift + "</font>")

                val fee: String = if (orderFeeType == "PERCENT") {
                    orderFee + "%" + " " + getString(R.string.text_tariff_commision)
                } else {
                    TypefaceSpanEx.getCurrencyStr(applicationContext, viewGroup, orderFee) + " " + getString(R.string.text_tariff_commision)
                }

                (tariffView.findViewById<View>(R.id.textview_tariff_detail_green) as TextView).text = Html.fromHtml(TypefaceSpanEx.getCurrencyStr(context, viewGroup, tariffCost)
                        + "<br>" + fee)

                tariffView.tag = tariffId

                (linearLayoutSub.getChildAt(1) as LinearLayout).addView(tariffView)

                linearLayoutSub.visibility = View.VISIBLE
            }
        }
    }

    fun setTariff(v: View) {
        appPreferences.saveText("drivertariff_id", v.tag.toString())
        val intent = Intent(this, WorkActivity::class.java)
        intent.putExtra("service_id", getIntent().extras!!.getLong("service_id"))
        intent.putExtra("end_workshift", false)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
