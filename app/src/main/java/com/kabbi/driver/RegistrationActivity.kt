package com.kabbi.driver

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.dao.SoundDao
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.database.entities.Sound
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.InternetConnection
import com.kabbi.driver.network.WebService
import com.kabbi.driver.network.responses.AuthWorkerResponse
import com.kabbi.driver.util.CustomToast
import com.kabbi.driver.util.getDeviceToken
import kotlinx.coroutines.*

class RegistrationActivity : BaseActivity() {

    private lateinit var edLogin: EditText
    private lateinit var edPass: EditText
    private lateinit var edComment: EditText
    private lateinit var appPreferences: AppPreferences
    private lateinit var soundDao: SoundDao
    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        soundDao = AppDatabase.getInstance(this).soundDao()
        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences.getText("theme")))
        setContentView(R.layout.activity_registration_other)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.title_toolbar_registration)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        edLogin = findViewById(R.id.edittext_nickname)
        edPass = findViewById(R.id.edittext_passdriver)
        edComment = findViewById(R.id.edittext_commentdriver)
    }

    fun addService(v: View) {
        if (InternetConnection.isOnline(this)) {
            if (edLogin.text.toString().isBlank())
                edLogin.error = getString(R.string.edittext_valid_required)
            if (edPass.text.toString().isBlank())
                edPass.error = getString(R.string.edittext_valid_required)
            if (edLogin.text.toString().isNotBlank() && edPass.text.toString().isNotBlank()) {
                var driverReg: Driver? = null
                var serviceReg: Service? = null

                scope.launch {
                    withContext(Dispatchers.IO) {
                        val db = AppDatabase.getInstance(this@RegistrationActivity)
                        driverReg = db.driverDao().get(edLogin.text.toString().trim())

                        driverReg?.also { driver ->
                            serviceReg = db.serviceDao().getByDriver(driver.id)

                            if (serviceReg!!.uri != AppParams.TENANT) {
                                serviceReg = null
                                driverReg = null
                            }
                        }
                    }

                    if (driverReg == null && serviceReg == null) {
                        findViewById<View>(R.id.button_addservice).visibility = View.GONE
                        findViewById<View>(R.id.pbHeaderProgress).visibility = View.VISIBLE

                        getDeviceToken().addOnSuccessListener { result ->

                            scope.launch {
                                val dEvent = withContext(Dispatchers.IO) {
                                    WebService.authWorker(result.token, AppParams.TENANT, edLogin.text.toString().trim(), edPass.text.toString().trim())
                                }
                                onEvent(dEvent)
                            }
                        }
                    } else {
                        CustomToast.showMessage(this@RegistrationActivity, getString(R.string.registration_driver_error))
                    }
                }
            }
        } else {
            CustomToast.showMessage(this, getString(R.string.text_internet_access))
        }
    }

    fun onEvent(workerEvent: AuthWorkerResponse) {
        when (workerEvent.status) {
            "OK" -> {
                val newDriver = Driver()
                newDriver.secretCode = workerEvent.workerSecretKey
                newDriver.callSign = edLogin.text.toString().trim()
                newDriver.password = edPass.text.toString().trim()
                newDriver.token = appPreferences.getText("device_token")
                var newDriverId: Long = 0
                runBlocking {
                    withContext(Dispatchers.IO) {
                        newDriverId = AppDatabase.getInstance(this@RegistrationActivity).driverDao().save(newDriver)
                        newDriver.id = newDriverId
                    }
                }

                val newService = Service()
                newService.serviceName = workerEvent.companyName
                newService.logo = workerEvent.companyLogo
                newService.comment = edComment.text.toString().trim()
                newService.driver = newDriverId
                newService.uri = AppParams.TENANT
                var newServiceId: Long = 0
                runBlocking {
                    withContext(Dispatchers.IO) {
                        newServiceId = AppDatabase.getInstance(this@RegistrationActivity).serviceDao().save(newService)
                        newService.id = newServiceId
                    }
                }

                appPreferences.saveText("car_id", "")
                appPreferences.saveText("drivertariff_id", "")
                appPreferences.saveText("city_id", "")
                appPreferences.saveText("driver_id", "")
                appPreferences.saveText("parking", "")
                appPreferences.saveText("navigation", getString(R.string.dialog_nav_google))

                appPreferences.saveText("step_1", "0")
                appPreferences.saveText("step_2", "0")
                appPreferences.saveText("step_3", "0")
                appPreferences.saveText("step_4", "0")

                appPreferences.saveText("fake_gps", "0")

                if (appPreferences.getText("favorite_map").isEmpty())
                    appPreferences.saveText("favorite_map", AppParams.MAP)
                if (appPreferences.getText("push_balance").isEmpty())
                    appPreferences.saveText("push_balance", "1")
                if (appPreferences.getText("push_preorder").isEmpty())
                    appPreferences.saveText("push_preorder", "1")
                if (appPreferences.getText("push_sos").isEmpty())
                    appPreferences.saveText("push_sos", "1")
                if (appPreferences.getText("push_sound").isEmpty())
                    appPreferences.saveText("push_sound", "1")
                if (appPreferences.getText("favorite_lang").isEmpty())
                    appPreferences.saveText("favorite_lang", AppParams.DEFAULT_LANG)
                if (appPreferences.getText("far_navi").isEmpty())
                    appPreferences.saveText("far_navi", "0")

                if (appPreferences.getText("push_sound_freeorder").isEmpty())
                    appPreferences.saveText("push_sound_freeorder", "1")
                if (appPreferences.getText("push_sound_preorder").isEmpty())
                    appPreferences.saveText("push_sound_preorder", "1")

                if (appPreferences.getText("push_show_freeorder").isEmpty())
                    appPreferences.saveText("push_show_freeorder", "1")
                if (appPreferences.getText("push_show_preorder").isEmpty())
                    appPreferences.saveText("push_show_preorder", "1")

                if (appPreferences.getText("in_shift").isEmpty())
                    appPreferences.saveText("in_shift", "0")

                if (appPreferences.getText("gps").isEmpty())
                    appPreferences.saveText("gps", "0")
                if (appPreferences.getText("gps_server").isEmpty())
                    appPreferences.saveText("gps_server", "0")

                if (appPreferences.getText("allow_cancel_order").isEmpty())
                    appPreferences.saveText("allow_cancel_order", "0")
                if (appPreferences.getText("allow_cancel_order_after_time").isEmpty())
                    appPreferences.saveText("allow_cancel_order_after_time", "20")

                if (appPreferences.getText("show_chat_with_dispatcher_before_start_shift").isEmpty())
                    appPreferences.saveText("show_chat_with_dispatcher_before_start_shift", "1")
                if (appPreferences.getText("show_chat_with_dispatcher_on_shift").isEmpty())
                    appPreferences.saveText("show_chat_with_dispatcher_on_shift", "1")
                if (appPreferences.getText("show_general_chat_on_shift").isEmpty())
                    appPreferences.saveText("show_general_chat_on_shift", "1")
                if (appPreferences.getText("deny_fakegps").isEmpty())
                    appPreferences.saveText("deny_fakegps", "0")
                if (appPreferences.getText("allow_worker_to_create_order").isEmpty())
                    appPreferences.saveText("allow_worker_to_create_order", "1")
                if (appPreferences.getText("deny_setting_arrival_status_early").isEmpty())
                    appPreferences.saveText("deny_setting_arrival_status_early", "0")

                if (appPreferences.getText("min_distance_to_set_arrival_status").isEmpty())
                    appPreferences.saveText("min_distance_to_set_arrival_status", "500")
                if (appPreferences.getText("start_time_by_order").isEmpty())
                    appPreferences.saveText("start_time_by_order", "30")
                if (appPreferences.getText("require_password_every_time_log_in_application").isEmpty())
                    appPreferences.saveText("require_password_every_time_log_in_application", "0")
                if (appPreferences.getText("print_check").isEmpty())
                    appPreferences.saveText("print_check", "0")
                if (appPreferences.getText("control_own_car_mileage").isEmpty())
                    appPreferences.saveText("control_own_car_mileage", "0")
                if (appPreferences.getText("show_driver_privacy_policy").isEmpty())
                    appPreferences.saveText("show_driver_privacy_policy", "0")

                runBlocking {
                    withContext(Dispatchers.IO) {
                        val soundList = soundDao.getAll()
                        if (soundList.isEmpty()) {
                            val prefix = "android.resource://$packageName/"

                            soundDao.saveAll(
                                    Sound("ringtone_pop", "Pop", prefix + R.raw.pop, isDefault = true, isShortSound = true),
                                    Sound("ringtone_cute_bells", "Cute bells", prefix + R.raw.cute_bells, isDefault = true, isShortSound = true),
                                    Sound("ringtone_free_order", "Free order", prefix + R.raw.free_order, isDefault = true, isShortSound = true),
                                    Sound("ringtone_mirimba_chord", "Mirimba chord", prefix + R.raw.marimba_chord, isDefault = true, isShortSound = false),
                                    Sound("ringtone_own", "Own", prefix + R.raw.own, isDefault = true, isShortSound = true),
                                    Sound("ringtone_preorder", "Preorder", prefix + R.raw.pre_order, isDefault = true, isShortSound = true),
                                    Sound("ringtone_rejected", "Rejected", prefix + R.raw.rejected, isDefault = true, isShortSound = true),
                                    Sound("ringtone_remind_preorder", "Remind preorder", prefix + R.raw.remind_preorder, isDefault = true, isShortSound = true),
                                    Sound("ringtone_sos", "Sos", prefix + R.raw.sos, isDefault = true, isShortSound = true))
                        }
                    }
                }

                val intentEntrance = Intent(applicationContext, WorkActivity::class.java)
                intentEntrance.putExtra("service_id", newService.id)
                intentEntrance.putExtra("end_workshift", false)

                (application as App).driver = newDriver
                (application as App).service = newService

                appPreferences.saveText("driver_id", newDriverId.toString())
                appPreferences.saveText("service_id", newServiceId.toString())

                startActivity(intentEntrance)
                finish()
            }
            "WORKER_NOT_EXIST" -> CustomToast.showMessage(this, getString(R.string.registration_fail))
            "ACCESS_DENIED" -> CustomToast.showMessage(this, getString(R.string.registration_fail_access_denied))
            else -> CustomToast.showMessage(this, getString(R.string.error_click))
        }
        findViewById<View>(R.id.pbHeaderProgress).visibility = View.GONE
        findViewById<View>(R.id.button_addservice).visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}