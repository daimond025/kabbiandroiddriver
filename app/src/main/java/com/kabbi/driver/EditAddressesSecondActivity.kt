package com.kabbi.driver

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import android.util.TypedValue
import android.view.MenuItem
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.*
import com.kabbi.driver.taximeter.TaximeterTabAboutFragment
import com.kabbi.driver.helper.AppPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class EditAddressesSecondActivity : BaseActivity() {

    private var appPreferences: AppPreferences? = null
    private var typedValueMain: TypedValue? = null
    private var typedValueSubscribe: TypedValue? = null

    lateinit var service: Service
        internal set
    lateinit var currentOrder: CurrentOrder
        internal set
    lateinit var driver: Driver
        internal set
    lateinit var workDay: WorkDay
        internal set
    lateinit var order: Order
        internal set

    internal lateinit var fragment: androidx.fragment.app.Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appPreferences = AppPreferences(this)
        setTheme(Integer.valueOf(appPreferences!!.getText("theme")))
        setContentView(R.layout.activity_edit_second_addresses)

        typedValueMain = TypedValue()
        typedValueSubscribe = TypedValue()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "  " + getString(R.string.change_addresses)
        toolbar.setTitleTextColor(resources.getColor(R.color.white))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        workDay = (application as App).workDay
        driver = (application as App).driver
        service = (application as App).service
        val db = AppDatabase.getInstance(this)
        runBlocking {
            withContext(Dispatchers.IO) {
                currentOrder = db.currentOrderDao().getCurOrder(driver.id)
                order = db.orderDao().getByOrderId(currentOrder.orderId)
            }
            fragment = TaximeterTabAboutFragment()
            supportFragmentManager.beginTransaction().replace(R.id.fl_container, fragment).commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }
}
