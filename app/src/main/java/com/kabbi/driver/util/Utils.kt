package com.kabbi.driver.util

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.maps.android.PolyUtil
import com.kabbi.driver.App
import com.kabbi.driver.R
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.Parking
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.helper.AppParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.util.*

object Utils {


    // UNICODE 0x23 = #
    val UNICODE_TEXT = byteArrayOf(0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23, 0x23)

    private val hexStr = "0123456789ABCDEF"
    private val binaryArray = arrayOf("0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111")

    fun getParkingDopCost(lat: Double, lon: Double, service: Service, context: Context): Parking? {
        val point = LatLng(lat, lon)
        val parkingList = runBlocking {
            withContext(Dispatchers.IO) {
                AppDatabase.getInstance(context).parkingDao().getAll(service.id)
            }
        }
        for (parking in parkingList) {
            val listPolygon = ArrayList<LatLng>()
            try {
                val jsonPolygon = JSONObject(parking.parkingPolygon)
                val jsonArrayCoords = jsonPolygon.getJSONObject("parking_polygone").getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(0)
                for (k in 0 until jsonArrayCoords.length()) {
                    val jsonArrayPoint = jsonArrayCoords.getJSONArray(k)
                    listPolygon.add(LatLng(jsonArrayPoint.getDouble(1), jsonArrayPoint.getDouble(0)))
                }
                if (PolyUtil.containsLocation(point, listPolygon, false) && parking.parkingType != "basePolygon") {
                    return parking
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return null
    }

    fun addParkingCost(parking: Parking, cost: Double, type: Int): Double? {
        var finalCost: Double = cost
        try {
            if (type == 0 && parking.outDistrCoefficientType != "null") {
                when (parking.outDistrCoefficientType) {
                    "PERCENT" -> finalCost += finalCost * (parking.outDistrict / 100)
                    "MONEY" -> finalCost += parking.outDistrict
                }
            } else if (parking.inDistrCoefficientType != "null") {
                when (parking.inDistrCoefficientType) {
                    "PERCENT" -> finalCost += finalCost * (parking.inDistrict / 100)
                    "MONEY" -> finalCost += parking.inDistrict
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return finalCost
    }

    fun getResourceId(context: Context, name: String, lang: String): Int {
        val langList = Arrays.asList(*AppParams.LANG_SOUND)
        return if (langList.contains(lang)) {
            context.resources.getIdentifier(name + "_" + lang, "raw", context.packageName)
        } else {
            context.resources.getIdentifier(name, "raw", context.packageName)
        }
    }

    fun decodeBitmap(bmp: Bitmap): ByteArray? {
        val bmpWidth = bmp.width
        val bmpHeight = bmp.height

        val list = ArrayList<String>() //binaryString list
        var sb: StringBuffer


        var bitLen = bmpWidth / 8
        val zeroCount = bmpWidth % 8

        var zeroStr = ""
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1
            for (i in 0 until 8 - zeroCount) {
                zeroStr = zeroStr + "0"
            }
        }

        for (i in 0 until bmpHeight) {
            sb = StringBuffer()
            for (j in 0 until bmpWidth) {
                val color = bmp.getPixel(j, i)

                val r = color shr 16 and 0xff
                val g = color shr 8 and 0xff
                val b = color and 0xff

                // if color close to white，bit='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0")
                else
                    sb.append("1")
            }
            if (zeroCount > 0) {
                sb.append(zeroStr)
            }
            list.add(sb.toString())
        }

        val bmpHexList = binaryListToHexStringList(list)
        val commandHexString = "1D763000"
        var widthHexString = Integer
                .toHexString(if (bmpWidth % 8 == 0)
                    bmpWidth / 8
                else
                    bmpWidth / 8 + 1)
        if (widthHexString.length > 2) {
            Log.e("decodeBitmap error", " width is too large")
            return null
        } else if (widthHexString.length == 1) {
            widthHexString = "0$widthHexString"
        }
        widthHexString = widthHexString + "00"

        var heightHexString = Integer.toHexString(bmpHeight)
        if (heightHexString.length > 2) {
            Log.e("decodeBitmap error", " height is too large")
            return null
        } else if (heightHexString.length == 1) {
            heightHexString = "0$heightHexString"
        }
        heightHexString = heightHexString + "00"

        val commandList = ArrayList<String>()
        commandList.add(commandHexString + widthHexString + heightHexString)
        commandList.addAll(bmpHexList)

        return hexList2Byte(commandList)
    }

    private fun binaryListToHexStringList(list: List<String>): List<String> {
        val hexList = ArrayList<String>()
        for (binaryStr in list) {
            val sb = StringBuffer()
            var i = 0
            while (i < binaryStr.length) {
                val str = binaryStr.substring(i, i + 8)

                val hexString = myBinaryStrToHexString(str)
                sb.append(hexString)
                i += 8
            }
            hexList.add(sb.toString())
        }
        return hexList

    }

    private fun myBinaryStrToHexString(binaryStr: String): String {
        var hex = ""
        val f4 = binaryStr.substring(0, 4)
        val b4 = binaryStr.substring(4, 8)
        for (i in binaryArray.indices) {
            if (f4 == binaryArray[i])
                hex += hexStr.substring(i, i + 1)
        }
        for (i in binaryArray.indices) {
            if (b4 == binaryArray[i])
                hex += hexStr.substring(i, i + 1)
        }

        return hex
    }

    private fun hexList2Byte(list: List<String>): ByteArray {
        val commandList = ArrayList<ByteArray?>()

        for (hexStr in list) {
            commandList.add(hexStringToBytes(hexStr))
        }
        return sysCopy(commandList)
    }

    private fun hexStringToBytes(hexString: String?): ByteArray? {
        var hexString = hexString
        if (hexString == null || hexString == "") {
            return null
        }
        hexString = hexString.toUpperCase()
        val length = hexString.length / 2
        val hexChars = hexString.toCharArray()
        val d = ByteArray(length)
        for (i in 0 until length) {
            val pos = i * 2
            d[i] = (hexChars[pos].toInt() shl (4) or hexChars[pos + 1].toInt()).toByte()
        }
        return d
    }

    private fun sysCopy(srcArrays: List<ByteArray?>): ByteArray {
        var len = 0
        for (srcArray in srcArrays) {
            len += srcArray!!.size
        }
        val destArray = ByteArray(len)
        var destLen = 0
        for (srcArray in srcArrays) {
            srcArray?.also {
                System.arraycopy(srcArray, 0, destArray, destLen, srcArray.size)
                destLen += srcArray.size
            }
        }
        return destArray
    }

    private fun charToInt(c: Char): Int {
        return "0123456789ABCDEF".indexOf(c)
    }

    fun <T : View?> showSnackBar(status: String, view: T) {

        view?.also {
            it as View
            val message = when (status) {
                AppParams.EMPTY_RESPONSE -> App.appContext.getString(R.string.contact_support)
                AppParams.RESPONSE_ERROR -> App.appContext.getString(R.string.response_error)
                else -> App.appContext.getString(R.string.request_error)
            }
            val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE)
            snackbar.setAction("OK") {
                snackbar.dismiss()
            }
            snackbar.show()
        }
    }
}
