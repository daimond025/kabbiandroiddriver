package com.kabbi.driver.util

import android.content.Intent
import com.kabbi.driver.WorkShiftActivity
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.CurrentOrder
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.ClientTariffParser
import com.kabbi.driver.myOrders.OrderRaw
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

fun buildOrder(result: Int, orderId: String, orderRaw: OrderRaw, context: WorkShiftActivity) {

    val driver = context.driver!!
    val workDay = context.workDay!!
    val service = context.service!!
    val appPreferences = AppPreferences(context.applicationContext)


    if (result == 1 && orderId == orderRaw.order_id && orderRaw.costData != null) {

        runBlocking {
            withContext(Dispatchers.IO) {
                AppDatabase.getInstance(context).currentOrderDao().deleteAll(driver.id)
            }
        }

        val costData = orderRaw.costData

        val order = Order()

        val options = if (orderRaw.options.isNotEmpty()) orderRaw.options.toString() else ""

        val comment = if (!orderRaw.comment.isNullOrEmpty() && orderRaw.comment != "null") orderRaw.comment else ""

        val currentOrder = CurrentOrder(
                orderId = orderId,
                timeToClient = 0,
                beginOrderTime = Date().time,
                waitTimeBegin = Date().time,
                waitTimeEnd = Date().time,
                waitTime = 0,
                stage = 0,
                tariffData = orderRaw.costData.toString(),
                address = orderRaw.address.toString(),
                options = options,
                comment = comment,
                phone = "",
                driver = driver.id
        )

        order.workDay = workDay.id
        order.pasName = AppParams.ORDER_LABEL_NEW
        order.statusOrder = AppParams.ORDER_LABEL_NEW
        order.createDatetime = Date().time
        order.orderId = orderId
        order.orderIdService = orderRaw.order_number
        order.driver = driver.id
        order.tariffData = costData.toString()
        order.address = orderRaw.address.toString()
        order.orderTime = orderRaw.order_time
        order.comment = comment

        order.clientName = orderRaw.clientPassenger?.run { last_name ?: "" + name ?: "" + second_name ?: "" } ?: orderRaw.clientPassenger?.run { last_name ?: "" + name ?: "" + second_name ?: "" }
                ?: ""

        order.clientPhone = orderRaw.client_passenger_phone ?: orderRaw.phone ?: ""
        order.day = costData.tariffInfo.isDay == 1
        order.tariffLabel = orderRaw.tariff?.name ?: ""
        order.dopCost = costData.additionals_cost?.toDouble() ?: 0.0

        order.promoCodeDiscount = orderRaw.promo_code_discount?.toInt() ?: 0

        val parser = ClientTariffParser()
        val tariffCity = parser.createTariff(costData.tariffInfo.tariffDataCity, costData.tariffInfo.isDay, ClientTariffParser.AREA_CITY, orderRaw)
        val tariffTrack = parser.createTariff(costData.tariffInfo.tariffDataTrack, costData.tariffInfo.isDay, ClientTariffParser.AREA_TRACK, orderRaw)

        order.costOrder = if (costData.is_fix == 1 && order.promoCodeDiscount > 0) {
            costData.summary_cost_no_discount?.toDouble() ?: 0.0
        } else {
            costData.summary_cost?.toDouble() ?: 0.0
        }

        if (appPreferences.getText("gps") == "1" && appPreferences.getText("gps_server") == "1") {
            order.fix = 1
            tariffCity.accrual = ClientTariffParser.ACCRUAL_WIFI
            tariffTrack.accrual = ClientTariffParser.ACCRUAL_WIFI
        } else {
            order.fix = costData.is_fix!!
        }

        order.timeWaitCityClient = tariffCity.waitTime * -60000
        order.timeWaitTrackClient = tariffTrack.waitTime * -60000

        order.startPoint = if (order.onlyCity) "in" else costData.start_point_location!!

        if (costData.start_point_location == "in") {
            order.plantingCost = tariffCity.plantingPrice
            order.minPrice = tariffCity.minPrice
            order.rounding = if (tariffCity.rounding > 0) tariffCity.rounding else 1.0
            order.roundingType = tariffCity.roundingType

            when (tariffCity.accrual) {
                ClientTariffParser.ACCRUAL_TIME -> order.timeCityInOrder = tariffCity.plantingInclude.toLong() * -60000
                ClientTariffParser.ACCRUAL_DISTANCE -> order.disCityInOrder = -tariffCity.plantingInclude
                ClientTariffParser.ACCRUAL_MIXED -> {
                    order.timeCityInOrder = tariffCity.plantingIncludeTime.toLong() * -60000
                    order.disCityInOrder = -tariffCity.plantingInclude
                }
                ClientTariffParser.ACCRUAL_INTERVAL -> order.disCityInOrder = -tariffCity.plantingInclude
            }
        } else {
            order.plantingCost = tariffTrack.plantingPrice
            order.minPrice = tariffTrack.minPrice
            order.rounding = if (tariffTrack.rounding > 0) tariffTrack.rounding else 1.0
            order.roundingType = tariffTrack.roundingType

            when (tariffTrack.accrual) {
                ClientTariffParser.ACCRUAL_TIME -> order.timeTrackInOrder = tariffTrack.plantingInclude.toLong() * -60000
                ClientTariffParser.ACCRUAL_DISTANCE -> order.disTrackInOrder = -tariffTrack.plantingInclude
                ClientTariffParser.ACCRUAL_MIXED -> {
                    order.timeTrackInOrder = tariffTrack.plantingIncludeTime.toLong() * -60000
                    order.disTrackInOrder = -tariffTrack.plantingInclude
                }
                ClientTariffParser.ACCRUAL_INTERVAL -> order.disTrackInOrder = -tariffTrack.plantingInclude
            }
        }

        order.dopOption = orderRaw.options.toString()
        order.typeCost = orderRaw.payment
        order.fixCityCost = costData.city_cost?.toDouble() ?: 0.0
        order.fixOutCityCost = costData.out_city_cost?.toDouble() ?: 0.0
        order.handBrake = orderRaw.tariff?.auto_downtime ?: ""

        appPreferences.saveText("require_point_confirmation_code", orderRaw.settings?.require_point_confirmation_code)

        val intent = Intent(context, WorkShiftActivity::class.java)
        intent.putExtra("service_id", service.id)
        intent.putExtra("workday_id", workDay.id)
        intent.putExtra("time", "0")
        intent.putExtra("showDialogGps", false)
        intent.putExtra("order_id", order.orderId)
        intent.putExtra("showDialog", "empty")

        try {
            runBlocking {
                withContext(Dispatchers.IO) {

                    val db = AppDatabase.getInstance(context)
                    currentOrder.id = db.currentOrderDao().save(currentOrder)
                    order.curOrderId = currentOrder.id
                    order.id = db.orderDao().save(order)
                    tariffCity.order = order.id
                    tariffTrack.order = order.id

                    val clientTariffDao = AppDatabase.getInstance(context).clientTariffDao()
                    clientTariffDao.save(tariffCity)
                    clientTariffDao.save(tariffTrack)
                }
            }
            intent.putExtra("stageOrder", 1)

            context.stageOrder = 1

        } catch (e: Exception) {
            context.stageOrder = 0
            context.runOnUiThread { CustomToast.showMessage(context, "Save Order - Error") }
        }

        context.order = order
        context.currentOrder = currentOrder

        context.onNavigationDrawerItemSelected(1)
    } else {
        context.runOnUiThread { CustomToast.showMessage(context, "Empty tariff") }
    }
}