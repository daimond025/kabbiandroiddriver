package com.kabbi.driver.util

import java.util.*

open class UuidProvider {

    private var uuid = generateUuid()

    fun getNewUuid(): String {
        uuid = generateUuid()
        return uuid
    }

    private fun generateUuid(): String {
        return UUID.randomUUID().toString()
    }

    fun clearUuid() {
        uuid = ""
    }

    fun getUuid(): String {
        return uuid
    }

}