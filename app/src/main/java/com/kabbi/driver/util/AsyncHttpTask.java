package com.kabbi.driver.util;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.kabbi.driver.App;
import com.kabbi.driver.BuildConfig;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.HashMD5;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

public class AsyncHttpTask {

    private AsyncTaskInterface callback;
    private AsyncHttpClient client = new AsyncHttpClient();
    private String userAgent;
    private CloseableHttpClient clientHttp;
    public AsyncHttpTask(AsyncTaskInterface callback, Context context) {
        this.callback = callback;

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000)
                .setSocketTimeout(30000).build();
        clientHttp = HttpClientBuilder.create().setDefaultRequestConfig(config).build();

        int versionCode = BuildConfig.VERSION_CODE;
        userAgent = "driver/" + versionCode + " (" + "android " + Build.VERSION.RELEASE + "; " + Build.MANUFACTURER + " " + Build.MODEL + ")";
    }

    public void getGeoJson(String url, RequestParams requestParams) {

        client.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                callback.doPostExecute(response, "");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.doPostExecute(null, "");
            }
        });
    }

    public void getGeoJsonArray(String url, RequestParams requestParams) {

        client.get(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    callback.doPostExecute(response.getJSONObject(0), "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.doPostExecute(null, "");
            }
        });
    }

    public void get(final String url, final RequestParams requestParams, String driverSecretCode) {
        AsyncTaskGet myAsyncTask = new AsyncTaskGet(url, requestParams, driverSecretCode);
        myAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void post(final String url, RequestParams requestParams, String driverSecretCode) {
        AsyncTaskPost myAsyncTask = new AsyncTaskPost(url, requestParams, driverSecretCode);
        myAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void postRoute(final String url, RequestParams requestParams, String driverSecretCode) {
        AsyncTaskRoutePost myAsyncTask = new AsyncTaskRoutePost(url, requestParams, driverSecretCode);
        myAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public interface AsyncTaskInterface {
        void doPostExecute(JSONObject jsonObject, String typeJson);
    }

    private class AsyncTaskPost extends AsyncTask<Void, Void, Void> {

        String url;
        RequestParams requestParams;
        String driverSecretCode;
        String result;

        AsyncTaskPost(String url, RequestParams requestParams, String driverSecretCode) {
            this.url = url;
            this.requestParams = requestParams;
            this.driverSecretCode = driverSecretCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                HttpPost httpPost = new HttpPost(AppParams.HOST + "/" + url);
                httpPost.setHeader("signature", HashMD5.getHash(requestParams.toString().replaceAll("%5C%2F", "%2F%2F").replaceAll(":", "%3A") + driverSecretCode));
                httpPost.setHeader("worker-device", "android");
                httpPost.setHeader("deviceid", AppParams.getUDID(App.Companion.getAppContext()));
                httpPost.setHeader("lang", Locale.getDefault().getLanguage());
                httpPost.setHeader("User-Agent", userAgent);
                httpPost.setHeader("Connection", "Keep-Alive");
                try {
                    httpPost.setEntity(requestParams.getEntity(new ResponseHandlerInterface() {
                        @Override
                        public void sendResponseMessage(HttpResponse response) throws IOException {

                        }

                        @Override
                        public void sendStartMessage() {

                        }

                        @Override
                        public void sendFinishMessage() {

                        }

                        @Override
                        public void sendProgressMessage(long bytesWritten, long bytesTotal) {

                        }

                        @Override
                        public void sendCancelMessage() {

                        }

                        @Override
                        public void sendSuccessMessage(int statusCode, Header[] headers, byte[] responseBody) {

                        }

                        @Override
                        public void sendFailureMessage(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }

                        @Override
                        public void sendRetryMessage(int retryNo) {

                        }

                        @Override
                        public URI getRequestURI() {
                            return null;
                        }

                        @Override
                        public Header[] getRequestHeaders() {
                            return new Header[0];
                        }                        @Override
                        public void setRequestURI(URI requestURI) {

                        }

                        @Override
                        public boolean getUseSynchronousMode() {
                            return false;
                        }

                        @Override
                        public boolean getUsePoolThread() {
                            return false;
                        }                        @Override
                        public void setRequestHeaders(Header[] requestHeaders) {

                        }

                        @Override
                        public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

                        }

                        @Override
                        public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

                        }                        @Override
                        public void setUseSynchronousMode(boolean useSynchronousMode) {

                        }

                        @Override
                        public Object getTag() {
                            return null;
                        }

                        @Override
                        public void setUsePoolThread(boolean usePoolThread) {

                        }







                        @Override
                        public void setTag(Object TAG) {

                        }
                    }));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    HttpResponse response = clientHttp.execute(httpPost);

                    HttpEntity entity = response.getEntity();
                    InputStream instream = entity.getContent();
                    result = read(instream);

                    Log.d("REQ_LISTENER", url + ": " + result);

                    instream.close();
                    //client.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (result != null && result.length() > 0)
                try {
                    Log.d("ASYNC_POST", result);
                    callback.doPostExecute(new JSONObject(result), url);
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (url.equals("send_photos_to_review")) {
                        try {
                            callback.doPostExecute(new JSONObject("{\"code\":0,\"info\":\"OK\",\"result\":\"fail\"}"), url);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
        }

        private String read(InputStream instream) {
            StringBuilder sb = null;
            try {
                sb = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(instream));
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    sb.append(line);
                }
                instream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
    }

    private class AsyncTaskRoutePost extends AsyncTask<Void, Void, Void> {

        String url;
        RequestParams requestParams;
        String driverSecretCode;
        String result;

        AsyncTaskRoutePost(String url, RequestParams requestParams, String driverSecretCode) {
            this.url = url;
            this.requestParams = requestParams;
            this.driverSecretCode = driverSecretCode;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                HttpPost httpPost = new HttpPost(AppParams.HOST + "/" + url);
                httpPost.setHeader("signature", HashMD5.getHash(new LinkedHashMap<String, String>() + driverSecretCode));
                httpPost.setHeader("worker-device", "android");
                httpPost.setHeader("deviceid", AppParams.getUDID(App.Companion.getAppContext()));
                httpPost.setHeader("lang", Locale.getDefault().getLanguage());
                httpPost.setHeader("User-Agent", userAgent);
                httpPost.setHeader("Connection", "Keep-Alive");

                try {
                    httpPost.setEntity(requestParams.getEntity(new ResponseHandlerInterface() {
                        @Override
                        public void sendResponseMessage(HttpResponse response) throws IOException {

                        }

                        @Override
                        public void sendStartMessage() {

                        }

                        @Override
                        public void sendFinishMessage() {

                        }

                        @Override
                        public void sendProgressMessage(long bytesWritten, long bytesTotal) {

                        }

                        @Override
                        public void sendCancelMessage() {

                        }

                        @Override
                        public void sendSuccessMessage(int statusCode, Header[] headers, byte[] responseBody) {

                        }

                        @Override
                        public void sendFailureMessage(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }

                        @Override
                        public void sendRetryMessage(int retryNo) {

                        }

                        @Override
                        public URI getRequestURI() {
                            return null;
                        }

                        @Override
                        public Header[] getRequestHeaders() {
                            return new Header[0];
                        }                        @Override
                        public void setRequestURI(URI requestURI) {

                        }

                        @Override
                        public boolean getUseSynchronousMode() {
                            return false;
                        }

                        @Override
                        public boolean getUsePoolThread() {
                            return false;
                        }                        @Override
                        public void setRequestHeaders(Header[] requestHeaders) {

                        }

                        @Override
                        public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

                        }

                        @Override
                        public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

                        }                        @Override
                        public void setUseSynchronousMode(boolean useSynchronousMode) {

                        }

                        @Override
                        public Object getTag() {
                            return null;
                        }

                        @Override
                        public void setUsePoolThread(boolean usePoolThread) {

                        }







                        @Override
                        public void setTag(Object TAG) {

                        }
                    }));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    HttpResponse response = clientHttp.execute(httpPost);

                    HttpEntity entity = response.getEntity();
                    InputStream instream = entity.getContent();
                    result = read(instream);

                    Log.d("REQ_LISTENER", url + ": " + result);

                    instream.close();
                    //client.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (result != null && result.length() > 0)
                try {
                    Log.d("ASYNC_POST", result);
                    callback.doPostExecute(new JSONObject(result), url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

        private String read(InputStream instream) {
            StringBuilder sb = null;
            try {
                sb = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(instream));
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    sb.append(line);
                }
                instream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
    }

    private class AsyncTaskGet extends AsyncTask<Void, Void, Void> {

        String url;
        RequestParams requestParams;
        String driverSecretCode;
        String result;

        AsyncTaskGet(String url, RequestParams requestParams, String driverSecretCode) {
            this.url = url;
            this.requestParams = requestParams;
            this.driverSecretCode = driverSecretCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpGet httpGet = new HttpGet(AppParams.HOST + "/" + url + "?" + requestParams.toString());

            if (driverSecretCode != null)
                httpGet.setHeader("signature", HashMD5.getHash(requestParams.toString().replaceAll("%5C%2F", "%2F%2F").replaceAll(":", "%3A") + driverSecretCode));
            httpGet.setHeader("worker-device", "android");
            try {
                httpGet.setHeader("deviceid", AppParams.getUDID(App.Companion.getAppContext()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            httpGet.setHeader("lang", Locale.getDefault().getLanguage());
            httpGet.setHeader("User-Agent", userAgent);
            httpGet.setHeader("Connection", "Keep-Alive");

            if (url.equals("get_order_for_id"))
                httpGet.setHeader("version", "3.17.0");

            RequestConfig config = RequestConfig.custom()
                    .setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000)
                    .setSocketTimeout(30000).build();

            CloseableHttpClient client =
                    HttpClientBuilder.create().setDefaultRequestConfig(config).build();

            try {
                HttpResponse response = client.execute(httpGet);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                result = read(instream);

                try {
                    Log.d("REQ_LISTENER", url + " # " + result);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                instream.close();
                client.close();

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (result != null && result.length() > 0)
                try {
                    callback.doPostExecute(new JSONObject(result), url);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

        private String read(InputStream instream) {
            StringBuilder sb = null;
            try {
                sb = new StringBuilder();
                BufferedReader r = new BufferedReader(new InputStreamReader(instream));
                for (String line = r.readLine(); line != null; line = r.readLine()) {
                    sb.append(line);
                }
                instream.close();
            } catch (IOException e) {
            }
            return sb.toString();
        }
    }
}
