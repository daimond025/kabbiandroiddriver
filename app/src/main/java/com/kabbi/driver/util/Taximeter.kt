package com.kabbi.driver.util


import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.annotation.NonNull
import com.kabbi.driver.database.AppDatabase
import com.kabbi.driver.database.entities.ClientTariff
import com.kabbi.driver.database.entities.Order
import com.kabbi.driver.database.entities.Route
import com.kabbi.driver.events.StopTimer
import com.kabbi.driver.events.taximeter.SummaryTaximeterEvent
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppPreferences
import com.kabbi.driver.helper.ClientTariffParser
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_DISTANCE
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_FIX
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_INTERVAL
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_MIXED
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_TIME
import com.kabbi.driver.helper.ClientTariffParser.Companion.ACCRUAL_WIFI
import com.kabbi.driver.helper.DistanceGPS
import com.kabbi.driver.models.Interval
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap
import kotlin.math.abs

class Taximeter {
    var order: Order = Order()
        private set
    var stage = 0
        private set

    var timeWaitCityClient: Long = 0
    var timeWaitTrackClient: Long = 0

    private var timeFreeInOrder: Long = 0
    private var timeWaitCityInOrder: Long = 0
    private var timeWaitCityOutOrder: Long = 0
    private var timeWaitTrackInOrder: Long = 0
    private var timeWaitTrackOutOrder: Long = 0
    private var timeAll: Long = 0
    private var timeCityInOrder: Long = 0
    private var timeTrackInOrder: Long = 0
    private var timeCityOutOrder: Long = 0
    private var timeTrackOutOrder: Long = 0
    private var disCityInOrder: Double = 0.0
    private var disTrackInOrder: Double = 0.0
    private var disCityOutOrder: Double = 0.0
    private var disTrackOutOrder: Double = 0.0
    private var priceCityOutOrder: Double = 0.0
    private var priceTrackOutOrder: Double = 0.0
    private var priceCityWaitOutOrder: Double = 0.0
    private var priceTrackWaitOutOrder: Double = 0.0
    private var isStart = false
    private var disAllInOrder: Double = 0.0
    private var priceCityInOrder: Double = 0.0
    private var priceTrackInOrder: Double = 0.0
    private var priceCityWaitInOrder: Double = 0.0
    private var priceTrackWaitInOrder: Double = 0.0
    private var priceClientWait: Double = 0.0
    private var timeWaitWifi = false
    private var waitCountCity: Long = 0
    private var waitCountTrack: Long = 0
    private var serverTime: Long = 0
    private var deltaTime: Long = 0
    private var tariffCity: ClientTariff? = null
    private var tariffTrack: ClientTariff? = null
    private var prePoint: MutableMap<String, Double> = HashMap()
    private var postPoint: Map<String, Double> = HashMap()
    private var priceIntervalInOrder: Double = 0.toDouble()
    private var preTime: Long = 0
    private var postTime: Long = 0
    private var intent: Intent? = null
    private var statusOrder: String = ""
    private var handBrake = "1"
    private var lastLocationPoint: String? = null
    private var intervalCity: MutableList<Interval>? = null
    private var intervalTrack: MutableList<Interval>? = null
    private val decimalFormat: DecimalFormat
    private val dateFormat: SimpleDateFormat
    private lateinit var context: Context

    init {
        statusOrder = AppParams.ORDER_STATUS_CONFIRM
        dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")
        decimalFormat = NumberFormat.getNumberInstance(Locale.ENGLISH) as DecimalFormat
        decimalFormat.applyPattern("###.######")
    }

    fun start(@NonNull _order: Order, context: Context, _stage: Int, _statusOrder: String) {

        this.context = context
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

        val appPreferences = AppPreferences(context)
        handBrake = appPreferences.getText("hand_brake")

        isStart = true
        statusOrder = _statusOrder
        lastLocationPoint = ""

        this.order = _order
        this.stage = _stage

        handBrake = order.handBrake

        timeWaitCityInOrder = order.timeWaitCityInOrder
        timeWaitCityOutOrder = order.timeWaitCityOutOrder
        timeWaitTrackInOrder = order.timeWaitTrackInOrder
        timeWaitTrackOutOrder = order.timeWaitTrackOutOrder
        timeAll = order.timeAll
        timeWaitCityClient = order.timeWaitCityClient
        timeWaitTrackClient = order.timeWaitTrackClient
        timeCityInOrder = order.timeCityInOrder
        timeTrackInOrder = order.timeTrackInOrder
        timeCityOutOrder = order.timeCityOutOrder
        timeTrackOutOrder = order.timeTrackOutOrder
        timeFreeInOrder = order.timeFreeInOrder

        disCityInOrder = order.disCityInOrder
        disTrackInOrder = order.disTrackInOrder
        disCityOutOrder = order.disCityOutOrder
        disTrackOutOrder = order.disTrackOutOrder
        disAllInOrder = 0.0

        priceCityInOrder = order.priceCityInOrder
        priceCityOutOrder = order.priceCityOutOrder
        priceTrackInOrder = order.priceTrackInOrder
        priceTrackOutOrder = order.priceTrackOutOrder
        priceCityWaitInOrder = order.priceCityWaitInOrder
        priceCityWaitOutOrder = order.priceCityWaitOutOrder
        priceTrackWaitInOrder = order.priceTrackWaitInOrder
        priceTrackWaitOutOrder = order.priceTrackWaitOutOrder
        priceClientWait = order.priceClientWait
        priceIntervalInOrder = order.priceIntervalInOrder

        waitCountCity = 0
        waitCountTrack = 0
        serverTime = 0
        deltaTime = 0
        preTime = 0
        postTime = 0

        prePoint = hashMapOf()
        postPoint = hashMapOf()

        intervalCity = ArrayList()
        intervalTrack = ArrayList()

        val clientTariffDao = AppDatabase.getInstance(context).clientTariffDao()

        runBlocking {

            withContext(Dispatchers.IO) {
                tariffCity = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_CITY)
                tariffTrack = clientTariffDao.getTariff(order.id, ClientTariffParser.AREA_TRACK)
            }

            if (tariffCity?.accrual == ACCRUAL_INTERVAL) {
                val jsonArrayOrder: JSONArray?
                try {
                    if (order.intervalCityJson.isEmpty()) {
                        jsonArrayOrder = JSONArray(tariffCity!!.nextKmPriceArray)
                        for (i in 0 until jsonArrayOrder.length()) {
                            intervalCity!!.add(Interval(0.0, 0.0, false))
                        }
                    } else {
                        jsonArrayOrder = JSONArray(order.intervalCityJson)
                        for (i in 0 until jsonArrayOrder.length()) {
                            val jsonItem = jsonArrayOrder.getJSONObject(i)
                            intervalCity!!.add(Interval(jsonItem.getDouble("dis"), jsonItem.getDouble("price"), jsonItem.getBoolean("full")))
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            if (tariffTrack?.accrual == ACCRUAL_INTERVAL) {
                val jsonArrayOrder: JSONArray?
                try {
                    if (order.intervalTrackJson.isEmpty()) {
                        jsonArrayOrder = JSONArray(tariffTrack!!.nextKmPriceArray)
                        for (i in 0 until jsonArrayOrder.length()) {
                            intervalTrack!!.add(Interval(0.0, 0.0, false))
                        }
                    } else {
                        jsonArrayOrder = JSONArray(order.intervalTrackJson)
                        for (i in 0 until jsonArrayOrder.length()) {
                            val jsonItem = jsonArrayOrder.getJSONObject(i)
                            intervalTrack!!.add(Interval(jsonItem.getDouble("dis"), jsonItem.getDouble("price"), jsonItem.getBoolean("full")))
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun nextStep() = CoroutineScope(Dispatchers.Default).launch {
        intent?.also { intent ->
            try {

                val speed: Double = if (timeWaitWifi) 0.0 else intent.extras!!.getDouble(AppParams.SPEED_GPS)

                val route = Route()
                try {
                    route.lat = (decimalFormat.format(intent.extras!!.getDouble(AppParams.COORDS_GPS_LAT))).toDouble()
                    route.lon = (decimalFormat.format(intent.extras!!.getDouble(AppParams.COORDS_GPS_LON))).toDouble()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                route.speed = intent.extras!!.getDouble(AppParams.SPEED_GPS)
                route.bearing = intent.extras!!.getFloat(AppParams.BEARING_GPS)
                route.datetime = Date().time
                route.parking = intent.extras!!.getString(AppParams.POLYGON_GPS) ?: ""
                route.order = order.id
                route.corn = statusOrder
                route.hdop = intent.getDoubleExtra("dop", 100.0)

                withContext(Dispatchers.IO) {
                    AppDatabase.getInstance(this@Taximeter.context).routeDao().save(route)
                }

                prePoint = HashMap()
                prePoint["lat"] = intent.extras!!.getDouble(AppParams.COORDS_GPS_LAT)
                prePoint["lon"] = intent.extras!!.getDouble(AppParams.COORDS_GPS_LON)

                preTime = Date().time

                val summaryTaximeterEvent = SummaryTaximeterEvent()

                var fixPrice = 0.0

                if (order.startPoint == "in" && tariffCity?.accrual == ACCRUAL_FIX)
                    fixPrice = order.fixCityCost - order.plantingCost

                if (order.startPoint == "out" && tariffTrack?.accrual == ACCRUAL_FIX)
                    fixPrice = order.fixOutCityCost - order.plantingCost

                try {
                    when (stage) {
                        0 -> stageZero(intent, summaryTaximeterEvent, fixPrice)

                        1 -> if (!stageOne(intent, summaryTaximeterEvent, fixPrice))
                            return@launch

                        2 -> stageTwo(intent, summaryTaximeterEvent, speed)
                    }

                    saveOrder()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                postPoint = HashMap(prePoint)
                postTime = Date().time
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private suspend fun stageZero(intent: Intent, summaryTaximeterEvent: SummaryTaximeterEvent, fixPrice: Double) {
        if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "base" && postPoint.isNotEmpty() || order.onlyCity && postPoint.isNotEmpty()) {
            if (intent.extras!!.getDouble(AppParams.SPEED_GPS) > 0)
                disCityOutOrder += DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0, postPoint["lon"] ?: 0)

            if (intent.extras!!.getDouble(AppParams.SPEED_GPS) >= tariffCity!!.speedDowntime) {
                timeCityOutOrder += AppParams.UNIT_TIME

            }
        } else if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "track" && postPoint.isNotEmpty()) {
            if (intent.extras!!.getDouble(AppParams.SPEED_GPS) > 0)
                disTrackOutOrder += DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0, postPoint["lon"] ?: 0)

            timeTrackOutOrder += AppParams.UNIT_TIME

            if (order.startPoint == "out")
                priceTrackOutOrder = disTrackOutOrder * tariffTrack!!.supplyPrice
        }

        summaryTaximeterEvent.timeWaitClient = if (timeWaitCityClient > 0) timeWaitCityClient else 0 + if (timeWaitTrackClient > 0) timeWaitTrackClient else 0
        summaryTaximeterEvent.area = intent.extras!!.getString(AppParams.POLYGON_GPS)!!
        summaryTaximeterEvent.order = order
        summaryTaximeterEvent.stage = stage

        if (order.fix == 1) {
            summaryTaximeterEvent.summaryCost = order.costOrder
        } else {
            summaryTaximeterEvent.summaryCost = order.plantingCost + order.dopCost + fixPrice + priceCityOutOrder + priceTrackOutOrder
        }
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(summaryTaximeterEvent)
        }
    }

    private suspend fun stageOne(intent: Intent, summaryTaximeterEvent: SummaryTaximeterEvent, fixPrice: Double): Boolean {
        val currentPhoneTime = Date().time / 1000
        val currentServerTime = serverTime / 1000

        if (abs(currentServerTime - currentPhoneTime) > 30) {

            deltaTime = Date().time - serverTime * 1000

            if (order.pasName == AppParams.ORDER_LABEL_RESERVE && Date().time - deltaTime < dateFormat.parse(order.orderTime).time) {
                return false
            }
        } else {
            if (order.pasName == AppParams.ORDER_LABEL_RESERVE && Date().time < dateFormat.parse(order.orderTime).time) {
                return false
            }
        }

        if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "base" || order.onlyCity) {
            timeWaitCityClient += AppParams.UNIT_TIME
            if (timeWaitCityClient > 0)
                priceClientWait = timeWaitCityClient / 60000.0 * tariffCity!!.waitPrice

        } else if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "track") {
            timeWaitTrackClient += AppParams.UNIT_TIME
            if (timeWaitTrackClient > 0)
                priceClientWait = timeWaitTrackClient / 60000.0 * tariffTrack!!.waitPrice
        }

        summaryTaximeterEvent.timeWaitClient = (if (timeWaitCityClient > 0) timeWaitCityClient else 0) + if (timeWaitTrackClient > 0) timeWaitTrackClient else 0
        summaryTaximeterEvent.area = intent.extras!!.getString(AppParams.POLYGON_GPS)!!
        summaryTaximeterEvent.order = order
        summaryTaximeterEvent.stage = stage

        if (order.fix == 1) {
            summaryTaximeterEvent.summaryCost = order.costOrder
        } else {
            summaryTaximeterEvent.summaryCost = order.plantingCost + priceClientWait + order.dopCost + fixPrice + priceCityOutOrder + priceTrackOutOrder
        }
        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(summaryTaximeterEvent)
        }
        return true
    }

    private suspend fun stageTwo(intent: Intent, summaryTaximeterEvent: SummaryTaximeterEvent, speed: Double) {
        if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "base" && postPoint.isNotEmpty() || order.onlyCity && postPoint.isNotEmpty())
            basePolygon(speed)
        else if (intent.extras!!.getString(AppParams.POLYGON_GPS) == "track" && postPoint.isNotEmpty())
            trackPolygon(speed)
        else {
            if (lastLocationPoint == "base" || order.onlyCity) {
                timeCityInOrder += AppParams.UNIT_TIME
            } else if (lastLocationPoint == "track") {
                timeTrackInOrder += AppParams.UNIT_TIME
            }
        }

        summaryTaximeterEvent.area = intent.extras!!.getString(AppParams.POLYGON_GPS)!!
        summaryTaximeterEvent.timeWaitClient = (if (timeWaitCityClient > 0) timeWaitCityClient else 0) + if (timeWaitTrackClient > 0) timeWaitTrackClient else 0
        summaryTaximeterEvent.order = order
        summaryTaximeterEvent.stage = stage

        if (order.fix == 1) {
            summaryTaximeterEvent.summaryCost = order.costOrder
        } else {
            if (postPoint.isNotEmpty()) {
                summaryTaximeterEvent.summaryCost = order.plantingCost +
                        priceCityInOrder +
                        priceTrackInOrder +
                        priceCityWaitInOrder +
                        priceTrackWaitInOrder +
                        priceClientWait +
                        order.dopCost +
                        priceIntervalInOrder +
                        priceCityOutOrder +
                        priceTrackOutOrder
            }
        }

        withContext(Dispatchers.Main) {
            EventBus.getDefault().post(summaryTaximeterEvent)
        }
    }

    private fun basePolygon(speed: Double) {
        lastLocationPoint = "base"
        if (speed > 0)
            disCityInOrder += DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0, postPoint["lon"] ?: 0)

        var priceCityInOrderDis: Double
        var priceCityInOrderTime: Double

        if (speed > tariffCity!!.speedDowntime || handBrake == "0" && !timeWaitWifi) {
            timeCityInOrder += AppParams.UNIT_TIME

            if (tariffCity!!.accrual == ACCRUAL_DISTANCE) {
                if (disCityInOrder > 0)
                    priceCityInOrder = disCityInOrder * tariffCity!!.nextKmPrice
            } else if (tariffCity!!.accrual == ACCRUAL_INTERVAL) {
                disAllInOrder = (if (disCityInOrder > 0) disCityInOrder else 0.0) + if (disTrackInOrder > 0) disTrackInOrder else 0.0

                if (disCityInOrder > 0) {
                    priceIntervalInOrder = getPriceInterval(tariffCity!!.nextKmPriceArray, DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0.0, postPoint["lon"]
                            ?: 0.0), disAllInOrder, "city")
                }
            } else if (tariffCity!!.accrual == ACCRUAL_TIME) {
                if (timeCityInOrder > 0) {
                    priceCityInOrder = when (tariffCity!!.nextCostUnit) {
                        ClientTariffParser.COST_UNIT_30 -> (timeCityInOrder / 1800000 + 1) * tariffCity!!.nextTimePrice
                        ClientTariffParser.COST_UNIT_60 -> (timeCityInOrder / 3600000 + 1) * tariffCity!!.nextTimePrice
                        else -> timeCityInOrder / 60000.0 * tariffCity!!.nextTimePrice
                    }
                }
            } else if (tariffCity!!.accrual == ACCRUAL_FIX) {
                priceCityInOrder = if (order.startPoint == "in")
                    order.fixCityCost - order.plantingCost
                else
                    order.fixCityCost
            } else if (tariffCity!!.accrual == ACCRUAL_MIXED) {

                priceCityInOrderTime = 0.0
                priceCityInOrderDis = 0.0

                if (disCityInOrder > 0)
                    priceCityInOrderDis = disCityInOrder * tariffCity!!.nextKmPrice

                if (timeCityInOrder > 0) {
                    priceCityInOrderTime = when (tariffCity!!.nextCostUnit) {
                        ClientTariffParser.COST_UNIT_30 -> (timeCityInOrder / 1800000 + 1) * tariffCity!!.nextTimePrice
                        ClientTariffParser.COST_UNIT_60 -> (timeCityInOrder / 3600000 + 1) * tariffCity!!.nextTimePrice
                        else -> timeCityInOrder / 60000.0 * tariffCity!!.nextTimePrice
                    }
                }

                priceCityInOrder = priceCityInOrderTime + priceCityInOrderDis
            }

            waitCountCity = 0
        } else {
            if (tariffCity!!.accrual == ACCRUAL_TIME) {
                if (timeWaitWifi) {
                    waitCountCity += AppParams.UNIT_TIME
                    if (waitCountCity > tariffCity!!.waitDrivingTime * 1000) {
                        timeWaitCityInOrder += 1000
                        priceCityWaitInOrder = timeWaitCityInOrder / 60000.0 * tariffCity!!.waitPrice
                    } else {
                        timeFreeInOrder += AppParams.UNIT_TIME
                    }
                } else {
                    timeCityInOrder += AppParams.UNIT_TIME

                    if (timeCityInOrder > 0) {
                        priceCityInOrder = when (tariffCity!!.nextCostUnit) {
                            ClientTariffParser.COST_UNIT_30 -> (timeCityInOrder / 1800000 + 1) * tariffCity!!.nextTimePrice
                            ClientTariffParser.COST_UNIT_60 -> (timeCityInOrder / 3600000 + 1) * tariffCity!!.nextTimePrice
                            else -> timeCityInOrder / 60000.0 * tariffCity!!.nextTimePrice
                        }
                    }
                }

            } else if (tariffCity!!.accrual == ACCRUAL_MIXED) {
                timeCityInOrder += AppParams.UNIT_TIME

                priceCityInOrderTime = 0.0
                priceCityInOrderDis = 0.0

                if (disCityInOrder > 0)
                    priceCityInOrderDis = disCityInOrder * tariffCity!!.nextKmPrice

                if (timeCityInOrder > 0) {
                    priceCityInOrderTime = when (tariffCity!!.nextCostUnit) {
                        ClientTariffParser.COST_UNIT_30 -> (timeCityInOrder / 1800000 + 1) * tariffCity!!.nextTimePrice
                        ClientTariffParser.COST_UNIT_60 -> (timeCityInOrder / 3600000 + 1) * tariffCity!!.nextTimePrice
                        else -> timeCityInOrder / 60000.0 * tariffCity!!.nextTimePrice
                    }
                }

                priceCityInOrder = priceCityInOrderTime + priceCityInOrderDis

            } else if (tariffCity!!.accrual == ACCRUAL_WIFI) {
                if (timeWaitWifi) {
                    waitCountCity += AppParams.UNIT_TIME
                    if (waitCountCity > tariffCity!!.waitDrivingTime * 1000) {
                        timeWaitCityInOrder += 1000
                        priceCityWaitInOrder = timeWaitCityInOrder / 60000.0 * tariffCity!!.waitPrice
                    } else {
                        timeFreeInOrder += AppParams.UNIT_TIME
                    }
                } else {
                    waitCountCity = 0
                    timeCityInOrder += AppParams.UNIT_TIME
                }
            } else if (tariffCity!!.accrual == ACCRUAL_FIX) {
                if (timeWaitWifi) {
                    waitCountCity += AppParams.UNIT_TIME
                    if (waitCountCity > tariffCity!!.waitDrivingTime * 1000) {
                        timeWaitCityInOrder += 1000
                        priceCityWaitInOrder = timeWaitCityInOrder / 60000.0 * tariffCity!!.waitPrice
                    } else {
                        timeFreeInOrder += AppParams.UNIT_TIME
                    }
                } else {
                    timeCityInOrder += AppParams.UNIT_TIME

                    priceCityInOrder = if (order.startPoint == "in") {
                        order.fixCityCost - order.plantingCost
                    } else {
                        order.fixCityCost
                    }
                }
            } else {
                if (timeWaitWifi) {
                    waitCountCity += AppParams.UNIT_TIME
                    if (waitCountCity > tariffCity!!.waitDrivingTime * 1000) {
                        timeWaitCityInOrder += 1000
                        priceCityWaitInOrder = timeWaitCityInOrder / 60000.0 * tariffCity!!.waitPrice
                    } else {
                        timeFreeInOrder += AppParams.UNIT_TIME
                    }
                } else {
                    waitCountCity += AppParams.UNIT_TIME
                    if (waitCountCity > tariffCity!!.waitDrivingTime * 1000) {
                        timeWaitCityInOrder += 1000
                        priceCityWaitInOrder = timeWaitCityInOrder / 60000.0 * tariffCity!!.waitPrice
                    } else {
                        timeFreeInOrder += AppParams.UNIT_TIME
                    }
                }
            }
        }

    }

    private fun trackPolygon(speed: Double) {
        lastLocationPoint = "track"
        if (speed > 0)
            disTrackInOrder += DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0, postPoint["lon"] ?: 0)

        var priceTrackInOrderDis: Double
        var priceTrackInOrderTime: Double

        if (speed > tariffTrack!!.speedDowntime || handBrake == "0" && !timeWaitWifi) {

            timeTrackInOrder += AppParams.UNIT_TIME

            when (tariffTrack!!.accrual) {

                ACCRUAL_DISTANCE -> if (disTrackInOrder > 0) priceTrackInOrder = disTrackInOrder * tariffTrack!!.nextKmPrice

                ACCRUAL_FIX -> priceTrackInOrder = if (order.startPoint == "out") order.fixOutCityCost - order.plantingCost else order.fixOutCityCost

                ACCRUAL_INTERVAL -> {
                    disAllInOrder = (if (disCityInOrder > 0) disCityInOrder else 0.0)
                    disAllInOrder += (if (disTrackInOrder > 0) disTrackInOrder else 0.0)

                    if (disTrackInOrder > 0) {
                        priceIntervalInOrder = getPriceInterval(tariffTrack!!.nextKmPriceArray, DistanceGPS.getDistance(prePoint["lat"]!!, prePoint["lon"]!!, postPoint["lat"] ?: 0.0, postPoint["lon"]
                                ?: 0.0), disAllInOrder, "track")
                    }
                }

                ACCRUAL_TIME -> {
                    if (timeTrackInOrder > 0) {
                        priceTrackInOrder = when (tariffTrack!!.nextCostUnit) {
                            ClientTariffParser.COST_UNIT_30 -> (timeTrackInOrder / 1800000 + 1) * tariffTrack!!.nextTimePrice
                            ClientTariffParser.COST_UNIT_60 -> (timeTrackInOrder / 3600000 + 1) * tariffTrack!!.nextTimePrice
                            else -> timeTrackInOrder / 60000.0 * tariffTrack!!.nextTimePrice
                        }
                    }
                }

                ACCRUAL_MIXED -> {

                    priceTrackInOrderTime = 0.0
                    priceTrackInOrderDis = 0.0

                    if (disTrackInOrder > 0)
                        priceTrackInOrderDis = disTrackInOrder * tariffTrack!!.nextKmPrice

                    if (timeTrackInOrder > 0) {
                        priceTrackInOrderTime = when (tariffTrack!!.nextCostUnit) {
                            ClientTariffParser.COST_UNIT_30 -> (timeTrackInOrder / 1800000 + 1) * tariffTrack!!.nextTimePrice
                            ClientTariffParser.COST_UNIT_60 -> (timeTrackInOrder / 3600000 + 1) * tariffTrack!!.nextTimePrice
                            else -> timeTrackInOrder / 60000.0 * tariffTrack!!.nextTimePrice
                        }
                    }

                    priceTrackInOrder = priceTrackInOrderTime + priceTrackInOrderDis
                }
            }

            waitCountTrack = 0
        } else {

            when (tariffTrack!!.accrual) {

                ACCRUAL_TIME -> {
                    if (timeWaitWifi) {
                        waitCountTrack += AppParams.UNIT_TIME
                        if (waitCountTrack > tariffTrack!!.waitDrivingTime * 1000) {
                            timeWaitTrackInOrder += AppParams.UNIT_TIME
                            priceTrackWaitInOrder = timeWaitTrackInOrder / 60000.0 * tariffTrack!!.waitPrice
                        } else {
                            timeFreeInOrder += AppParams.UNIT_TIME
                        }
                    } else {
                        timeTrackInOrder += AppParams.UNIT_TIME
                        if (timeTrackInOrder > 0) {
                            priceTrackInOrder = when (tariffTrack!!.nextCostUnit) {
                                ClientTariffParser.COST_UNIT_30 -> (timeTrackInOrder / 1800000 + 1) * tariffTrack!!.nextTimePrice
                                ClientTariffParser.COST_UNIT_60 -> (timeTrackInOrder / 3600000 + 1) * tariffTrack!!.nextTimePrice
                                else -> timeTrackInOrder / 60000.0 * tariffTrack!!.nextTimePrice
                            }
                        }
                    }
                }

                ACCRUAL_MIXED -> {
                    timeTrackInOrder += AppParams.UNIT_TIME

                    priceTrackInOrderTime = 0.0
                    priceTrackInOrderDis = 0.0

                    if (disTrackInOrder > 0)
                        priceTrackInOrderDis = disTrackInOrder * tariffTrack!!.nextKmPrice

                    if (timeTrackInOrder > 0) {
                        priceTrackInOrderTime = when (tariffTrack!!.nextCostUnit) {
                            ClientTariffParser.COST_UNIT_30 -> (timeTrackInOrder / 1800000 + 1) * tariffTrack!!.nextTimePrice
                            ClientTariffParser.COST_UNIT_60 -> (timeTrackInOrder / 3600000 + 1) * tariffTrack!!.nextTimePrice
                            else -> timeTrackInOrder / 60000.0 * tariffTrack!!.nextTimePrice
                        }
                    }
                    priceTrackInOrder = priceTrackInOrderTime + priceTrackInOrderDis
                }

                ACCRUAL_WIFI -> {
                    if (timeWaitWifi) {
                        waitCountTrack += AppParams.UNIT_TIME
                        if (waitCountTrack > tariffTrack!!.waitDrivingTime * 1000) {
                            timeWaitTrackInOrder += AppParams.UNIT_TIME
                            priceTrackWaitInOrder = timeWaitTrackInOrder / 60000.0 * tariffTrack!!.waitPrice
                        } else {
                            timeFreeInOrder += AppParams.UNIT_TIME
                        }
                    } else {
                        waitCountTrack = 0
                        timeTrackInOrder += AppParams.UNIT_TIME
                    }
                }

                ACCRUAL_FIX -> {
                    if (timeWaitWifi) {
                        waitCountTrack += AppParams.UNIT_TIME
                        if (waitCountTrack > tariffTrack!!.waitDrivingTime * 1000) {
                            timeWaitTrackInOrder += AppParams.UNIT_TIME
                            priceTrackWaitInOrder = timeWaitTrackInOrder / 60000.0 * tariffTrack!!.waitPrice
                        } else {
                            timeFreeInOrder += AppParams.UNIT_TIME
                        }
                    } else {
                        timeTrackInOrder += AppParams.UNIT_TIME

                        priceTrackInOrder = if (order.startPoint == "out")
                            order.fixOutCityCost - order.plantingCost
                        else
                            order.fixOutCityCost
                    }
                }

                else -> {
                    if (timeWaitWifi) {
                        waitCountTrack += AppParams.UNIT_TIME
                        if (waitCountTrack > tariffTrack!!.waitDrivingTime * 1000) {
                            timeWaitTrackInOrder += AppParams.UNIT_TIME
                            priceTrackWaitInOrder = timeWaitTrackInOrder / 60000.0 * tariffTrack!!.waitPrice
                        } else {
                            timeFreeInOrder += AppParams.UNIT_TIME
                        }
                    } else {
                        waitCountTrack += AppParams.UNIT_TIME
                        if (waitCountTrack > tariffTrack!!.waitDrivingTime * 1000) {
                            timeWaitTrackInOrder += AppParams.UNIT_TIME
                            priceTrackWaitInOrder = timeWaitTrackInOrder / 60000.0 * tariffTrack!!.waitPrice
                        } else {
                            timeFreeInOrder += AppParams.UNIT_TIME
                        }
                    }
                }
            }
        }
    }

    @Subscribe
    fun onEvent(stopTimer: StopTimer) {
        EventBus.getDefault().unregister(this)
    }

    fun stop(statusOrder: String) {
        isStart = false
        if (statusOrder != "close") {
            this.statusOrder = statusOrder
            saveOrder()
        }
    }

    fun setPosition(intent: Intent) {
        this.intent = intent
    }

    fun setStage(stage: Int, statusOrder: String) {
        this.stage = stage
        this.statusOrder = statusOrder
        saveOrder()
    }

    fun timeWaitStart() {
        timeWaitWifi = true
    }

    fun timeWaitStop() {
        timeWaitWifi = false
    }

    private fun saveOrder() {
        order.timeWaitCityInOrder = timeWaitCityInOrder
        order.timeWaitCityOutOrder = timeWaitCityOutOrder
        order.timeWaitTrackInOrder = timeWaitTrackInOrder
        order.timeWaitTrackOutOrder = timeWaitTrackOutOrder
        order.timeAll = timeAll
        order.timeWaitCityClient = timeWaitCityClient
        order.timeWaitTrackClient = timeWaitTrackClient
        order.timeCityInOrder = timeCityInOrder
        order.timeTrackInOrder = timeTrackInOrder
        order.timeCityOutOrder = timeCityOutOrder
        order.timeTrackOutOrder = timeTrackOutOrder
        order.timeFreeInOrder = timeFreeInOrder

        order.disCityInOrder = disCityInOrder
        order.disTrackInOrder = disTrackInOrder
        order.disCityOutOrder = disCityOutOrder
        order.disTrackOutOrder = disTrackOutOrder

        order.priceCityInOrder = priceCityInOrder
        order.priceCityOutOrder = priceCityOutOrder
        order.priceTrackInOrder = priceTrackInOrder
        order.priceTrackOutOrder = priceTrackOutOrder
        order.priceCityWaitInOrder = priceCityWaitInOrder
        order.priceCityWaitOutOrder = priceCityWaitOutOrder
        order.priceTrackWaitInOrder = priceTrackWaitInOrder
        order.priceTrackWaitOutOrder = priceTrackWaitOutOrder
        order.priceClientWait = priceClientWait
        order.priceIntervalInOrder = priceIntervalInOrder
        order.stage = stage

        val intervalCityJsonArray = JSONArray()
        val intervalTrackJsonArray = JSONArray()
        for (interval in intervalCity!!) {
            val jsonItem = JSONObject()
            try {
                jsonItem.put("dis", interval.dis)
                jsonItem.put("price", interval.price)
                jsonItem.put("full", interval.isFull)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            intervalCityJsonArray.put(jsonItem)
        }

        for (interval in intervalTrack!!) {
            val jsonItem = JSONObject()
            try {
                jsonItem.put("dis", interval.dis)
                jsonItem.put("price", interval.price)
                jsonItem.put("full", interval.isFull)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            intervalTrackJsonArray.put(jsonItem)
        }
        order.intervalCityJson = intervalCityJsonArray.toString()
        order.intervalTrackJson = intervalTrackJsonArray.toString()
        runBlocking {
            withContext(Dispatchers.IO) {
                AppDatabase.getInstance(this@Taximeter.context).orderDao().save(order)
            }
        }
    }

    private fun getPriceInterval(tariff: String, intervalDis: Double, allDis: Double, type: String): Double {
        var price1 = 0.0
        var price2 = 0.0
        if (allDis <= 0) return price1 + price2

        try {
            if (type == "city") {
                for (i in intervalCity!!.indices) {
                    val interval = intervalCity!![i]
                    if (interval.isFull) {
                        continue
                    }
                    val jsonArrayTariff = JSONArray(tariff)
                    if (jsonArrayTariff.getJSONObject(i).getJSONArray("interval").length() > 1) {
                        val interval2 = jsonArrayTariff.getJSONObject(i).getJSONArray("interval").getDouble(1)
                        if (allDis > interval2) {
                            interval.dis = interval.dis
                            interval.isFull = true
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalCity!![i] = interval
                        } else {
                            interval.dis = interval.dis + intervalDis
                            interval.isFull = false
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalCity!![i] = interval
                            break
                        }
                    } else {
                        val interval1 = jsonArrayTariff.getJSONObject(i).getJSONArray("interval").getDouble(0)
                        if (allDis > interval1) {
                            interval.dis = interval.dis + intervalDis
                            interval.isFull = false
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalCity!![i] = interval
                        }
                        break
                    }
                }

            } else {
                for (i in intervalTrack!!.indices) {
                    val interval = intervalTrack!![i]
                    if (interval.isFull) {
                        continue
                    }
                    val jsonArrayTariff = JSONArray(tariff)
                    if (jsonArrayTariff.getJSONObject(i).getJSONArray("interval").length() > 1) {
                        val interval2 = jsonArrayTariff.getJSONObject(i).getJSONArray("interval").getDouble(1)
                        if (allDis > interval2) {
                            interval.dis = interval.dis
                            interval.isFull = true
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalTrack!![i] = interval
                        } else {
                            interval.dis = interval.dis + intervalDis
                            interval.isFull = false
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalTrack!![i] = interval
                            break
                        }
                    } else {
                        val interval1 = jsonArrayTariff.getJSONObject(i).getJSONArray("interval").getDouble(0)
                        if (allDis > interval1) {
                            interval.dis = interval.dis + intervalDis
                            interval.isFull = false
                            interval.price = interval.dis * jsonArrayTariff.getJSONObject(i).getDouble("price")
                            intervalTrack!![i] = interval
                        }
                        break
                    }
                }
            }

            for (interval in intervalCity!!) {
                price1 += interval.price
            }

            for (interval in intervalTrack!!) {
                price2 += interval.price
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return price1 + price2
    }

    fun setServerTime(server_time: Long) {
        this.serverTime = server_time
    }

    companion object {
        private val instance = Taximeter()

        fun getInstance(): Taximeter {
            return instance
        }
    }
}
