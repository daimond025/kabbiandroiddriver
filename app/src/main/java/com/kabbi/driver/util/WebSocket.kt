package com.kabbi.driver.util

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.kabbi.driver.R
import com.kabbi.driver.database.entities.Driver
import com.kabbi.driver.database.entities.Service
import com.kabbi.driver.events.orderStatus.OrderStatus
import com.kabbi.driver.events.taximeter.CostChange
import com.kabbi.driver.helper.AppParams
import com.kabbi.driver.helper.AppParams.BROADCAST_PREORDER
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import io.socket.engineio.client.transports.WebSocket
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URISyntaxException
import java.text.SimpleDateFormat
import java.util.*

class WebSocket(private val context: Context, private val callback: DriverChatInterface, private val driver: Driver, private val service: Service) {

    private var mSocket: Socket? = null

    val isConnected: Boolean
        get() = mSocket!!.connected()

    fun connect() {

        try {
            val opts = IO.Options()
            opts.forceNew = true
            opts.reconnection = true
            opts.reconnectionDelay = 1000
            opts.reconnectionAttempts = Integer.MAX_VALUE
            opts.transports = arrayOf(WebSocket.NAME)
            mSocket = IO.socket(AppParams.CHAT_SERVER, opts)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

        mSocket!!.on("send_client_data", onSendClientData)
        mSocket!!.on("new_message", onNewMessage)
        mSocket!!.on("unreaded_message", onUnreadMessage)
        mSocket!!.on("last_messages", onLastMessages)
        mSocket!!.on("offer_order", onNewOrder)
        mSocket!!.on("update_order", onUpdateOrder)
        mSocket!!.on("reject_order", onRejectOrder)
        mSocket!!.on("reject_worker_from_order", onRejectDriverFromOrder)
        mSocket!!.on("response", onResponse)
        mSocket!!.on("order_event", onOrderEvent)
        mSocket!!.on("photo_control_event", onPhotocontrolEvent)

        mSocket!!.connect()
    }

    private val onSendClientData = Emitter.Listener {
        try {
            val fullName = driver.name!!.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val last = fullName[0]
            var name = ""
            var middle = ""
            if (fullName.size == 3) {
                name = fullName[1]
                middle = fullName[2]
            }

            mSocket!!.emit("save_client_data", JSONObject()
                    .put("client_type", "worker")
                    .put("client_id", driver.callSign)
                    .put("client_role", context.getString(R.string.chat_driver))
                    .put("client_f", last)
                    .put("client_i", name)
                    .put("client_o", middle)
                    .put("city_arr", JSONArray().put(Integer.valueOf(driver.cityID)))
                    .put("tenant_login", service.uri)
                    .put("socket_id", mSocket!!.id())
                    .put("device", "android")
                    .put("device_info", "android " + Build.VERSION.RELEASE + "; " + Build.MANUFACTURER + " " + Build.MODEL))
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    private val onNewOrder = Emitter.Listener { args ->
        if (args[0] != null) {
            try {
                val jsonObject = JSONObject(args[0].toString())
                callback.onNewOrder(jsonObject.getString("order_id"), jsonObject.getString("callsign"), jsonObject.getString("tenant_login"), 0, "", IntArray(0))

            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private val onUpdateOrder = Emitter.Listener { args ->
        if (args[0] != null) {
            try {
                val jsonObject = JSONObject(args[0].toString())
                callback.onUpdateOrder(jsonObject.getString("order_id"), jsonObject.getString("callsign"), jsonObject.getString("tenant_login"), "")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private val onRejectOrder = Emitter.Listener { args ->
        if (args[0] != null) {
            try {
                Log.d("WebSocket: onRejectOrd", args[0].toString())
                val jsonObject = JSONObject(args[0].toString())
                callback.onRejectOrder(jsonObject.getString("order_id"), jsonObject.getString("callsign"), jsonObject.getString("tenant_login"), "")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private val onRejectDriverFromOrder = Emitter.Listener { args ->
        if (args[0] != null) {

            try {
                Log.d("WebSocket: onRejectDr..", args[0].toString())
                val jsonObject = JSONObject(args[0].toString())
                callback.onRejectDriverFromOrder(jsonObject.getString("order_id"), jsonObject.getString("callsign"), jsonObject.getString("tenant_login"), "")
            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: NullPointerException) {
                e.printStackTrace()
            }
        }
    }

    private val onResponse = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        try {
            Log.d("WebSocket: onResponse", data.toString())

            // todo - сообщение дальше не обрабатывается !!!!
           /* if (data.has("code")) {
                if (data.getString("code") == "125" || data.getInt("code") == 100) {

                    val intent = Intent(BROADCAST_PREORDER)

                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                    return@Listener
                }
            }*/
            var jsonObject = JSONObject()
            try {
                val result = data.getJSONObject("result")
                if (result.has("cost_data"))
                    jsonObject = result.getJSONObject("cost_data")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            callback.onResponse(data.getString("uuid"), data.getString("info"), data.getJSONObject("result").getInt("set_result"), jsonObject)
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private val onPhotocontrolEvent = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        try {
            val uuid = data.getString("uuid")
            emitUuid(uuid)
            callback.onPhotocontrolEvent(uuid)

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    private val onOrderEvent = Emitter.Listener { args ->
        val data = args[0] as JSONObject

        Log.d("WebSocket: onOrderEvent", data.toString())

        try {
            when (data.getString("command")) {

                "offer_order" -> {
                    val params = data.getJSONObject("params")
                    val arrivalString = params.getString("worker_arrival")
                    var arrival = IntArray(0)
                    if (arrivalString != null && arrivalString.length > 2) {
                        val split = arrivalString.substring(1, arrivalString.length - 1).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                        arrival = IntArray(split.size)
                        for (i in split.indices) {
                            arrival[i] = Integer.valueOf(split[i])
                        }
                    }
                    callback.onNewOrder(
                            params.getString("order_id"),
                            data.getString("worker_callsign"),
                            data.getString("tenant_login"),
                            params.getInt("offer_sec"),
                            data.getString("uuid"),
                            arrival)
                }

                "reject_order" -> callback.onRejectOrder(data.getJSONObject("params").getString("order_id"),
                        data.getString("worker_callsign"),
                        data.getString("tenant_login"),
                        data.getString("uuid"))

                "update_order" -> callback.onUpdateOrder(data.getJSONObject("params").getString("order_id"),
                        data.getString("worker_callsign"),
                        data.getString("tenant_login"),
                        data.getString("uuid"))

                "reject_worker_from_order" -> callback.onRejectDriverFromOrder(data.getJSONObject("params").getString("order_id"),
                        data.getString("worker_callsign"),
                        data.getString("tenant_login"),
                        data.getString("uuid"))

                "order_list_add_item" -> callback.onOrderListAddItem(
                        data.getJSONObject("params").getString("type"),
                        data.getJSONObject("params").getString("order_id"),
                        data.getString("uuid"))

                "order_list_del_item" -> callback.onOrderListDelItem(
                        data.getJSONObject("params").getString("type"),
                        data.getJSONObject("params").getString("order_id"),
                        data.getString("uuid"))

                "complete_order" -> callback.onCompleteOrder(data.getJSONObject("params").getString("order_id"),
                        data.getString("worker_callsign"),
                        data.getString("tenant_login"),
                        data.getString("uuid"))

                "confirm_preorder" -> callback.onConfirmPreOrder(data.getJSONObject("params").getString("order_id"),
                        data.getString("worker_callsign"),
                        data.getString("tenant_login"),
                        data.getJSONObject("params").getInt("confirm_sec"),
                        data.getString("uuid"))

                "pre_order_offer" -> {
                    emitUuid(data.getString("uuid"))
                    callback.onPreOrder(data.getJSONObject("params").getString("order_id"),
                            data.getString("worker_callsign"),
                            data.getString("tenant_login"),
                            60,
                            data.getString("uuid"))
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        val data = args[0] as JSONObject
        try {
            if (data.getString("sender_id") == driver.callSign) return@Listener
            callback.onNewMessage(
                    data.getString("sender_f") + " " + data.getString("sender_i"),
                    data.getString("time"),
                    data.getString("sender_role"),
                    data.getString("message"),
                    data.getString("receiver_type"),
                    true)
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    private val onUnreadMessage = Emitter.Listener { args ->
        val jsonArray = args[0] as JSONArray

        val messages = ArrayList<HashMap<String, String>>()
        if (jsonArray.length() > 0) {
            for (j in jsonArray.length() - 1 downTo 0) {
                try {
                    val data = jsonArray.getJSONObject(j)
                    if (data.getString("receiver_type") != TYPECHAT_DRIVER)
                        break
                    val message = HashMap<String, String>()
                    message["user"] = data.getString("sender_time") + " " + data.getString("sender_f") + " " + data.getString("sender_i") + " (" + data.getString("sender_role") + ")"
                    message["message"] = data.getString("message")
                    message["timestamp"] = data.getString("timestamp")
                    message["sender_id"] = data.getString("sender_id")
                    messages.add(message)
                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }
            }
        }
        callback.onUnreadMessage(messages)
    }

    private val onLastMessages = Emitter.Listener { args ->
        val jsonArray = args[0] as JSONArray

        val messages = ArrayList<HashMap<String, String>>()

        var receiverType = TYPECHAT_DRIVER
        if (jsonArray.length() > 0) {

            for (j in jsonArray.length() - 1 downTo 0) {
                try {
                    val data = jsonArray.getJSONObject(j)

                    val message = HashMap<String, String>()
                    message["user"] = data.getString("sender_time") + " " + data.getString("sender_f") + " " + data.getString("sender_i") + " (" + data.getString("sender_role") + ")"
                    message["sender_time"] = data.getString("sender_time")
                    message["message"] = data.getString("message")
                    message["timestamp"] = data.getString("timestamp")
                    message["receiver_type"] = data.getString("receiver_type")
                    message["sender_id"] = data.getString("sender_id")
                    messages.add(message)

                    receiverType = data.getString("receiver_type")
                } catch (e: JSONException) {
                    e.printStackTrace()
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }
            }
        }
        callback.onLastMessages(receiverType, messages)
    }

    fun emitNewMessage(typeChat: String, message: String) {
        mSocket?.apply {
            emit("new_message", JSONObject()
                    .put("receiver_type", typeChat)
                    .put("receiver_id", if (typeChat == TYPECHAT_DRIVER) Integer.valueOf(driver.callSign) else Integer.valueOf(driver.cityID))
                    .put("city_id", Integer.valueOf(driver.cityID))
                    .put("message", message)
                    .put("message_time", SimpleDateFormat("dd.MM HH:mm").format(Date())))
        }
    }

    fun emitOrderStatus(status: OrderStatus) {
        mSocket?.apply {

            val string = Gson().toJson(status)
            Log.d("EmitOrderStatus", string)

            emit("set_order_status", JSONObject(string))
        }
    }

    fun emitCostChange(costChange: CostChange) {
        mSocket?.apply {
            emit("set_order_status", JSONObject(Gson().toJson(costChange)))
        }
    }

    fun emitUuid(uuid: String) {
        mSocket?.apply {
            emit(uuid, JSONObject().put("result", 1))
        }
    }

    fun emitGetUnreadMessage() {
        mSocket?.apply {
            emit("get_unreaded_message", JSONObject()
                    .put("tenant_login", service.uri)
                    .put("client_type", "worker")
                    .put("client_id", Integer.valueOf(driver.callSign.trim())))
        }
    }

    fun emitMessageIsRead() {
        mSocket?.apply {
            emit("message_is_readed", JSONObject()
                    .put("tenant_login", service.uri)
                    .put("city_id", Integer.valueOf(driver.cityID))
                    .put("sender_id", null)
                    .put("sender_type", null)
                    .put("receiver_id", Integer.valueOf(driver.callSign))
                    .put("receiver_type", "worker"))
        }
    }

    fun emitGetLastMessages(typeChat: String) {
        mSocket?.apply {
            emit("get_last_messages", JSONObject()
                    .put("tenant_login", service.uri)
                    .put("receiver_id", if (typeChat == TYPECHAT_DRIVER) Integer.valueOf(driver.callSign) else Integer.valueOf(driver.cityID))
                    .put("receiver_type", typeChat)
                    .put("city_id", Integer.valueOf(driver.cityID))
                    .put("count", 10))
        }
    }

    fun emitGetHistoryMessages(typeChat: String, timestamp: Long) {
        mSocket?.apply {
            emit("get_history_messages", JSONObject()
                    .put("tenant_login", service.uri)
                    .put("receiver_id", if (typeChat == TYPECHAT_DRIVER) Integer.valueOf(driver.callSign) else Integer.valueOf(driver.cityID))
                    .put("receiver_type", typeChat)
                    .put("city_id", Integer.valueOf(driver.cityID))
                    .put("last_timestamp", timestamp - 1)
                    .put("count", 10))
        }
    }

    fun disconnect() {
        Log.d("WebSocket", "disconnect")
        mSocket!!.off("send_client_data", onSendClientData)
        mSocket!!.off("new_message", onNewMessage)
        mSocket!!.off("unreaded_message", onUnreadMessage)
        mSocket!!.off("last_messages", onLastMessages)
        mSocket!!.off("offer_order", onNewOrder)
        mSocket!!.off("update_order", onUpdateOrder)
        mSocket!!.off("reject_order", onRejectOrder)
        mSocket!!.off("reject_driver_from_order", onRejectDriverFromOrder)
        mSocket!!.off("response", onResponse)
        mSocket!!.off("photo_control_event", onPhotocontrolEvent)
        mSocket!!.off("order_event", onOrderEvent)
        mSocket!!.disconnect()
    }

    interface DriverChatInterface {
        fun onNewMessage(userName: String, time: String, role: String, message: String, typeChat: String, read: Boolean)

        fun onUnreadMessage(messages: List<HashMap<String, String>>)

        fun onLastMessages(typeChat: String, messages: List<HashMap<String, String>>)

        fun onNewOrder(orderId: String, callsign: String, tenant_login: String, secToClose: Int?, uuid: String, arrival: IntArray)

        fun onPreOrder(orderId: String, callsign: String, tenant_login: String, secToClose: Int?, uuid: String)

        fun onUpdateOrder(orderId: String, callsign: String, tenant_login: String, uuid: String)

        fun onRejectOrder(orderId: String, callsign: String, tenant_login: String, uuid: String)

        fun onRejectDriverFromOrder(orderId: String, callsign: String, tenant_login: String, uuid: String)

        fun onCompleteOrder(orderId: String, callsign: String, tenant_login: String, uuid: String)

        fun onResponse(uuid: String, infoCode: String, result: Int, jsonObject: JSONObject)

        fun onPhotocontrolEvent(uuid: String)

        fun onOrderListAddItem(type: String, order_id: String, uuid: String)

        fun onOrderListDelItem(type: String, order_id: String, uuid: String)

        fun onConfirmPreOrder(orderId: String, callsign: String, tenant_login: String, secConfirm: Int?, uuid: String)
    }

    companion object {

        const val BROADCAST_UNREAD_MESSAGE = "com.gootax.driverchat.message.unreaded"
        const val BROADCAST_UNREAD_MESSAGE_COUNT = "com.gootax.driverchat.message.unreadedcount"
        const val TYPECHAT_DRIVER = "worker"
        const val TYPECHAT_CITY = "main_city_chat"
    }

}

