package com.kabbi.driver.util;


import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class CustomToast {
    public static void showMessage(Context context, String text) {
        try {
            Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
