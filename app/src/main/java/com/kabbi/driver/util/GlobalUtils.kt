package com.kabbi.driver.util

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.annotations.SerializedName
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.google.maps.GeoApiContext
import com.google.maps.GeocodingApi
import com.google.maps.model.AddressComponentType
import com.google.maps.model.LatLng
import org.json.JSONObject


private var context: GeoApiContext = GeoApiContext.Builder()
        .apiKey("AIzaSyBgVIObHG9w4w-ZsGeA2aHsiCheZRVA7m4")
        .build()

fun getDeviceToken(): Task<InstanceIdResult> {
    return FirebaseInstanceId.getInstance().instanceId
}

fun buildAddressFromJson(json: JSONObject): AddressParsed {
    val gson = GsonBuilder().registerTypeAdapter(Long::class.java, LongTypeAdapter()).create()
    val address = gson.fromJson(json.toString(), AddressParsed::class.java)
    address.json = json
    return address
}

fun buildAddress(latLng: LatLng, json: JSONObject?): String {
    val fields: Array<String> = arrayOf("street", "housing", "house", "city")

    json?.also { _json ->
        fields.forEach { field ->
            if (_json.has(field)) {
                val address = json.getString(field).divide()
                if (address.isNotEmpty()) {
                    return address
                }
            }
        }
    }

    var address = ""

    try {
        val request = GeocodingApi.reverseGeocode(context, latLng).await()
        val components = request[0].addressComponents

        var street: String? = null
        var house: String? = null
        var postal: String? = null
        var city: String? = null

        for (component in components) {
            val types = component.types
            for (type in types) {

                when (type) {
                    AddressComponentType.STREET_NUMBER -> house = component.longName
                    AddressComponentType.ROUTE -> street = component.longName
                    AddressComponentType.POSTAL_CODE -> postal = component.longName
                    AddressComponentType.LOCALITY -> city = component.longName
                    else -> {
                    }
                }
            }
        }

        address = String.format("%s %s, %s %s", street, house, postal, city)

    } catch (e: Exception) {
        Log.e("Address", e.message ?: "")
    }
    return clean(address)
}

private fun String?.divide(): String {
    if (this.isNullOrEmpty()) return ""
    val array = this.split("|")
    return if (array.size > 1) array[1] else this
}

private fun clean(str: String): String {
    return str.replace("  ", "")
            .replace(" ,", ",")
            .replace("null", "")
}

class AddressParsed {
    var json: JSONObject? = null

    @SerializedName("city_id")
    var cityId: String? = null

    @SerializedName("confirmation_code")
    var confirmationCode: String? = null

//    @SerializedName("parking_id")
//    var parkingId: Long = 0

    var city: String? = null
    var street: String? = null
    var housing: String? = null
    var house: String? = null
    var porch: String? = null
    var apt: String? = null
    var lat: Double = 0.0
    var lon: Double = 0.0
    var comment: String? = null
    var phone: String? = null
    var parking: String? = null

    fun stringValue(): String {
        return if (!house.isNullOrBlank()) {
            street ?: buildAddress(LatLng(lat, lon), json)
        } else {
            buildAddress(LatLng(lat, lon), json)
        }
    }
}

class LongTypeAdapter : TypeAdapter<Long?>() {

    override fun read(reader: JsonReader): Long? {
        if (reader.peek() === JsonToken.NULL) {
            reader.nextNull()
            return 0
        }
        val stringValue: String = reader.nextString()
        return try {
            stringValue.toLongOrNull() ?: 0
        } catch (e: NumberFormatException) {
            0
        }
    }

    override fun write(writer: JsonWriter, value: Long?) {
        if (value == null) {
            writer.value(0)
        } else {
            writer.value(value)
        }
    }
}