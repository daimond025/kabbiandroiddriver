package com.kabbi.driver.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import android.location.GpsStatus.NmeaListener
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import android.location.OnNmeaMessageListener as OnNmeaMessageListener1

class GpsTracker(private val callback: GpsTrackerInterface, private val context: Context) : LocationListener {

    private val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val gpsStatusListener = GpsStatusListener()

    private val gnssStatusCallback = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) GnssStatusCallback() else null
    private val nmeaListener = if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) NmeaMessageListener() else null

    fun startTracking(provider: String, providerServer: String) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
            callback.onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER))
        }

        if (provider == "1" && providerServer == "1") {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0f, this)
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0f, this)
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            locationManager.addGpsStatusListener(gpsStatusListener)
            locationManager.addNmeaListener(gpsStatusListener)
        } else {
            locationManager.registerGnssStatusCallback(gnssStatusCallback)
            locationManager.addNmeaListener(nmeaListener)
        }

        var lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        lastKnownLocation = lastKnownLocation ?: locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

        if (lastKnownLocation != null) callback.onLocationChanged(lastKnownLocation)
    }

    fun stopTracking() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        locationManager.removeUpdates(this)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            locationManager.removeGpsStatusListener(gpsStatusListener)
            locationManager.removeNmeaListener(gpsStatusListener)
        } else {
            locationManager.unregisterGnssStatusCallback(gnssStatusCallback)
            locationManager.removeNmeaListener(nmeaListener)
        }
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            callback.onLocationChanged(location)
        }
    }

    private fun nmeaReceived(nmea: String?) {
        if (nmea != null) {
            val nmeaParts = nmea.split(",").toTypedArray()
            if (nmeaParts[0].equals("\$GPGSA", ignoreCase = true)) {
                try {
                    callback.onNmeaReceived(nmeaParts[16].toDouble())
                } catch (e: Exception) { //
                }
            }
        }
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}

    interface GpsTrackerInterface {
        fun onLocationChanged(location: Location?)
        fun onGpsStatusChanged(usedSatellite: Int, visibleSatellite: Int)
        fun onNmeaReceived(hdop: Double)
    }

    inner class GpsStatusListener : NmeaListener, GpsStatus.Listener, OnNmeaMessageListener1 {

        override fun onNmeaReceived(timestamp: Long, nmea: String?) {
            nmeaReceived(nmea)
        }

        override fun onGpsStatusChanged(event: Int) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return
            }
            try {
                val gpsStatus = locationManager.getGpsStatus(null)
                if (gpsStatus != null) {
                    val satellites = gpsStatus.satellites
                    val sat: Iterator<GpsSatellite> = satellites.iterator()
                    val i = 0
                    var use = 0
                    while (sat.hasNext()) {
                        val satellite = sat.next()
                        if (satellite.usedInFix()) use++
                    }
                    callback.onGpsStatusChanged(use, i)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onNmeaMessage(p0: String?, p1: Long) {

        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    inner class GnssStatusCallback : GnssStatus.Callback() {
        override fun onSatelliteStatusChanged(status: GnssStatus?) {
            super.onSatelliteStatusChanged(status)
            status?.also {mStatus ->
                val satellites = mStatus.satelliteCount
                var inUse = 0

                for(i in 0 until satellites){
                    if(mStatus.usedInFix(i)) inUse++
                }
                callback.onGpsStatusChanged(inUse, satellites)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    inner class NmeaMessageListener : OnNmeaMessageListener1 {
        override fun onNmeaMessage(message: String?, timestamp: Long) {
            nmeaReceived(message)
        }
    }

}