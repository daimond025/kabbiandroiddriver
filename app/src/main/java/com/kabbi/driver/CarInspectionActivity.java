package com.kabbi.driver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kabbi.driver.adapters.PhotoReviewAdapter;
import com.kabbi.driver.database.entities.Driver;
import com.kabbi.driver.database.entities.Service;
import com.kabbi.driver.events.ThemeEvent;
import com.kabbi.driver.helper.AppParams;
import com.kabbi.driver.helper.AppPreferences;
import com.kabbi.driver.helper.InternetConnection;
import com.kabbi.driver.models.Photo;
import com.kabbi.driver.service.MainService;
import com.kabbi.driver.util.AsyncHttpTask;
import com.kabbi.driver.util.CustomToast;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CarInspectionActivity extends BaseActivity implements AsyncHttpTask.AsyncTaskInterface {

    private static final int CAMERA_REQUEST_CODE = 100;

    private AppPreferences appPreferences;
    private AsyncHttpTask asyncHttpTask;
    private ArrayList<Photo> photoList;
    private Toolbar toolbar;
    private BroadcastReceiver broadcastReceiverLang;
    private Button passBtn, recaptureBtn;
    private ProgressBar progressBar;
    private RequestParams requestParams;
    private Driver driver;
    private Service service;

    private LinearLayout layoutMain;
    private RelativeLayout layoutSecond;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_inspection);
        appPreferences = new AppPreferences(this);
        toolbar = findViewById(R.id.toolbar_pc);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.pc_activity_label));
        setTheme(Integer.valueOf(appPreferences.getText("theme")));

        photoList = new ArrayList<>();
        driver = ((App) getApplication()).driver;
        service = ((App) getApplication()).service;
        asyncHttpTask = new AsyncHttpTask(this, getApplicationContext());

        layoutMain = findViewById(R.id.ll_main_inspection);
        layoutMain.setVisibility(View.VISIBLE);
        layoutSecond = findViewById(R.id.ll_second_inspection);
        layoutSecond.setVisibility(View.GONE);

        try {
            if (getIntent().getExtras() != null) {
                String comment = getIntent().getExtras().getString("comment");
                HashMap<String, String> photoTypes = getIntent().getExtras().getParcelable("photoTypes");
                photoList.clear();
                for (String key : photoTypes.keySet()) {
                    String value = photoTypes.get(key);
                    photoList.add(new Photo(key, value));
                    //photoList.add(new Photo(id, comment, link));
                }

                layoutMain = findViewById(R.id.ll_main_inspection);
                layoutMain.setVisibility(View.GONE);
                layoutSecond = findViewById(R.id.ll_second_inspection);
                layoutSecond.setVisibility(View.VISIBLE);

                RecyclerView recyclerView = findViewById(R.id.rv_review);
                recyclerView.setHasFixedSize(true);
                PhotoReviewAdapter adapter = new PhotoReviewAdapter(getApplicationContext(), photoList);
                recyclerView.setAdapter(adapter);

                recaptureBtn = findViewById(R.id.btn_recapture);
                recaptureBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                        intent.putExtra("types_array", photoList);
                        startActivity(intent);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        progressBar = findViewById(R.id.pbHeaderProgress);
        passBtn = findViewById(R.id.btn_pass_control);
        passBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    makeRequest();
                } else {
                    ActivityCompat.requestPermissions(CarInspectionActivity.this, new String[]{android.Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
                }
            }
        });
//        getLogger().info(getClass().getSimpleName() + " onCreate ");
    }

    private void makeRequest() {
        try {
            if (InternetConnection.isOnline(getApplicationContext())) {
                requestParams = new RequestParams();
                requestParams.add("tenant_login", service.getUri());
                requestParams.add("worker_login", driver.getCallSign());

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        asyncHttpTask.get("get_photo_types", requestParams, driver.getSecretCode());
                    }
                }, 1000);
                passBtn.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            } else {
                if (!InternetConnection.isOnline(getApplicationContext())) {
                    CustomToast.showMessage(getApplicationContext(), getApplicationContext().getString(R.string.text_internet_access));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(ThemeEvent themeEvent) {
        appPreferences = new AppPreferences(this);
        setTheme(Integer.valueOf(appPreferences.getText("theme")));
    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiverLang = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    toolbar.setTitle("  " + getString(R.string.text_menu_field_authrize3));
                    toolbar.setTitleTextColor(getResources().getColor(R.color.white));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiverLang, new IntentFilter(AppParams.BROADCAST_LANG));
    }

    @Override
    public void doPostExecute(JSONObject jsonObject, String typeJson) {
        if (jsonObject != null && typeJson.equals("get_photo_types")) {
            try {
                if (jsonObject.getString("info").equals("OK")) {
                    photoList.clear();
                    JSONArray jsonPhotoTypes = jsonObject.getJSONArray("result");
                    for (int i = 0; i < jsonPhotoTypes.length(); i++) {
                        try {
                            String key = jsonPhotoTypes.getJSONObject(i).getString("type_id");
                            String value = jsonPhotoTypes.getJSONObject(i).getString("type_name");
                            String comment = jsonPhotoTypes.getJSONObject(i).getString("link");
                            String link = jsonPhotoTypes.getJSONObject(i).getString("comment");
                            Photo photo = new Photo(key, value, comment, link);
                            Log.d("TEST","PHOTO: "+photo.toString());
                            photoList.add(photo);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                    intent.putExtra("types_array", photoList);
                    startActivity(intent);
                } else if (jsonObject.getString("info").equals("PHOTOCONTROL_IS_NOT_NEEDED")) {
                    Toast.makeText(this, getString(R.string.pc_error_non_needed), Toast.LENGTH_SHORT).show();
                    startService(new Intent(getApplicationContext(), MainService.class));
                    startActivity(new Intent(CarInspectionActivity.this, WorkShiftActivity.class));
                } else if (jsonObject.getString("info").equals("INCORRECT_FORMAT_PHOTO")) {
                    Toast.makeText(this, getString(R.string.pc_error_incorrect_format_photo), Toast.LENGTH_LONG).show();
                } else if (jsonObject.getString("info").equals("INCORRECT_TYPE_ID")) {
                    Toast.makeText(this, getString(R.string.pc_error_incorrect_type_id), Toast.LENGTH_LONG).show();
                } else if (jsonObject.getString("info").equals("LIMIT_FILE_SIZE")) {
                    Toast.makeText(this, getString(R.string.pc_error_limit_file_size), Toast.LENGTH_LONG).show();
                } else if (jsonObject.getString("info").equals("MISSING_INPUT_PARAMETER")) {
                    Toast.makeText(this, getString(R.string.pc_error_missing_parameter), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (jsonObject == null) {
            CustomToast.showMessage(getApplicationContext(), getString(R.string.pc_error_loading_types));
        }
        passBtn.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        getLogger().info(getClass().getSimpleName() + " onStop ");

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiverLang);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        toolbar.setTitle("  " + getString(R.string.pc_activity_label));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        return true;
    }

    @Override
    protected void onDestroy() {
//        getLogger().info(getClass().getSimpleName() + " onDestroy ");

        EventBus.getDefault().unregister(CarInspectionActivity.this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeRequest();
            } else {
                Toast.makeText(this, getString(R.string.pc_error_camera_permission), Toast.LENGTH_LONG).show();
            }
        }
    }
}