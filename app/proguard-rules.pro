# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/noir/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes Deprecated
-keepattributes SourceFile
-keepattributes LineNumberTable
-keepattributes *Annotation*
-keepattributes EnclosingMethod

# For SearchView
-keep class android.support.v7.widget.SearchView { *; }

# For RoboSpice
-keep,includedescriptorclasses class com.octo.android.robospice.** { *; }
-keepclassmembers class com.gootax.client.requests.** { *; }
-dontwarn com.octo.android.robospice.SpiceService
-dontwarn android.support.**
-dontwarn com.sun.xml.internal.**
-dontwarn com.sun.istack.internal.**
-dontwarn org.codehaus.jackson.**
-dontwarn org.springframework.**
-dontwarn java.awt.**
-dontwarn javax.security.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn java.util.**
-dontwarn org.w3c.dom.**
-dontwarn com.google.common.**
-dontwarn com.octo.android.robospice.persistence.**
-dontwarn com.octo.android.robospice.request.**

# For Retrofit
-keep class com.octo.android.robospice.retrofit.** { *; }
-keep class retrofit.** { *; }
-keep interface retrofit.** { *;}
-keepclasseswithmembers class * {
@retrofit.** *;
}
-keepclassmembers class * {
@retrofit.** *;
}
-dontwarn retrofit.appengine.UrlFetchClient
-dontwarn retrofit.converter.JacksonConverter
-dontwarn com.fasterxml.jackson.databind.ObjectMapper
-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.james.mime4j.* { *; }
-keep class javax.inject.** { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**

# For log4j
-dontwarn org.apache.**
-dontwarn org.apache.log4j.lf5.viewer.**
-dontnote org.apache.log4j.lf5.viewer.**
-dontwarn javax.naming.**
-dontwarn javax.servlet.**
-dontwarn org.slf4j.**

# For OkHttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

# For GSON
#-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }

# For Active Android
-keep class com.activeandroid.** { *; }
-keep class com.activeandroid.** { *** mId; }
-keep class com.gootax.client.models.** { *; }

# For SwipeLayout lib
-keep,includedescriptorclasses class com.daimajia.swipe.** { *; }
-keep class com.daimajia.swipe.** { int gravity; }

# For Google GMS
-keep,includedescriptorclasses class com.google.android.gms.** { *; }
-keep class com.google.android.gms.** { java.lang.String MODULE_ID; }
-keep class com.google.android.gms.** { int MODULE_VERSION; }
-keep class com.google.android.gms.** { *** theUnsafe; }

# For jars
-dontnote android.net.http.**
-dontnote org.apache.http.**

#For Crashlytics
-keepattributes SourceFile,LineNumberTable

# ActiveAndroid
-keep class com.activeandroid.** { *; }
-keep class com.activeandroid.**.** { *; }
-keep class * extends com.activeandroid.Model
-keep class * extends com.activeandroid.serializer.TypeSerializer

# open street map
-keep class org.osmdroid.** { *; }
-keep class org.osmdroid.**.** { *; }

# google map
-keep class com.google.android.gms.maps.** { *; }
-keep interface com.google.android.gms.maps.** { *; }
-dontwarn com.google.maps.android.geojson.GeoJsonLineStringStyle
-dontwarn com.google.maps.android.geojson.GeoJsonLayer
-dontwarn com.google.maps.android.clustering.ClusterManager
-dontwarn com.google.maps.android.geojson.GeoJsonLayer$1
-dontwarn com.google.maps.android.geojson.GeoJsonLayer$3
-dontwarn com.google.maps.android.geojson.GeoJsonRenderer
-dontwarn com.google.maps.android.kml.KmlRenderer

# yandex map
-keep class ru.yandex.** { *; }
-keep class ru.yandex.yandexmapkit.** { *; }
-keep class ru.yandex.yandexmapkit.**.** { *; }
-keep class ** implements ru.yandex.**

# io
-keep class io.socket.** { *; }
-keep class ** implements io.socket.**
-dontwarn io.socket.**
-dontwarn okio.**

# gcm
-keep class com.google.android.gms.gcm.** { *; }
-keep class com.google.android.gms.iid.** { *; }
-keep class com.google.android.gms.location.** { *; }
-dontwarn com.google.android.gms.location.**
-dontwarn com.google.android.gms.gcm.**
-dontwarn com.google.android.gms.iid.**

# Mapsme
-keep class com.mapswithme.maps.api.** { *; }

# For EventBus
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
-keep class com.gootax.client.events.** { *; }



-keep class org.osmdroid.** { *; }
-keep class com.google.maps.** { *; }
-dontwarn com.google.maps.**
-dontwarn com.google.maps.android.geojson.GeoJsonLineStringStyle
-dontwarn org.osmdroid.views.overlay.compass.CompassOverlay
-dontwarn org.osmdroid.tileprovider.modules.NetworkAvailabliltyCheck
-dontwarn retrofit.client.ApacheClient
-dontwarn org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
-dontwarn retrofit.android.**

-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}


